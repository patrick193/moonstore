<?php

class Bootstrap
{

    private static $modules            = ['Roles',
        'UserManagement',
        'Posts',
        'SocialLikes',
        'Admin',
        'Comments',
        'Contact',
        'Aboutus',
        'Subscribe',
        'MoonstoreAdmin'];
    private static $modulesDir         = "./Modules/";
    private static $overrideModulesDir = "./src/";

    /**
     * Function to get override dir
     * @return string
     */
    public static function getOverrideDir()
    {
        return self::$overrideModulesDir;
    }

    /**
     * 
     * @return string
     */
    public static function getModules()
    {
        return self::$modules;
    }

    /**
     * Function for get module dir(override or not)
     * @param string $moduleName name of module
     * @return string
     */
    public static function getModulesDir($moduleName)
    {
        return self::OverridePath($moduleName);
    }

    /**
     * Function for checking does module 
     * @param string $moduleName
     * @return boolean
     */
    public static function HasModule($moduleName)
    {
        if (in_array($moduleName, self::$modules)) {
            return true;
        }
        return false;
    }

    /**
     * function for adding a module
     * @param string $moduleName
     */
    public static function AddModule($moduleName)
    {
        array_push(self::$modules, $moduleName);
    }

    public static function OverridePath($moduleName)
    {
        $dir   = self::$overrideModulesDir . $moduleName;
        $class = self::$overrideModulesDir . $moduleName . "/" . $moduleName . ".php";

        if (is_dir($dir) and file_exists($class)) {
            return self::$overrideModulesDir;
        }
        return self::$modulesDir;
    }

    public static function getDir()
    {
        return self::$modulesDir;
    }

}
