<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Config
 *
 * @author vpohorielov
 */
class Config {

    public static $host = "localhost";
    public static $user = "root";
    public static $password = "root";
    public static $db = "moonstore";
    public static $execute = true; // execute or not queries without execute function
    //--------------------------------------------------------------//
    public static $web = './web/';
    public static $langDir = './Modules/system_lang/';
    public static $coreDefaultLanguage = "en-GB";
    //------------------------PAGINATION----------------------------//
    public static $itemsPerPage = 3;
    public static $imageUserPath = './web/images/user_profile/';
    public static $image = __DIR__ . '/web/templates/Site/assets/images/Posts/';
    public static $imageGallery = '/web/templates/Site/assets/images/Slider/';
    public static $imageGalleryThumb = '/web/templates/Site/assets/images/Slider/thumb/';

}
