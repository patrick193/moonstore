<?php

namespace Modules\Aboutus;

use PowerPlay\Module;
use Modules\Posts\Helprers\PostsHelper;
use Modules\Posts\Helprers\CategotyHelper;

class Aboutus extends Module
{

    public function actionAbout()
    {
        $pos            = $this->db->Select(['*',
                    'powerplay_posts',
                    'post_name like "%system_О нас%"'])[0];
        $pos->post_name = str_replace("system_", "", $pos->post_name);
        $ch             = new CategotyHelper();
        $categories     = $ch->getCategories();
        $pps            = $this->Render('post', $pos, 'Site/aboutus', false);
        $pps            = $this->addVariebles($pps, 'categoties', $categories);
        $pps->Display('Site/aboutus');
    }

}
