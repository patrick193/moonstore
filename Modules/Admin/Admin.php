<?php

namespace Modules\Admin;

use Modules\Admin\Helpers\AdminHelper;
use PowerPlay\Module;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of Admin
 *
 * @author vladyslav-root
 */
class Admin extends Module
{

    public function actionEmailSettings($args)
    {
        $ah = new AdminHelper();
        if ($ah->saveEmailSettings($args)) {
            $yaml = new \PowerPlay\YamlConfiguration();
            echo json_encode(['success' => 1]);
        } else {
            echo '{error: 1}';
        }
    }

    public function actionSubShow()
    {
        $ah          = new AdminHelper();
        $subscribers = $ah->getAllCustomers();

        $pps = $this->Render('customers', $subscribers, 'Admin/subscribe', false);
        $pps->Display('Admin/subscribe');
    }

    public function actionSendEmail($args)
    {
        if (!$args) {
            throw new PowerplayException(MOD_EMTY);
        }
        $mailer  = new \PowerPlay\Mailer\Mailer();
        $newArgs = [];
        $users   = $_POST['customers'];
        $u       = '';
        foreach ($users as $userId) {
            $user = $this->db->Select([
                        'email',
                        'powerplay_subscribers',
                        ['subscriber_id' => $userId]])[0];
            $u .= $user->email . ",";
        }

        $newArgs['to']      = $u;
        $newArgs['message'] = $args['content'];
        $newArgs['subject'] = $args['subject'];
        $newArgs['from']    = $args['email_name'];

        $send = $mailer->Send($newArgs);
        if ($send === true) {
            $this->Redirect("/admin");
        }
    }

    /**
     * 
     * @param type $args
     */
    public function actionInvert($args)
    {
        $ah  = new AdminHelper();
        $res = $ah->invertColors((int) $args['id']);
        if ($res > 0) {
            echo json_encode(['success' => 1]);
        } else {
            echo '{error: 0}';
        }
    }

    public function actionTagAdd($args)
    {
        if (!$args['tag']) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $ah = new AdminHelper();
        $id = $ah->TagAdd($args['tag']);
        if ($id) {
            echo json_encode(['success' => 1,
                'tagname' => $args['tag'],
                'tagid'   => $id]);
        } else {
            echo '{error}';
        }
    }

    public function actionGallerySaveAjax($args)
    {
        if ($args['save'] and $args['img']) {
            $ah = new AdminHelper();
            $ah->saveGallery($args);
        } else {
            $this->actionSliderSettings($args, true);
            $this->convertImanges();
        }
    }

    public function actionCheckGalleryNameAjax($args)
    {
        if (!$args) {
            echo '{error: no data for checki	ng}';
            exit();
        }
        $file = $this->storage->getValue($args['gallery_name']);
        if ($file === false or $file === null) {
//            $this->storage->set($args['gallery_name'], array());
            echo json_encode(array(
                'success' => 0));
        } else {
            echo '{error: ' . $args['gallery_name'] . '}';
        }
    }

    public function actionGalleryShow($args)
    {
        $gall = $this->getGallery();
        $this->Render('galleries', $gall, 'Admin/createGallery');
    }

    private function getGallery()
    {
        $modules    = \Bootstrap::getModules();
        $allStor    = $this->storage->getAllModules();
        $galleryies = [];
        foreach ($allStor as $value) {
            if (!in_array($value, $modules)) {

                $gall = $this->storage->getValue($value);
                $g    = $gall->image;
                foreach ($g as $key => $value1) {
                    if (strstr($value1, ".") !== false) {
                        $galleryies[$value][$key]['image_path']    = \Config::$imageGallery . $value1;
                        $galleryies[$value][$key]['th_image_path'] = \Config::$imageGalleryThumb . $value1;
                    }
                }
            }
        }
	ksort($galleryies);
        return $galleryies;
    }

    public function actionKeepost($args)
    {
        $args       = $_GET;
        $post       = $args['post'];
        $post       = str_replace('"', "", $post);
        $sliderPath = $args['image_id'];
        $postId     = explode(".", $post)[0];
        $ah         = new AdminHelper();
        if ($args['delete'] == 1) {
            $ah->saveSliderSettings($sliderPath, (int) $postId, true);
        } else {
            $ah->saveSliderSettings($sliderPath, (int) $postId);
        }
    }

    public function actionRemovePost($args)
    {
        if (!$args['post_id'] or $args['post_id'] == 0) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $ah = new AdminHelper();
        $ah->PostRemove((int) $args['post_id']);
        $this->Redirect('/admin/posts');
    }

    public function actionFindPost($args)
    {
        $imgsrc = $args['imgsrc'];
        if ($imgsrc) {
            $ah  = new AdminHelper();
            $res = $ah->getPostForSlider($imgsrc);
            echo json_encode($res);
            exit();
        }
    }

    public function actionAjaxSearch($args)
    {
        $ah    = new AdminHelper();
        $posts = $ah->getAjaxPost(trim(strip_tags($args['q'])));
        if (is_array($posts)) {
            echo json_encode($posts);
            exit();
        } else {
            echo '{error: no data}';
        }
    }

    public function actionSliderSettings($args, $gallery = false)
    {
        // A list of permitted file extensions
        $allowed = array(
            'png',
            'jpg',
            'gif',
            'zip');

        if (isset($_FILES['upl']) && $_FILES['upl']['error'] == 0) {

            $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

            if (!in_array(strtolower($extension), $allowed)) {
                echo '{"status":"error"}';
                exit;
            }

            if (move_uploaded_file($_FILES['upl']['tmp_name'], __DIR__ . '/../../web/templates/Site/assets/images/Slider/' . $_FILES['upl']['name'])) {
                if ($gallery === false) {
                    $this->db->Insert([[
                    'image_path' => "/web/templates/Site/assets/images/Slider/" . $_FILES['upl']['name']],
                        'slider-setting']);
                }
                echo '{"status":"success"}';
                exit;
            }
        }

        echo '{"status":"error"}';
        exit;
    }

    public function actionSaveCategoryCount($args)
    {
        $args = $_POST;
        $ah   = new AdminHelper();
        $ah->SaveCountCategory($args['count']);
    }

    public function actionSavePaginationCount($args)
    {
        $args = $_POST;

        $ah = new AdminHelper();
        $ah->SavePagination(($args['count']));
    }

    public function actionAdmin($args)
    {
        $ah = new AdminHelper();

        $userS = @unserialize($this->session->get('user_auth'));
        if (is_object($userS)) {
            $post     = new \Modules\Posts\Helprers\PostsHelper();
            $category = new \Modules\Posts\Helprers\CategotyHelper();
            $yaml     = new \PowerPlay\YamlConfiguration();

            $ah             = new AdminHelper();
            $subscribers    = $ah->getLastSubscribers();
            $posts          = $post->getLastPosts();
            $categories     = $category->getCategories();
            $slider         = $post->getSliderSettings();
            $postsStatistic = $post->getCount();
            $owner          = $yaml->GetConfigurations(__DIR__ . "/../../PowerPlay/Mailer/Config/MailerCustomConfig.yml");

            $pps = $this->Render('user', $userS, 'Admin/index.admin', false);
            $pps = $this->addVariebles($pps, 'ipList', $ah->getIPList());
            $pps = $this->addVariebles($pps, 'comments', $ah->getComments());
            $pps = $this->addVariebles($pps, 'slider', $slider);
            $pps = $this->addVariebles($pps, 'lastNews', $ah->getLastNews());
            $pps = $this->addVariebles($pps, 'lastpost', $posts);
            $pps = $this->addVariebles($pps, 'categories', $categories);
            $pps = $this->addVariebles($pps, 'posts_statistic', $postsStatistic);
            $pps = $this->addVariebles($pps, 'subscribers', $subscribers);
            $pps = $this->addVariebles($pps, 'owner', [$owner]);
            $pps = $this->getSiteSettings($pps);
            $pps->Display('Admin/index.admin');
        } else {
            throw new PowerplayException(MOD_ACCESS);
        }
    }

    public function actionProfile($args)
    {

        if (!is_array($args)) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $ah = new AdminHelper();

        $ah->Detect($args);
    }

    public function actionProfileUMD($args)
    {
        $um = new \Modules\UserManagement\UserManagement();
        $um->actionDelete($args);
    }

    public function actionProfileUME($args)
    {
        $um = new \Modules\UserManagement\UserManagement();
        $um->actionEditUser($args);
    }

    public function actionProfileUMA($args)
    {
        $um = new \Modules\UserManagement\UserManagement();
        $um->actionAddUser($args);
    }

    public function actionProfileUMAa($args)
    {
        $um = new \Modules\UserManagement\UserManagement();
        $um->actionAdmin($args);
    }

    public function actionProfileUM($args)
    {
        $um = new \Modules\UserManagement\UserManagement();
        $um->actionMain();
    }

    public function actionProfilePE($args)
    {
        $pe = new \Modules\Posts\Posts();
        $pe->actionEditPost($args);
    }

    public function actionProfilePA($args)
    {
        $pe = new \Modules\Posts\Posts();
        $pe->actionAddPost($args);
    }

    public function actionProfileIndex($args)
    {
        if (!isset($args['user_id'])) {
            throw new PowerplayException(MOD_USER_ID);
        }
        $post   = new \Modules\Posts\Helprers\PostsHelper();
        $ah     = new AdminHelper();
        $user   = $ah->getUser((int) $args['user_id']);
        $slider = $post->getSliderSettings();

        $pps = $this->Render('user', $user[0], 'Admin/profile', false);
        $pps = $this->addVariebles($pps, 'role', $user[1]);
        $pps = $this->addVariebles($pps, 'slider', $slider);
        $pps = $this->getSiteSettings($pps);
        $pps = $this->addVariebles($pps, 'lastpost', $user[2]);
        $pps->Display('Admin/profile');
    }

    public function actionAddUser()
    {
        $user = @unserialize($this->session->get('user_auth'));
        if ($user) {
            $ah     = new AdminHelper();
            $user   = $ah->getUser((int) $user->getUserId());
            $post   = new \Modules\Posts\Helprers\PostsHelper();
            $posts  = $post->getLastPosts();
            $slider = $post->getSliderSettings();

            $pps = $this->Render('user', $user[0], 'Admin/profile', false);
            $pps = $this->addVariebles($pps, 'role', $user[1]);
            $pps = $this->addVariebles($pps, 'slider', $slider);
            $pps = $this->addVariebles($pps, 'add', 'yes');
            $pps = $this->addVariebles($pps, 'lastpost', $posts);
            $pps = $this->getSiteSettings($pps);
            $pps->Display('Admin/profile');
        }
    }

    public function actionpostAdd()
    {
        $ah        = new AdminHelper();
        $cat       = $ah->getCategories();
        $post      = new \Modules\Posts\Helprers\PostsHelper();
        $posts     = $post->getLastPosts();
        $positions = $post->getPosition();
        $tags      = $post->getAllTags();
        $aligns    = $post->getAligns();
        $slider    = $post->getSliderSettings();

        $pps = $this->Render('cat', $cat, 'Admin/postAdd', false);
        $pps = $this->addVariebles($pps, 'slider', $slider);
        $pps = $this->addVariebles($pps, 'positions', $positions);
        $pps = $this->addVariebles($pps, 'lastpost', $posts);
        $pps = $this->addVariebles($pps, 'tags', $tags);
        $pps = $this->addVariebles($pps, 'aligns', $aligns);
        $pps = $this->getSiteSettings($pps);
        $pps->Display('Admin/postAdd');
    }

    public function actionTable($args)
    {
        if ($args['page']) {
            $page = trim(strip_tags($args['page']));
        } else {
            $page = 1;
        }
        $ah        = new AdminHelper();
        $p         = new \Modules\Posts\Helprers\PostsHelper();
        $posts     = $p->getAll($page, true);
        $lastposts = $p->getLastPosts();
        $slider    = $p->getSliderSettings();

        $pps = $this->Render('posts', $posts, 'Admin/tables', false);
        $pps = $this->addVariebles($pps, 'lastpost', $lastposts);
        $pps = $this->addVariebles($pps, 'slider', $slider);
        $pps = $this->getSiteSettings($pps);

        $pps = $this->addVariebles($pps, 'page', $page);
        $pps->Display('Admin/tables');
    }

    public function actionEditShow($args)
    {
        if (!isset($args['post_id'])) {
            throw new PowerplayException('No id');
        }
        $ph         = new \Modules\Posts\Helprers\PostsHelper();
        $post       = $ph->getOnePost((int) $args['post_id'], true);
        $ah         = new AdminHelper();
        $categories = $ah->getCategories();
        $posts      = $ph->getLastPosts();
        $positions  = $ph->getPosition();
        $align      = $ph->getAligns();
        $tags       = $ph->getAllTags();
        $slider     = $ph->getSliderSettings();

        $tagsp = $ph->getTagsId((int) $args['post_id']);
        $pp    = $this->Render('post_edit', [$post,
            $categories], "Admin/postAdd", false);
        $pps   = $this->addVariebles($pp, 'positions', $positions);
        $pps   = $this->addVariebles($pps, 'slider', $slider);
        $pps   = $this->addVariebles($pps, 'lastpost', $posts);
        $pps   = $this->addVariebles($pps, 'tags', $tags);
        $pps   = $this->addVariebles($pps, 'tagsp', $tagsp);
        $pps   = $this->addVariebles($pps, 'aligns', $align);
        $pps   = $this->getSiteSettings($pps);

        $pps->Display('Admin/postAdd');
    }

    private function convertImanges()
    {
        $thumb_directory = \Config::$web . "templates/Site/assets/images/Slider/thumb";     //Папка для миниатюр 
        $orig_directory  = \Config::$web . "templates/Site/assets/images/Slider";     //Папка для полноразмерных изображений 

        $dir_handle = @opendir($orig_directory);  //Открываем папку с полноразмерными изображениями 
        if ($dir_handle > 1) {     //Проверяем, что папка открыта и в ней есть файлы
            $allowed_types = array(
                'jpg',
                'jpeg',
                'gif',
                'png'); // Список обрабатываемых расширений
            $file_parts    = array();
            $ext           = '';
            $title         = '';
            $i             = 0;

            while ($file = @readdir($dir_handle)) {
                /* Пропускаем системные файлы: */
                if ($file == '.' || $file == '..')
                    continue;

                $file_parts = explode('.', $file);     //Разделяем имя файла на части 
                $ext        = strtolower(array_pop($file_parts));

                /* Используем имя файла (без расширения) как заголовок изображения: */
                $title = implode('.', $file_parts);
                $title = htmlspecialchars($title);

                /* Если расширение входит в список обрабатываемых: */
                if (in_array($ext, $allowed_types)) {

                    /* Если вы планируете хранить изображения в базе данных, вставьте код для запроса здесь */


                    /* Выводим каждое изображение: */

                    $nw     = 150;
                    $nh     = 100;
                    $source = $orig_directory . $file;
                    $stype  = explode(".", $source);
                    $stype  = $stype[count($stype) - 1];
                    $dest   = $thumb_directory . $file;

                    $size = getimagesize($source);
                    $w    = $size[0];
                    $h    = $size[1];

                    switch ($stype) {
                        case 'gif':
                            $simg = imagecreatefromgif($source);
                            break;
                        case 'jpg':
                            $simg = imagecreatefromjpeg($source);
                            break;
                        case 'png':
                            $simg = imagecreatefrompng($source);
                            break;
                    }

                    $dimg     = imagecreatetruecolor($nw, $nh);
                    $wm       = $w / $nw;
                    $hm       = $h / $nh;
                    $h_height = $nh / 2;
                    $w_height = $nw / 2;

                    if ($w > $h) {
                        $adjusted_width = $w / $hm;
                        $half_width     = $adjusted_width / 2;
                        $int_width      = $half_width - $w_height;
                        imagecopyresampled($dimg, $simg, -$int_width, 0, 0, 0, $adjusted_width, $nh, $w, $h);
                    } elseif (($w < $h) || ($w == $h)) {
                        $adjusted_height = $h / $wm;
                        $half_height     = $adjusted_height / 2;
                        $int_height      = $half_height - $h_height;

                        imagecopyresampled($dimg, $simg, 0, -$int_height, 0, 0, $nw, $adjusted_height, $w, $h);
                    } else {
                        imagecopyresampled($dimg, $simg, 0, 0, 0, 0, $nw, $nh, $w, $h);
                    }
                    imagejpeg($dimg, $dest, 100);
                }
            }

            /* Закрываем папку */
            @closedir($dir_handle);
        }
    }

    /**
     * function for getting all settings for the site
     * @param \PowerPlay\PPS\PPS $pps
     * @return \PowerPlay\PPS\PPS
     */
    private function getSiteSettings($pps)
    {
        $ah       = new AdminHelper();
        $settings = $ah->getSettings();
        return $this->addVariebles($pps, 'settings', $settings);
    }

}
