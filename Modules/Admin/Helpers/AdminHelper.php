<?php

namespace Modules\Admin\Helpers;

use Modules\Admin\Admin;
use Modules\Roles\Users;
use PowerPlay\Database;
use PowerPlay\PowerplayException\PowerplayException;
use PowerPlay\Filter;

/**
 * Description of AdminHelper
 *
 * @author vladyslav-root
 */
class AdminHelper extends Admin
{

    public function saveGallery($args)
    {
        /* TODO */
        $std        = new \stdClass();
        $std->all   = $args['img'];
        $std->image = [];
        $this->storage->set($args['save'], $std);
        $img        = $args['img'];
        $images     = explode(" ", $img);
        foreach ($images as $img) {
            $gallery          = $this->storage->getValue($args['save']);
            $gallery->image[] = $img;
            $this->storage->set($args['save'], $gallery);
        }
    }

    private function convertImgName($string)
    {
        
    }

    public function findTags($query)
    {
        if (!$query) {
            throw new PowerplayException(MOD_EMTYP);
        }
        $this->db->Start();
        return $this->db->Execute("select * from tags where tag_name like '%$query%'");
    }

    public function Login($args)
    {
        !$this->db ? $this->db = new Database() : '';

        if (!isset($args['username']) and ! isset($args['password'])) {
            throw new PowerplayException('нет данных');
        }

        $filter = new Filter('string');
        $pass   = md5($filter->ToFilter($args['password']));
        $login  = $filter->ToFilter($args['username']);
//        $this->db->setExecute($execute)
        $u      = new Users();
        $user   = $u->Load(['user_email',
            $login]);
        if (count($user) === 1 and $user->getIsLocked() != 1) {
            $dbPass = $user->getPassword();
            if ($dbPass === $pass) {
                $this->session->set('user_auth', @serialize($user));
                return $user;
            }
            return MOD_USER_DID_NOT_FIND;
        } else {
            return MOD_USER_DID_NOT_FIND;
        }
    }

    public function Logout()
    {
        $user = @unserialize($this->session->get('user_auth'));
        if ($user) {
            $this->db->Update([[
            'last_visit' => date("Y-m-d H:m:s")],
                'powerplay_users',
                ['user_id' => $user->getUserId()]]);
        }
        $this->session->Remove('user_auth');
    }

    public function getIPList()
    {
        !$this->db ? $this->db = new Database() : '';

        $ipList = $this->db->Select(['*',
            'ip_block',
            ['isLocked' => 1]]);

        return $ipList;
    }

    public function getComments()
    {
        !$this->db ? $this->db = new Database() : '';

        $q        = 'SELECT * FROM `post_comments` ORDER BY `date` desc LIMIT 3';
        $comments = $this->db->Execute($q);

        return $comments;
    }

    public function getLastNews()
    {
        !$this->db ? $this->db = new Database() : '';

        $this->db->setExecute(false);
        $news  = $this->db->Select(['*',
                    'powerplay_posts',
                    ['isLocked' => 0],
                    [
                        'date_created',
                        'DESC']]) . ' LIMIT 1';
        $post  = $this->db->Execute($news);
        date_default_timezone_set('UTC');
        $date1 = new \DateTime($post[0]->date_redaction);
        $date2 = new \DateTime(date('Y-m-d H:m:s'));
        $diff  = $date2->diff($date1);


        $post[0]->update = $diff->days;
        return $post[0];
    }

    public function Detect($args)
    {
        if (!is_array($args)) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $module = $args['module'];
        $action = $args['action'];
        unset($args['module'], $args['action']);
//        $mainParams = isset($args['main_params']) ? $args['main_params'] : null;
//        $other = isset($args['other']) ? $args['other'] : null;

        $url = '';

        foreach ($args as $key => $value) {
            $url .= $key . "=" . str_replace("/", "!@#", base64_encode(mysql_escape_string($value))) . "/";
        }
        $url = $module . "/" . $action . "/" . $url;
//        if (is_null($mainParams) and is_null($other)) {
//            $url = $args['module'] . '/' . $args['action'];
//        } else {
//            $url = implode("/", $mainParams) . "/"
//                    . implode("/", $other);
//        }

        $routing = new \PowerPlay\Route();
        $url     = trim($url);
        $user    = @unserialize($this->session->get('user_auth'));
        $role    = new \Modules\Roles\Roles();
        $code    = $role->FindById((int) $user->getRoleId());
        $module  = $routing->FindModule($url, $code->getRoleCode());
        $result  = $module['module']->getModule()->doAction($module['info']);
    }

    public function getUser($userId)
    {
        if (!is_int($userId) or $userId === 0) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $this->db->setExecute(false);
        $query = $this->db->Select(['*',
            'powerplay_users',
            ['user_id' => $userId]]);
        $user  = $this->db->Execute($query, Users::class)[0];
        if ($user) {
            $post     = new \Modules\Posts\Helprers\PostsHelper();
            $posts    = $post->getLastPosts();
            $role     = new \Modules\Roles\Roles();
            $userRole = $role->FindById((int) $user->getRoleId());

            return [$user,
                $userRole,
                $posts];
        } else {
            return 0;
        }
    }

    public function Roles()
    {

        $roles = $this->db->SelectAll('powerplay_roles');
        return $roles;
    }

    public function SaveCountCategory($count)
    {
        $this->db->Update([[
        'category_count' => abs($count)],
            'settings',
            ['setting_id' => 1]]);
    }

    public function SavePagination($count)
    {
        $this->db->Update([[
        'pagination' => abs($count)],
            'settings',
            ['setting_id' => 1]]);
    }

    public function getCategories()
    {
        $category   = new \Modules\Posts\Helprers\CategotyHelper();
        $categories = $category->getCategories();
        return $categories;
    }

    public function getAjaxPost($query)
    {
        if (!$query) {
            throw new PowerplayException('{error: no query}');
        }
        $posts = $this->db->Select([[
        'post_id',
        'post_name'],
            'powerplay_posts',
            'post_name like "%' . $query . '%"']);
        foreach ($posts as $key => $value) {
            $posts[$key] = $value->post_id . ". " . $value->post_name;
        }
        return $posts;
    }

    public function saveSliderSettings($slider_path, $post_id, $delete = false)
    {

        if (!is_int($post_id)) {
            throw new PowerplayException(MOD_INT);
        }
        $slider_path = str_replace("%2F", "/", $slider_path);
        if ($delete === true) {
            $this->db->Start();
            $this->db->Execute('delete from `slider-setting` where image_path = "' . $slider_path . '"');
        } else {
            $this->db->Update([[
            'post_id' => $post_id],
                'slider-setting',
                ['image_path' => $slider_path]]);
        }
    }

    public function getPostForSlider($sliderPath)
    {
        if (!$sliderPath) {
            throw new Exception('{error}');
        }
        $sliderPath = str_replace("%2F", "/", $sliderPath);
        $res        = $this->db->Select([[
                'post_name',
                'post_id'],
                    'powerplay_posts',
                    ['post_id' => $this->db->Select(['post_id',
                            'slider-setting',
                            ['image_path' => $sliderPath]])[0]->post_id]])[0];
        if ($res) {
            $res = $res->post_id . ". " . $res->post_name;
            return str_replace('"', "", $res);
        } else {
            echo '';
            exit;
        }
    }

    /**
     * function for removing a post
     * @param int $postId
     * @return type
     * @throws PowerplayException
     */
    public function PostRemove($postId)
    {
        if (!is_int($postId) or $postId == 0) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if ($this->db->Select(['isLocked',
                    'powerplay_posts',
                    ['post_id' => $postId]])[0]->isLocked == 1) {
            return
                    $this->db->Update([[
                    'isLocked' => 0],
                        'powerplay_posts',
                        ['post_id' => $postId]]);
        }
        return
                $this->db->Delete(['powerplay_posts',
                    ['post_id' => $postId]]);
    }

    /**
     * function for adding a new tag
     * @param string $tagName
     * @return type
     * @throws PowerplayException
     */
    public function TagAdd($tagName)
    {
        if (!$tagName) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $this->db->Insert([[
        'tag_name' => $tagName],
            'tags']);
        return $this->db->LastInsert();
    }

    public function getSettings()
    {
        return $this->db->Select(['*',
                    'settings']);
    }

    public function getLastSubscribers()
    {
        return $this->db->Select(['*',
                    'powerplay_subscribers',
                    '1 limit 10']);
    }

    /**
     * invert colors
     * @param array $categoryId
     * @return int
     */
    public function invertColors($categoryId)
    {
        if (!is_int($categoryId)) {
            throw new PowerplayException(MOD_INT);
        }

        $ch = new \Modules\Posts\Helprers\PostsHelper();

        if ($ch->CheckCategory($categoryId)) {
            $this->db->Start();
            $this->db->Execute("update posts_category set invert= if(invert=0, 1, 0) where posts_category_id = $categoryId");
            return 1;
        }
        return 0;
    }

    /*     * function fer getting all subscribers from the site
     * 
     * @return array
     */

    public function getAllCustomers()
    {
        return $this->db->Select(['*',
                    'powerplay_subscribers']);
    }

    public function saveEmailSettings($args)
    {
        if (!$args) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $array  = [
            'Host'     => $args['host'],
            'Username' => $args['email'],
            'Password' => $args['emailPss'],
            'Port'     => $args['emailPort']
        ];
        $dumper = new \Symfony\Component\Yaml\Dumper();
        $yaml   = $dumper->dump($array);
        if (file_put_contents(\Bootstrap::getDir() . "../PowerPlay/Mailer/Config/MailerCustomConfig.yml", $yaml)) {
            return true;
        }
        return false;
    }

}
