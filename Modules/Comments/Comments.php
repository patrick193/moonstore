<?php

namespace Modules\Comments;

use Modules\Comments\Helpers\CommentHelper;
use PowerPlay\Module;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of Comments
 *
 * @author vladyslav-root
 */
class Comments extends Module {

    public function actionCommentAdd($args) {
        if (!is_array($args)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $helper = new CommentHelper();
        $helper->CommentAdd($args);
    }

    public function actionCommentBlock($args) {
        if (!isset($args['comment_id'])) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $helper = new CommentHelper();
        $helper->CommentBlock((int) $args['comment_id']);
        header("Location: " . $_SERVER['HTTP_REFERER']);
        exit();
//        $this->
    }

    public function actionBlockIp($args) {
        if (!isset($args['ip'])) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $helper = new CommentHelper();
        $helper->BlockIp($args['ip']);
    }

    public function actionUnBlockIp($args) {
        if (!isset($args['ip'])) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $helper = new CommentHelper();
        $helper->UnBlockIp($args['ip']);
    }

    public function actionGetAll($args) {
        if(!isset($args['post_id'])){
            throw new PowerplayException('Post was not selected', 'user');
        }
        $helper = new CommentHelper();
        $com = $helper->getAllComments($args['post_id']);
        echo '<pre>';
        print_r($com);
        die;
        /** on render */
    }
}
