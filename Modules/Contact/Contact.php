<?php

namespace Modules\Contact;

use PowerPlay\Module;
use Modules\Posts\Helprers\PostsHelper;
use Modules\Posts\Helprers\CategotyHelper;

class Contact extends Module
{

    public function actionContact()
    {
        $pos            = $this->db->Select(['*',
                    'powerplay_posts',
                    'post_name like "%system_Контакты%"'])[0];
        $pos->post_name = str_replace("system_", "", $pos->post_name);
        $ch             = new CategotyHelper();
        $categories     = $ch->getCategories();
        $pps            = $this->Render('post', $pos, 'Site/contact', false);
        $pps            = $this->addVariebles($pps, 'categoties', $categories);
        $pps->Display('Site/contact');
    }

}
