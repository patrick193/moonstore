<?php

namespace Modules\Language;

use Modules\Language\Helpers\LanguageModel;
use PowerPlay\Database;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of Language
 *
 * @author Developer Pohorielov Vladyslav
 */
class Language {

    const DEFAULT_LANGUAGE = "en-GB";

    /**
     * Array of languages
     * @var array 
     */
    private $languages;

    /**
     * User language from the database
     * @var String 
     */
    protected $userLanguage;

    /**
     * Database var
     * @var Database 
     */
    private $db;

    public function __construct() {
        !$this->languages ? $this->languages = new LanguageModel() : '';
        !$this->db ? $this->db = new Database : '';
    }

    public function getUserLanguage() {
        return $this->userLanguage;
    }

    public function setUserLanguage($userLanguage = null) {
        if($userLanguage) {
            $this->userLanguage = $userLanguage;
        } else {
            $this->userLanguage = self::DEFAULT_LANGUAGE;
        }
    }

    public function FindUserLanguage($userId) {
        if(!is_int($userId)) {
            throw new PowerplayException('Wrong user id.');
        }
        $userId === 0 ? $userId = 1 : '';

        $lang = $this->db->Select(['*', 'powerplay_language', ['language_id' => $this->db->Select([[
                'language_id'], 'powerplay_users', ['user_id' => $userId]])[0]->language_id]]);

        if(is_array($lang) and count($lang) === 1) {
            $this->userLanguage = $lang[0]->language_code;
        } else {
            throw new Exception('We did not find any languages in the database');
        }
        return $this->userLanguage;
    }

    public function DefineLanguage($languageCode) {
        if(!$languageCode) {
            $languageCode = self::DEFAULT_LANGUAGE;
        }
        $fileName = \Config::$langDir . $languageCode . "/" . $languageCode . ".system.ini";
        if(is_dir(\Config::$langDir) and file_exists($fileName)) {
            $system = new \Modules\system_lang\System($languageCode);
            $system->UseLanguage();
        }
        if(defined('LANGUAGE')) {
            return true;
        }
        return false;
    }

}
