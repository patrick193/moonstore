<?php

namespace Modules\MoonstoreAdmin;

use Modules\Admin\Helpers\AdminHelper;
use PowerPlay\Module;
use PowerPlay\PowerplayException\PowerplayException;

class MoonstoreAdmin extends Module
{

    public function actionAdmin()
    {
        $err = $this->session->get('err');
        if (!is_null($err)) {
            $this->Render('err', $err, 'Admin/index.login');
        } else {
            $this->session->Remove('err');
            $this->Render('', '', 'Admin/index.login');
        }
    }

    public function actionLogin($args = null)
    {
        $ah = new AdminHelper();

        if (!isset($args['username']) and ! isset($args['password'])) {
            $userS = @unserialize($this->session->get('user_auth'));
            if (is_object($userS)) {
                $this->Redirect('/admin');
            } else {
                throw new PowerplayException(MOD_ACCESS);
            }
        }
        $login = $ah->Login($args);

        if (is_object($login)) {
            $this->Redirect('/admin');
        } else {
            $this->session->set('err', $login);
            header("Location: /moonstoreadmin");
        }
    }

    public function actionLogout()
    {
        $ah = new AdminHelper();
        $ah->Logout();
        header("Location:/moonstoreadmin");
    }

}
