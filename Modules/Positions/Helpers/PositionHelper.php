<?php

namespace Modules\Positions\Helpers;

use PowerPlay\Database;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of PositionHelper
 *
 * @author vladyslav-root
 */
class PositionHelper {

    private $db;

    public function __construct() {

        $this->db ? : $this->db = new Database();
    }

    public function PositionCount($positionId, $count) {
        if (!is_int($positionId) or ! is_int($count)) {
            throw new PowerplayException('Wrong post id');
        }
        if ($this->PositionCheck($positionId) === true) {
            $this->db->Update([['count' => $count],
                'positions', ['position_id' => $positionId]]);
        }
    }

    private function PositionCheck($positionId) {
        if (!is_int($positionId)) {
            throw new PowerplayException('Wrong post id');
        }
        $position = $this->db->Count('*', 'positions', ['position_id' => $positionId]);
        if ($position != 0) {
            return true;
        }
        return false;
    }

}
