<?php

namespace Modules\Posts\Helprers;

use PowerPlay\Pagination\Pagination;
use Exception;
use PowerPlay\Database;
use PowerPlay\Session;
use Modules\Posts\Helprers\posts_cron;

/**
 * Description of PostsHelper
 *
 * @author vladyslav-root
 */
class PostsHelper extends \Modules\Posts\Posts
{

    /**
     *
     * @var \Modules\Roles\Users
     */
    private $user;

    const youtubePlayer = "https://www.youtube.com/embed/";
    const vimeoPlayer   = "https://player.vimeo.com/video/";
    const youtubeTH     = "http://img.youtube.com/vi/";
    const vimeoTH       = "http://vimeo.com/api/v2/video/";

    const maxImgOnPage = 6;

    public function __construct()
    {
        !$this->db ? $this->db = new Database() : '';
        if (!$this->user) {
            $session = new Session();
            $user = @unserialize($session->get('user_auth'));
            if ($user) {
                $this->user = $user;
            } else {
//                throw new Exception('You dont have this access');
            }
        }
    }

    public function Search($search)
    {
        $this->db->Start();
        $res = $this->db->Execute("select * from powerplay_posts where post_name like '%$search%'"
            . " or post_text like '%$search%' or short_desc like '%$search%' order by date_created desc");
        $fposts = array(
            'query' => [],
        );
        $this->db->setExecute(true);
        foreach ($res as $post) {
            $post->post_category_id = $this->db->Select([
                'category_name',
                'posts_category',
                ['posts_category_id' => $post->post_category_id]
            ])[0]->category_name;
            $post->post_text = substr(trim(strip_tags($post->post_text)), 0, 50);

            array_push($fposts['query'], $post);
        }
        return $res;
    }

    public function getSliderSettings()
    {

        return $this->db->Select([
            '*',
            'slider-setting'
        ]);
    }

    public function getPostsCategory($categoryId, $page = 1)
    {
        $pag = $this->db->Select([
            'pagination',
            'settings'
        ])[0]->pagination;
        $this->db->setExecute(false);
        $posts = $this->db->Select([
            '*',
            'powerplay_posts',
            "post_category_id = '$categoryId' and isLocked = 0 and post_name not like '%system_%'",
            "date_created DESC"
        ]);
        $posts = Pagination::paginate($posts, $page, $pag);
        $fposts = array(
            'query' => [],
            'page_num' => []
        );
        $this->db->setExecute(true);
        foreach ($posts['query'] as $post) {
            $post->post_category_id = $this->db->Select([
                'category_name',
                'posts_category',
                ['posts_category_id' => $post->post_category_id]
            ])[0]->category_name;
            $post->post_text = substr(trim(strip_tags($post->post_text)), 0, 50);
            array_push($fposts['query'], $post);
        }
        $fposts['page_num'] = $posts['page_num'];
        return $posts;
    }

    /**
     * Function to add a new post into the db
     * @param array $args
     * @throws Exception
     */
    public function AddPost($args)
    {
        if (!is_array($args) or empty($args)) {
            throw new Exception('Wrong parameters Type in Posts Module');
        }

        if ($this->CheckCategory((int)$args['category_id'])) {
            $dateObj = new \DateTime();

            if (!empty($args['date'])) {
                $date = explode(" ", $args['date'])[0];
                $time = explode(" ", $args['date'])[1];
                $dateArr = explode("-", $date);
                $timeArr = explode(":", $time);
                $dateObj->setDate($dateArr[0], $dateArr[1], $dateArr[2]);
                $dateObj->setTime($timeArr[0], $timeArr[1], $timeArr[2]);
                $isLocked = 1;
            } else {
                $isLocked = 0;
            }
            $args['short_desc'] = $args['short_desc'] ? $args['short_desc'] : substr(trim(strip_tags($args['content'])), 0, 50);
            $args['post_seo_title'] = $args['post_seo_title'] ? $args['post_seo_title'] : $args['post_name'];
            $args['post_seo_desc'] = $args['post_seo_desc'] ? $args['post_seo_desc'] : $args['short_desc'];
            $args['align'] = $args['align'] ? $args['align'] : 3;
            $date = $dateObj->format("Y-m-d H:i:s");

            if ($_FILES['image_file']['name']) {
                $newName = $this->imgUpload();
                $imgPath = '/web/templates/Site/assets/images/Posts/' . $newName;
                $imgPathTh = '/web/templates/Site/assets/images/Posts/thumb_' . $newName;
            } else {
                $imgPath = '/web/templates/Site/assets/images/bebinka_add.png';
                $imgPathTh = '/web/templates/Site/assets/images/bebinka_add.png';
            }
            $this->db->Insert([
                [
                    'post_name' => $args['post_name'],
                    'short_desc' => $args['short_desc'],
                    'post_text' => $args['content'],
                    'post_category_id' => $args['category_id'],
                    'created_by' => $this->user->getUserId(),
                    'position_id' => $args['position'],
                    'date_created' => $date,
                    'date_redaction' => $date,
                    'post_img' => $imgPath,
                    'post_img_th' => $imgPathTh,
                    'post_seo_title' => $args['post_seo_title'],
                    'post_seo_desc' => $args['post_seo_desc'],
                    'isLocked' => $isLocked,
                    'img_type' => $args['img_type'],
                    'type_id' => $args['align']
                ],
                'powerplay_posts'
            ]);

            $insert = $this->db->LastInsert();
            if ($args['img_type']) {
                $video = $args['video_url'];

                strpos($video, "vimeo.com") ? $url = function () use ($video) {
                    $code = end(explode("/", $video));
                    $hash = unserialize(file_get_contents(self::vimeoTH . "$code.php"));
                    return [
                        'video' => self::vimeoPlayer . $code,
                        'img' => $hash[0]['thumbnail_medium']
                    ];
                } : $url = function () use ($video) {
                    $code = end(explode("=", $video));
                    return [
                        'video' => self::youtubePlayer . $code,
                        'img' => self::youtubeTH . $code . "/0.jpg"
                    ];
                };
                if ($args['img_type'] == 2) {
                    $update = [
                        'vimeo' => $url()['video'],
                        'display' => 1,
                        'post_img' => ($_FILES['image_file']['name']) ?
                            $imgPath = '/web/templates/Site/assets/images/Posts/' . $this->imgUpload() : ''
                    ];
                } elseif ($args['img_type'] == 1) {
                    $update = [
                        'vimeo' => $url()['video'],
                        'display' => 1,
                        'post_img' => $url()['img']
                    ];
                }
                $this->db->Update([
                    $update
                    ,
                    'powerplay_posts',
                    ['post_id' => $insert]
                ]);
            }
            if ((is_array($_POST['tags']) and (count($_POST['tags']) > 0))) {
                foreach ($_POST['tags'] as $value) {
                    if ($this->db->Count('*', 'posts_to_tags', [
                            'post_id' => $insert,
                            'tag_id' => $value
                        ]) == 0
                    ) {
                        $this->db->Insert([
                            [
                                'tag_id' => $value,
                                'post_id' => $insert
                            ],
                            'posts_to_tags'
                        ]);
                    }
                }
            }
            if (!empty($args['date'])) {
                $posts = new posts_cron();
                $posts->check($insert, $date);
                $f = fopen('../../../log', "w");
                fwrite($f, "log\n");
                fclose($f);
            }
            return $insert;
        } else {
            throw new Exception('We can not find any category.');
        }
    }

    /**
     * Function for checking category. Is category is exist we return true
     * @param int $categoryId
     * @return boolean
     * @throws Exception
     */
    public function CheckCategory($categoryId)
    {
        if (!is_int($categoryId)) {
            throw new Exception('Wrong category id. Category Id Should be an integar type.');
        }
        $select = $this->db->Select([
            [
                'posts_category_id'
            ],
            'posts_category',
            ['posts_category_id' => $categoryId]
        ])[0]->posts_category_id;
        if (is_bool($select) or $select == 0) {
            return false;
        }
        return true;
    }

    /**
     * Function for checking existing post in the database
     * @param int $postId
     * @return boolean
     * @throws Exception
     */
    public function CheckPost($postId)
    {
        if (!is_int($postId) or $postId == 0) {
            throw new Exception('Wrong post id.');
        }
        $update = $this->db->Select([
            'COUNT(*)',
            'powerplay_posts',
            ['post_id' => $postId]
        ]);
        if ($update != 0) {
            return true;
        }
        return false;
    }

    /**
     * Function for edit any post
     * @param array $args
     * @throws Exception
     */
    public function EditPost($args)
    {
        if (!is_array($args) or empty($args)) {
            throw new Exception('Wrong parameters Type in Posts Module');
        }
        if ($this->CheckPost((int)$args['post_id'])) {
            if ($_FILES['image_file']['name']) {
                $newName = $this->imgUpload();
                $imgPath = '/web/templates/Site/assets/images/Posts/' . $newName;
                $imgPathTh = '/web/templates/Site/assets/images/Posts/thumb_' . $newName;

                $update = [
                    'post_id' => $args['post_id'],
                    'post_name' => str_replace("'", '"', $args['post_name']),
                    'post_category_id' => str_replace("'", '"', $args['category_id']),
                    'position_id' => str_replace("'", '"', $args['position']),
                    'post_img' => str_replace("'", '"', $imgPath),
                    'post_img_th' => str_replace("'", '"', $imgPathTh),
                    'short_desc' => str_replace("'", '"', $args['short_desc']),
                    'post_text' => str_replace("'", '"', $args['content']),
                    'post_seo_desc' => str_replace("'", '"', $args['post_seo_desc']),
                    'type_id' => str_replace("'", '"', $args['align']),
                    'display' => 0,
                    'img_type' => str_replace("'", '"', $args['img_type']),
                    'post_seo_title' => $args['post_seo_title']
                ];
            } else {
                $update = [
                    'post_id' => $args['post_id'],
                    'post_name' => str_replace("'", '"', $args['post_name']),
                    'post_category_id' => str_replace("'", '"', $args['category_id']),
                    'position_id' => str_replace("'", '"', $args['position']),
                    'short_desc' => str_replace("'", '"', $args['short_desc']),
                    'post_text' => str_replace("'", '"', $args['content']),
                    'post_seo_desc' => str_replace("'", '"', $args['post_seo_desc']),
                    'type_id' => str_replace("'", '"', $args['align']),
                    'display' => 0,
                    'img_type' => str_replace("'", '"', $args['img_type']),
                    'post_seo_title' => $args['post_seo_title']
                ];
            }
            $update['date_redaction'] = date("Y-m-d H:m:s");
            $update['redactor_id'] = $this->user->getUserId();
            $u = $this->db->Update([
                $update,
                'powerplay_posts',
                ['post_id' => $args['post_id']]
            ]);
            /* if we have a vimeo logo */
            if ($args['img_type']) {
                $video = $args['video_url'];

                strpos($video, "vimeo.com") ? $url = function () use ($video) {
                    $code = end(explode("/", $video));
                    $hash = unserialize(file_get_contents(self::vimeoTH . "$code.php"));
                    return [
                        'video' => self::vimeoPlayer . $code,
                        'img' => $hash[0]['thumbnail_medium']
                    ];
                } : $url = function () use ($video) {
                    $code = explode("=", $video);
                    count($code) == 1 ? $code = explode("/", $video) : '';
                    $code = end($code);
                    return [
                        'video' => self::youtubePlayer . $code,
                        'img' => self::youtubeTH . $code . "/0.jpg"
                    ];
                };

                if ($args['img_type'] == 2) {
                    $update = [
                        'vimeo' => $url()['video'],
                        'display' => 1,
                        'post_img' => ($_FILES['image_file']['name']) ?
                            $imgPath = '/web/templates/Site/assets/images/Posts/' . $this->imgUpload() : ''
                    ];
                } elseif ($args['img_type'] == 1) {
                    $update = [
                        'vimeo' => $url()['video'],
                        'display' => 1,
                        'post_img' => $url()['img']
                    ];
                }
                $this->db->Update([
                    $update
                    ,
                    'powerplay_posts',
                    ['post_id' => $args['post_id']]
                ]);

            }
            if ((is_array($_POST['tags']) and (count($_POST['tags']) > 0))) {
                foreach ($_POST['tags'] as $value) {
                    if ($this->db->Count('*', 'posts_to_tags', [
                            'post_id' => $args['post_id'],
                            'tag_id' => $value
                        ]) == 0
                    ) {
                        $this->db->Insert([
                            [
                                'tag_id' => $value,
                                'post_id' => $args['post_id']
                            ],
                            'posts_to_tags'
                        ]);
                    }
                }
                if ($this->db->Count('tag_id', 'posts_to_tags', ['post_id' => $args['post_id']]) != count($_POST['tags'])) {
                    $tags = $this->getTagsId($args['post_id']);
                    foreach ($tags as $newtag) {
                        if (!in_array($newtag, $_POST['tags'])) {
                            $this->db->Start();
                            $this->db->Execute("delete from posts_to_tags where post_id=" . $args['post_id'] . " and tag_id= $newtag");
                        }
                    }
                }
            }
        } else {
            throw new Exception('Post does not exist');
        }
    }

    private function imgUpload()
    {

############ Configuration ##############
        $thumb_square_size = 200; //Thumbnails will be cropped to 200x200 pixels
        $max_image_size = 5000; //Maximum image size (height and width)
        $thumb_prefix = "thumb_"; //Normal thumb Prefix
        $destination_folder = \Config::$image; //upload directory ends with / (slash)
        $jpeg_quality = 100; //jpeg quality
##########################################
//continue only if $_POST is set and it is a Ajax request
        if (isset($_FILES)) {

// check $_FILES['ImageFile'] not empty
            if (!isset($_FILES['image_file']) || !is_uploaded_file($_FILES['image_file']['tmp_name'])) {
                die('Image file is Missing!'); // output error when above checks fail.
            }

//uploaded file info we need to proceed
            $image_name = $_FILES['image_file']['name']; //file name
            $image_size = $_FILES['image_file']['size']; //file size
            $image_temp = $_FILES['image_file']['tmp_name']; //file temp

            $image_size_info = getimagesize($image_temp); //get image size

            if ($image_size_info) {
                $image_width = $image_size_info[0]; //image width
                $image_height = $image_size_info[1]; //image height
                $image_type = $image_size_info['mime']; //image type
            } else {
                die("Make sure image file is valid!");
            }

//switch statement below checks allowed image type
//as well as creates new image from given file
            switch ($image_type) {
                case 'image/png':
                    $image_res = imagecreatefrompng($image_temp);
                    break;
                case 'image/gif':
                    $image_res = imagecreatefromgif($image_temp);
                    break;
                case 'image/jpeg':
                case 'image/pjpeg':
                    $image_res = imagecreatefromjpeg($image_temp);
                    break;
                default:
                    $image_res = false;
            }

            if ($image_res) {
//Get file extension and name to construct new file name
                $image_info = pathinfo($image_name);
                $image_extension = strtolower($image_info["extension"]); //image extension
                $image_name_only = strtolower($image_info["filename"]); //file name only, no extension
//create a random name for new image (Eg: fileName_293749.jpg) ;
                $new_file_name = $image_name_only . '_' . rand(0, 9999999999) . '.' . $image_extension;

//folder path to save resized images and thumbnails
                $thumb_save_folder = $destination_folder . $thumb_prefix . $new_file_name;
                $image_save_folder = $destination_folder . $new_file_name;
//call normal_resize_image() function to proportionally resize image
                if ($this->normal_resize_image($image_res, $image_save_folder, $image_type, $max_image_size, $image_width, $image_height, $jpeg_quality)) {
//call crop_image_square() function to create square thumbnails
                    if (!$this->crop_image_square($image_res, $thumb_save_folder, $image_type, $thumb_square_size, $image_width, $image_height, $jpeg_quality)) {
                        die('Error Creating thumbnail');
                    }
                }

                imagedestroy($image_res); //freeup memory
                return $new_file_name;
            }
        }

#####  This function will proportionally resize image #####
    }

    protected function normal_resize_image($source, $destination, $image_type, $max_size, $image_width, $image_height, $quality)
    {

        if ($image_width <= 0 || $image_height <= 0) {
            return false;
        } //return false if nothing to resize
//do not resize if image is smaller than max size
        if ($image_width <= $max_size && $image_height <= $max_size) {
            if ($this->save_image($source, $destination, $image_type, $quality)) {
                return true;
            }
        }

//Construct a proportional size of new image
        $image_scale = min($max_size / $image_width, $max_size / $image_height);
        $new_width = ceil($image_scale * $image_width);
        $new_height = ceil($image_scale * $image_height);

        $new_canvas = imagecreatetruecolor($new_width, $new_height); //Create a new true color image
//Copy and resize part of an image with resampling
        if (imagecopyresampled($new_canvas, $source, 0, 0, 0, 0, $new_width, $new_height, $image_width, $image_height)) {
            $this->save_image($new_canvas, $destination, $image_type, $quality); //save resized image
        }

        return true;
    }

##### This function corps image to create exact square, no matter what its original size! ######

    protected function crop_image_square($source, $destination, $image_type, $square_size, $image_width, $image_height, $quality)
    {
        if ($image_width <= 0 || $image_height <= 0) {
            return false;
        } //return false if nothing to resize

        if ($image_width > $image_height) {
            $y_offset = 0;
            $x_offset = ($image_width - $image_height) / 2;
            $s_size = $image_width - ($x_offset * 2);
        } else {
            $x_offset = 0;
            $y_offset = ($image_height - $image_width) / 2;
            $s_size = $image_height - ($y_offset * 2);
        }
        $new_canvas = imagecreatetruecolor($square_size, $square_size); //Create a new true color image
//Copy and resize part of an image with resampling
        if (imagecopyresampled($new_canvas, $source, 0, 0, $x_offset, $y_offset, $square_size, $square_size, $s_size, $s_size)) {
            $this->save_image($new_canvas, $destination, $image_type, $quality);
        }

        return true;
    }

##### Saves image resource to file #####

    protected function save_image($source, $destination, $image_type, $quality)
    {
        switch (strtolower($image_type)) {//determine mime type
            case 'image/png':
                imagepng($source, $destination);
                return true; //save png file
                break;
            case 'image/gif':
                imagegif($source, $destination);
                return true; //save gif file
                break;
            case 'image/jpeg':
            case 'image/pjpeg':
                imagejpeg($source, $destination, $quality);
                return true; //save jpeg file
                break;
            default:
                return false;
        }
    }

    /**
     * Function for getting one post from the db
     * @param int $postId
     * @throws Exception
     */
    public function getOnePost($postId, $admin = false)
    {
        if (!is_int($postId)) {
            throw new Exception('Wrong post id.');
        }
        $select = $this->db->Select([
            '*',
            'powerplay_posts',
            [
                'post_id' => $postId,
            ]
        ])[0];

        if ($select) {

            $select->comments = $this->db->Select([
                '*',
                'post_comments',
                ['post_id' => $select->post_id]
            ]);
            $select->post_category_id = $this->db->Select([
                'category_name',
                'posts_category',
                ['posts_category_id' => $select->post_category_id]
            ])[0]->category_name;
        }

        if ((stristr($select->post_text, "gallery=((") !== false) and $admin === false) {
            $galleryName = explode("=", explode("gallery=((", explode("))", strip_tags(trim(stristr($select->post_text, "gallery=(("))))[0])[1])[0];
            $storage = new \PowerPlay\Storage();
            $gallery_prev = $storage->getValue($galleryName);
            $g = $gallery_prev->image;
            $i = 0;
            $html = '';
            $htmlreplace = "<div class='hovergallery'>";
            foreach ($g as $value1) {
                if (strstr($value1, ".") !== false) {
                    $html .= '<li class="gallery-thumb thumbnail" style="overflow-x: hidden">
                            <a href="#"
                               class="thumbnail-link active not-fade" data-path="' . \Config::$imageGallery . $value1 . '"
                                   data-fade="1">
                                <img alt="MoonStore" data-aspect-ratio="1.50" style="max-height:100px;width: auto; "
                                data-aspect-ratio-type="landscape"
                                class="not-fade"
                                id="' . $i . '"
                                src="' . \Config::$imageGallery . $value1 . '" />
                            </a>
                        </li>';
                    if ($i < self::maxImgOnPage) {
                        $htmlreplace .= "<a href='#' data-id='$i' class='show-gallery'><img "
                            . "style='max-height:150px; width: auto;  ' src=" . \Config::$imageGallery . $value1 . " id='this$i'/></a>";
                        $i == self::maxImgOnPage - 1 ?
                            $htmlreplace .= '<a href="#" data-id="' . ($i) . '" class="show-gallery">'.
                                ' <span class="moreimg" id="this' . $i .'">+' . ((count($g) - 1)-self::maxImgOnPage) . '</span></a>'
                            : '';
                    }
                    $i++;
                }
            }
            $htmlreplace .= "</div>";
            $gallery = file_get_contents(\Config::$web . "templates/Site/gallery/index.html");
            $gallery = str_replace("###gallery_images###", $html, $gallery);
            $gallery = str_replace("###srcbg###", \Config::$imageGallery . $g[0], $gallery);
            $gallery = str_replace("###id###", $postId, $gallery);
            $gallery = str_replace("###count###", count($g) - 1, $gallery);
            $select->gallery = $gallery;
            $select->post_text = str_replace("gallery=(($galleryName))", $htmlreplace, $select->post_text);
        }
        if (stripos($select->post_name, "system_") !== false) {
            $select->post_name = str_replace("system_", "", $select->post_name);
        }
        return $select;
    }

    public function getTags($postId)
    {
        $tagsArray = array();

        $tagsId = $this->db->Select([
            'tag_id',
            'posts_to_tags',
            ['post_id' => $postId]
        ]);
        foreach ($tagsId as $tag) {
            $tagO = $this->db->Select([
                'tag_name',
                'tags',
                ['tag_id' => $tag->tag_id]
            ])[0];
            $tagsArray[$tag->tag_id] = $tagO->tag_name;
        }
        return $tagsArray;
    }

    public function getTagsId($postId)
    {
        $tags = $this->db->Select([
            [
                'tag_id'
            ],
            'posts_to_tags',
            ['post_id' => $postId]
        ]);
        $array = [];
        foreach ($tags as $value) {
            array_push($array, $value->tag_id);
        }
        return $array;
    }

    public function getFromCategory($postId)
    {
        if (!is_int($postId)) {
            throw new Exception('Wrong post id.');
        }
        $count = $this->db->Select([
            'category_count',
            'settings',
            ['setting_id' => 1]
        ])[0]->category_count;
        $q = "select * from powerplay_posts where post_name not like '%system_%'  and isLocked = 0 and post_category_id in"
            . " (select post_category_id from powerplay_posts where post_id = $postId) "
            . " order by date_created desc limit $count";
        $this->db->Connect();
        return $this->db->Execute($q);
    }

    /**
     * We dont have a delete function in the system<br>
     * We just block content. This funciton will block a post in the database
     * @param int $postId
     * @throws Exception
     */
    public function DeletePost($postId)
    {
        if (!is_int($postId) or $postId == 0) {
            throw new Exception('Wrong posts id');
        }
        if ($this->CheckPost($postId)) {
            $this->db->Update([
                [
                    'isLocked' => 1
                ],
                'powerplay_posts',
                ['post_id' => $postId]
            ]);
        }
    }

    /**
     *
     * @param int $postId
     * @throws Exception
     */
    public function Unpublish($postId)
    {
        if (!is_int($postId) or $postId == 0) {
            throw new Exception('Wrong posts id');
        }
        if ($this->CheckPost($postId)) {
            $this->db->Update([
                [
                    'isLocked' => 0
                ],
                'powerplay_posts',
                ['post_id' => $postId]
            ]);
        }
    }

    /**
     * Function for getting all posts(for example it will be news)
     */
    public function getAll($page = 1, $admin = true)
    {

        $pag = $this->db->Select([
            'pagination',
            'settings'
        ])[0]->pagination;
        $this->db->setExecute(false);
//        $posts = $this->db->Select(['*', 'powerplay_posts', [], ['date_created', 'desc']]);
        if (!$admin) {
            if (isset($_GET['category'])) {
                $posts = $this->db->Select([
                    '*',
                    'powerplay_posts',
                    [
                        'post_category_id' => strip_tags(trim($_GET['category']))
                    ],
                    "date_redaction DESC"
                ]);
            } else {
                $posts = $this->db->Select([
                    '*',
                    'powerplay_posts',
                    "isLocked = 0 and post_name not like '%system_%'",
                    "date_created DESC"
                ]);
            }
        } else {

            $posts = $this->db->Select([
                '*',
                'powerplay_posts',
                '',
                "date_redaction DESC"
            ]);
        }
        $posts = Pagination::paginate($posts, $page, $pag);
        if (is_array($posts['query'])) {
            $fposts = array(
                'query' => [],
                'page_num' => []
            );
            $this->db->setExecute(true);
            foreach ($posts['query'] as $post) {
                $post->post_category_id = $this->db->Select([
                    'category_name',
                    'posts_category',
                    ['posts_category_id' => $post->post_category_id]
                ])[0]->category_name;
                $post->post_text = substr(trim(strip_tags($post->post_text)), 0, 50);

                array_push($fposts['query'], $post);
            }
            $fposts['page_num'] = $posts['page_num'];
            return $fposts;
        } else {
            throw new Exception('Somthing wrong');
        }
    }

    public function Positions()
    {
        $positions = $this->db->Select([
            '*',
            'positions'
        ]);
        return $positions;
    }

    public function getLastPosts()
    {
        $this->db->Connect();
        $q = "select * from powerplay_posts where post_name not like '%system_%'"
            . " order by date_redaction desc limit 4";
        $posts = $this->db->Execute($q);
        if ($posts) {
            $fposts = array();
            foreach ($posts as $post) {
                $post->post_category_id = $this->db->Select([
                    'category_name',
                    'posts_category',
                    ['posts_category_id' => $post->post_category_id]
                ])[0]->category_name;
                array_push($fposts, $post);
            }
            return $fposts;
        }
    }

    public function getPosition()
    {
        return $this->db->Select([
            '*',
            'positions'
        ]);
    }

    public function getAllTags()
    {
        return $this->db->Select([
            '*',
            'tags'
        ]);
    }

    public function FindByTag($tagName)
    {
        $tagName = urldecode($tagName);
        if (!$tagName) {
            throw new Exception('Wrong tag');
        }
        $q = "select p.* from powerplay_posts as p left "
            . "join posts_to_tags ptt on (p.post_id = ptt.post_id) "
            . "left join tags t on (t.tag_id = ptt.tag_id) "
            . "where t.tag_name = '$tagName' and p.post_name not like '%system_%' order by p.date_created DESC";
        $this->db->Start();
        $tags = Pagination::paginate($q);
        foreach ($tags['query'] as $key => $tag) {
            $tag->post_category_id = $this->db->Select([
                'category_name',
                'posts_category',
                ['posts_category_id' => $tag->post_category_id]
            ])[0]->category_name;
            $tags['query'][$key] = $tag;
        }


        return $tags;
    }

    public function getAligns()
    {
        $aligns = $this->db->Select([
            '*',
            'moonstore_aligns'
        ]);
        return $aligns;
    }

    /**
     * getting all counts of posts
     * @return array
     */
    public function getCount()
    {
        $date = new \DateTime('now');
        $month = $date->format('m');
        $array = [
            'all' => '',
            'this_month' => '',
            'blocked' => ''
        ];
        $array['all'] = $this->db->Count('post_id', 'powerplay_posts');
        $array['this_month'] = $this->db->Count('post_id', 'powerplay_posts', 'MONTH(date_created) = "' . $month . '"');
        $array['blocked'] = $this->db->Count('post_id', 'powerplay_posts', ['isLocked' => 1]);
        return $array;
    }

    public function preview($args)
    {
        if ($args['post_id']) {
            $this->db->Insert([
                [
                    'post_id' => str_replace("'", '"', $args['post_id']),
                    'post_name' => str_replace("'", '"', $args['post_name']),
                    'post_text' => str_replace("'", '"', $args['content'])
                ],
                'preview'
            ]);
        } else {
            $postId = $this->AddPost($args);
            $args['post_id'] = $postId;
        }
        return $post;
    }

    public function getpreview($args)
    {
        $postP = $this->db->Select([
            '*',
            'preview',
            ['post_id' => $args['post_id']]
        ])[0];
        $post = $this->getOnePost((int)$args['post_id']);
        if ($postP->post_text) {
            $post->post_text = $postP->post_text;
        }
        if ($postP->post_name) {
            $post->post_name = $postP->post_name;
        }
        if ((stristr($post->post_text, "gallery=((") !== false)) {
            $galleryName = explode("=", explode("gallery=((", explode("))", strip_tags(trim(stristr($post->post_text, "gallery=(("))))[0])[1])[0];
            $storage = new \PowerPlay\Storage();
            $gallery_prev = $storage->getValue($galleryName);
            $g = $gallery_prev->image;
            $i = 0;
            $html = '';
            $htmlreplace = "<div class='hovergallery'>";
            foreach ($g as $value1) {
                if (strstr($value1, ".") !== false) {
                    $html .= '<li class="gallery-thumb thumbnail">
                            <a href="#"
                               class="thumbnail-link active not-fade" data-path="' . \Config::$imageGallery . $value1 . '"
                                   data-fade="1">
                                <img alt="MoonStore" data-aspect-ratio="1.50"
                                data-aspect-ratio-type="landscape" 
                                data-max-height="1334" 
                                data-max-width="2000"
                                class="not-fade"
                                id="' . $i . '"
                                src="' . \Config::$imageGallery . $value1 . '" />
                            </a>
                        </li>';
                    $htmlreplace .= "<a href='#' data-id='$i' class='show-gallery'><img "
                        . "style='max-width:150px; height: auto' src=" . \Config::$imageGallery . $value1 . " id='this$i'/></a>";
                    $i++;
                }
            }
            $htmlreplace .= "</div>";
            $gallery = file_get_contents(\Config::$web . "templates/Site/gallery/index.html");
            $gallery = str_replace("###gallery_images###", $html, $gallery);
            $gallery = str_replace("###srcbg###", \Config::$imageGallery . $g[0], $gallery);
            $gallery = str_replace("###id###", $postId, $gallery);
            $gallery = str_replace("###count###", count($g) - 1, $gallery);
            $post->gallery = $gallery;
            $post->post_text = str_replace("gallery=(($galleryName))", $htmlreplace, $post->post_text);
        }
        if (stripos($post->post_name, "system_") !== false) {
            $post->post_name = str_replace("system_", "", $post->post_name);
        }
        $this->db->Start();
        $this->db->Execute("delete from preview where post_id = " . $args['post_id']);
        return $post;
    }

}