<?php

namespace Modules\Posts\Helprers;

use PowerPlay\Database;

class posts_cron
{

    /**
     *
     * @var Database
     */
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function check($id, $date)
    {
        $wholeDate    = $date;
        $ex           = explode(":", $date);
        $ex[end($ex)] = "";
        $date         = implode(":", $ex);
        $q            = file_get_contents(__DIR__ . '/schedule.sql');
        $sql          = str_replace("test_post_active_id", "test_post_active_$id", $q);
        $sql          = str_replace("###id###", "$id", $sql);
        $sql          = str_replace("###whole-date####", $wholeDate, $sql);
        $this->db->Start();
        $this->db->Execute("SET GLOBAL event_scheduler = ON;");
        $this->db->Execute($sql);
    }

}
