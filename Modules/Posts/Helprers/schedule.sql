CREATE EVENT test_post_active_id 
ON SCHEDULE
AT '###whole-date####'
DO
UPDATE `powerplay_posts` 
SET `isLocked` = 0 WHERE `post_id` = ###id###
AND `isLocked` = 1;