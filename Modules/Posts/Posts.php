<?php

namespace Modules\Posts;

use PowerPlay\Module;
use Modules\Posts\Helprers\PostsHelper;
use PowerPlay\PowerplayException\PowerplayException;
use Modules\Roles\Users;

class Posts extends Module
{

    private $helper;

    public function actionPreviewSave($args)
    {
        !$this->helper ? $this->helper = new PostsHelper() : '';

        $this->helper->preview($args);
    }

    public function actionPreviewShow($args)
    {
        !$this->helper ? $this->helper = new PostsHelper() : '';
	$userManagement = new Users();
        $post         = $this->helper->getpreview($args);
        $fromCat      = $this->helper->getFromCategory((int) $args['post_id']);
        $tags         = $this->helper->getTags((int) $args['post_id']);
        $slider       = $this->helper->getSliderSettings();
	 $autho          = $userManagement->Load(['user_id',
            $post->created_by]);
        $ch           = new Helprers\CategotyHelper();
        $categories   = $ch->getCategories();
        $pps          = $this->Render('post', [$post], 'Site/Post', false);
        $pps          = $this->addVariebles($pps, 'last', $fromCat);
        $pps          = $this->addVariebles($pps, 'tags', $tags);
        $pps          = $this->addVariebles($pps, 'categoties', $categories);
        $pps          = $this->addVariebles($pps, 'sliders', $slider);
        $pps            = $this->addVariebles($pps, 'author', [$autho]);
        $pps->Display('Site/Post');
    }

    public function actionSearch($args)
    {
        !$this->helper ? $this->helper      = new PostsHelper() : '';
        $posts             = [];
        $searched          = $this->helper->Search(trim(strip_tags(urldecode($args['q']))));
        $posts['query']    = $searched;
        $posts['page_num'] = 1;
        $page              = 1;
        $positions         = $this->helper->Positions();
        $slider            = $this->helper->getSliderSettings();
        $ch                = new Helprers\CategotyHelper();
        $categories        = $ch->getCategories();
        $pps               = $this->Render('posts', $posts, 'Site/index', false);
        $pps               = $this->addVariebles($pps, 'pos', $positions);
        $pps               = $this->addVariebles($pps, 'page', $page);
        $pps               = $this->addVariebles($pps, 'categoties', $categories);
        $pps               = $this->addVariebles($pps, 'sliders', $slider);
        $pps->Display('Site/index');
    }

    public function actionMain()
    {
        !$this->helper ? $this->helper = new PostsHelper() : '';
        $this->helper->getAll('', false);
    }

    public function actionPagination($args)
    {
        if (!$args['page'] or $args['page'] <= 0) {
            $args['page'] = 1;
        }
        $this->actionShowAll($args);
    }

    public function actionAjaxTag($args)
    {

        if (!$args) {
            echo '{error: no data for uery}';
            exit();
        }
        $ah   = new \Modules\Admin\Helpers\AdminHelper();
        $tags = $ah->findTags($args['q']);
        $json = [];
        foreach ($tags as $tag) {
            array_push($json, $tag->tag_id . ". " . $tag->tag_name);
        }
        echo json_encode($json);
    }

    public function actionCategory($args)
    {
        if (!$args['page'] or $args['page'] != 0) {
            $page = 1;
        } else {
            $page = $args['page'];
        }
        if (!$args['category_id'] or $args['category_id'] == 0) {
            throw new PowerplayException(MOD_EMPTY);
        }
        !$this->helper ? $this->helper = new PostsHelper() : '';
        $ch           = new Helprers\CategotyHelper();
        $categories   = $ch->getCategories();
        $category     = $ch->getOneCategory($args['category_id']);
        $positions    = $this->helper->Positions();
        $slider       = $this->helper->getSliderSettings();
        $pots         = $this->helper->getPostsCategory($args['category_id']);
        $pps          = $this->Render('posts', $pots, 'Site/index', false);
        $pps          = $this->addVariebles($pps, 'category', $category);
        $pps          = $this->addVariebles($pps, 'categoties', $categories);
        $pps          = $this->addVariebles($pps, 'page', $page);
        $pps          = $this->addVariebles($pps, 'pos', $positions);
        $pps          = $this->addVariebles($pps, 'sliders', $slider);
        $pps->Display('Site/index');
    }

    public function actionFindTag($args)
    {
        if (!$args['page'] or $args['page'] != 0) {
            $page = 1;
        } else {
            $page = $args['page'];
        }
        if (!$args) {
            throw new PowerplayException('Wrong name');
        }
        !$this->helper ? $this->helper = new PostsHelper() : '';
        $ch           = new Helprers\CategotyHelper();

        $tags       = $this->helper->FindByTag($args['tagname']);
        $categories = $ch->getCategories();
        $positions  = $this->helper->Positions();
        $slider     = $this->helper->getSliderSettings();

        $pps = $this->Render('posts', $tags, 'Site/index', false);
        $pps = $this->addVariebles($pps, 'categoties', $categories);
        $pps = $this->addVariebles($pps, 'page', $page);
        $pps = $this->addVariebles($pps, 'pos', $positions);
        $pps = $this->addVariebles($pps, 'sliders', $slider);
        $pps = $this->addVariebles($pps, 'tagsfinder', [1]);
        $pps->Display('Site/index');
    }

    public function actionAddPost($args)
    {

        if (!is_array($args)) {
            throw new PowerplayException('Wrong arguments type');
        }
        !$this->helper ? $this->helper = new PostsHelper() : '';

        $postId = $this->helper->AddPost($args);
        $this->Redirect('/admin/post/edit/' . $postId);
    }

    public function actionEditPost($args)
    {
        if (!is_array($args) or empty($args)) {
            throw new PowerplayException('Wrong arguments in posts system');
        }
        if ($args['main_params']) {
            $args = $args['main_params'];
        }
        !$this->helper ? $this->helper = new PostsHelper : '';
        $this->helper->EditPost($args);
        $this->Redirect('/admin/post/edit/' . $args['post_id']);
    }

    public function actionShowPost($args)
    {
//        if (! isset($args['post_id']) or $args['post_id'] == 0) {
//            throw new PowerplayException('Wrong arguments.');
//        }
        !$this->helper ? $this->helper = new PostsHelper : '';

        $userManagement = new Users();
        $post           = $this->helper->getOnePost((int) $args['post_id']);
        $autho          = $userManagement->Load(['user_id',
            $post->created_by]);

        $fromCat        = $this->helper->getFromCategory((int) $args['post_id']);
        $tags           = $this->helper->getTags((int) $args['post_id']);
        $slider         = $this->helper->getSliderSettings();
        $ch             = new Helprers\CategotyHelper();
        $categories     = $ch->getCategories();
        $pps            = $this->Render('post', [$post], 'Site/Post', false);
        $pps            = $this->addVariebles($pps, 'last', $fromCat);
        $pps            = $this->addVariebles($pps, 'tags', $tags);
        $pps            = $this->addVariebles($pps, 'categoties', $categories);
        $pps            = $this->addVariebles($pps, 'sliders', $slider);
        $pps            = $this->addVariebles($pps, 'author', [$autho]);
        $pps->Display('Site/Post');
    }

    public function actionShowAll($args)
    {
        if (!$args['page'] or $args['page'] <= 0) {
            $page = 1;
        } else {
            $page = $args['page'];
        }
        !$this->helper ? $this->helper = new PostsHelper() : '';
        $posts        = $this->helper->getAll($page, false);
        $positions    = $this->helper->Positions();
        $slider       = $this->helper->getSliderSettings();
        $ch           = new Helprers\CategotyHelper();
        $categories   = $ch->getCategories();
        $pps          = $this->Render('posts', $posts, 'Site/index', false);
        $pps          = $this->addVariebles($pps, 'pos', $positions);
        $pps          = $this->addVariebles($pps, 'page', $page);
        $pps          = $this->addVariebles($pps, 'categoties', $categories);
        $pps          = $this->addVariebles($pps, 'sliders', $slider);
        $pps->Display('Site/index');
    }

    public function actionDeletePost($args)
    {
        if (!isset($args['post_id']) or empty($args['post_id'])) {
            throw new PowerplayException('Wrong post id');
        }
        !$this->helper ? $this->helper = new PostsHelper() : '';
        $this->helper->DeletePost((int) $args['post_id']);
        header("Location: " . $_SERVER['HTTP_REFERER']);
        exit();
    }

    public function actionUnblockPost($args)
    {
        if (!isset($args['post_id']) or empty($args['post_id'])) {
            throw new PowerplayException('Wrong post id');
        }
        !$this->helper ? $this->helper = new PostsHelper() : '';
        $this->helper->Unpublish((int) $args['post_id']);
        header("Location: " . $_SERVER['HTTP_REFERER']);
        exit();
    }

    public function actionUpload($args)
    {
        require_once '../../uploadimg.php';
    }

    public function actionCategoryAdd($args)
    {
        if (!is_array($args)) {
            throw new PowerplayException('Wrong parameters');
        }
        $category = new Helprers\CategotyHelper();
        $category->AddCategory($args);
    }

    public function actionCategoryEdit($args)
    {
        if (!is_array($args)) {
            throw new PowerplayException('Wrong arguments');
        }
        $cat = new Helprers\CategotyHelper();
        $cat->EditCategory($args);
    }

    public function actionCategoryDelete($args)
    {
        if (!isset($args['category_id'])) {
            throw new PowerplayException('Wrong arguments');
        }
        $cat = new Helprers\CategotyHelper();
        $cat->DeleteCategory((int) $args['category_id']);
    }

    public function actionZag()
    {
        $this->Render('', '', 'Main/index');
    }

}
