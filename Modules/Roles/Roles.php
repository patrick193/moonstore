<?php

namespace Modules\Roles;

use Modules\Roles\Rules\RoleRules;
use PowerPlay\YamlConfiguration;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of Rules
 *
 * @author Developer Pohorielov Vladyslav
 */
class Roles extends RoleRules {

    private $actionAllowed = ['create', 'edit'];
    
    /**
     * Function for checking to access to the module
     * @param string $roleCode
     * @param \PowerPlay\ModuleLoader\AbstractsModuleModel $module 
     * @throws Exception
     */
    public function CheckAccess($roleCode, $module) {
        if (empty($roleCode) or is_null($roleCode) or ! is_string($roleCode)) {
            throw new PowerplayException(MOD_WRONG_ROLE_CODE . " ". $roleCode);
        }
        $define = $module->getDefineAccess();
        $yaml = new YamlConfiguration();
        $standartAccess = $yaml->GetMainConfiguration();
        $comparedDefine = $this->Compare($define, $standartAccess['AccessGroups']['Define']);
        if (count($comparedDefine) == count($define)) {
            $cred = $this->Define($roleCode, $define);
            $finalCredential = array();
            foreach ($cred as $access => $code) {
                $finalCredential[$access] = $code;
            }
            return $cred;
        } else {
            throw new PowerplayException(MOD_DIFFERNT_CONFIGS);
        }
    }

    /**
     * Function to find all credentionals for this role
     * @param string $code
     * @param array $define
     * @return type
     * @throws Exception
     */
    public function Define($code, $define) {
        if (!is_array($define) or ! is_string($code)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $defined = array();
        foreach ($define as $access => $roles) {
            if (in_array($code, $roles) or $roles[0] == 'all') {
                $defined[$access] = $code;
            }
        }
        return $defined;
    }

    /**
     * Function for compare keys in arrays
     * @param array $one
     * @param array $two
     * @return array
     */
    private function Compare($one, $two) {
        if (is_array($one) and is_array($two)) {
            $compare = array();
            foreach ($one as $key => $value) {
                foreach ($two as $key1 => $value1) {
                    if (($key == $key1) or ( $value === 'all')) {
                        array_push($compare, $key);
                    }
                }
            }
            return $compare;
        }
    }

    public function CheckAction($roleCode, $roleCodeCreated, $action) {
        if (!is_string($roleCode) or ! is_string($roleCodeCreated) or ! $this->ActionPossible($action)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
       
    }
    
    /**
     * Functio for checking in array
     * @param string $action
     * @return boolean
     * @throws Exception
     */
    public function ActionPossible($action){
        if(!is_string($action)){
            throw new PowerplayException(MOD_STRING_ACTION);
        }
        if(in_array($action, $this->actionAllowed)){
            return true;
        }
        return false;
        
    }


}
