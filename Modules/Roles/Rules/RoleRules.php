<?php

namespace Modules\Roles\Rules;

use PowerPlay\Role;
use Modules\Roles\System\RulesInterface;
use Modules\Roles\Users;
use PowerPlay\Database;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of Roles
 *
 * @author Developer Pohorielov Vladyslav
 */
class RoleRules extends Role implements RulesInterface {

    private $db;
    private $roles = array();

    public function __construct() {
        if (!$this->db) {
            $this->db = new Database();
        }
    }

    /**
     * Function to get all structure of roles
     * @return array 
     */
    public function Dependencies() {
        !$this->db ? $this->db = new Database() : '';
        $groups = $this->db->SelectAll('powerplay_groups');
        foreach ($groups as $group) {
            $this->db->setExecute(false);
            $select = $this->db->Select(['*', 'powerplay_roles', ['group_id' => $group->group_id]]);
            $this->roles[$group->group_name] = $this->db->Execute($select, RoleRules::class);
        }
        foreach ($this->roles as $groupName => $roles) {
            foreach ($roles as $key => $role) {
                $count = function() use ($role) {
                    $count = $this->db->Select(['COUNT(*)', 'powerplay_roles', ['role_code' => $role->getRoleCode()]]);
                    return $count;
                }; // anon function to get count of same code roles.
                if (!$this->CodeCheck($role->getRoleCode(), (int) $role->getGroupId(), $role->getRoleName())) {
                    unset($this->roles[$groupName][$key]);
                }
                if ($count() > 1 and $role->getRoleName() == '1A') {
                    throw new PowerplayException(MOD_SECURITY_ROLES);
                }
            }
        }
        return $this->roles;
    }

    /**
     * Function to check right code
     * @param string $code Code of the role
     * @param int $groupId Group id in the database
     * @param string $groupName Name of the group
     * @return boolean
     * @throws Exception
     */
    protected function CodeCheck($code, $groupId, $groupName) {
        if (!is_string($code) or ! is_int($groupId) or ! is_string($groupName)) {
            throw new PowerplayException(MOD_ROLE_CODE_CHECK);
        }
        $groupId = abs($groupId);
        if ($code{0} == $groupId) {

            $literal = explode($code{0}, $code);
            $pos = strpos(strtoupper($groupName), strtoupper($literal[1]));
            if ($pos === false) {
                return false;
            }
            return true;
        }
    }

    /**
     * Function to check can role create another roles
     * @param integer $id Id of role
     * @return boolean
     * @throws Exception
     */
    public function CanCreate($id) {
        if (!is_int($id)) {
            throw new PowerplayException(MOD_INT);
        }
        if (!$this->getChild(abs($id))) {
            return false;
        }
        return true;
    }

    /**
     * Function to get all children for role
     * @param integer $id Role id
     * @return mixed Function can return an array of objects type Role<br>or can return false if could not find
     * @throws Exception
     */
    public function getChild($id) {
        if (!is_int($id)) {
            throw new PowerplayException(MOD_INT);
        }
        $id = abs($id);
        !$this->db ? $this->db = new Database() : '';
        $this->db->setExecute(false);
        $select = $this->db->Select(['*', 'powerplay_roles', ['parent_id' => $id]]);
        $children = $this->db->Execute($select, Role::class);
        if (is_array($children) and ! empty($children)) {
            return $children;
        }
        return false;
    }

    /**
     * Function to get a Parent of role
     * @param integer $parentId Id of parent role
     * @return mixed Function can return an array of objects type Role<br>or can return false if could not find
     * @throws Exception
     */
    public function getParent($parentId) {
        if (!is_int($parentId)) {
            throw new PowerplayException(MOD_INT);
        }
        !$this->db ? $this->db = new Database() : '';
        $parentId = abs($parentId);
        $this->db->setExecute(false);
        $parent = $this->db->Execute($this->db->Select(['*', 'powerplay_roles', ['role_id' => $parentId]]), Role::class);
        if (is_array($parent) and ! empty($parent)) {
            return $parent;
        }
        return false;
    }

    /**
     * Function to find a role by id
     * @param integer $id Id of role
     * @param string $group Group name
     * @return mixed Function can return an object type Role<br>or can return false if could not find
     * @throws Exception
     */
    public function FindById($id) {
        if (!is_int($id)) {
            throw new PowerplayException(MOD_INT);
        }
        !$this->roles ? $this->Dependencies() : '';
        foreach ($this->roles as $group) {
            foreach ($group as $role) {
                if ($id === (int) $role->getRoleId()) {
                    return $role;
                }
            }
        }
        return false;
    }

    /**
     * Function to find a role by code
     * @param string $code Code of the role
     * @param string $group Group name
     * @return mixed Function can return an object type Role<br>or can return false if could not find
     * @throws Exception
     */
    public function FindByCode($code, $group = 'external') {
        if (!is_string($code)) {
            throw new PowerplayException(MOD_STRING);
        }
        !$this->roles ? $this->Dependencies() : '';
        foreach ($this->roles[$group] as $role) {
            if ($code === $role->getRoleCode()) {
                return $role;
            }
        }
        return false;
    }

    /**
     * Function to get users with parameters
     * @param array $param
     * @throws Exception
     */
    public function Load($param) {
        if (!is_array($param) or empty($param) or count($param) > 2) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        !$this->db ? $this->db = new Database() : '';
        $fields = explode(",", $param[0]);
        $args = explode(",", $param[1]);
        if (count($fields) === count($args)) {
            $query = 'SELECT * FROM `powerplay_users` WHERE ';
            for ($i = 0, $c = count($fields); $i < $c; $i++) {
                $query .= "`" . trim($fields[$i]) . "` = '" . trim($args[$i]) . "'";
                $i < ($c - 1) ? $query .= " AND " : '';
            }
            $this->db->Connect();
            $users = $this->db->Execute($query, Users::class);
            $this->db->Close();
            return $users;
        } else {
            throw new PowerplayException(MOD_LOAD);
        }
    }

    /**
     * Function to return 
     * @return \Modules\Roles\Rules\RoleRules
     */
    public function Create() {
        return new RoleRules();
    }

    public function Execute(Role $role) {
        if (is_null($role)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        !$this->db ? $this->db = new Database() : '';
        if ($this->db->Select(['COUNT(*)', 'powerplay_roles', ['role_id' => $role->getRoleId()]])) {
            return $this->Edit($role);
        } else {
            return $this->Add($role);
        }
    }

    private function Add(RoleRules $role) {
        !$this->db ? $this->db = new Database() : '';
        $res = $this->db->Insert([['role_code' => $role->getRoleCode(), 'role_name' => $role->getRoleName(), 'isLocked' => $role->getIsLocked(),
        'parent_id' => $role->getParentId(), 'group_id' => $role->getGroupId()], 'powerplay_roles']);
        return $res;
    }

    private function Edit(RoleRules $role) {
        !$this->db ? $this->db = new Database() : '';
        $res = $this->db->Update([['role_code' => $role->getRoleCode(), 'role_name' => $role->getRoleName(), 'isLocked' => $role->getIsLocked(),
        'parent_id' => $role->getParentId(), 'group_id' => $role->getGroupId()], 'powerplay_roles', ['role_id' => $role->getRoleId()]]);
        return $res;
    }

}
