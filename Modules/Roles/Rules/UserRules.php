<?php

namespace Modules\Roles\Rules;

use PowerPlay\User;
use PowerPlay\ModuleLoader\Model;
use PowerPlay\Database;
use PowerPlay\Storage;
use Modules\Roles\System\UserInterface;
use Modules\Roles\Roles;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of UserRules
 *
 * @author Developer Pohorielov Vladyslav
 */
class UserRules extends User implements UserInterface {

    use traitUsers;

    private $db, $storage;

    public function __construct() {
        if (!$this->db) {
            $this->db = new Database();
        }
        !$this->storage ? $this->storage = new Storage('users') : '';
    }

    /**
     * Function to check access to the module
     * @param int $id
     * @param Model $module
     * @return boolean
     * @throws Exception
     */
    public function CheckAccess($id, Model $module) {
        if (!is_int($id)) {
            throw new PowerplayException(MOD_INT);
        }
        !$this->db ? $this->db = new Database() : '';
        $id = abs($id);
        $roleCode = $this->db->Select(['role_code', 'powerplay_roles', ['role_id' =>
                $this->db->Select(['role_id', 'powerplay_users', ['user_id' => $id]])[0]->role_id]]);
        if (!is_null($roleCode)) {
            $role = new Roles();
            $access = $role->CheckAccess($roleCode[0]->role_code, $module);
            return $access;
        }
        return false;
    }

    /**
     * Function to get all children by user
     * @param int $userId
     * @return mixed The function will return an array of users objects or false if could not find any users
     * @throws Exception
     */
    public function getChild($userId) {
        if (!is_int($userId) or empty($userId)) {
            throw new PowerplayException(MOD_INT);
        }
        !$this->db ? $this->db = new Database() : '';
        $userId = abs($userId);
        $this->db->setExecute(false);
        $query = $this->db->Select(['*', 'powerplay_users', ['parent_id' => $userId]]);
        $users = $this->db->Execute($query, UserRules::class);
        return $users;
    }

    /**
     * Function to get parent user
     * @param int $userId
     * @return mixed The function will return an array of users objects or false if could not find any users
     * @throws Exception
     */
    public function getParent($userId) {
        if (!is_int($userId) or empty($userId)) {
            throw new PowerplayException(MOD_INT);
        }
        !$this->db ? $this->db = new Database() : '';
        $userId = abs($userId);
        $this->db->setExecute(false);
        $q = $this->db->Select(['*', 'powerplay_users', ['user_id' =>
                "(" . $this->db->Select(['`parent_id`', 'powerplay_users', ['user_id' => $userId]]) . ")"]]);
        $q = str_replace("'", "", $q);
        $user = $this->db->Execute($q, UserRules::class);
        return $user;
    }

    public function getAddress($userId) {
        if (!is_int($userId) or empty($userId)) {
            throw new PowerplayException(MOD_INT);
        }
        !$this->db ? $this->db = new Database() : '';
        $userId = abs($userId);

        $userAddress = $this->db->Select(['*', 'user_address', ['user_address_id' => $this->db->Select(
                        [['user_address_id'], 'powerplay_users', ['user_id' => $userId]])[0]->user_address_id]]);

        if ($userAddress) {
            $address = array();
            $city = $this->db->Select(['*', 'cities', ['cityId' => $userAddress[0]->city_id]]);
            $region = $this->db->Select(['name', 'region', ['regionId' => $city[0]->regionId]]);
            $country = $this->db->Select(['name', 'country', ['countryId' => $city[0]->countryId]]);
            $address['country'] = $country;
            $address['region'] = $region[0]->name;
            $address['city'] = $city[0]->name;
            $address['street'] = $userAddress[0]->street;
            $address['postal_code'] = $userAddress[0]->postal_code;
            return $address;
        }
        return false;
    }

    /**
     * Create a new user
     * @return \Modules\Roles\Rules\UserRules
     */
    public function Create() {
        return new UserRules();
    }

    public function Delete($userId) {
        if ($userId === 0 or ! is_int($userId)) {
            throw new Exception('Error');
        }
        $res = $this->db->Select(['`isLocked`', 'powerplay_users', ['user_id' => $userId]])[0];
        if ($res->isLocked == 1) {
            $this->db->Update([['isLocked' => 0], 'powerplay_users', ['user_id' => $userId]]);
        } else {
            $this->db->Delete(['powerplay_users', ['user_id' => $userId]]);
        }
    }

    /**
     * Function to create or edit user
     * @param User $user
     * @return boolean true if all work finished correct
     * @throws Exception
     */
    public function Execute(User $user) {
        if (!$this->getSwitcher()) {
            throw new PowerplayException(MOD_SWITCHER);
        }
        if (is_null($user)) {
            throw new Exception('User is null.');
        }
        !$this->db ? $this->db = new Database() : '';
        if ($this->db->Count('*', 'powerplay_users', ['user_id' => $user->getUserId()]) > 0) {
            $this->Switcher();
            return $this->Edit($user);
        } else {
            $this->Switcher();
            return $this->Add($user);
        }
    }

    /**
     * Function to add new user
     * @param User $user
     * @return boolean
     */
    private function Add(User $user) {
        !$this->db ? $this->db = new Database() : '';

        $res = $this->db->Insert([['user_first_name' => $user->getUserFirstName(),
        'user_last_name' => $user->getUserLastName(),
        'user_email' => $user->getUserEmail(), 'parent_id' => $user->getParentId(),
        'role_id' => $user->getRoleId(), 'isLocked' => $user->getIsLocked(),
        'cell_phone' => $user->getCellPhone(), 'user_password' => $user->getPassword()
            ],
            'powerplay_users']);
        return $res;
    }

    /**
     * Function to edit user
     * @param User $user
     * @return boolean
     */
    private function Edit(User $user) {
        !$this->db ? $this->db = new Database() : '';

        $res = $this->db->Update([['user_first_name' => $user->getUserFirstName(),
        'user_last_name' => $user->getUserLastName(),
        'user_email' => $user->getUserEmail(), 'parent_id' => $user->getParentId(),
        'role_id' => $user->getRoleId(), 'isLocked' => $user->getIsLocked(),
        'cell_phone' => $user->getCellPhone()],
            'powerplay_users', ['user_id' => $user->getUserId()]]);
        if ($user->getPassword()) {
            $this->db->Update([['user_password' => $user->getPassword()], 'powerplay_users',
                ['user_id' => $user->getUserId()]]);
        }
        return $res;
    }

    /**
     * Function to getting users with parameters
     * @param array $param
     * @throws Exception
     */
    public function Load($param) {
        if (!is_array($param) or empty($param) or count($param) > 2) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPEs);
        }
        !$this->db ? $this->db = new Database() : '';
        $fields = explode(",", $param[0]);
        $args = explode(",", $param[1]);
        if (count($fields) === count($args)) {
            $query = 'SELECT * FROM `powerplay_users` WHERE ';
            for ($i = 0, $c = count($fields); $i < $c; $i++) {
                $query .= "`" . trim($fields[$i]) . "` = '" . trim($args[$i]) . "'";
                $i < ($c - 1) ? $query .= " AND " : '';
            }
            $this->db->Connect();
            $users = $this->db->Execute($query, UserRules::class);
            $this->db->Close();
            if (count($users) === 1) {
                return $users[0];
            }
            return $users;
        } else {
            throw new PowerplayException(MOD_LOAD);
        }
    }

    protected function Check($user_id) {
        !$this->db ? $this->db = new Database() : '';
        $action = 'create';
        if ($this->db->Select(['COUNT(*)', 'powerplay_users', ['user_id' => $user_id]])) {
            $action = 'edit';
        } else {
            $action = 'create';
        }
        return $action;
    }

}
