<?php

namespace Modules\Roles\Rules;

trait traitUsers {

    protected 
            $cell_phone,
            $language_id,
            $last_visit;

    public function getLastVisit() {
        return $this->last_visit;
    }

    public function setLastVisit($last_visit) {
        $this->last_visit = $last_visit;
    }


    public function getLanguageId() {
        return $this->language_id;
    }

    public function setLanguageId($languageId) {
        $this->language_id = $languageId;
        return $this;
    }

    public function getCellPhone() {
        return $this->cell_phone;
    }

    public function setCellPhone($cellPhone) {
        $this->cell_phone = $cellPhone;
        return $this;
    }

}
