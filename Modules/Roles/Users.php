<?php

/**
 * postal codes
 * users id
 */

namespace Modules\Roles;

use Modules\Roles\Rules\UserRules;
use Modules\Roles\Roles;
use PowerPlay\YamlConfiguration;
use PowerPlay\User;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of Users
 *
 * @author Developer Pohorielov Vladyslav
 */
class Users extends UserRules {

    /**
     * Array of existed actions
     * @var array 
     */
    private $actions = ['create', 'edit', 'auth',];

    /**
     * Function for getting all rules for user
     * @param User $user
     * @return array Structure of array: [roleCode]=>array{[action]=>{[0]=>roleCode,...}}
     */
    public function getRoleRules(User $user) {
        $roles = new Roles();
        $role = $roles->FindById((int) $user->getRoleId());
        unset($roles);
        $yaml = new YamlConfiguration();
        $roleRules = $yaml->GetConfigurations(__DIR__ . "/Config/UsersRules.yml");
        $rules = array();
        foreach ($roleRules->RoleRules[$role->getGroupId()] as $action => $roles) {
            if ($roles[$role->getRoleCode()]) {
                $rules[$action] = $roles[$role->getRoleCode()];
            }
        }
        $rulesRole[$role->getRoleCode()] = $rules;
        unset($rules);
        return $rulesRole;
    }

    /**
     * Function for getting access for some action
     * @param int $userId Id of user
     * @param string $roleCode Code of role
     * @param string $action Name of action
     * @return boolean
     * @throws Exception
     */
    public function CheckAction($userId, $roleCode, $action = '') {
        if (!is_int($userId) or ! is_string($roleCode) or ! $this->ActionPossible($action)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        empty($action) ? $action = $this->Check($userId) : '';

        $user = $this->Load(['user_id', $userId]);
        if (!is_null($user)) {
            $role = $this->getRoleRules($user);
            if (in_array($roleCode, $role[key($role)][$action])) {
                return true;
            }
            return false;
        } else {
            throw new PowerplayException(MOD_USER_DID_NOT_FIND);
        }
    }

    /**
     * Private function for checking existed action
     * @param string $action
     * @return boolean
     * @throws Exception
     */
    private function ActionPossible($action) {
        if (!is_string($action) or empty($action)) {
            throw new PowerplayException(MOD_MISSED_ACTION);
        }
        if (in_array($action, $this->actions)) {
            return true;
        }
        return false;
    }

    /**
     * Function to execute add or edit
     * @param User $user
     * @return boolean
     * @throws Exception
     */
    public function Execute(User $user) {
        $args = func_get_args();
        if ($args[1] and is_int($args[1]) and ! empty($args[1])) {
            $role = new Roles();
            $action = $this->Check($user->getUserId());
            if ($this->CheckAction($args[1], $role->FindById((int) $user->getRoleId())->getRoleCode(), $action)) {
                $this->Switcher(true);
                return parent::Execute($user);
            } else {
                throw new PowerplayException(MOD_DOES_NOT_HAVE_ACCESS);
            }
        } else {
            throw new PowerplayException(MOD_MISSED_SECON_ARG);
        }
    }

}
