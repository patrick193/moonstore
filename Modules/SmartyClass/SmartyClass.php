<?php

namespace Modules\SmartyClass;

use PowerPlay\Module;

class SmartyClass extends Module {

    private $smarty;

    public function Prepare() {
        $classReq = 'libs/Smarty.class.php';
        require_once $classReq;

        $this->smarty = new \Smarty();

        $this->smarty->template_dir = \Config::$web . "templates";
        $this->smarty->compile_dir = \Config::$web . "templates_c/";
//        $this->smarty->config_dir = '/web/www.example.com/guestbook/configs/';
        $this->smarty->cache_dir = \Config ::$web . "cache/";
//        $this->smarty->debugging = true;
        $this->smarty->caching = true;
        $this->smarty->cache_lifetime = 120;
    }

    public function Render($page) {
//        var_dump($this->smarty);die;
        $this->smarty->display($page . ".tpl");
    }

    public function getSmarty() {
        return $this->smarty;
    }

    public function setSmarty($variebleName, $smarty) {
        $this->smarty->assign($variebleName, $smarty);
    }

}
