<?php

namespace Modules\Comments\Helpers;

use PowerPlay\Database;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of CommentHelper
 *
 * @author vladyslav-root
 */
class CommentHelper {

    private $db;

    public function __construct() {
        !$this->db ? $this->db = new Database() : '';
    }

    /**
     * Method for adding a new comment
     * @param array $args
     * @return mixed
     * @throws PowerplayException
     */
    public function CommentAdd($args) {
        if (!is_array($args) or ! isset($args['post_id']) or ! isset($args['comment'])) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $postId = $args['post_id'];
        $comment = $args['comment'];
        $ip = $_SERVER['REMOTE_ADDR'];

        if ($this->CheckIp($ip) === true) {
            return $this->db->Insert([['post_id' => $postId, 'comment' => $comment,
                    'date' => date("Y-m-d H:m:s"), 'published' => 1, 'ip' => $ip], 'post_comments']);
        } else {
            throw new PowerplayException('Ip add is blocked', 'user');
        }
    }

    /**
     * Method for unpublish a comment
     * @param int $commentId
     * @return mixed
     * @throws PowerplayException
     */
    public function CommentBlock($commentId) {
        if (!$commentId) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        return $this->db->Update([['published' => 0], 'post_comments', ['comment_id' => $commentId]]);
    }
    

    /**
     * Method for block ip
     * @param string $ip
     * @return mixed
     * @throws PowerplayException
     */
    public function BlockIp($ip) {
        if (!$ip) {
            throw new PowerplayException('Ip was not selected', 'user');
        }
        if ($this->CheckIp($ip) === false) {
            throw new PowerplayException('Ip addr alredy blocked');
        }
        return $this->db->Insert([['ip' => $ip, 'isLocked' => 1], 'ip_block']);
    }

    public function UnBlockIp($ip) {
        if (!$ip) {
            throw new PowerplayException('Ip was not selected', 'user');
        }
        return $this->db->Update([['isLocked' => 0], 'block_ip', ['ip' => $ip]]);
    }

    /**
     * Funciton for check whether ip blocked or not
     * @param string $ip
     * @return boolean
     * @throws PowerplayException
     */
    public function CheckIp($ip) {
        if (!$ip) {
            throw new PowerplayException('Ip was not selected', 'user');
        }
        $dbIp = $this->db->Select([['ip'], 'ip_block', ['ip' => $ip, 'isLocked' => 1]]);
        $dbIp = isset($dbIp) ? : null;

        if (!is_bool($dbIp) and ! is_null($dbIp)) {
            return false;
        }

        return true;
    }

    /**
     * Method for getting all comments for one post
     * @param int $postId
     * @return array
     */
    public function getAllComments($postId) {
        $comments = $this->db->Select(['*', 'post_comments', ['post_id' => $postId, 'published' => 1]]);
        if (is_array($comments)) {
            return $comments;
        }
    }

}
