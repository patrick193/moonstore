<?php

namespace Modules\SocialLikes\Helpers;

use PowerPlay\YamlConfiguration;

/**
 *
 * @author vladyslav-root
 */
class SocialCountHelper {

    private $social;
    private $url;
    private $allowed = array();

    public function __construct() {
        $yaml = new YamlConfiguration();
        $config = $yaml->GetConfigurations(\Bootstrap::getDir() . "SocialLikes/Config/CustomConfig.yml");
        $this->url ? : $this->url = $config->site;
        $this->allowed ? : $this->allowed = $config->allowed_social;
    }

    /**
     * Function for getting tweeter count
     * @return int
     */
    public function getTweeter() {

        $json_string = file_get_contents('http://urls.api.twitter.com/1/urls/count.json?url=' . $this->url);
        $json = json_decode($json_string, true);

        return intval($json['count']);
    }

    /**
     * Function for getting facebook count
     * @return int
     */
    public function getFacebook() {

        $json_string = file_get_contents('http://graph.facebook.com/?ids=' . $this->url);
        $json = json_decode($json_string, true);

        return intval($json[$url]['shares']);
    }

    /**
     * Function for getting google plus count
     * @return int
     */
    public function getGooglePlus() {

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://clients6.google.com/rpc");
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"' . $this->url . '","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        $curl_results = curl_exec($curl);

        curl_close($curl);

        $json = json_decode($curl_results, true);

        return intval($json[0]['result']['metadata']['globalCounts']['count']);
    }

    /**
     * Function for getting count
     * @param string $social
     * @return int
     */
    public function setSocial($social) {
        if (in_array($social, $this->allowed)) {
            $this->social = $social;
            $method = 'get' . $social;
            $count = $this->$method();

            return $count;
        }
    }

}
