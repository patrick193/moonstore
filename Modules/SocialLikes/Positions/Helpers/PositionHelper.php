<?php

namespace Modules\Positions\Helpers;

use PowerPlay\Database;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of PositionHelper
 *
 * @author vladyslav-root
 */
class PositionHelper {

    private $db;

    public function __construct() {

        $this->db ? : $this->db = new Database();
    }

    public function AddPostPosition($postId, $positionId) {
        if (!is_int($postId) or ! is_int($positionId)) {
            throw new PowerplayException('Wrong post id');
        }
        if ($this->PostCheck($postId) === true) {
            $positionPost = $this->db->Insert([['position_id' => $positionId, 'post_id' => $postId],
                'post_position']);
            return $positionId;
        }
    }

    private function PostCheck($postId) {
        if (!is_int($postId)) {
            throw new PowerplayException('Wrong post id');
        }
        $post = $this->db->Select(['*', 'powerplay_posts', ['post_id' => $postId]])[0];
        if (!is_object($post)) {
            return false;
        }
        return true;
    }

}
