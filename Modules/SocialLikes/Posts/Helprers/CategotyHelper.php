<?php

namespace Modules\Posts\Helprers;

use PowerPlay\Database;
use Exception;

/**
 * Description of CategotyHelper
 *
 * @author vladyslav-root
 */
class CategotyHelper {

    private $db;

    public function __construct() {
        !$this->db ? $this->db = new Database() : '';
    }

    /**
     * Method for adding a new category
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    public function AddCategory($args) {
        if (!is_array($args)) {
            throw new Exception('Wrong arguments');
        }
        $categoryName = isset($args['category_name']) ? $args['category_name'] : null;
        $categoryDesc = isset($args['category_desc']) ? $args['category_desc'] : null;
        $categotySeoTitle = isset($args['category_seo_title']) ? $args['category_seo_title'] : null;
        $categotySeoDesc = isset($args['category_seo_desc']) ? $args['category_seo_desc'] : null;

        $category = $this->db->Insert([['category_name' => $categoryName, 'category_desc' => $categoryDesc,
        'category_seo_title' => $categotySeoTitle, 'category_seo_desc' => $categotySeoDesc, 'isLocked' => 1], 'posts_category']);
        return $category;
    }

    /**
     * Method for Edit a categoty
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    public function EditCategory($args) {
        if (!is_array($args) or ! isset($args['category_id'])) {
            throw new Exception('Wrong arguments');
        }
        $categoryName = isset($args['category_name']) ? $args['category_name'] : null;
        $categoryDesc = isset($args['category_desc']) ? $args['category_desc'] : null;
        $categotySeoTitle = isset($args['category_seo_title']) ? $args['category_seo_title'] : null;
        $categotySeoDesc = isset($args['category_seo_desc']) ? $args['category_seo_desc'] : null;
        $categoryId = $args['category_id'];

        $update = $this->db->Update([['category_name' => $categoryName, 'category_desc' => $categoryDesc,
        'category_seo_title' => $categotySeoTitle, 'category_seo_desc' => $categotySeoDesc], 'posts_category',
            ['posts_category_id' => $categoryId]]);

        return $update;
    }
    
    /**
     * Method for block a category
     * @param int $categoryId
     * @return mixed
     * @throws Exception
     */
    public function DeleteCategory($categoryId) {
        if(!$categoryId or $categoryId == 0 or !  is_int($categoryId)){
            throw new Exception('Wrong arguments');
        }
        
        return $this->db->Delete(['posts_category', ['posts_category_id' => $categoryId]]);
    }

}
