<?php

namespace Modules\Posts\Helprers;

use Exception;
use PowerPlay\Database;
use PowerPlay\Session;

/**
 * Description of PostsHelper
 *
 * @author vladyslav-root
 */
class PostsHelper {

    private $db;

    /**
     *
     * @var \Modules\Roles\Users 
     */
    private $user;

    public function __construct() {
        date_default_timezone_set('UTC');
        !$this->db ? $this->db = new Database() : '';
        if (!$this->user) {
            $session = new Session();
            $user = @unserialize($session->get('user_auth'));
            if ($user) {
                $this->user = $user;
            } else {
//                throw new Exception('You dont have this access');
            }
        }
    }

    /**
     * Function to add a new post into the db
     * @param array $args
     * @throws Exception
     */
    public function AddPost($args) {
        if (!is_array($args) or empty($args)) {
            throw new Exception('Wrong parameters Type in Posts Module');
        }
        if ($this->CheckCategory((int) $args['category_id'])) {
            $insert = $this->db->Insert([['post_name' => $args['post_name'], 'post_text' => $args['post_text'],
            'post_category_id' => $args['category_id'], 'created_by' => $this->user->getUserId(),
            'date_created' => date("Y-m-d H:m:s"), 'post_seo_title' => $args['post_seo_title'],
            'post_seo_desc' => $args['post_seo_desc']], 'powerplay_posts']);
        } else {
            throw new Exception('We can not find any category.');
        }
    }

    /**
     * Function for checking category. Is category is exist we return true
     * @param int $categoryId
     * @return boolean
     * @throws Exception
     */
    public function CheckCategory($categoryId) {
        if (!is_int($categoryId)) {
            throw new Exception('Wrong category id. Category Id Should be an integar type.');
        }
        $select = $this->db->Select([['posts_category_id'], 'posts_category', ['posts_category_id' => $categoryId]])[0]->posts_category_id;
        if (is_bool($select) or $select == 0) {
            return false;
        }
        return true;
    }

    /**
     * Function for checking existing post in the database
     * @param int $postId
     * @return boolean
     * @throws Exception
     */
    public function CheckPost($postId) {
        if (!is_int($postId) or $postId == 0) {
            throw new Exception('Wrong post id.');
        }
        $update = $this->db->Select(['COUNT(*)', 'powerplay_posts', ['post_id' => $postId]]);
        if ($update != 0) {
            return true;
        }
        return false;
    }

    /**
     * Function for edit any post
     * @param array $args
     * @throws Exception
     */
    public function EditPost($args) {
        if (!is_array($args) or empty($args)) {
            throw new Exception('Wrong parameters Type in Posts Module');
        }
        if ($this->CheckPost((int) $args['post_id'])) {
            $update = [];
            foreach ($args as $key => $value) {
                $update[$key] = $value;
            }
            $update['date_redaction'] = date("Y-m-d H:m:s");
            $update['redactor_id'] = $this->user->getUserId();
            $u = $this->db->Update([$update, 'powerplay_posts', ['post_id' => $args['post_id']]]);
//            echo 'done';
        } else {
            throw new Exception('Post does not exist');
        }
    }

    /**
     * Function for getting one post from the db
     * @param int $postId
     * @throws Exception
     */
    public function getOnePost($postId) {
        if (!is_int($postId)) {
            throw new Exception('Wrong post id.');
        }
        $select = $this->db->Select(['*', 'powerplay_posts', ['post_id' => $postId,
                        'isLocked' => 0]])[0];
        $this->Render(['varriable' => 'post', 'value' => $select, 'file' => 'Post']);
//        echo $select->post_text;
    }

    /**
     * We dont have a delete function in the system<br>
     * We just block content. This funciton will block a post in the database
     * @param int $postId
     * @throws Exception
     */
    public function DeletePost($postId) {
        if (!is_int($postId) or $postId == 0) {
            throw new Exception('Wrong posts id');
        }
        if ($this->CheckPost($postId)) {
            $this->db->Update([['isLocked' => 1], 'powerplay_posts', ['post_id' => $postId]]);
        }
    }

    /**
     * Function for getting all posts(for example it will be news)
     */
    public function getAll() {
        $posts = $this->db->SelectAllNotLocked('powerplay_posts');
        if (is_array($posts)) {
            foreach ($posts as $post) {
                echo $post->post_name;
                echo '<br>';
            }
        } else {
            throw new Exception('Somthing wrong');
        }
    }

    public function Render($args) {
        $smarty = new \Modules\SmartyClass\SmartyClass();
        $smarty->Prepare();
        $smarty->setSmarty($args['varriable'], $args['value']);
        $smarty->Render($args['file']);
    }

}
