<?php

namespace Modules\Posts;

use PowerPlay\Module;
use Modules\Posts\Helprers\PostsHelper;
use PowerPlay\PowerplayException\PowerplayException;

class Posts extends Module {

    private $helper;

    public function __construct() {
        $this->helper = new PostsHelper();
    }

    public function actionMain() {
        !$this->helper ? $this->helper = new PostsHelper() : '';
        $this->helper->getAll();
    }

    public function actionAddPost($args) {
        if (!is_array($args)) {
            throw new PowerplayException('Wrong arguments type');
        }
        !$this->helper ? $this->helper = new PostsHelper() : '';

        $this->helper->AddPost($args);
    }

    public function actionEditPost($args) {
        if (!is_array($args) or empty($args)) {
            throw new PowerplayException('Wrong arguments in posts system');
        }
        !$this->helper ? $this->helper = new PostsHelper : '';
        $this->helper->EditPost($args);
    }

    public function actionShowPost($args) {
//        if (! isset($args['post_id']) or $args['post_id'] == 0) {
//            throw new PowerplayException('Wrong arguments.');
//        }
        $this->helper->getOnePost((int) $args['post_id']);
    }

    public function actionShowAll() {
        !$this->helper ? $this->helper = new PostsHelper() : '';
        $this->helper->getAll();
    }

    public function actionDeletePost($args) {
        if (!isset($args['post_id']) or empty($args['post_id'])) {
            throw new PowerplayException('Wrong post id');
        }
        !$this->helper ? $this->helper = new PostsHelper() : '';
        $this->helper->DeletePost((int) $args['post_id']);
    }

    public function actionShow() {
        echo '<form action="/posts/edit/" method="post">'
        . 'Type a post id<input type="text" name="post_id"/><br>'
        . 'Type a post name<input type="text" name="post_name"/><br>'
        . 'Type a post text<input type="text" name="post_text"/><br>'
        . 'Type a post Category id<input type="text" name="category_id"/><br>'
        . 'Type a post seo title<input type="text" name="post_seo_title"/><br>'
        . 'Type a post seo desc<input type="text" name="post_seo_desc"/><br>'
        . '<input type="submit" value="Submit">'
        . '</form>';
    }

    public function actionShowAdd() {
        $form = '<form action="/posts/add/" method="post">'
                . 'Type a post name<input type="text" name="post_name"/><br>'
                . 'Type a post text<input type="text" name="post_text"/><br>'
                . 'Type a post Category id<input type="text" name="category_id"/><br>'
                . 'Type a post seo title<input type="text" name="post_seo_title"/><br>'
                . 'Type a post seo desc<input type="text" name="post_seo_desc"/><br>'
                . '<input type="submit" value="Submit">'
                . '</form>';
        echo $form;
//        $this->Render('form', $form);
    }
    public function actionCategoryAddForm() {
        $form = '<form action="/posts/category/add/" method="post">'
                . 'Type a post name<input type="text" name="post_name"/><br>'
                . 'Type a post text<input type="text" name="post_text"/><br>'
                . 'Type a post Category id<input type="text" name="category_id"/><br>'
                . 'Type a post seo desc<input type="text" name="post_seo_desc"/><br>'
                . '<input type="submit" value="Submit">'
                . '</form>';
        echo $form;
//        $this->Render('form', $form);
    }
    public function actionCategoryEditForm() {
        $form = '<form action="/posts/category/edit/" method="post">'
                . 'Type a post name<input type="text" name="post_name"/><br>'
                . 'Type a post name<input type="text" name="post_name"/><br>'
                . 'Type a post text<input type="text" name="post_text"/><br>'
                . 'Type a post Category id<input type="text" name="category_id"/><br>'
                . 'Type a post seo desc<input type="text" name="post_seo_desc"/><br>'
                . '<input type="submit" value="Submit">'
                . '</form>';
        echo $form;
//        $this->Render('form', $form);
    }
    
    public function actionCategoryAdd($args) {
        if(!is_array($args)){
            throw new PowerplayException('Wrong parameters');
        }
        $category = new Helprers\CategotyHelper();
        $category->AddCategory($args);
    }
    
    public function actionCategoryEdit($args) {
        if(!is_array($args)){
            throw new PowerplayException('Wrong arguments');
        }
        $cat = new Helprers\CategotyHelper();
        $cat->EditCategory($args);
    }
    
    public function actionCategoryDelete($args) {
        if(!isset($args['category_id'])){
            throw new PowerplayException('Wrong arguments');
        }
        $cat = new Helprers\CategotyHelper();
        $cat->DeleteCategory((int)$args['category_id']);
    }

    public function Render($var, $value) {
        $smartyClass = new \Modules\SmartyClass\SmartyClass();
        $smartyClass->Prepare();
        $smartyClass->setSmarty($var, $value);
        $smartyClass->Render('index.tpl');
    }

}
