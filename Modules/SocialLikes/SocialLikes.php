<?php

namespace Modules\SocialLikes;

use PowerPlay\Module;
use Modules\SocialLikes\Helpers\SocialCountHelper as SCH;

/**
 * Description of SocialLikes
 *
 * @author vladyslav-root
 */
class SocialLikes extends Module {

    public function actionMain($args){
        if(!is_array($args) or !isset($args['social'])){
            throw new Exception('Wrong arguments');
        }
        $sch = new SCH();
        $sch->setSocial($args['social']);
    }

}
