<?php

namespace Modules\Subscribe\Helpers;

use Modules\Subscribe\Subscribe;

class Helper extends Subscribe
{

    /**
     * email should be already valid
     * @param string $email
     */
    public function subscriberAdd($email)
    {
        return $this->db->Insert([[
        'email' => $email,
        'date'  => date("Y-m-d H:m:s")],
            'powerplay_subscribers']);
        
    }

}
