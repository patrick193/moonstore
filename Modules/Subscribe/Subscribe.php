<?php

namespace Modules\Subscribe;

use Modules\Subscribe\Helpers\Helper;
use PowerPlay\Module;
use PowerPlay\PowerplayException\PowerplayException;

class Subscribe extends Module
{

    public function actionAddSunscriber($args)
    {
        if (!$args['email']) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $helper = new Helper();
        $helper->subscriberAdd((string) $args['email']);
        echo json_encode(['success' => 1]);
    }

}
