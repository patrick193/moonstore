<?php

namespace Modules\UserManagement\Helpers;

use Modules\Roles\Users;
use Modules\Roles\Roles;
use PowerPlay\Session;
use PowerPlay\Database;
use Exception;

/**
 * Description of UserHelper
 *
 * @author Developer Pohorielov Vladyslav
 */
class UserHelper {

    private $users, $user;

    public function __construct() {
        !$this->users ? $this->users = new Users() : '';
    }

    /**
     * Function for adding user in the system
     * @param array $arguments Array of arguments for user<br>
     * Example:<br>
     * ['first_name' => first name, 'last_name' => last name ....]
     * @throws Exception
     */
    public function AddUser($arguments) {
        $this->CheckExc($arguments);
        $newUser = $this->users->Create();
        $newUser->setIsLocked(strip_tags($arguments['islocked']));
        $newUser->setParentId($this->user->getUserId());
        $newUser->setRoleId(2);
        $newUser->setUserEmail(strip_tags(str_replace("%40", "@", $arguments['email'])));
        $newUser->setUserFirstName(strip_tags($arguments['name']));
        $newUser->setUserLastName(strip_tags($arguments['lastname']));
        $newUser->setCellPhone(strip_tags($arguments['cell_phone']));
        ($arguments['password']) ? $newUser->setPassword(strip_tags($arguments['password'])) : '';

        return $this->users->Execute($newUser, (int) $this->user->getUserId());
    }

    /**
     * Function for edit user information in the system
     * @param array $arguments Array of arguments for user<br>
     * Example:<br>
     * ['first_name' => first name, 'last_name' => last name ....]
     * @return boolean
     */
    public function EditUser($arguments) {
        $this->CheckExc($arguments);
        $newUser = $this->users->Create();
        $db = new Database();
        $role = $db->Select(['`role_id`', 'powerplay_users', ['user_id' => strip_tags($arguments['user_id'])]]);

        $roleId = $role[0]->role_id;
        $newUser->setIsLocked(strip_tags($arguments['islocked']));
        $newUser->setParentId($this->user->getUserId());
        $newUser->setRoleId($roleId);
        $newUser->setUserEmail(str_replace("%40", "@", $arguments['email']));
        $newUser->setUserFirstName(strip_tags($arguments['name']));
        $newUser->setUserLastName(strip_tags($arguments['lastname']));
        $newUser->setUserId(strip_tags($arguments['user_id']));
        $newUser->setCellPhone(strip_tags($arguments['cell_phone']));
        ($arguments['password']) ? $newUser->setPassword(strip_tags($arguments['password'])) : '';
//        $newUser->


        return $this->users->Execute($newUser, (int) $this->user->getUserId());
    }

    public function DeleteUser($userId) {

        if ($userId === 0 or ! is_int($userId)) {
            throw new \PowerPlay\PowerplayException\PowerplayException(MOD_USER_ID);
        }
        $this->users->Delete($userId);
    }

    public function ChangeRole($userId) {
        if (!is_int($userId) or $userId == 0) {
            throw new \PowerPlay\PowerplayException\PowerplayException(MOD_USER_ID);
        }
        $db = new Database();
        $role = $db->Select(['role_id', 'powerplay_users', ['user_id' => $userId]])[0];
        $roleId = 1;
        if ($role->role_id == 1) {
            $roleId = 2;
        }
        $db->Update([['role_id' => $roleId], 'powerplay_users', ['user_id' => $userId]]);
    }

    /**
     * Function for auth users under other user
     * @param string $userId
     * @param string $userIdAuth
     * @return boolean|int
     */
    public function AuthUser($userId, $userIdAuth) {
        if ($userId == $userIdAuth or $userId == 0 or $userIdAuth == 0) {
            return false;
        }
        $role = new Roles();
        $authUser = $this->users->Load(['user_id', $userIdAuth]);
        if ($authUser) {
            $rules = $this->users->CheckAction((int) $userId, $role->FindById((int) $authUser->getRoleId())->getRoleCode(), 'auth');
            $parent = $this->users->getParent((int) $userIdAuth);
            if ($rules === true and ( $parent[0]->getUserId() == $userId)) {
                $session = new Session();
                $session->set('user_back_up', $session->get('user_auth'));
                $session->Remove('user_auth');
                $session->set('user_auth', serialize($authUser));
                return 1;
            }
            return 0;
        }
        return -1;
    }

    /**
     * Function to get all children
     * @return array
     * @throws Exception
     */
    public function getAll() {
        !$this->user ? $session = new Session() : '';
        $user = unserialize($session->get('user_auth'));
        if (!is_object($user)) {
            throw new Exception('User did not find');
        }
        $this->user = $user;
        $children = $this->user->getChild((int) $this->user->getUserId());
        return $children;
    }

    /**
     * Private funtion for check all arguments and user
     * @param array $arguments
     * @throws Exception
     */
    private function CheckExc($arguments) {
        if (!is_array($arguments)) {
            throw new Exception('Wrong format of arguments.');
        }
        if (!$this->user) {
            $session = new Session();
            $user = @unserialize($session->get('user_auth'));
            if (!is_object($user)) {
                throw new Exception('User did not find');
            } else {
                $this->user = $user;
            }
        }
    }

}
