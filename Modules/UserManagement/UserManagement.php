<?php

namespace Modules\UserManagement;

use PowerPlay\Module;
use Modules\UserManagement\Helpers\UserHelper;

/**
 * User Managment
 * We should change render and args befor render
 * @author Developer Pohorielov Vladyslav
 */
class UserManagement extends Module {

    public function actionAddUser($args) {
        if (!is_array($args)) {
            throw new \Exception('Wrong type of array.');
        }
        $helper = new UserHelper();
        $helper->AddUser($args);
        $this->actionMain();
    }

    public function actionAdmin($args) {
        if (!isset($args['user_id'])) {
            die;
        }
        $helper = new UserHelper();
        $helper->ChangeRole((int) $args['user_id']);
        $this->Redirect("/admin/profile/usermanagement");
    }

    public function actionDelete($userId) {
        if (!isset($userId['user_id']) or $userId['user_id'] == 0) {
            throw new \PowerPlay\PowerplayException\PowerplayException(MOD_EMPTY);
        } else {
            $userId = $userId['user_id'];
        }

        $h = new UserHelper();
        $h->DeleteUser((int) $userId);
        $this->actionMain();
    }

    public function actionEditUser($args) {
        if (!is_array($args)) {
            throw new \Exception('Wrong type of array.');
        }
        $helprer = new UserHelper();
        $helprer->EditUser($args);
        $this->Redirect("/admin/profile/usermanagement");
    }

    public function actionAuth($args) {
        if (!is_array($args)) {
            throw new \Exception('Wrong type of array.');
        }
        $helprer = new UserHelper();
        $helprer->AuthUser($args['parent_id'], $args['customer_id']);
        $this->Render($args);
    }

    public function actionMain() {
        $helper = new UserHelper();
        $users = $helper->getAll();
        $post = new \Modules\Posts\Helprers\PostsHelper();
        $posts = $post->getLastPosts();     
        $post   = new \Modules\Posts\Helprers\PostsHelper();
        $slider = $post->getSliderSettings();
        $pps = $this->Render('users', $users, 'Admin/users',false);
        $pps    = $this->addVariebles($pps, 'slider', $slider);
        $pps = $this->addVariebles($pps, 'lastpost', $posts);
        $pps->Display('Admin/users');
    }

    /**
     * @param \PowerPlay\PPS\PPS $pps
     * @param string $varName
     * @param mixed $varValue
     */
    public function addVariebles($pps, $varName, $varValue) {

        if (!$pps) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if (is_object($varValue)) {
            $pps->setObject($varName, $varValue);
        } else {
            $pps->setParameters($varName, $varValue);
        }
        return $pps;
    }

}
