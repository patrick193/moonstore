<?php

namespace Modules\system_lang;

use Exception;

class System {
    
    /**
     * Default Language for the core
     * @var string 
     */
    const DEFAULT_LANGUAGE = 'en-GB';

     /**
     *Path to the config file 
     * @var string 
     */
    protected $filePath;

    public function __construct($languageCode) {
        if(!$languageCode) {
            $languageCode = self::DEFAULT_LANGUAGE;
        }
        $this->filePath = \Config::$langDir . $languageCode . "/" . $languageCode . ".system.ini";
    }

    /**
     * Function fot parse ini config and send this data to the function for defining
     * @return boolean
     */
    public function UseLanguage() {

        if(file_exists($this->filePath)) {
            $language = parse_ini_file($this->filePath);
            $this->DefineLanguage($language);
            return true;
        }
        return false;
    }

    /**
     * Function for defining all data from ini config
     * @param array $parsedIni
     * @return boolean
     * @throws Exception
     */
    protected function DefineLanguage($parsedIni) {
        if(!is_array($parsedIni)) {
            throw new Exception('Parsed ini should be an array');
        }
        foreach($parsedIni as $key => $value) {
            define($key, $value);
        }
        return true;
    }

}
