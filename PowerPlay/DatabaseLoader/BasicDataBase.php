<?php

namespace PowerPlay\DatabaseLoader;

use PowerPlay\String\SQLFilter;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of DataBase
 *
 * @author vpohorielov
 */
class BasicDataBase
{

    private $db;
    private $user, $password, $host, $database, $execute;
    private $filter;

    public function setExecute($execute)
    {
        $this->execute = $execute;
        return $this;
    }

    public function getDb()
    {
        return $this->db;
    }

    /**
     * Get all configurations to the connection
     */
    public function __construct($host = '', $user = '', $password = '', $database = '', $execute = true)
    {
        if (!empty($host) and ! empty($user) and ! empty($database)) {
            $this->user     = $user;
            $this->password = $password;
            $this->host     = $host;
            $this->database = $database;
            $this->execute  = $execute;
        } else {
            $this->user     = \Config::$user;
            $this->password = \Config::$password;
            $this->host     = \Config::$host;
            $this->database = \Config::$db;
            $this->execute  = \Config::$execute;
        }
        $this->filter = new SQLFilter();
    }

    /**
     * Connection to the DataBase
     * @return mysqli
     * @throws Exception
     */
    public function Connect()
    {

        if (!is_null($this->user) and ! is_null($this->host) and ! is_null($this->database)) {
            if (!$this->Ping()) {
                $this->db = new \mysqli($this->host, $this->user, $this->password, $this->database) or die("No connection to the database. Please check your DataBase Configurations.");
                $this->db->set_charset('utf8');
            }
            return $this->db;
        } else {
            throw new PowerplayException('Please fill out all configurations for the database connection');
        }
        return $this->db;
    }

    /**
     * Alias for the function Connect()
     * @return type
     */
    public function Open()
    {
        return $this->Connect();
    }

    /**
     * Alias for the function Connect()
     * @return type
     */
    public function Start()
    {
        return $this->Connect();
    }

    /**
     * Function to check whether open  or not connection to database
     * @return boolean
     */
    public function Ping()
    {
        if (isset($this->db)) {
            return mysqli_ping($this->db);
        } else {
            return false;
        }
    }

    /**
     * Function for closing connection if it open
     * @return boolean
     */
    public function Close()
    {
        if ($this->db or $this->Ping()) {
            $this->db->close();
            unset($this->db);
            return true;
        }
        return false;
    }

    /**
     * Alias for the function Close()
     * @return boolean
     */
    public function Disconnect()
    {
        return $this->Close();
    }

    /**
     * Function for creating a query string from array 
     * Posibility:
     * @param array $parameters How to use:<br> $select = ['*', 'tablename', 'where', 'order by'];<br>
     * $select = [['field1', 'field2'], 'tablename', ['where_field' => 'value']]; <br>
     * $select = ['string', 'tablename', ['where_field' => 'value']];
     * @return mixed If in configuration file execute config is true the function should start execute function after query generate.<br>
     * And that is why we can get an array of stdClass objects.<br>
     * If execute config is false, the function will return <i>strng</i> type(<b>query</b>)
     * @throws Exception The function throw Exception if we send a wrong parameters
     */
    public function Select(array $parameters)
    {
        if (count($parameters) > 4) {
            throw new PowerplayException('Wrong structure');
        }
        !$this->Ping() ? $this->Open() : '';
        $select = '';
        $where  = '';
        if ($this->CheckArray($parameters[0])) {
            for ($i = 0, $count = count($parameters[0]); $i < $count; $i++) {
                $select .= "`" . $parameters[0][$i] . "`";
                $i == ($count - 1) ? : $select .= ",";
            }
            unset($i, $count);
        } else {
            $select = $parameters[0];
        }
        $where = $this->Where(isset($parameters[2]) ? $parameters[2] : null);

        $query = "SELECT $select FROM `$parameters[1]` WHERE $where";
        if (isset($parameters[3])) {
            $query .= $this->OrderBy('', $parameters[3]);
        }
        $this->execute ? $result = $this->Execute($query) : $result = $query;
        return $result;
    }

    /**
     * Function to order by 
     * @param strng $parameters
     * @return string
     * @throws Exception
     */
    public function OrderBy($query, $parameters)
    {
        !$this->Ping() ? $this->Open() : '';
        $allowed = ['DESC',
            'ASC'];
        if (is_null($parameters)) {
            throw new PowerplayException('Wrong ORDER type: ' . $parameters[1]);
        }
        if (is_array($parameters)) {
            $ordered = " ORDER BY `" . $parameters[0] . "` " . $parameters[1];
            if (!is_null($query)) {
                $ordered = $query . $ordered;
            }
        } else {
            $ordered = " ORDER BY $parameters";
            $ordered = $query . $ordered;
        }
        if ($this->execute and ! empty($query)) {
            $result = $this->Execute($ordered);
        } else {
            $result = $ordered;
        }
        return $result;
    }

    /**
     * Update any record in the DataBase
     * @param mixed $parameters How to use: <br> $update = [['field' => 'value'],'table_name',  'where'];<br>
     * $update = [['field' => 'value'],'table_name',  ['where' => 'value']];<br>
     * $update = [ 'set values','table_name', 'where'];
     * @return mixed If in configuration file execute config is true the function should start execute function after query generate.<br>
     * And that is why we can get an array of stdClass objects.<br>
     * If execute config is false, the function will return <i>strng</i> type(<b>query</b>)
     * @throws Exception The function throw Exception if we send a wrong parameters
     */
    public function Update($parameters)
    {
        if (count($parameters) > 3) {
            throw new PowerplayException('Wrong structure of parameters');
        }
        !$this->Ping() ? $this->Open() : '';
        $set   = "";
        is_null($parameters[2]) ? $where = '1' : $where = '';

        if ($this->CheckArray($parameters[0])) {
            for ($i = 1, $c = count($parameters[0]); $element = each($parameters[0]); $i++) {
                $set .= "`" . $element['key'] . "`='" . $element['value'] . "' ";
                $i <= ($c - 1) ? $set.=',' : ' ';
            }
            unset($element, $i, $c);
        } else {
            $set = $parameters[0];
        }
        if ($this->CheckArray($parameters[2])) {
            $where = $this->Where($parameters[2]);
        } else {
            $where = $parameters[2];
        }
        $query  = "UPDATE `" . $parameters[1] . "` SET " . $set . "  WHERE $where";
        $this->execute ? $result = $this->Execute($query) : $result = $query;

        return $result;
    }

    /**
     * Private function to add a where parameter to query
     * @param type $parameters
     * @return string
     */
    private function Where($parameters)
    {
        if (is_array($parameters)) {
            $where = '';
            for ($j = 1, $count = count($parameters); $element = each($parameters); $j++) {
                $where .= "`" . $element['key'] . "` = '" . $element['value'] . "'";
                $j <= ($count - 1) ? $where .= " AND " : '';
            }
            unset($j, $count, $element);
            return $where;
        } elseif (is_string($parameters) and $parameters) {
            return $parameters;
        }
        return '1';
    }

    /**
     * Function for insert a new record into table
     * @param mixed $parameters How to use: <br/>$insert = [['field' => 'value'], 'table_name'];
     * @return mixed If in configuration file execute config is true the function should start execute function after query generate.<br>
     * And that is why we can get an array of stdClass objects.<br>
     * If execute config is false, the function will return <i>strng</i> type(<b>query</b>)
     * @throws Exception The function throw Exception if we send a wrong parameters
     */
    public function Insert($parameters)
    {
        if (count($parameters) > 2) {
            throw new PowerplayException('Wrong structure of parameters');
        }
        !$this->Ping() ? $this->Open() : '';
        $values = "";
        $fields = "";
        if ($this->CheckArray($parameters[0])) {

            for ($i = 0, $c = count($parameters[0]); $element = each($parameters[0]); $i++) {
                $fields .= "`" . $element['key'] . "`";
                $values .= "'" . $element['value'] . "'";
                if (!($i >= ($c - 1))) {
                    $fields .= ",";
                    $values .= ",";
                }
            }
            unset($element, $i, $c);
        }
        $query  = "INSERT INTO `" . $parameters[1] . "`($fields) VALUES ($values)";
        $this->execute ? $result = $this->Execute($query) : $result = $query;

        return $result;
    }

    /**
     * Function for delete the record(not delete, instead delete we will just lock a record)
     * @param mixed $parameters How to use: <br>$delete = ['table_name', [where=>value]];<br>
     * $delete = ['table_name', 'where'];
     * @return mixed If in configuration file execute config is true the function should start execute function after query generate.<br>
     * And that is why we can get an array of stdClass objects.<br>
     * If execute config is false, the function will return <i>strng</i> type(<b>query</b>)
     */
    public function Delete($parameters)
    {
        !$this->Ping() ? $this->Open() : '';
        $q = [[
        'isLocked' => 1],
            $parameters[0],
            $parameters[1]];
        return $this->Update($q);
    }

    /**
     * Alias for Delete()
     * @param mixed $parameters How to use: <br>$lock = ['table_name', [where=>value]];<br>
     * $lock = ['table_name', 'where'];
     * @return mixed If in configuration file execute config is true the function should start execute function after query generate.<br>
     * And that is why we can get an array of stdClass objects.<br>
     * If execute config is false, the function will return <i>strng</i> type(<b>query</b>)
     */
    public function Lock($parameters)
    {
        return $this->Delete($parameters);
    }

    /**
     * Join documentation
     * Function for JOIN couple tables in one
     * @param mixed $parameters This is an array:<br>[['table1', 'table2'...], 'join_type', [Conditions ON], 'using filed']
     * <p>If you want to use couple conditions ON just do it with array: <br>[['table1', 'table2'...], 'join_type', [['field1' => 'value'],
     *  ['field2' => 'value']...], 'using field']
     * @return mixed If in configuration file execute config is true the function should start execute function after query generate.<br>
     * And that is why we can get an array of stdClass objects.<br>
     * If execute config is false, the function will return <i>strng</i> type(<b>query</b>)
     * @throws Exception The function throw Exception if we send a wrong parameters
     */
    public function Join($parameters)
    {
        if (count($parameters) > 4 or count($parameters) < 3) {
            throw new PowerplayException('Wrong parameters');
        }
        if (!$this->AllowedJoin($parameters[1])) {
            throw new PowerplayException('Wrong JOIN type: ' . $parameters[1]);
        }
        !$this->Ping() ? $this->Open() : '';
        $query       = "SELECT * FROM `" . $parameters[0][0] . "` ";
        $tablesCount = count($parameters[0]);
        for ($i = 0; $i < $tablesCount; $i++) {
            if ($i < ($tablesCount - 1)) {
                $query .= $parameters[1] . " JOIN " . $parameters[0][$i + 1] . " ";
                $this->CheckArray($parameters[2]) ? $query .= " ON " . ($this->CheckArray($parameters[2][$i]) ? $this->JoinOn($parameters[2][$i]) : $this->JoinOn($parameters[2])) . " " : '';
                !is_null($parameters[3]) ? $query .= $this->JoinUse($parameters[3]) : '';
            }
        }
        $this->execute ? $result = $this->Execute($query) : $result = $query;
        return $result;
    }

    /**
     * Check a type of join
     * @param string $joinType 
     * @return boolean
     */
    private function AllowedJoin($joinType)
    {
        $allowed = ['LEFT',
            'RIGHT',
            'OUTER',
            'INNER',
            'LEFT OUTER',
            'RIGHT OUTER'];
        if (in_array(strtoupper($joinType), $allowed)) {
            return true;
        }
        return false;
    }

    /**
     * Private function for JOIN USING condition
     * @param mixed $parameter
     * @return string
     */
    private function JoinUse($parameter)
    {
        return " using(`$parameter`) ";
    }

    /**
     * Private function for JOIN ON conditions
     * @param mixed $parameters
     * @return string
     */
    private function JoinOn($parameters)
    {
        $condition = "";
        foreach ($parameters as $key => $value) {
            $condition .= $key . " = " . $value;
        }
        return $condition;
    }

    public function ResetConnection()
    {
        if ($this->Ping()) {
            $this->Disconnect();
            $this->Connect();
        } else {
            $this->Start();
        }
    }

    /**
     * Function to get a count of fields;
     */
    public function Count($fields, $tableName, $whereConditional)
    {
        if (!$tableName) {
            throw new PowerplayException('Select a table in the database');
        }
        !$this->Ping() ? $this->Open() : '';

        $what = '';
        if (is_array($fields)) {
            foreach ($fields as $field) {
                $what .= $field . ", ";
            }
            $what = rtrim($what, ",");
        } else {
            $what = $fields;
        }
        $where = '';
        if (is_array($whereConditional)) {
            foreach ($whereConditional as $key => $conditional) {
                $where .= "`" . $key . "` = '" . $conditional . "' ; ";
            }
            $where = str_replace(";", " AND ", rtrim($where, "; "));
        } else {
            !is_null($whereConditional) ? $where = $whereConditional : $where = "1";
        }
        $q = "SELECT COUNT($what) FROM `$tableName` WHERE $where";

        if ($this->execute) {
            $res    = $this->Execute($q);
            $key    = key($res[0]);
            $result = $res[0]->$key;
        } else {
            $result = $q;
        }
        return $result;
    }

    /**
     * Function to geet a max value in the table in the database
     * @param string $field
     * @param string $table
     * @param mixed $conditional
     * @return boolean
     * @throws PowerplayException
     */
    public function SelectMax($field, $table, $conditional = null)
    {
        if (!$field or ! $table) {
            throw new PowerplayException('We need a couple parameters');
        }
        $where = '';
        if (is_array($conditional)) {
            foreach ($conditional as $key => $value) {
                $where .= $key . " = " . $value . "; ";
            }
            $where = str_replace(";", " AND ", rtrim($where, "; "));
        } else {
            $where = $conditional;
        }
        $max = $this->Select(['MAX(`' . $field . '`)',
            $table,
            $where]);

        if ($this->execute and $max) {
            $key = key($max[0]);
            return $max[0]->$key;
        }
        return $max;
    }

    /**
     * Function to get all params between $before and $after
     * @param string $query
     * @param string $after
     * @param string $before
     * @return int
     * @throws PowerplayException
     */
    public function getParamsInQuery($query, $after = 'SELECT', $before = 'FROM')
    {
        if (is_null($query) or ! is_string($query)) {
            throw new PowerplayException('Query can not be null');
        }
        $beforeQ = explode($before, $query)[0];
        $afterQ  = explode($after, $beforeQ)[1];

        if (strpos($query, $afterQ) === false) {
            return 0;
        }

        return $afterQ;
    }

    /**
     * Function to replace all parameters in the query
     * @param string $query
     * @param string $oldParam
     * @param string $newParam
     * @return int
     * @throws PowerplayException
     */
    public function setNewParams($query, $oldParam, $newParam)
    {
        if (is_null($query) or ! is_string($query)) {
            throw new PowerplayException("Query can not be empty");
        }
        if (strpos($query, $oldParam) === false) {
            return 0;
        }
        $newQuery = str_replace($oldParam, " $newParam ", $query);

        $this->execute ? $result = $this->Execute($newQuery) : $result = $newQuery;
        return $result;
    }

    /**
     * Function to get all records from any table. It is a short <b>Select()</b> function
     * @param string $table Tablename where data keep in.
     * @return mixed If in configuration file execute config is true the function should start execute function after query generate.<br>
     * And that is why we can get an array of stdClass objects.<br>
     * If execute config is false, the function will return <i>strng</i> type(<b>query</b>)
     */
    public function SelectAll($table)
    {
        return $this->Select(['*',
                    $table]);
    }

    /**
     * Alias to the SelectAll() function
     * @param string $table A table in the DataBase which you want to use
     * @return mixed If in configuration file execute config is true the function should start execute function after query generate.<br>
     * And that is why we can get an array of stdClass objects.<br>
     * If execute config is false, the function will return <i>strng</i> type(<b>query</b>)
     */
    public function GetAllRecords($table)
    {
        return $this->SelectAll($table);
    }

    /**
     * Function to get all not locked records from any table. It is a short <b>Select()</b> function
     * @param string $table Tablename where data keep in.
     * @return mixed If in configuration file execute config is true the function should start execute function after query generate.<br>
     * And that is why we can get an array of stdClass objects.<br>
     * If execute config is false, the function will return <i>strng</i> type(<b>query</b>)
     */
    public function SelectAllNotLocked($table)
    {
        return $this->Select(['*',
                    $table,
                    ['isLocked' => 0]]);
    }

    /**
     * Alias to the SelectAllNotLocked() function.
     * @param string $table A table in the DataBase
     * @return type
     */
    public function GetAllNotLocked($table)
    {
        return $this->SelectAllNotLocked($table);
    }

    /**
     * Return the last insert id
     * @return intager The last autoincrement Id after the last insert query.
     */
    public function InsertId()
    {
        return $this->db->insert_id;
    }

    /**
     * Alias for InsertId()
     * @return intager The last autoincrement Id after the last insert query.
     */
    public function LastInsert()
    {
        return $this->InsertId();
    }

    /**
     * Check is a varieble array or not
     * @param mixed $varieble Is varieble an array or not<br>
     * Just call this function with our varieble
     * @return boolean
     */
    private function CheckArray($varieble)
    {
        return is_array($varieble);
    }

    /**
     * Fnction for execute query
     * @param string $query A final varian of query.
     * @return boolean If the query does not return any result(for example: insert, delete, update etc.) this function will return true.
     * @return object stdClass If the query like SELECT the function will return an array of objects(stdClass).
     */
    public function Execute($query, $object = \stdClass::class)
    {
        if (!is_null($this->db)) {
            try {
                !$this->Ping() ? $this->Connect() : '';
                $query = $this->filter->Filter($query, $this->db);

                $result = $this->db->query($query);

                $data = $this->QueryResult($result, $query, $object);

                return $data;
            } catch (Exception $e) {
                $this->setExecute(\Config::$execute);
                echo 'You have an exception: ' . $e->getMessage();
                exit();
            }
        }
    }

    /**
     * Private function for find returning parameters
     * @param \mysqli $queryResult
     * @param object $object
     * @return boolean
     * @throws PowerplayException
     */
    private function QueryResult($queryResult, $query, $object = \stdClass::class)
    {
        /** if we have an error in our query */
        if ($queryResult === false) {
            throw new PowerplayException($this->db->error . " in " . $query);
        }

        /** if we have any record in the db we will return an object or array of objects */
        if ($queryResult->num_rows > 0) {
            $obj = array();
            while ($row = $queryResult->fetch_object($object)) {
                $obj[] = $row;
            }
            $this->setExecute(\Config::$execute);
            return $obj;
        } else {
            $this->setExecute(\Config::$execute);
            $isInsert = $this->isInsert($query);
            /** if we have query without returning results like insert or update */
            if ($isInsert === true) {
                return $isInsert;
            } elseif (!$this->db->error) {
                return true;
            } else {
                throw new PowerplayException($this->db->error . " in " . $query);
            }
        }
    }

    /**
     * Functyion to detect a count in the query.
     * @param string $query
     * @return boolean
     * @throws PowerplayException
     */
    private function isCount($query)
    {
        if (empty($query) or is_null($query)) {
            throw new PowerplayException('Query can not be empty. ');
        }

        if (strpos(strtoupper($query), "COUNT") !== false) {
            return true;
        }
        return false;
    }

    /**
     * Function to detect an insert query and return a last inserted id
     * @param string $query
     * @return boolean
     * @throws PowerplayException
     */
    public function isInsert($query)
    {
        if (empty($query) or is_null($query)) {
            throw new PowerplayException('Query can not be empty. ');
        }
        if (strpos(strtoupper($query), "INSERT") !== false) {
            return $this->InsertId();
        }
        return false;
    }

    /**
     * We have to close a connection
     */
    public function __destruct()
    {
        if ($this->Ping()) {
            $this->Close();
        }
    }

}
