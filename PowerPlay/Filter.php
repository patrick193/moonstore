<?php

namespace PowerPlay;

use PowerPlay\String\Bridge;

/**
 * Description of Filter
 *
 * @author Developer Pohorielov Vladyslav
 */
class Filter {

    private $filter;
    
    public function __construct($type = 'url'){
        $bridge = new Bridge();
        $this->filter = $bridge->Type($type);
    }
    
    public function ToFilter($string){
        return $this->filter->Filter($string);
    }
}
