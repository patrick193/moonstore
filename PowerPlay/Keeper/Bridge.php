<?php

namespace PowerPlay\Keeper;

use PowerPlay\Keeper\FileStorage;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * A bridge between session(keeper) and whole system
 *
 * @author Developer Pohorielov Vladyslav
 */
class Bridge extends FileStorage {

    private $storage;

    public function __construct($part = 'register') {
        parent::__construct();
        if (is_null($this->storage) or empty($this->storage)) {
            $this->storage = $this->getRegister();
        }
    }

    /**
     * Function to get any data from session
     * 
     * <p style="color: red">NOT USE!!! <br> Alternative for this function is getValue</p>
     * @param string $key
     * @return object
     */
    public function get($key, $part = 'register') {

        if ($this->storage[$part][$key]) {
            $obj = json_decode(base64_decode($this->storage[$part][$key]));
            return $obj;
        }
        
        return false;
    }

    /**
     * Function to add some data in the storage
     * @param string $key
     * @param object $object
     */
    public function set($key, $object, $part = 'register') {
        $this->setRegister($key, $object, $part);
        unset($this->storage);
        $this->storage = $this->getRegister();
    }

    /**
     * Get all session
     * @return type
     */
    public function getAllModules() {

        if (in_array("register", $this->storage)) {
            $dir = \Bootstrap::getDir() . "Serialize/";
            $dirFiles = array();
            if (is_dir($dir . "register")) {
                $dirOpen = opendir($dir . "register");
                $dirFiles = array();
                while (($file = readdir($dirOpen))) {
                    strpos($file, ".") === false ? $dirFiles['register'][] = $file : '';
                }
            }
            return $dirFiles['register'];
        }
    }

    public function getValue($key, $part = 'register') {
        if (!is_string($part) and ! is_string($key)) {
            throw new PowerplayException('Wrong format of data');
        }
        $file = \Bootstrap::getDir() . "Serialize/" . $part . "/" . $key;
        if (file_exists($file)) {
            $fp = fopen($file, 'r');
            $str = fread($fp, filesize($file));
            $deserialize = unserialize(base64_decode($str));
            if (is_object($deserialize)) {
                return $deserialize;
            } else {
                throw new PowerplayException('We can not deserialize an object.');
            }
        }
    }

    /**
     * Destroy all session<br>
     * <p style="color: red">ONLY FOR DEVELOPMENT</p>
     */
    public function UnsetStorage() {
        $this->Destroy();
        unset($this->storage);
    }

    /**
     * Remove some data from the session
     * @param string $key
     */
    public function Remove($key) {
        $this->Delete($key);
    }

}
