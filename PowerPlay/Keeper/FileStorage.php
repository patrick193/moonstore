<?php

namespace PowerPlay\Keeper;

/**
 * Description of Session
 *
 * @author Developer Pohorielov Vladyslav
 */
class FileStorage {

    private $register = array();

    /**
     * Start the session if it did not
     */
    protected function __construct() {
    }

    /**
     * Function to get register
     * @return type
     */
    protected function getRegister() {
        if (is_null($this->register) or empty($this->register) and $this->Exist()) {
            $this->getStorage();
        }
        return $this->register;
    }

    /**
     * Function to set up some data in the storage
     * @param string $key
     * @param object $object
     */
    protected function setRegister($key, $object, $part) {
        $register[$key] = $object;
        $this->Save($register, $part);
    }

    /**
     * Save all data from storage in session
     */
    protected function Save($register, $part = 'register') {
        if (!is_null($register) and ! empty($register)) {
            foreach ($register as $key => $value) {
                $serialize = serialize($value);
                $dir = \Bootstrap::getDir() . "Serialize/" . $part;
                if (!is_dir($dir)) {
                    mkdir($dir);
                }
                touch($dir . "/" . $key);
                $fp = fopen($dir . "/" . $key, 'w');
                fwrite($fp, base64_encode($serialize));
                fclose($fp);
            }
        }
        unset($this->register);
        $this->getStorage();
    }

    /**
     * Function for getting all data
     */
    protected function getStorage() {
        $dir = \Bootstrap::getDir() . "Serialize/";
        $open = opendir($dir);
        $parts = array();
        while ($part = readdir($open)) {
            if (strpos($part, ".") === false) {
                array_push($parts, trim($part));
            }
        }

        if (is_array($parts) and ! empty($parts) and ! is_null($parts)) {
            $this->register = $parts;
            return $parts;
        }
        return false;
    }

    /**
     * Function for checking exist or not our keeper(register) in the session
     * @return boolean
     */
    protected function Exist($part = 'register') {
        if (is_dir(\Bootstrap::getDir() . 'Serialize/' . $part)) {
            return true;
        }
        return false;
    }

    /**
     * Function to destroy all session<br>
     * <p style="color: red"> Only for development<p>
     */
    protected function Destroy($part = 'register') {
        $directory = \Bootstrap::getDir() . 'Serialize/' . $part;
        $dir = opendir($directory);
        while (($file = readdir($dir))) {
            if (is_file($directory . "/" . $file)) {
                unlink($directory . "/" . $file);
            } else if (is_dir($directory . "/" . $file) && ( $file != ".") && ($file != "..")) {
                full_del_dir($directory . "/" . $file);
            }
        }
        closedir($dir);
        rmdir($directory);
    }

    /**
     * Delete some key from session
     * @param string $key
     */
    protected function Delete($key, $part = 'register') {
        $file = \Bootstrap::getDir() . "Serialize/" . $part . "/" . $key;
        if (file_exists($file)) {
            unlink($file);
            return true;
        }
        return false;
    }

}
