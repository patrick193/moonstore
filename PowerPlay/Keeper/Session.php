<?php

namespace PowerPlay\Keeper;

/**
 * 
 * Description of Memento
 * @author Developer Pohorielov Vladyslav
 */
class Session {

    private $register = array();

    /**
     * Start the session if it did not
     */
    protected function __construct() {
        if (session_status() != PHP_SESSION_ACTIVE) {
//            ini_set('session.save_path', $_SERVER['DOCUMENT_ROOT'] . '/../sessions/');
            session_start();
        }
    }

    /**
     * Function to get register
     * @return type
     */
    protected function getRegister() {
        if (is_null($this->register) or empty($this->register) and $this->Exist()) {
            $this->getSession();
        }
        return $this->register;
    }

    /**
     * Function to set up some data in the storage
     * @param string $key
     * @param object $object
     */
    protected function setRegister($key, $object, $part) {
        $register[$key] = $object;
        $this->register[$part][$key] = base64_encode(($object));
        $this->Save($register, $part);
    }

    /**
     * Save all data from storage in session
     */
    protected function Save($register, $part = 'register') {
        if (!is_null($register) and ! empty($register)) {
            foreach ($register as $key => $value) {
                $serialize = $value;
                $_SESSION[$part][$key] = base64_encode($serialize);
            }
        }
    }

    /**
     * Functio for getting all data in the session
     */
    protected function getSession() {
        $this->register = $_SESSION;
    }

    /**
     * Function for checking exist or not our keeper(register) in the session
     * @return boolean
     */
    protected function Exist($part = 'register') {
        if ($_SESSION[$part]) {
            return true;
        }
        return false;
    }

    /**
     * Function to destroy all session<br>
     * <p style="color: red"> Only for development<p>
     */
    protected function Destroy($part = 'register') {
        unset($_SESSION[$part]);
        unset($this->register);
    }

    /**
     * Delete some key from session
     * @param string $key
     */
    protected function Delete($key, $part = 'register') {
        unset($_SESSION[$part][$key]);
    }

}
