<?php

namespace PowerPlay\Mailer\Helpers;

use PowerPlay\YamlConfiguration;
use Exception;

class MailerHelper {

    /**
     * 
     * @var string $smtp_username - login
     * @var string $smtp_password - pass
     * @var string $smtp_host - host
     * @var string $smtp_from - sender
     * @var integer $smtp_port - port
     * @var string $smtp_charset - charset
     *
     */
    public $smtp_username;
    public $smtp_password;
    public $smtp_host;
    public $smtp_from;
    public $smtp_port;
    public $smtp_charset;

    public function __construct($smtp_charset = "utf-8") {

        $yaml = new YamlConfiguration();

        $config = $yaml->GetConfigurations(__DIR__ . "/../Config/MailerCustomConfig.yml");

        if($config) {
            $this->smtp_username = $config->Username;
            $this->smtp_password = $config->Password;
            $this->smtp_host = $config->Host;
            $this->smtp_from = $config->From;
            $this->smtp_port = $config->Port;
            $this->smtp_charset = $smtp_charset;
        }else{
            throw new Exception('We can not connect to the server');
        }
    }

    /**
     * Отправка письма
     * 
     * @param string $mailTo - получатель письма
     * @param string $subject - тема письма
     * @param string $message - тело письма
     * @param string $headers - заголовки письма
     *
     * @return bool|string В случаи отправки вернет true, иначе текст ошибки    *
     */
    public function send($mailTo, $subject, $message, $headers) {
        $contentMail = "Date: " . date("D, d M Y H:i:s") . " UT\r\n";
        $contentMail .= 'Subject: =?' . $this->smtp_charset . '?B?' . base64_encode($subject) . "=?=\r\n";
        $contentMail .= $headers . "\r\n";
        $contentMail .= $message . "\r\n";

        try {
            if(!$socket = @fsockopen($this->smtp_host, $this->smtp_port, $errorNumber, $errorDescription, 30)) {
                throw new Exception($errorNumber . "." . $errorDescription);
            }
            if(!$this->_parseServer($socket, "220")) {
                throw new Exception('Connection error');
            }

            $server_name = $_SERVER["SERVER_NAME"];
            fputs($socket, "HELO $server_name\r\n");
            if(!$this->_parseServer($socket, "250")) {
                fclose($socket);
                throw new Exception('Error of command sending: HELO');
            }

            fputs($socket, "AUTH LOGIN\r\n");
            if(!$this->_parseServer($socket, "334")) {
                fclose($socket);
                throw new Exception('Autorization error');
            }



            fputs($socket, base64_encode($this->smtp_username) . "\r\n");
            if(!$this->_parseServer($socket, "334")) {
                fclose($socket);
                throw new Exception('Autorization error');
            }

            fputs($socket, base64_encode($this->smtp_password) . "\r\n");
            if(!$this->_parseServer($socket, "235")) {
                fclose($socket);
                throw new Exception('Autorization error');
            }

            fputs($socket, "MAIL FROM: <" . $this->smtp_username . ">\r\n");
            if(!$this->_parseServer($socket, "250")) {
                fclose($socket);
                throw new Exception('Error of command sending: MAIL FROM');
            }

            $mailTo = ltrim($mailTo, '<');
            $mailTo = rtrim($mailTo, '>');
            fputs($socket, "RCPT TO: <" . $mailTo . ">\r\n");
            if(!$this->_parseServer($socket, "250")) {
                fclose($socket);
                throw new Exception('Error of command sending: RCPT TO');
            }

            fputs($socket, "DATA\r\n");
            if(!$this->_parseServer($socket, "354")) {
                fclose($socket);
                throw new Exception('Error of command sending: DATA');
            }

            fputs($socket, $contentMail . "\r\n.\r\n");
            if(!$this->_parseServer($socket, "250")) {
                fclose($socket);
                throw new Exception("E-mail didn't sent");
            }

            fputs($socket, "QUIT\r\n");
            fclose($socket);
        } catch(Exception $e) {
            return $e->getMessage();
        }
        return true;
    }

    private function _parseServer($socket, $response) {
        while(@substr($responseServer, 3, 1) != ' ') {
            if(!($responseServer = fgets($socket, 256))) {
                return false;
            }
        }
        if(!(substr($responseServer, 0, 3) == $response)) {
            return false;
        }
        return true;
    }

}
