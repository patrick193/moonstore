<?php

namespace PowerPlay\Mailer;

use PowerPlay\Module;
use PowerPlay\Mailer\Helpers\MailerHelper;

/**
 * Description of Mailer
 *
 * @author Developer Pohorielov Vladyslav
 */
class Mailer extends Module {

    private $config;

    public function getConfig() {
        if(!$this->config) {
            $helper = new MailerHelper();
            $this->config = $helper->getConfig();
        }
        return $this->config;
    }

    public function Send($args) {
        if(!is_array($args)) {
            throw new Exception('Wrong data');
        }
        try {
            $mailSMTP = new MailerHelper();
            $result = $mailSMTP->send($args['to'], $args['subject'], $args['message'], $args['header']);

            if($result !== true) {
                throw new \Exception($result);
            }
        } catch(\Exception $ex){
            
            print_r("We have an exception with the following text: " . $args['message'] . "<br>But we we can not send this exception to developers"
                    . " because "
                    . "this exception have thrown when we tryed to send the exception. Please, inform us about this message."
                    . "<p style='color: red'>Exception is: ".$ex->getMessage() . "</p> in " . $ex->getFile() . " on " . $ex->getLine());
            exit;
            
        }
    }

}
