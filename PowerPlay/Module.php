<?php

namespace PowerPlay;

use PowerPlay\ModuleLoader\ModuleLoader as ML;
use PowerPlay\YamlConfiguration;
use PowerPlay\Storage;
use PowerPlay\Session;
use PowerPlay\PPS\PPS;

/**
 *
 * @author Pohorielov Vladyslav
 */
class Module
{

    private $modules;
    
    /**
     *
     * @var Storage
     */
    protected $storage;
    private $loader;

    /**
     *
     * @var Database 
     */
    protected $db;

    /**
     *
     * @var Session 
     */
    protected $session;

    public function __construct()
    {
        $this->loader = ML::getBasicModuleObject();
        !$this->session ? $this->session = new Session() : '';
        !$this->db ? $this->db = new Database() : '';
        !$this->storage ? $this->storage = new Storage() : '';

    }

    /**
     * Function for getting all activated modules(get a singleton object)
     * @return array
     */
    public function getBasicModuleObject()
    {
        $this->storage = new Storage();
        if ($this->storage->getAllModules()) { // first of all we should check our session data
            $this->CheckUpdates(); // than check if we have some updates
            $this->modules = $this->storage->getAllModules();
            ML::setActivatedModules($this->modules);
        } else {
            $this->modules = $this->loader->LoadModules(); // if we dont have any modules in the session we will get from main it class
            $modules = $this->modules;
            foreach ($modules as $key => $module) {
                $this->storage->set($key, $module); // write it in 
            }
        }
        return $this->modules;
    }

    /**
     * Check updates in the bootstrap
     * @return boolean
     */
    public function CheckUpdates()
    {
        $activatedModules = $this->storage->getAllModules();
        $modules = $this->loader->getModules();
        if (count($activatedModules) == count($modules)) {
            return true;
        } else {
            $this->Compare($activatedModules, $modules);
            return false;
        }
    }

    /**
     * Function for compare two arrays of modules. <br> The first it is an activated modules and the second another array of modules
     * @param array $activatedModules ['key' => object of ModuleModel or AbstractModuleModel]
     * @param array $modules ['key' => object of ModuleModel or AbstractModuleModel]
     */
    public function Compare($activatedModules, $modules)
    {
        if (!is_null($activatedModules) and ! is_null($modules) and is_array($activatedModules) and is_array($modules)) {
            $not_compare = array(); //for Load modules again
            foreach ($activatedModules as $value) {
                foreach ($modules as $key1 => $value1) {
                    if (!$this->CompareKeys($value, $key1)) {
                        if ((\Bootstrap::HasModule($value) === false)) {
//                            $this->storage->Remove($value);
                        } else {
                            $not_compare[$key1] ? '' : $not_compare[$key1] = $value1;
                        }
                    }
                }
            }
            if (!is_null($not_compare) and is_array($not_compare) and ! empty($not_compare)) {
                $activated = $this->loader->LoadModules($not_compare);
                foreach ($activated as $key => $module) {
                    $this->storage->set($key, $module);
                }
            }
        }
    }

    /**
     * Function for compare two keys 
     * @param string $key1
     * @param string $key2
     * @return boolean
     */
    private function CompareKeys($key1, $key2)
    {
        if ($key1 == $key2) {
            return true;
        }
        return false;
    }

    /**
     * Function to get module information. <br>
     * The function scan a configuration file and get a result
     * @param string $moduleName
     * @return mixed
     */
    public function getModuleInformation($moduleName)
    {
        $ses = new Session();
        $language = $ses->get('language');
        if (\Bootstrap::HasModule($moduleName) and file_exists(\Bootstrap::GetModulesDir() . $moduleName . "/Config/" . $moduleName . "Config_ " . $language . ".yml")) {
            $modulConfig = new YamlConfiguration();
            return $modulConfig->GetConfigurations(\Bootstrap::GetModulesDir() . $moduleName . "/Config/" . $moduleName . "Config_$language.yml");
        } else {
            throw new Exception("Module does not initializated");
        }
    }

    /**
     * Function to get all activated modules
     * @return type
     */
    public function getActivatedModules()
    {
        return $this->loader->getActivatedModules();
    }

    /**
     * Function fot getting all modules configs
     * @return type
     */
    public function getAllModules()
    {
        return $this->loader->getModules();
    }

    /**
     * Function to load in the system one module 
     * @param stdClass $module
     * @return type
     */
    public function LoadModule($module)
    {
        return $this->loader->LoadModule($module);
    }

    public function doAction($arg)
    {
        if (is_array($arg) and ! empty($arg)) {
            $actionName = $arg['name'];
            if (is_null($arg['arguments']) or empty($arg['arguments'])) {
                call_user_func(array(
                    $this,
                    $actionName));
            } else {
                call_user_func_array(array(
                    $this,
                    $actionName), [$arg['arguments']]);
            }
        }
    }

    public function Render($variableName, $variableValue, $file, $display = true)
    {
        $pps = new PPS();
        if (is_object($variableValue)) {
            $pps->setObject($variableName, $variableValue);
        } else {
            $pps->setParameters($variableName, $variableValue);
        }
        if ($display) {
            $pps->Display($file);
        } else {
            return $pps;
        }
    }

    /**
     * @param \PowerPlay\PPS\PPS $pps
     * @param string $varName
     * @param mixed $varValue
     */
    public function addVariebles($pps, $varName, $varValue)
    {

        if (!$pps) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if (is_object($varValue)) {
            $pps->setObject($varName, $varValue);
        } else {
            $pps->setParameters($varName, $varValue);
        }
        return $pps;
    }

    public function Redirect($url)
    {
        header("Location: $url");
    }

    public function __wakeup()
    {
        $this->session = new Session();
    }

}
