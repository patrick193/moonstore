<?php

namespace PowerPlay\ModuleLoader;

use PowerPlay\ModuleLoader\AbstractsModuleModel;
use PowerPlay\YamlConfiguration;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Model  of Module
 *
 * @author Your Name Pohorielov Vladyslav
 */
final class Model extends AbstractsModuleModel {

    private $configuration;

    public function __construct() {
        $this->configuration = new YamlConfiguration();
    }

    /**
     * Prepere array for converting to yaml
     * @param AbstractsModuleModel $module
     * @return array
     * @throws Exception
     */
    public function PrepareYaml(AbstractsModuleModel $module) {
        if (!is_object($module) and ! is_a($module, 'AbstractsModuleClass')) {
            throw new PowerplayException('We can not generate a new configuration');
        }
        $moduleName = $module->getModuleName();
        $moduleDescription = $module->getModuleDescription();
        $moduleLocation = $module->getModuleLocation();
        $moduleAnable = $module->getAnable();
        $moduleURI = $module->getURI();
        $moduleAccessGroups = $module->getGroups();
        $moduleAccessPromission = $module->getAccess();

        $yaml = ['ModuleName' => $moduleName, 'ModuleDescription' => $moduleDescription,
            'ModuleLocation' => $moduleLocation,
            'AccessGroups' => ['Groups' => $moduleAccessGroups, 'Access' => $moduleAccessPromission],
            'Anable' => $moduleAnable, 'URI' => $moduleURI];
        return $yaml;
    }

    /**
     * Function for writing an updating configuration
     * @param string $moduleName
     * @param array $yaml
     * @throws Exception
     */
    public function ExecuteUpdate($moduleName, $yaml) {
        if (\Bootstrap::HasModule($moduleName) and file_exists(\Bootstrap::GetModulesDir() . $moduleName . "/Config/" . $moduleName . "Config.yml")) {
            $mainConfig = $this->configuration->GetMainConfiguration();
            $newConfig = $yaml;
            $result = $this->Comparison($mainConfig, $newConfig);
            if($result === true){
                $this->configuration->YamlToFile($yaml, \Bootstrap::GetModulesDir() . $moduleName . "/Config/" . $moduleName . "Config.yml");
            }else{
                throw new PowerplayException("You have a bad pre-yaml array: " . print_r($yaml));
            }
        }
    }

    /**
     * To compore a configs files
     * @param type $mainConfiguration
     * @param type $newConfiguration
     * @return boolean
     */
    protected function Comparison($mainConfiguration, $newConfiguration) {
        $result = array();
        if (is_array($mainConfiguration) and is_array($newConfiguration)) {
            foreach ($mainConfiguration as $key1 => $value1) {
                foreach ($newConfiguration as $key2 => $value2) {
                    if ($this->KeyCompare($key1, $key2) > 0) {
                        $result[$key1] = $value1;
                    }
                }
            }
            if (count($result) == count($newConfiguration)) {
                unset($result);
                return true;
            }
        }
        return false;
    }

    /**
     * To compare two keys in array
     * @param string $key1
     * @param string $key2
     * @return int
     */
    private function KeyCompare($key1, $key2) {
        if ($key1 == $key2) {
            return 1;
        } else {
            return 0;
        }
    }

}
