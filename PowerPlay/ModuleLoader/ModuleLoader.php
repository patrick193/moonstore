<?php

namespace PowerPlay\ModuleLoader;

use PowerPlay\ModuleLoader\Model;
use PowerPlay\Session;
use PowerPlay\YamlConfiguration;

/**
 * ModuleLoader it is a class to load all modules
 *
 * @author Your Name Pohorielov Vladyslav
 */
class ModuleLoader {

    const DEFAULT_LANGUAGE = 'en-GB';

    private static $basicModuleObject; // our singelton
    private static $yaml, $db, $routing = array(); // for object
    private static $activatedModules = array(); // all activated modules
    private $session;
    
    /**
     * Init Class
     */

    private function __construct() {
        
    }

    public static function getBasicModuleObject() {
        if(is_null(self::$basicModuleObject)) {
            self::$basicModuleObject = new ModuleLoader();
        }
        return self::$basicModuleObject;
    }

    /**
     * Function to the load module configuration
     * @param string $configPath Path to the module location
     */
    private function LoadConfiguration($configPath, $obj = true) {
        $moduleConfig = new YamlConfiguration();
        return $moduleConfig->GetConfigurations($configPath, $obj);
    }

    /**
     * Function to get all modules in the project <br>
     * The function will fill out an array of modules configurations
     * @return mixed Return an array of objects with all information about every module
     */
    public function getModules() {
        if(self::$yaml) {
            return self::$yaml;
        }
        !$this->session ? $this->session = new Session() : '';
        !$this->session->get('language') ? $language = self::DEFAULT_LANGUAGE : $language = $this->session->get('language');

        $allModules = \Bootstrap::getModules();
        foreach($allModules as $module) {
            $modulePath = \Bootstrap::getModulesDir($module) . $module;
            $classPath = $modulePath . "/" . $module . ".php";
            $configPath = $modulePath . "/Config/" . $module . "Config_" . $language . ".yml";
            $alternativeConfigPath = $modulePath . "/Config/" . $module . "Config.yml";
            $dbPath = $modulePath . "/Config/" . $module . "Database.yml";
            $routingPath = $modulePath . "/Config/Routing.yml";
            if(is_dir($modulePath) and file_exists($classPath) and (file_exists($configPath) or file_exists($alternativeConfigPath))) {
                file_exists($configPath) ? : $configPath = $alternativeConfigPath ;
                $moduleConfig = $this->LoadConfiguration($configPath);
                self::$yaml[$module] = $moduleConfig;
            }
            if(is_dir($modulePath) and file_exists($classPath) and file_exists($dbPath) and ! is_null($moduleConfig)) {
                self::$db[$module]['db'] = $this->LoadConfiguration($dbPath);
//                echo 'We have an overrided db config in module ' . $module . "<br>";
            }
            if(is_dir($modulePath) and file_exists($classPath) and file_exists($routingPath)) {
                self::$routing[$module]['routing'] = $this->LoadConfiguration($routingPath, false);
            }
        }
        return self::$yaml;
    }

    /**
     * Function to load in the system one module 
     * @param stdClass $module
     */
    public function LoadModule($module) {
        if(is_object($module) and ! is_null($module->Enable) and $module->Enable) {
            $moduleRequire = \Bootstrap::getModulesDir($module->ModuleName) . $module->ModuleName . "/" . $module->ModuleName . ".php";
            require_once $moduleRequire;
            $moduleObject = $module->ModuleLocation . "\\" . $module->ModuleName;
            if(!in_array($module->ModuleName, self::$activatedModules)) {
                self::$activatedModules[$module->ModuleName] = new Model();
                self::$activatedModules[$module->ModuleName]->setEnable($module->Enable);
                self::$activatedModules[$module->ModuleName]->setModuleDescription($module->ModuleDescription);
                self::$activatedModules[$module->ModuleName]->setModuleLocation($module->ModuleLocation);
                self::$activatedModules[$module->ModuleName]->setModuleName($module->ModuleName);
                self::$activatedModules[$module->ModuleName]->setURI($module->URI);
                self::$activatedModules[$module->ModuleName]->setGroups($module->AccessGroups['Groups']);
                self::$activatedModules[$module->ModuleName]->setAccess($module->AccessGroups['Access']);
                self::$activatedModules[$module->ModuleName]->setDefineAccess($module->AccessGroups['Define']);
                self::$activatedModules[$module->ModuleName]->setDb(self::$db[$module->ModuleName]['db']);
                self::$activatedModules[$module->ModuleName]->setRouting(self::$routing[$module->ModuleName]['routing']);
                self::$activatedModules[$module->ModuleName]->setModule(new $moduleObject());
            }
        }
    }

    /**
     * Function to load all anabled modules. <br>
     * You can to send an array of object and load a couple of modules or you can<br>
     * just call this function to load all modules
     * @param array $modules
     * @return array
     * @throws Exception
     */
    public function LoadModules($modules = null) {
        if(is_null($modules)) {
            if(is_null(self::$yaml) or empty(self::$yaml)) {
                $this->getModules();
            }
            $modules = self::$yaml;
        }
        if(is_array($modules)) {
            foreach($modules as $module) {
                $this->LoadModule($module);
            }
            return self::$activatedModules;
        } else {
            throw new \PowerPlay\PowerplayException\PowerplayException("No modules to load");
        }
    }

    /**
     * Get all activated(loaded) modules
     * @return array
     */
    public function getActivatedModules() {
        return self::$activatedModules;
    }

    /**
     * Function for set up our array of activated modules
     * @param array $modules
     */
    public static function setActivatedModules($modules) {
        self::$activatedModules = $modules;
    }

    private function __clone() {
        
    }

}
