<?php

namespace PowerPlay\PPS;

/**
 * Description of PPS
 *
 * @author Developer Pohorielov Vladyslav
 */
class PPS
{

    private $smarty;

    public function __construct()
    {

        $this->SmartyStart();
    }

    private function SmartyStart()
    {

        require __DIR__ . "/libs/Smarty.class.php";
        $this->smarty = new \Smarty();
        $this->SmartySetUp();
    }

    protected function SmartySetUp()
    {
        $this->smarty->template_dir = \Config::$web . 'templates/';
        $this->smarty->compile_dir  = \Config::$web . 'templates_c/';
        $this->smarty->cache_dir    = \Config::$web . 'cache/';

        $this->smarty->caching = false;
//        $this->smarty->registerFilter('unset', 'smarty_modifier_unset');
    }

    public function setParameters($variableName, $variableValue)
    {
        !$this->smarty ? $this->SmartyStart() : '';

        $this->smarty->assign($variableName, $variableValue);
    }

    public function setObject($variableName, $object)
    {
        !$this->smarty ? $this->SmartyStart() : '';

        $this->smarty->registerObject($variableName, $object);
    }

    public function Display($template)
    {

        !$this->smarty ? $this->smarty = $this->SmartyStart() : '';
        strpos($template, ".tpl") ? '' : $template .= ".tpl";
        $this->smarty->display($template);
    }

}
