<?php

namespace PowerPlay\PowerplayException;

use Exception;

class PowerplayException extends Exception {

    /**
     *
     * @var string Message for display 
     */
    protected $message;

    /**
     *
     * @var string File where thrown an Exception 
     */
    protected $file;

    /**
     *
     * @var int Line where was called an Exception 
     */
    protected $line;

    /**
     *
     * @var string Message type 
     */
    private $messageType;

    /**
     *
     * @var array All allowed messages types 
     */
    private $allowedMessageTypes = ['sys' => 'SystemInfo', '404' => 'Type404', '500' => 'Type500',
        'warning' => 'Warning', 'user' => 'UserInfo'];

    /**
     *
     * @var \Modules\Mailer\Mailer 
     */
    private $mail;

    /**
     *
     * @var \PowerPlay\YamlConfiguration 
     */
    private $yaml;

    /**
     * 
     * @param string $message Text of the message
     * @param string $messageType Type of the message. <br>Allowed: sys, 404, 500, warning
     * @param boolean $debug
     */
    public function __construct($message, $messageType = 'sys', $debug = true) {
        if($debug) {
            $backtrace = debug_backtrace();

            $backtrace[0]['file'] ? $this->file = $backtrace[0]['file'] : '';
            $backtrace[0]['line'] ? $this->line = $backtrace[0]['line'] : '';
        }

        if(array_key_exists($messageType, $this->allowedMessageTypes)) {
            $this->messageType = $messageType;
        }
        is_string($message) ? $this->message = $message : '';
        $this->mail = new \PowerPlay\Mailer\Mailer();
        $this->yaml = new \PowerPlay\YamlConfiguration();

        $this->DefineAction();
    }

    /**
     * Function to find an action in this class which match the type of Exception
     * @return boolean
     */
    private function DefineAction() {
        if(!$this->messageType) {
            return false;
        }
        if(method_exists($this, $this->allowedMessageTypes[$this->messageType])) {
            call_user_func(array($this, $this->allowedMessageTypes[$this->messageType]));
        } else {
            return false;
        }
    }

    /**
     * Function to notify a developer about the troubles on the site
     */
    private function SystemInfo() {

        $config = $this->yaml->GetConfigurations(__DIR__ . "/Config/PowerplayExceptionMailConfig.yml");
        $to = $config->to;
        $header = $config->header;
        $subject = $config->subject;
        $message = $config->Templates['SystemInfo']['Text'] . $this->message ."\r\n";

        $args = ['to' => $to, 'subject' => $subject, 'message' => $message, 'header' => $header];
//        $this->mail->Send($args);
        $this->Render($message, 'sys');
    }

    /**
     * Function to render 404 page with exception error
     */
    private function Type404() {
        $this->Render($this->message);
    }

    /**
     * Function to send a notification to developer and render a 500 page
     */
    private function Type500() {

        $this->SystemInfo();

        $this->Render($this->message, 500);
    }

    /**
     * Function to send a Warning to the developer
     */
    private function Warning() {

        $config = $this->yaml->GetConfigurations(__DIR__ . "/Config/PowerplayExceptionMailConfig.yml");
        $to = $config->to;
        $header = $config->header;
        $subject = $config->subject;
        $message = $config->Templates['Warning']['Text'] . $this->message . " in " . $this->file . " on line " . $this->line;

        $args = ['to' => $to, 'subject' => $subject, 'message' => $message, 'header' => $header];

        $this->mail->Send($args);
    }

    private function Render($message, $type = 404) {
        if($type == 'sys'){
            echo '<p style="color:#ff7502">Exception in the system. With this exception we can not work correct and we stoped the process.<br>'
            . 'The text of exception the following: <br></p><p style="color:red">' . $message . '</p>';
//            die;
        }elseif($type == 'user'){
            echo '<p style="color:#ff7502">' . $message.'<br>';
        }elseif($type == 404){
            $pps = new \PowerPlay\PPS\PPS();
            $pps->Display('Site/404');
        }
    }
    
    private function UserInfo(){
        $this->Render($this->message, 'user');
    }

}