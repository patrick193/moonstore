<?php

namespace PowerPlay\Routing;

use PowerPlay\Storage;
use PowerPlay\Filter;
use ReflectionMethod;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * 
 * @author Developer Pohorielov Vladyslav
 */
class BasicRouting
{

    private $storage;
    private $module;

    /**
     * Init Class
     */
    public function __construct(Storage $storage = null)
    {
        if (!is_null($storage) and is_object($storage)) {
            $this->storage = $storage;
        } else {
            $this->storage = new Storage();
        }
    }

    /**
     * Function for separete all url and find appropriate module and action
     * @param string $url Url in string format
     * @return array Return array with action name, arguments and module
     * @throws Exception
     */
    public function FindModule($url, $roleCode = '2G')
    {

        if (is_null($url) or empty($url) or ! is_string($url)) {
            throw new PowerplayException('Wrong url: ' . $url, 404);
        }
        if ($url{0} == "/") {
            $url = substr($url, 1);
        }
        $filter     = new Filter();
        $url        = $filter->ToFilter($url);
        $parameters = explode('/', $url);
        $modules    = $this->storage->getAllModules();
        $routing    = [];
        if (is_null($parameters[1]) or empty($parameters[1])) {
            $parameters[1] = 'index';
        }
        foreach ($modules as $value) {
            if (strtolower($parameters[0]) == strtolower($value)) {
                $module = $this->storage->getValue($value);
                if ($this->CheckAccess($module->getDefineAccess(), $roleCode)) {
                    $routing['base']          = $module->getURI();
                    $routing['baseCurrently'] = $url;
                    $routing['currently']     = $parameters;
                    $routing['route']         = $module->getRouting();
                    $routing['module']        = $value;
                    $this->module             = $module;
                    break;
                } else {
                    throw new PowerplayException('Access Deny.', 404);
                }
            }
        }
        if (!$routing or empty($routing) or is_null($routing)) {
            throw new PowerplayException('Wrong formt of url: ' . $url, 404);
        }
        try {
            return $this->Finder($routing);
        } catch (PowerplayException $ex) {
            echo 'We have an exception: ' . $ex->getMessage();
            exit();
        }
    }

    private function CheckAccess($defineAccess, $code)
    {
        foreach ($defineAccess as $codeRole) {
            if (in_array($code, $codeRole) or $codeRole[0] == 'all') {
                return true;
            }
        }
        return false;
    }

    /**
     * Function for finding all rules in the module
     * @param array $moduleRouting
     * @return array 
     * @throws Exception
     */
    public function Finder($moduleRouting)
    {
        if (is_null($moduleRouting) or empty($moduleRouting) or ! is_array($moduleRouting)) {
            throw new PowerplayException('Wrong array or you fogot to add some parameters to array', 404);
        }
        if ($moduleRouting['currently'][1] != 'index') {
            $pathName = $this->FindeRule($moduleRouting['route'], $moduleRouting['currently']);
        } else {
            $pathName = $moduleRouting['currently'][0] . "_" . $moduleRouting['currently'][1];
        }
        if (array_key_exists($pathName, $moduleRouting['route'])) {// if we have a rule
            $parameters = $this->Rules($moduleRouting['currently'], $moduleRouting['route'][$pathName]);
            $rules      = $moduleRouting['route'][$pathName];
            $existed    = $this->MethodExist($rules['default']); // just check have we weather action in our module or not
            if ($existed >= 0) {
		 foreach ($parameters as $key => $value) {
                    $parameters[$key] = urldecode($value);
                }
                $methodName = $this->getMethodName($rules['default']);
                $module     = $this->module;
                $args       = ['info'   => ['name'      => $methodName,
                        'arguments' => $parameters],
                    'module' => $module];
                return $args;
            } else {
                throw new PowerplayException('We did not find any action on this rule: ' . $pathName, 404);
            }
        } else {
            throw new PowerplayException('We did not find any rules about this url: ' . $moduleRouting['baseCurrently'], 404);
        }
    }

    /**
     * Private function to finde rule from url
     * @param array $route
     * @param array $currently
     * @throws Exception
     */
    private function FindeRule($route, $currently)
    {
        if (!is_array($route) or ! is_array($currently)) {
            throw new PowerplayException('Wrong parameters');
        }
        $currently = array_diff($currently, array(
            ''));
        $rulePost  = '';
        $ruleMain  = '';
        foreach ($currently as $rules) {
            $ruleMain .= $rules . "_";
        }
        $ruleMain = rtrim($ruleMain, "_");

        $detected = '';
        foreach ($route as $key => $value) {
            if ($key == $ruleMain) {
                return $ruleMain;
            }
        }
        unset($key);

//        var_dump($route);
//        die;
        foreach ($route as $key => $value) {

            $path = explode("/", $value['path']);
            $path = array_diff($path, array(
                ''));
            if ((strpos($value['path'], "{") !== false) and ( strpos($value['path'], "}") !== false)) {
                $newPath = explode("/", explode("{", $value['path'])[0]);
                $newPath = array_diff($newPath, array(
                    ''));
                for ($i = 0, $count = count($newPath); $i < $count; $i++) {
                    $rulePost .= $currently[$i] . "_";
                }

                $rulePost = rtrim($rulePost, "_");
                if ($rulePost == $key) {
                    return $rulePost;
                } else {
                    $rulePost = '';
                }
            } else {
                if ($this->PostGet() == 'POST') {
                    $post = $this->getPost();

                    $rulePost = '';
                    $argsPost = [];

                    $ruleCurrently = '';
                    $argsCurrently = [];

                    $notMatch = [];

                    foreach ($post as $name => $postValue) {
                        $pp = $name . "=" . ($this->PostGet() == 'POST' ? str_replace("/", "!@#", base64_encode(mysql_escape_string($postValue))) : $postValue);

                        $argsPost[] = $pp;
                    }


                    foreach ($currently as $currentName => $currentValue) {
                        if (strpos($currentValue, "=") === false) {
                            $ruleCurrently .= $currentValue . "_";
                        } else {
                            $argsCurrently[] = $currentValue;
                        }
                    }

                    $ruleCurrently = rtrim($ruleCurrently, '_');

                    if ((count($argsCurrently) === count($argsPost))) {
                        for ($j = 0, $c = count($argsCurrently); $j < $c; $j++) {
                            if ($argsCurrently[$j] != $argsPost[$j]) {
                                $notMatch[] = $argsPost[$j];
                            }
                        }
                    }
                    if (count($notMatch) <= 0) {
                        return $ruleCurrently;
                    }
                }
            }
        }
        unset($key, $path);


        return false;
    }

    /**
     * Function for generate all parameters to the action
     * @param array $parameters
     * @return array
     * @throws Exception
     */
    private function Rules($parameters, $rule)
    {
        if (is_array($parameters) and ! empty($parameters) and ! is_null($parameters) and is_array($rule) and ! empty($rule)) {
            $newData       = [];
            $preparameters = [];
            $path          = explode("/", $rule['path']);
            for ($i = 0, $c = count($path); $i < $c; $i++) {
                if (strpos($parameters[$i], "=") !== false and $this->PostGet() == 'GET') {
                    $prd                    = explode("=", $parameters[$i]);
                    $preparameters[$prd[0]] = $prd[1];
                }

                if (preg_match("/\{(.+)\}/", $path[$i])) {
                    preg_match('/\{(.+)\}/', $path[$i], $m);

                    $preparameters[$m[1]] = $parameters[$i];
                }
            }

            if (empty($preparameters)) {
                $post = $this->getPost();
                foreach ($post as $key => $value) {
                    $preparameters[$key] = $key . "=" . (( $this->PostGet() == 'POST') ? str_replace("/", "!@#", base64_encode($value)) : $value );
                }
            }
            foreach ($preparameters as $key => $value) {
                if (!preg_match("/=/", $value)) {
                    $this->PostGet() == 'POST' ? $value         = $this->Base64Valide($value) : '';
                    $newData[$key] = $value;
                } else {
                    if (preg_match('/\&/', $value)) {
                        $get = explode('&', $value);
                        $sub = [];
                        foreach ($get as $subGet) {
                            $sub[] = explode("=", $subGet)[1];
                        }
                        $subKey = explode("&", $key);
                        for ($i = 0, $count = count($sub); $i < $count; $i++) {
                            $this->PostGet() == 'POST' ? $sub[$i]              = $this->Base64Valide($sub[$i]) : '';
                            $newData[$subKey[$i]] = $sub[$i];
                        }
                    } else {
                        $match         = explode("=", $value);
                        $this->PostGet() == 'POST' ? $match[1]      = $this->Base64Valide($match[1]) : '';
                        $newData[$key] = $match[1];
                    }
                }
            }

            return $newData;
        }
        throw new PowerplayException('Wrong parameter: ' . $preparameters);
    }

    /**
     * Function for getting an action name
     * @param string $method
     * @return string
     * @throws Exception
     */
    private function getMethodName($method)
    {
        if (is_null($method) or empty($method)) {
            throw new PowerplayException("We can not generate a method name");
        }
        $action = "action" . $method;
        return $action;
    }

    /**
     * Function for checking does method(action) exist or not <br> with a ReflectionMethod class
     * @param ReflectionMethod $method
     * @return int
     */
    protected function MethodExist($method)
    {

        if (method_exists($this->module->getModule(), "action" . $method)) {
            $method = new ReflectionMethod($this->module->getModule(), "action" . $method);
            $args   = $method->getParameters();
            if (count($args) >= 0) {
                return count($args);
            }
            return 0;
        }
        return -1;
    }

    /**
     * Function to start routing
     * @param string $roleCode
     * @return mixed
     */
    public function Route($roleCode = '2G')
    {
        if ($this->PostGet() == 'POST') {
            $url = $this->PostPrepareUrl($_POST);
            return $this->FindModule($url, $roleCode);
        }
        if ($_SERVER['QUERY_STRING']) {
            (substr($_SERVER['REQUEST_URI'], "-1") != "/" and $_SERVER['QUERY_STRING'][0] != "/" ? $help = '/' : $help = '');
            $url  = $_SERVER['REQUEST_URI'];
        } else {
            $url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : null;
        }
        if (empty($url) or $url == '/') {
            $url = 'posts/show/all'; // заглушка
        }
	$url = str_replace("?", "/", $url);
        $url[count($url) - 1] == "/" ? : $url .= "/";
        return $this->FindModule($url, $roleCode);
    }

    /**
     * Function to generate a post query
     * @param array $url
     * @return string
     */
    public function PostPrepareUrl($url)
    {
        $finishedUrl = $_SERVER['REQUEST_URI'];
        $finishedUrl[count($url) - 1] == "/" ? : $finishedUrl .= "/";

        foreach ($url as $key => $value) {
            $value = base64_encode(mysql_escape_string($value));
            $finishedUrl .= $key . '=' . str_replace("/", "!@#", $value) . '/';
        }

        return $finishedUrl;
    }

    /**
     * Function for decoding a base 64 code
     * @param char $str
     * @return string
     */
    private function Base64Valide($str)
    {
        if (is_string($str)) {
            return base64_decode(str_replace("!@#", "/", $str));
        }
    }

    public function getPost()
    {
        return $_POST;
    }

    public function getGET()
    {
        return $_GET;
    }

    /**
     * Function to get a method
     * @return string
     */
    private function PostGet()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        return $method;
    }

}
