<?php
namespace PowerPlay\Routing\Methods;

/**
 * Description of Methods
 *
 * @author Developer Pohorielov Vladyslav
 */
abstract class AbstractMethods {

    
    abstract function RunAction($args);
    
}
