<?php

namespace PowerPlay;

use PowerPlay\Keeper\Session as BasicSession;

/**
 * Description of Session
 *
 * @author Developer Pohorielov Vladyslav
 */
class Session extends BasicSession {

    private $session;

    public function __construct() {
        parent::__construct();
        if (is_null($this->session) or empty($this->session)) {
            $this->session = $this->getRegister();
        }
    }

    /**
     * Function to get any data from session
     * @param string $key
     * @return object
     */
    public function get($key, $part = 'register') {
        if (isset($this->session[$part][$key])) {
            $obj = (base64_decode($this->session[$part][$key]));
            return $obj;
        }
    }

    /**
     * Function to add some data in the storage
     * @param string $key
     * @param object $object
     */
    public function set($key, $object, $part = 'register') {
        $this->setRegister($key, $object, $part);
        $this->session = $this->getRegister();
    }

    /**
     * Destroy all session<br>
     * <p style="color: red">ONLY FOR DEVELOPMENT</p>
     */
    public function DestroySession() {
        $this->Destroy();
        unset($this->session);
    }

    /**
     * Remove some data from the session
     * @param string $key
     */
    public function Remove($key) {
        $this->Delete($key);
    }
    
    public function __wakeup() {
        $this->session = $this->getRegister();
    }

}
