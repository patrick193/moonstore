<?php

namespace PowerPlay\String;

use PowerPlay\String\AbstractFilters;
use Exception;

/**
 * SQLFilter
 *
 * @author Developer Pohorielov Vladyslav
 */
class SQLFilter extends AbstractFilters {
    
    private $notAllowed = ['drop', 'truncate'];


    /**
     * Function for check our sql query.
     * @param string $string
     * @return string
     * @throws Exception
     */
    public function Filter($string){
        
        if(is_string($string) and !is_null($string)){
            foreach ($this->notAllowed as $rule) {
                if(stristr(strtolower($string), $rule) !== false){
                    throw new Exception('This is kind of query was blocked');
                }
            }
            
            return $string;
        }
    }
}
