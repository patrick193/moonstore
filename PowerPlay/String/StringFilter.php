<?php

namespace PowerPlay\String;

use PowerPlay\String\AbstractFilters;
use Exception;

/**
 * Filters 
 *
 * @author Developer Pohorielov Vladyslav
 */
class StringFilter extends AbstractFilters {

    
    /**
     * Easy filter does our param is string or not
     * @param string $str
     * @return boolean
     * @throws Exception
     */
    public function Filter($str) {
        if (!is_string($str) or is_null($str)) {
            throw new Exception('Not string format: ' . print_r($str));
        }
        $str = strip_tags($str);
        if (preg_match("/\D/", $str)) {
            return $str;
        }
        return false;
    }

   

}
