<?php

namespace PowerPlay;

use PowerPlay\View\View;

/**
 * Template
 *
 * @author Developer Pohorielov Vladyslav
 */
class Template extends View{

    public function __construct() {
        parent::__construct();
    }

    public function __destruct() {
        parent::__destruct();
    }

}
