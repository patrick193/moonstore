<?php
namespace PowerPlay\Users;

use PowerPlay\Users\AbstractUsers;
/**
 * a basic class of roles
 *
 * @author Developer Pohorielov Vladyslav
 */
abstract class AbstractRole extends AbstractUsers{

    protected $role_id;
    protected $role_name;
    protected $isLocked;
    protected $parent_id;
    
    
    
    public function getParentId() {
        return $this->parent_id;
    }

    public function setParentId($parentId) {
        $this->parent_id = $parentId;
        return $this;
    }

    
    public function getRoleId() {
        return $this->role_id;
    }

    public function getRoleName() {
        return $this->role_name;
    }

    public function getIsLocked() {
        return $this->isLocked;
    }

    public function setRoleId($roleId) {
        $this->role_id = $roleId;
        return $this;
    }

    public function setRoleName($roleName) {
        $this->role_name = $roleName;
        return $this;
    }

    public function setIsLocked($isLoacked) {
        $this->isLocked = $isLoacked;
        return $this;
    }
    
    
}
