<?php

namespace PowerPlay\Users;

use PowerPlay\Users\AbstractUsers;
/**
 * A basic class of users
 * 
 * @author Developer Pohorielov Vladyslav
 */
abstract class AbstractUser extends AbstractUsers{
   

    protected $user_id;
    protected $user_first_name;
    protected $user_last_name;
    protected $user_email;
    protected $role_id;
    protected $parent_id;
    protected $isLocked;
    protected $user_password;
    
    function getPassword() {
        return $this->user_password;
    }

    function setPassword($password) {
        $this->user_password = md5($password);
    }

    
    public function getUserId() {
        return $this->user_id;
    }

    public function setUserId($userId) {
        $this->user_id = $userId;
        return $this;
    }
    public function getUserFirstName() {
        return $this->user_first_name;
    }

    public function getUserLastName() {
        return $this->user_last_name;
    }

    public function getUserEmail() {
        return $this->user_email;
    }

    public function getRoleId() {
        return $this->role_id;
    }

    public function getParentId() {
        return $this->parent_id;
    }

    public function getIsLocked() {
        return $this->isLocked;
    }

    public function setUserFirstName($userFirstName) {
        $this->user_first_name = $userFirstName;
        return $this;
    }

    public function setUserLastName($userLastName) {
        $this->user_last_name = $userLastName;
        return $this;
    }

    public function setUserEmail($userEmail) {
        $this->user_email = $userEmail;
        return $this;
    }

    public function setRoleId($roleId) {
        $this->role_id = $roleId;
        return $this;
    }

    public function setParentId($parentId) {
        $this->parent_id = $parentId;
        return $this;
    }

    public function setIsLocked($isLocked) {
        $this->isLocked = $isLocked;
        return $this;
    }

    

}
