<?php

namespace PowerPlay\Users;

use PowerPlay\Users\InterfaceUsers;

/**
 *
 * @author Developer Pohorielov Vladyslav
 */
abstract class AbstractUsers implements InterfaceUsers{

   abstract public function Create();

   abstract public function Lock($id);

   abstract public function Load($parameters);
}
