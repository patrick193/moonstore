<?php

namespace PowerPlay\Users;

/**
 *
 * @author Developer Pohorielov Vladyslav
 */
interface InterfaceUsers {
    
    /**
     * function to create a new user or role
     */
    public function Create();
    
    /**
     * function to load any user by any arguments
     * @param array $parameters Type: ['id', 2]
     */
    public function Load($parameters);
    
    
    /**
     * function to lock user or role by id
     * @param intager $id
     */
    public function Lock($id);
}
