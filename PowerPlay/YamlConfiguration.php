<?php

namespace PowerPlay;

use PowerPlay\PowerplayException\PowerplayException;
use Symfony\Component\Yaml\Yaml;

/**
 * Description of YamlParser
 *
 * @author Your Name Pohorielov Vladyslav
 */
final class YamlConfiguration
{

    private $yaml;

    /**
     * Init Class
     */
    public function __construct()
    {
        
    }

    /**
     * Function to get module configurations
     * @param string $configFile Path to the config file
     * @param boolean $inObject Convert to object stdClass or not
     * @return array
     * @throws Exception
     */
    public function GetConfigurations($configFile, $inObject = true)
    {
        try {
            if (file_exists($configFile)) {

                $this->yaml = Yaml::parse(file_get_contents($configFile));
//                $this->yaml = yaml_parse_file($configFile);
//                $this->CheckFormat();
                $inObject ? $this->ConvertToObject() : '';
                return $this->yaml;
            } else {
                throw new PowerplayException("Wrong path to the configuration file: " . $configFile);
            }
        } catch (PowerplayException $e) {
            echo 'You have an exception in the YAML configuration: "' . $e->getMessage();
            exit;
        }
    }

    /**
     * Function for parsing yaml
     * @param yaml $yaml
     * @return array
     * @throws Exception
     */
    public function YAMLParse($yaml)
    {
        try {
            $this->yaml = Yaml::parse(file_get_contents($yaml));
            if ($this->yaml) {
                return $this->yaml;
            } else {
                throw new PowerplayException("You have an error in the yaml type");
            }
        } catch (PowerplayException $ex) {
            echo 'You have an error while parsing your yaml file. Error:' . $ex->getMessage();
            exit;
        }
    }

    /**
     * Convert to object
     */
    public function ConvertToObject()
    {
        $anonym = new \stdClass();
        foreach ($this->yaml as $key => $value) {
            $anonym->$key = $value;
        }
        if (!is_null($anonym) and is_object($anonym)) {
            $this->yaml = $anonym;
        }
    }

    /**
     * Check array format
     * @throws Exception
     */
    public function CheckFormat()
    {
//        if (is_null($this->yaml) or ! is_array($this->yaml)) {
//            throw new Exception("Wrong yaml format! Please check the configuration of the module");
//        }
    }

    /**
     * Generate yaml from array
     * @param array $data
     * @return yaml
     * @throws Exception
     */
    public function YamlGenerate(array $data)
    {

        if (is_array($data) and ! empty($data)) {
            $yaml = yaml_emit($data, YAML_ANY_ENCODING, YAML_ANY_BREAK);
            return $yaml;
        } else {
            throw new PowerplayException("Can not generate a yaml file because you sent wrong format of array");
        }
    }

    /**
     * Generate yaml file
     * @param array $yaml Array fot converting to yaml
     * @param string $path Path to the file
     * @return boolean
     */
    public function YamlToFile($yaml, $path)
    {
        if (file_exists($path)) {
            yaml_emit_file($path, $yaml);
            return true;
        }
        return false;
    }

    /**
     * Get a main default configuration file
     * @return array
     */
    public function GetMainConfiguration()
    {
        $this->GetConfigurations(__DIR__ . "/ConfigurationYaml/MainModuleConfiguration.yml", false);
        return $this->yaml;
    }

    /**
     * To get a main template of db config 
     * @return array
     */
    public function GetDataBaseConfiguration()
    {
        $this->GetConfigurations(__DIR__ . "/ConfigurationYaml/MainDataBaseConfiguration", false);
        return $this->yaml;
    }

    /**
     * To get a main routing file
     * @return array
     */
    public function GetRoutingBaseConfiguration()
    {
        $this->GetConfigurations(__DIR__ . "/ConfigurationYaml/MainRoutingConfiguration", false);
        return $this->yaml;
    }

    /**
     * Destruct of Class
     */
    public function __destruct()
    {
        unset($this->yaml);
    }

}
