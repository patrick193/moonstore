<?php

function __autoload($className) {
    $className = __DIR__ . '/' . str_replace('\\', '/', $className) . '.php';
    $className = str_replace("Override", "src", $className);
    if (file_exists($className)) {
        require_once $className;
        return true;
    }
    return false;
}
