<?php

require_once './autoloader.php';
autoloader::addAutoloader('./mainautoloader.php');

use PowerPlay\Database;

class import_links
{

    private $db;

    public function __init__($databasename)
    {
        $this->db = new Database(Config::$host, Config::$user, Config::$password, $databasename);
    }

    private function getPosts($categoryId)
    {
        $q     = "SELECT * FROM `wp_posts` WHERE ID in"
                . " (select object_id from wp_term_relationships where term_taxonomy_id = " . $categoryId . " "
                . "or "
                . "term_taxonomy_id in (select term_taxonomy_id from wp_term_taxonomy where parent = " . $categoryId . ""
                . " or term_id = " . $categoryId . "))"
                . " or `post_parent` in (select object_id from wp_term_relationships where term_taxonomy_id = " . $categoryId . ""
                . ") and post_content IS NOT NULL and post_content <>'';";
        $this->db->Start();
        $posts = $this->db->Execute($q);
        return $posts;
    }

    public function createLinks($categoryId)
    {
        $posts = $this->getPosts($categoryId);

        $links   = [];
        $link    = "/lifestyle/moon-store-studio/";
        $newlink = "/posts/show/";
        foreach ($posts as $post) {
            if (strpos($post->post_name, 'revision') === false) {
                $links[$post->ID]['old_link'] = $link . $post->post_name . ".html";
                $links[$post->ID]['new_link'] = $newlink . $post->ID;
            }
        }
        return $links;
    }

    public function redirect($links)
    {
        $filename = ".htaccess";
        $file     = file_get_contents($filename);
        foreach ($links as $link) {
            $file .= "\nRedirect 301 " . $link['old_link'] . " " . $link['new_link'];
        }
        if (file_put_contents($filename, $file)) {
            return true;
        } else {
            return false;
        }
    }

}

$import = new import_links();
$import->__init__('moon_restore');
$links  = $import->createLinks(30);
if ($import->redirect($links)) {
    echo "COOL! We have done it from first time\n\n\n\n";
} else {
    echo "=(\n\n\n\n";
}