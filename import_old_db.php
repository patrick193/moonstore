<?php

error_reporting(0);

require_once './autoloader.php';
require_once './Modules/HTMLParser/simple_html_dom.php';
autoloader::addAutoloader('./mainautoloader.php');

use PowerPlay\Database;

abstract class abstractImport
{

    const SITENAME      = "http://moonstore.it/wp-content/uploads/";
    const OLDDBNAME     = 'moon_restore';
    const NEWDBNAME     = 'moonstore';
############################################################
    const youtubePlayer = "https://www.youtube.com/embed/";
    const vimeoPlayer   = "https://player.vimeo.com/video/";
    const youtubeTH     = "http://img.youtube.com/vi/";
    const vimeoTH       = "http://vimeo.com/api/v2/video/";

    abstract public function import($array1);
}

class import extends abstractImport
{

    /**
     * Name of old database 
     * @var string
     */
    private $oldDBName;

    /**
     * Name of new database
     * @var string
     */
    private $newDBName;

    /**
     * category Id in the wp database
     * @var int
     */
    private $categoryId;

    /**
     * post image or video
     * @var array
     */
    private $postImg = [];

    /**
     *
     * @var object of this class
     */
    private static $instanse;

    /**
     * database class from PPCore
     * @var Database 
     */
    private $db;

    /**
     * close a constructor
     */
    private function __construct()
    {
        
    }

    /**
     * close clone
     */
    private function __clone()
    {
        
    }

    /**
     * Easy singelton pattern
     * @return \self
     */
    public static function getInstanse()
    {
        if (self::$instanse) {
            return self::$instanse;
        } else {
            return new self;
        }
    }

    /**
     * Function for init the data
     * @param string $oldDBName
     * @param string $newDBName
     * @param int $categoryId
     */
    public function __init__($oldDBName, $newDBName, $categoryId = 1)
    {
        if (empty($oldDBName)) {
            $this->oldDBName = self::OLDDBNAME;
        } else {
            $this->oldDBName = $oldDBName;
        }
#####
        if (empty($newDBName)) {
            $this->newDBName = self::NEWDBNAME;
        } else {
            $this->newDBName = $newDBName;
        }
#####
        if (!is_int($categoryId)) {
            throw new Exception("category id should be an int type");
        }
        $this->categoryId = $categoryId;
        !$this->db ? $this->db         = new Database(Config::$host, Config::$user, Config::$password, $this->oldDBName) : '';
    }

    public function getAllPosts()
    {
        /**
         * SELECT * FROM `wp_posts` WHERE `ID` in
          (select object_id from wp_term_relationships where term_taxonomy_id = 36 or
          term_taxonomy_id in (select term_taxonomy_id from wp_term_taxonomy where parent = 36 or term_id = 36)) or
          `post_parent` in (select object_id from wp_term_relationships where term_taxonomy_id = 36)
          and post_content IS NOT NULL and post_content <>'' ORDER BY `wp_posts`.`post_date` DESC
         */
        if (!$this->categoryId) {
            throw new LogicException("First you should do init!!!");
        }
        $q     = "SELECT * FROM `wp_posts` WHERE ID in"
                . " (select object_id from wp_term_relationships where term_taxonomy_id = " . $this->categoryId . " "
                . "or "
                . "term_taxonomy_id in (select term_taxonomy_id from wp_term_taxonomy where parent = " . $this->categoryId . ""
                . " or term_id = " . $this->categoryId . "))"
                . " or `post_parent` in (select object_id from wp_term_relationships where term_taxonomy_id = " . $this->categoryId . ""
                . ") and post_content IS NOT NULL and post_content <>'';";
        $this->db->Start();
        $posts = $this->db->Execute($q);
        if (count($posts) > 0) {
            return $posts;
        } else {
            return 0;
        }
    }

    /**
     * function for import all data from old db to new
     * @param array $array
     */
    public function import($array)
    {
        if (!$array or count($array) <= 0) {
            throw new Exception("Nothing to transfer");
        }
        $this->db = new Database(Config::$host, Config::$user, Config::$password, $this->newDBName);

        $count;
        $arrayCount = count($array);
        foreach ($array as $post) {
            if ($this->db->Count('post_id', 'powerplay_posts', ['post_id' => $post->ID]) < 1 and
                    strpos($post->post_name, 'revision') === false) {
                $this->db->Insert([[
                'post_id'          => $post->ID,
                'post_name'        => str_replace("'", '"', $post->post_title),
                'post_text'        => (str_replace("'", '"', $post->post_content)),
                'post_seo_title'   => (str_replace("'", '"', $post->post_title)),
                'short_desc'       => explode(".", strip_tags(trim(str_replace("'", '"', $post->post_content))))[0],
                'post_seo_desc'    => explode(".", strip_tags(trim(str_replace("'", '"', $post->post_content))))[0],
                'post_seo_title'   => str_replace("'", '"', $post->post_title),
                'date_created'     => $post->post_date,
                'post_category_id' => 2
                    ],
                    "powerplay_posts"
                ]);
                $getFile = function ($url, $postId = null) {
                    $fileName = end(explode("/", $url));
                    if ($fileName == "0.jpg") {
                        $fileName = $postId . ".jpg";
                    }
                    $newfile   = "./web/templates/Site/assets/images/Posts/" . "$fileName";
                    $userAgent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
                    $ch        = curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
                    curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
                    $output    = curl_exec($ch);
                    $fh        = fopen($newfile, 'w');
                    fwrite($fh, $output);
                    fclose($fh);
                    return "/web/templates/Site/assets/images/Posts/" . $fileName;
                };
                $img = $this->parseLink($post->guid)[0];
                if ((strpos($img, "youtube") !== false) or ( strpos($img, "vimeo") !== false) or ( strpos($img, "youtu.be") !== false)) {
                    $searchVideo = function ($link, $postId) {
                        $meta         = $link;
                        $youtubeVideo = function ($postId) use($meta) {
                            $code                            = end(explode("/", explode("?", $meta)[0]));
                            $this->postImg[$postId]['video'] = $meta;
                            $this->postImg[$postId]['img']   = self::youtubeTH . "$code/0.jpg";
                            return true;
                        };

                        $vimeoVideo = function ($postId) use($meta) {
                            $code                            = end(explode("/", $meta));
                            $hash                            = unserialize(file_get_contents(self::vimeoTH . "$code.php"));
                            $this->postImg[$postId]['video'] = self::vimeoPlayer . $code;
                            $this->postImg[$postId]['img']   = $hash[0]['thumbnail_medium'];
                            return true;
                        };
                        if (strpos($meta, "youtube") !== false) {
                            $youtubeVideo($postId);
                            echo "Video link generated(youtube)\n";
                        } elseif (strpos($meta, "youtu.be") !== false) {
                            $youtubeVideo($postId);
                            echo "Video link generated from short record(youtube)\n";
                        } else {
                            $vimeoVideo($postId);
                            echo "Video link generated(vimeo)\n";
                        }
                    };
                    $searchVideo($img, $post->ID);
                } else {
                    $this->postImg[$post->ID]['img'] = $img;
                }
                if ($this->postImg[$post->ID]['video']) {

                    $file = $getFile($this->postImg[$post->ID]['img'], $post->ID);
                    $this->db->Update([[
                    'post_img'    => $file,
                    'post_img_th' => $file,
                    'vimeo'       => $this->postImg[$post->ID]['video'],
                    'display'     => 1
                        ],
                        'powerplay_posts',
                        ['post_id' => $post->ID]
                    ]);
                    echo "Inserted video to the db from " . $this->postImg[$post->ID]['video'] . "\n";
                } elseif ($this->postImg[$post->ID]['img']) {
                    $file = $getFile($this->postImg[$post->ID]['img']);
                    $this->db->Update([[
                    'post_img'    => $file,
                    'post_img_th' => $file
                        ],
                        'powerplay_posts',
                        ['post_id' => $post->ID]
                    ]);
                    echo "Inserted image to the db from " . $this->postImg[$post->ID]['img'] . "\n";
                }
            }
            $percnet = $count / $arrayCount * 100;
            echo "Post " . $post->ID . " was inserted successfully\n";
            echo "$percnet\n";
            $count++;
        }

####
        if ($count === $arrayCount) {
            echo "All posts were moved successfully\n\n";
            return 1;
        } else {
            echo "Somthing hapend!!! Not all posts moved.\nCount of posts: " . $arrayCount . "\nMoved posts count: " . $count;
            return 0;
        }
    }

    /**
     * function for find an image on the site
     * @param string $link
     * @return array
     * @throws Exception
     */
    public function parseLink($link)
    {
        if (!$link) {
            throw new Exception('Link is empty');
        }

        $html    = file_get_html($link);
        $imgLink = ['/web/templates/Site/assets/images/bebinka_add.png'];
        if (!is_bool($html)) {
            foreach ($html->find("div.entry-media") as $div) {
                foreach ($div->find("img") as $image) {
                    if (!empty($image->src)) {
                        $imgLink[0] = str_replace("'", '"', $image->src);
                    }
                }
                if ($imgLink[0] == "/web/templates/Site/assets/images/bebinka_add.png") {
                    foreach ($div->find('iframe') as $video) {
                        $html       = $video->outertext;
                        $src        = explode('"', end(explode('src="', $html)))[0];
                        $imgLink[0] = $src;
                    }
                }
            }
        }

        return $imgLink;
    }

    /**
     * Function for finding post image or video
     * @param int $postId
     * @throws Exception
     */
    public function getImages($postId)
    {
        if (!is_int($postId)) {
            throw new Exception('Post od should be like int type');
        }
        $db   = new Database(Config::$host, Config::$user, Config::$password, $this->oldDBName);
        $q    = "select * from wp_postmeta where post_id = $postId";
        $db->Start();
        $meta = $db->Execute($q);
        if (is_array($meta)) {
            foreach ($meta as $met) {
                if ($met->meta_key == '_wp_attached_file') {
                    $searchImg = function () use($met) {
                        $this->postImg[$met->post_id]['img'] = self::SITENAME . $met->meta_value;
                    };
                    $searchImg();
                    echo "Image found(youtube)\n";
                } elseif (($met->meta_value == 'add_video_url')) {

                    $searchVideo();
                }
            }
        } else {
            echo "Error! We did not find data in the database!!! Maybe sothing wrong with SQL query!\n";
        }

        /**
         * 
         * @param object $meta
         */
    }

}

$importer = import::getInstanse();
$importer->__init__('', '', 31);
$res      = $importer->import($importer->getAllPosts());

if ($res > 0) {
    echo "\n\nWe've done export all posts";
} else {
    echo "\n\nError!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
}