<?php

error_reporting(0);

//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);

require_once './autoloader.php';
require_once './Modules/HTMLParser/simple_html_dom.php';
autoloader::addAutoloader('./mainautoloader.php');

use PowerPlay\Database;

class import_tags
{

    const OLDDBNAME = 'moon_restore';
    const NEWDBNAME = 'moonstore';

    /**
     *
     * @var Database
     */
    private $db;

    /**
     * Name of old database 
     * @var string
     */
    private $oldDBName;

    /**
     * Name of new database
     * @var string
     */
    private $newDBName;

    /**
     *
     * @var object of this class
     */
    private static $instanse;

    private function __construct()
    {
        
    }

    private function __clone()
    {
        
    }

    /**
     * Easy singelton pattern
     * @return \self
     */
    public static function getInstanse()
    {
        if (self::$instanse) {
            return self::$instanse;
        } else {
            return new self;
        }
    }

    public function __init__($newDBName, $oldDBName)
    {
        if (empty($oldDBName)) {
            $this->oldDBName = self::OLDDBNAME;
        } else {
            $this->oldDBName = $oldDBName;
        }
        #####
        if (empty($newDBName)) {
            $this->newDBName = self::NEWDBNAME;
        } else {
            $this->newDBName = $newDBName;
        }
        #####
        !$this->db ? $this->db = new Database(Config::$host, Config::$user, Config::$password, $this->newDBName) : '';
    }

    /**
     * Function for getting all tags from old db
     * @param int $postId
     * @throws Exception
     */
    public function getAllPostTags($postId)
    {
        /**
         * SELECT name
          FROM wp_term_relationships
          JOIN wp_term_taxonomy
          ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
          JOIN wp_terms
          ON wp_terms.term_id = wp_term_taxonomy.term_id
          WHERE object_id = '35806' AND taxonomy = 'post_tag'
         */
        if (!is_int($postId) or $postId == 0) {
            throw new Exception("Post id should be an int type");
        }
        $db   = new Database(Config::$host, Config::$user, Config::$password, $this->oldDBName);
        $q    = "SELECT name
                    FROM wp_term_relationships
                    JOIN wp_term_taxonomy
                    ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                    JOIN wp_terms
                    ON wp_terms.term_id = wp_term_taxonomy.term_id
                    WHERE object_id = '$postId' AND taxonomy = 'post_tag'";
        $db->Start();
        $tags = $db->Execute($q);
        return $tags;
    }

    /**
     * Function for checking a tag in the new database
     * @param string $tag
     * @return mixed
     * @throws Exception
     */
    public function chackTag($tag)
    {
        if (!$tag) {
            throw new Exception("Tag should be a string type");
        }
        $taginfo = $this->db->Select([
            '*',
            'tags',
            "tag_name like '%" . $tag . "%'"]);
        if (is_array($taginfo) and count($taginfo) > 0) {
            return $taginfo[0]->tag_id;
        } else {
            $this->db->Insert([
                [
                    'tag_name' => str_replace("'", '"', $tag)
                ],
                'tags'
            ]);
            return $this->db->LastInsert();
        }
        return $taginfo;
    }

    /**
     * function for insirting tags for post
     * @param int $tagId
     * @param int $postId
     * @throws Exception
     */
    public function tagToPost($tagId, $postId)
    {
        if (!$tagId or ! $postId) {
            throw new Exception("It should be int typo!!!");
        }
        $checking = $this->db->Select([
            '*',
            'posts_to_tags',
            [
                'tag_id'  => $tagId,
                'post_id' => $postId
            ]
        ]);
        if (is_array($checking) and count($checking) > 0) {
            echo "Tag alredy exist for this post $postId\n\n";
        } else {
            $this->db->Insert([
                [
                    'tag_id'  => $tagId,
                    'post_id' => $postId
                ],
                'posts_to_tags'
            ]);
        }
    }

    /**
     * function for getting all posts in the db
     * @return type
     */
    public function getPosts()
    {
        return $this->db->SelectAll('powerplay_posts');
    }

    public function import()
    {
        $posts     = $this->getPosts();
        $percent   = 0;
        $countpost = count($posts);
        if (count($posts) > 0) {
            foreach ($posts as $post) {
                $tags = $this->getAllPostTags((int) $post->post_id);
                if (is_array($tags)) {
                    foreach ($tags as $tag) {
                        $tagId = $this->chackTag(str_replace("'", '"', $tag->name));
                        $this->tagToPost($tagId, $post->post_id);
                        echo "Tag " . $tag->name . " was imported for " . $post->post_name . "\n";
                        echo "Working: " . (($percent / $countpost) * 100) . "\n";
                        $percent++;
                    }
                }
            }
        }
    }

}

$import = import_tags::getInstanse();
$import->__init__('', '');
$import->import();
