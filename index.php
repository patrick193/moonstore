<?php

////
//ini_set('error_reporting', E_ALL);
//	ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);

/*
 * полублок и блок задача для статей
 * 
 * страница статьи правый блок по желанию
 * 
 * теги в статье
 * 
 * timer for publish in the posts
 * 
 * 
 * 
 * 
 */


date_default_timezone_set('UTC');
require_once './autoloader.php';

try {

    $m = new PowerPlay\Module();
    $m->getBasicModuleObject();
    $session = new PowerPlay\Session();
    $storage = new PowerPlay\Storage();

//        $session->DestroySession();
//        $storage->UnsetStorage();
//        die;
    $rules = new Modules\Roles\Users();
    $lang = new Modules\Language\Language();
    $routing = new PowerPlay\Route();

//    $user = $rules->Load(['user_id', 1]);
//    $session->set('user_auth', serialize($user));
//    die;
    $user = @unserialize($session->get('user_auth'));
    if($user) {
        $userLanguage = $lang->FindUserLanguage((int)$user->getUserId());
        $session->set('language', $userLanguage);
        $lang->DefineLanguage($userLanguage);

        $role = new \Modules\Roles\Roles();
        $code = $role->FindById((int) $user->getRoleId());

        $route = $routing->Route($code->getRoleCode());
    } else {
        $session->set('language', 're-RU');
        $lang->DefineLanguage('re-RU');

        $route = $routing->Route();
    }

    $route['module']->getModule()->doAction($route['info']);
} catch(Modules\PowerplayException\PowerplayException $ex) {
    echo '<p style="color:red"> Error: ' . $ex->getMessage() . "</p>";
    $e = microtime();
    echo $e - $s;
    exit;
}