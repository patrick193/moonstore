<?php

function mainautoloader($className)
{
    $className = __DIR__ . '/' . str_replace('\\', '/', $className) . '.php';
    $className = str_replace("Override", "src", $className);
    if (file_exists($className)) {
        require_once $className;
        return true;
    }
    return false;
}

spl_autoload_register('mainautoloader');
