-- phpMyAdmin SQL Dump
-- version 4.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 24, 2015 at 06:21 AM
-- Server version: 10.0.20-MariaDB
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `moonstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `ip_block`
--

CREATE TABLE IF NOT EXISTS `ip_block` (
  `id` int(11) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `isLocked` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ip_block`
--

INSERT INTO `ip_block` (`id`, `ip`, `isLocked`) VALUES
(1, '127.0.0.1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `moonstore_aligns`
--

CREATE TABLE IF NOT EXISTS `moonstore_aligns` (
  `align_id` int(11) NOT NULL,
  `align_name` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `moonstore_aligns`
--

INSERT INTO `moonstore_aligns` (`align_id`, `align_name`) VALUES
(3, 'Центр'),
(4, 'По левой стороне');

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE IF NOT EXISTS `positions` (
  `position_id` int(11) NOT NULL,
  `position_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `count` int(2) NOT NULL DEFAULT '3'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`position_id`, `position_name`, `count`) VALUES
(2, 'Полный', 2),
(3, 'Не полный', 4);

-- --------------------------------------------------------

--
-- Table structure for table `posts_category`
--

CREATE TABLE IF NOT EXISTS `posts_category` (
  `posts_category_id` int(11) NOT NULL,
  `category_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `category_desc` text CHARACTER SET utf8 NOT NULL,
  `category_seo_title` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `category_seo_desc` text CHARACTER SET utf8 NOT NULL,
  `isLocked` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts_category`
--

INSERT INTO `posts_category` (`posts_category_id`, `category_name`, `category_desc`, `category_seo_title`, `category_seo_desc`, `isLocked`) VALUES
(1, 'Мода', '', '', '', 0),
(2, 'Искусство', '2', '2', '', 1),
(6, 'Партнеры', 'описание', 'название', 'описание', 0),
(9, 'MSs (Moon Store studio)', '', 'lastTest', 'lastTest', 0);

-- --------------------------------------------------------

--
-- Table structure for table `posts_to_tags`
--

CREATE TABLE IF NOT EXISTS `posts_to_tags` (
  `tag_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts_to_tags`
--

INSERT INTO `posts_to_tags` (`tag_id`, `post_id`) VALUES
(1, 14),
(2, 22),
(1, 21);

-- --------------------------------------------------------

--
-- Table structure for table `post_comments`
--

CREATE TABLE IF NOT EXISTS `post_comments` (
  `comment_id` int(11) NOT NULL,
  `comment` text CHARACTER SET utf8 NOT NULL,
  `date` datetime NOT NULL,
  `published` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post_comments`
--

INSERT INTO `post_comments` (`comment_id`, `comment`, `date`, `published`, `post_id`, `ip`) VALUES
(2, 'comment1', '2015-10-08 20:10:15', 1, 2, '127.0.0.1'),
(3, 'comment2', '2015-10-08 20:10:32', 1, 2, '127.0.0.1'),
(4, 'comment3', '2015-10-08 20:10:01', 1, 1, '127.0.0.1'),
(5, 'comment4', '2015-10-08 20:10:08', 1, 1, '127.0.0.1'),
(6, 'comment5', '2015-10-08 20:10:13', 1, 2, '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `post_position`
--

CREATE TABLE IF NOT EXISTS `post_position` (
  `post_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `post_types`
--

CREATE TABLE IF NOT EXISTS `post_types` (
  `type_id` int(11) NOT NULL,
  `type_name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post_types`
--

INSERT INTO `post_types` (`type_id`, `type_name`) VALUES
(1, 'post'),
(2, 'menu');

-- --------------------------------------------------------

--
-- Table structure for table `powerplay_groups`
--

CREATE TABLE IF NOT EXISTS `powerplay_groups` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `powerplay_groups`
--

INSERT INTO `powerplay_groups` (`group_id`, `group_name`) VALUES
(1, 'internal'),
(2, 'external');

-- --------------------------------------------------------

--
-- Table structure for table `powerplay_language`
--

CREATE TABLE IF NOT EXISTS `powerplay_language` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(100) NOT NULL DEFAULT 'Germany',
  `language_code` varchar(15) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `powerplay_language`
--

INSERT INTO `powerplay_language` (`language_id`, `language_name`, `language_code`) VALUES
(1, 'Russion', 're-RU'),
(2, 'English', 'en-GB');

-- --------------------------------------------------------

--
-- Table structure for table `powerplay_posts`
--

CREATE TABLE IF NOT EXISTS `powerplay_posts` (
  `post_id` int(11) NOT NULL,
  `post_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `post_text` text CHARACTER SET utf8 NOT NULL,
  `post_img` varchar(255) NOT NULL DEFAULT '',
  `post_img_th` varchar(255) NOT NULL DEFAULT '',
  `post_category_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_redaction` datetime DEFAULT NULL,
  `redactor_id` int(11) NOT NULL DEFAULT '0',
  `post_seo_desc` text CHARACTER SET utf8 NOT NULL,
  `post_seo_title` text CHARACTER SET utf8 NOT NULL,
  `isLocked` int(11) NOT NULL DEFAULT '0',
  `short_desc` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `type_id` int(11) NOT NULL DEFAULT '3',
  `position_id` int(11) NOT NULL DEFAULT '2'
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `powerplay_posts`
--

INSERT INTO `powerplay_posts` (`post_id`, `post_name`, `post_text`, `post_img`, `post_img_th`, `post_category_id`, `created_by`, `date_created`, `date_redaction`, `redactor_id`, `post_seo_desc`, `post_seo_title`, `isLocked`, `short_desc`, `type_id`, `position_id`) VALUES
(1, 'test post namedasdas', '<p>pohknghpokndpo</p>', '/web/templates/Site/assets/images/Posts/8589130591340-evil-angel-the-white-rose-wallpaper-hd_7092576893.jpg', '/web/templates/Site/assets/images/Posts/thumb_dragon-wallpaper-dragons-13975568-1280-800_4178117723.jpg', 1, 8, '2015-09-20 14:09:42', '2015-11-26 21:11:46', 1, 'sdasdasdasdasdasd', 'sadasdasda', 1, 'TestTestTestTestTestTest', 1, 2),
(3, 'test post name', '<p>test post textqweqwe</p>', '/web/templates/Site/assets/images/Posts/2_5375326834.jpg', '/web/templates/Site/assets/images/Posts/thumb_2_5375326834.jpg', 1, 8, '2015-09-20 19:09:02', '2015-12-23 23:12:21', 1, 'seo desc', 'seo title', 0, 'TestTestTestTestTestTest', 3, 2),
(5, 'test post name', 'test post textqweqwe', '/web/templates/Site/assets/images/Posts/8589130591340-evil-angel-the-white-rose-wallpaper-hd_7092576893.jpg', '/web/templates/Site/assets/images/Posts/thumb_dragon-wallpaper-dragons-13975568-1280-800_4178117723.jpg', 1, 8, '2015-09-20 19:09:02', NULL, 0, 'seo desc', 'seo title', 0, 'TestTestTestTestTestTest', 1, 2),
(6, 'test post name', 'test post textqweqwe', '/web/templates/Site/assets/images/bebinka_add.png', '/web/templates/Site/assets/images/Posts/thumb_dragon-wallpaper-dragons-13975568-1280-800_4178117723.jpg', 1, 8, '2015-09-20 19:09:02', NULL, 0, 'seo desc', 'seo title', 0, 'TestTestTestTestTestTest', 1, 2),
(10, 'Новая тестовая статья', '<p><img src="../../../web/templates/Admin/img/Risunok1_12.png" alt="" /></p>', '/web/templates/Site/assets/images/Posts/1_4588854210.jpg', '/web/templates/Site/assets/images/Posts/thumb_1_4588854210.jpg', 9, 1, '2015-11-08 19:11:13', '2015-12-02 22:12:15', 1, 'Новая тестовая статья', 'Новая тестовая статья', 0, 'TestTestTestTestTestTest', 1, 3),
(11, 'Video test', '<p style="text-align: center;">хахахахахахахаха</p>\r\n<div class="youtube"><object data="https://www.youtube.com/embed/0Rs-XCrxUpY&amp;modestbranding=1" type="application/x-shockwave-flash" width="640" height="385"><param name="movie" value="https://www.youtube.com/embed/0Rs-XCrxUpY&amp;modestbranding=1" /><param name="wmode" value="transparent" /></object></div>', '/web/templates/Site/assets/images/Posts/8589130591340-evil-angel-the-white-rose-wallpaper-hd_7092576893.jpg', '/web/templates/Site/assets/images/Posts/thumb_8589130591340-evil-angel-the-white-rose-wallpaper-hd_7092576893.jpg', 1, 1, '2015-11-08 20:11:01', '2015-12-02 21:12:16', 1, 'video from youtube', 'video test', 0, 'TestTestTestTestTestTest', 1, 3),
(12, 'Новая тестовая статья2', '<p style="text-align: center;"><strong>Новая тестовая статья2</strong></p>\r\n<p style="text-align: center;">&nbsp;</p>\r\n<p style="text-align: justify;"><strong><img src="../../web/templates/Admin/img/posts/8589130591340-evil-angel-the-white-rose-wallpaper-hd.jpg" alt="" width="433" height="325" /></strong></p>', '/web/templates/Site/assets/images/Posts/8589130591340-evil-angel-the-white-rose-wallpaper-hd_7092576893.jpg', '/web/templates/Site/assets/images/Posts/thumb_8589130591340-evil-angel-the-white-rose-wallpaper-hd_7092576893.jpg', 6, 1, '2015-11-11 21:11:23', '2015-12-02 22:12:58', 1, '', '', 0, 'TestTestTestTestTestTest', 1, 3),
(13, 'Test', '<p>asdasdasdsa</p>\r\n<p>asdasdasdsa</p>\r\n<p>asdasdasdsa</p>\r\n<p>asdasdasdsa</p>\r\n<p>asdasdasdsa</p>', '/web/templates/Site/assets/images/Posts/8589130591340-evil-angel-the-white-rose-wallpaper-hd_7092576893.jpg', '/web/templates/Site/assets/images/Posts/thumb_8589130591340-evil-angel-the-white-rose-wallpaper-hd_7092576893.jpg', 2, 1, '2015-11-15 20:11:41', NULL, 0, '', '', 0, 'short_desc', 1, 2),
(14, 'Новая тестовая статья1й', '<section class="faster-web">\r\n<h2>Швидший перегляд веб-сторінок</h2>\r\n<img class="hi-dpi section-hero" src="https://www.google.com/intl/uk_ALL/chrome/assets/common/images/marquee/benefits-2.jpg" alt="" />\r\n<div class="g-tpl-nest">\r\n<div class="g-unit g-col-6">\r\n<h3>Миттєвий пошук</h3>\r\n<p class="separator">Шукайте та відкривайте сайти в тому самому вікні. Результати й пропозиції, зокрема останні пошукові запити й відвідані веб-сайти, з&rsquo;являються ще під час введення запиту, тож ви можете миттєво відкривати їх. <a class="links" href="https://support.google.com/chrome/answer/95440">Про пошук з універсального вікна пошуку</a></p>\r\n</div>\r\n<div class="g-unit g-col-6">\r\n<h3>Друкуйте менше</h3>\r\n<p class="separator">Набридло постійно вводити у веб-формах ту саму інформацію? Заповнюйте їх автоматично лише одним кліком! Функція автозаповнення працює на всіх пристроях, тому вам більше не доведеться вводити текст на маленьких екранах. <a class="links" href="https://support.google.com/chrome/answer/142893">Про автозаповнення</a></p>\r\n</div>\r\n</div>\r\n</section>\r\n<section class="pick-up">\r\n<h2>Усе важливе під рукою</h2>\r\n<p class="desktop-only separator">Завдяки Chrome можна переносити всі свої відкриті вкладки, закладки й останні пошукові запити з комп&rsquo;ютера на телефон або планшет чи навпаки. Так ви отримуєте свою історію веб-перегляду на всіх ваших пристроях. Щоб почати синхронізацію, просто ввійдіть у свій обліковий запис на інших пристроях. <a class="links" href="https://support.google.com/chrome/answer/2392075">Докладніше</a></p>\r\n<img class="empty-area hi-dpi section-hero" src="https://www.google.com/intl/uk_ALL/chrome/assets/common/images/marquee/benefits-1.jpg" alt="" /></section>\r\n<section class="smarter-web">\r\n<h2>Максимум можливостей</h2>\r\n<p class="desktop-only separator">Працюйте в Chrome і отримуйте найкращі можливості від Google. Chrome та Google працюють разом, щоб надавати вам найкращі пропозиції й функції в усіх продуктах Google, таких як Голосовий пошук і Google Асистент. <a class="links" href="https://www.google.com/landing/now/#getitchrome">Увімкнути Google Асистент</a></p>\r\n<img class="empty-area hi-dpi section-hero" src="https://www.google.com/intl/uk_ALL/chrome/assets/common/images/marquee/benefits-3.jpg" alt="" /></section>\r\n<section class="make-yours">\r\n<h2>Ваш персональний Chrome</h2>\r\n<p class="desktop-only separator">Насолоджуйтеся переглядом веб-сторінок із темами, додатками та розширеннями Chrome. Завдяки закладам і початковим сторінкам ви можете швидко відкривати улюблені сайти. Щойно ви налаштуєте Chrome, ці налаштування синхронізуються з усіма вашими пристроями. <a class="links" href="https://chrome.google.com/webstore/category/themes">Почати налаштування</a></p>\r\n<img class="empty-area hi-dpi section-hero" src="https://www.google.com/intl/uk_ALL/chrome/assets/common/images/marquee/benefits-4.jpg" alt="" /></section>\r\n<section class="g-tpl-nest desktop-only get-chrome">\r\n<div class="g-unit g-col-7"><img class="empty-area hi-dpi section-hero" src="https://www.google.com/intl/uk_ALL/chrome/assets/common/images/marquee/benefits-5-mobile.png" alt="" /></div>\r\n<div class="g-unit g-col-5 chrome-promo">\r\n<h2 class="content-linux">Завантажити Chrome для ОС Linux</h2>\r\n<p class="browser-promo">Один веб-переглядач для вашого ноутбука, телефона та планшета</p>\r\n</div>\r\n</section>', '/web/templates/Site/assets/images/Posts/8589130591340-evil-angel-the-white-rose-wallpaper-hd_7092576893.jpg', '/web/templates/Site/assets/images/Posts/thumb_8589130591340-evil-angel-the-white-rose-wallpaper-hd_7092576893.jpg', 1, 1, '2015-11-15 20:11:15', '2015-12-20 21:12:03', 1, 'Новая тестовая статья222', '2123123123', 0, '2123123123', 3, 2),
(16, 'dsfsdfsd', '<div class="ttext_main_left">\r\n<div class="printicon" style="margin-left: 5px; width: 20px; float: right; margin-top: 9px; text-align: left;"><a href="http://nbnews.com.ua/ru/news/166338/print/"><img title="Распечатать" src="http://nbnews.com.ua/ru/img/print.gif" alt="" width="15px" align="absmiddle" border="0" /></a>&nbsp;</div>\r\n<div id="fonttextdesc" class="text_desc" style="margin-bottom: 10px; padding-bottom: 10px; width: 93% !important;"><img src="http://nbnews.com.ua/ru/img/spacer.gif" alt="" height="33px" />sad\r\n<h2>Следственное управление прокуратуры города Киева завершило расследование в уголовных производствах по делу судей Печерского районного суда города Киева Оксаны Царевич и Виктора Кицюка. Об этом в среду, 18 июля, сообщает пресс-служба Прокуратури Киева.</h2>\r\n<h2>&nbsp;</h2>\r\n</div>\r\n<div id="fonttext" class="_ga1_on_ text">&laquo;Следственным управлением прокуратуры города Киева завершено досудебное расследование в уголовных процессах по подозрению судей Печерского районного суда города Киева О. Царевич и В. Кицюка в совершении уголовных правонарушений, предусмотренных ч. 2 ст. 375 (вынесение судьей заведомо неправосудного решения) УК Украины&raquo;, &ndash; сказано в сообщении.<br /><br />Во время досудебного расследования было установлено, что эти судьи в январе 2014 года вынесли заведомо неправосудные постановления о привлечении к административной ответственности активистов Автомайдана. <br /><br />В настоящее время материалы предоставлены сторонам защиты для ознакомления, после чего обвинительные акты будут направлены в суд.<br /><br />Отмечается, что судья Кицюк уже ознакомился с материалами производства, сейчас их изучают его защитники. Оксана Царевич продолжает ознакомление.<br /><br />Напомним, 4 марта Верховная Рада дала согласие на задержание и арест трех судей Печерского районного суда Киева &ndash; Сергея Вовка, Виктора Кицюка и Оксаны Царевич. <br /><br />11 марта Винницкий городской суд избрал меру пресечения для Оксаны Царевич в виде личного обязательства с требованием носить электронное средство контроля. <br /><br />13 марта Винницкий городской суд частично удовлетворил ходатайство прокуратуры и избрал для судьи Печерского райсуда Киева Виктора Кицюка меру пресечения в виде личного обязательства с ношением электронного средства на два месяца. <br /><br />Царевич и Кицюк подозреваются в том, что вынесли заведомо неправосудные постановления по делам об административном правонарушении в отношении активистов Автомайдана и в других делах.</div>\r\n</div>', '/web/templates/Site/assets/images/Posts/screenshot from 2015-11-08 01-11-40_4005046081.png', '/web/templates/Site/assets/images/Posts/thumb_screenshot from 2015-11-08 01-11-40_4005046081.png', 1, 1, '2015-11-18 23:11:35', '2015-11-26 22:11:41', 1, 'dfsdfd', 'dsfsdfsd', 0, 'dfsdfd', 1, 2),
(17, 'Новая тестовая статья', '', '/web/templates/Site/assets/images/Posts/mhmzmdfyibbqqhglqrvx_1867818995.jpg', '/web/templates/Site/assets/images/Posts/thumb_mhmzmdfyibbqqhglqrvx_1867818995.jpg', 1, 1, '2015-11-18 23:11:57', '2015-12-23 22:12:09', 1, 'ads', 'Новая тестовая статья', 0, 'ads', 4, 3),
(18, 'Новая тестовая статья', '', '/web/templates/Site/assets/images/Posts/2_2416309588.jpg', '/web/templates/Site/assets/images/Posts/1477171_5761887067.png', 2, 1, '2015-11-18 23:11:06', '2015-12-02 22:12:20', 1, 'ads', 'Новая тестовая статья', 0, 'ads', 1, 3),
(19, 'asdasdas', '<h3 style="text-align: justify;">Россия под управлением Путина не представляет&nbsp;для Польши&nbsp;угрозы</h3>\r\n<p style="text-align: justify;">Лидер польской партии &laquo;Конгресс новых правых&raquo; и евродепутат Януш Корвин-Микке опасается больше всего Германии и Украины из-за их &laquo;территориальных претензий&raquo; к Польше, а Россия на сегодня не представляет никакой угрозы для поляков. Такое мнение депутат выразил в эфире телеканала <a href="http://www.tvn24.pl/korwin-mikke-w-tvn24-ukraina-jest-naszym-wrogiem-nie-rosja,585383,s.html" target="_blank">TVN24</a>.</p>\r\n<p style="text-align: justify;">&laquo;Я всегда говорил, что, как только восстанет Украина, изменится геополитика. Изменилась, а люди и дальше считают Россию врагом. Это Украина враг, а не Россия. Там сейчас голосуют за &laquo;бандеровцев&raquo;, &mdash; заявил польский политик.</p>\r\n<p style="text-align: justify;">Однако ведущий, в свою очередь, возразил депутату, указав, что за так называемых бандеровцев голосует несколько процентов украинских избирателей.</p>\r\n<p style="text-align: justify;">Кроме того, по словам Корвина-Микке, Германия и Украина имеют территориальные претензии к Польше, хотя у Варшавы подобных настроений нет.</p>\r\n<div class="wp-video" style="width: 640px;"><span class="mejs-offscreen">Video Player</span>\r\n<div id="mep_0" class="mejs-container svg wp-video-shortcode mejs-video" style="width: 640px; height: 360px;" tabindex="0">\r\n<div class="mejs-inner">\r\n<div class="mejs-mediaelement"><video id="video-84556-1" class="wp-video-shortcode" src="http://r.dcs.redcdn.pl/http/o2/tvn/web-content/m/p1/v/d010396ca8abf6ead8cacc2c2f2f26c7/20d205b7-cf47-4e88-9038-4c82af72da99-480p.mp4?_=1" preload="metadata" width="640" height="360"></video></div>\r\n<div class="mejs-layers">&nbsp;</div>\r\n<div class="mejs-controls" style="display: block; visibility: visible;">&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class="mejs-time mejs-currenttime-container"><span class="mejs-currenttime">00:00</span></div>\r\n<div class="mejs-time-rail" style="width: 496px;">&nbsp;</div>\r\n<div class="mejs-time mejs-duration-container"><span class="mejs-duration">00:45</span></div>\r\n<div class="mejs-button mejs-volume-button mejs-mute">&nbsp;</div>\r\n<div class="mejs-button mejs-volume-button mejs-mute">&nbsp;</div>\r\n<div class="mejs-button mejs-fullscreen-button">&nbsp;</div>\r\n<div class="wp-video" style="width: 640px;">&nbsp;</div>\r\n<p>Как известно, основатель и лидер польской партии &laquo;Конгресс новых правых&raquo; Януш Корвин-Микке&nbsp;ранее <a href="http://politeka.net/61100-deputaty-treh-partij-polshi-namereny-posetit-okkupirovannyj-krym/" target="_blank">неоднократно выступал</a> в поддержку агрессивной в отношении Украины российской политики.</p>\r\n<p style="text-align: justify;">Напомним, в августе текущего года скандальный&nbsp;евродепутат от Польши&nbsp;заявил, что польские парламентарии изъявили желание <a href="http://politeka.net/61100-deputaty-treh-partij-polshi-namereny-posetit-okkupirovannyj-krym/" target="_blank">посетить оккупированный Крым</a>.</p>', '/web/templates/Site/assets/images/Posts/2_2416309588.jpg', '/web/templates/Site/assets/images/Posts/thumb_1477171_5761887067.png', 1, 1, '2015-11-18 23:11:22', '2015-11-19 00:11:35', 1, 'asdasdas', 'asdasdas', 0, 'asdasdas', 1, 2),
(20, 'asdasd', '', '/web/templates/Site/assets/images/Posts/2_2416309588.jpg', '/web/templates/Site/assets/images/Posts/thumb__ksp9kytuf0_8398912823.jpg', 1, 1, '2015-11-18 23:11:48', '2015-11-18 23:11:48', 0, 'ds', 'asdasd', 0, 'ds', 1, 2),
(21, 'adsasd', '<h3 class="undertitle">Пока Владимир Путин продолжает внушать всем страх возможным применением ядерного оружия &ndash; этим объясняется в основном пассивность и робость западных лидеров, от Вашингтона до Берлина, они же демонстрируют активность и смелость в подготовке к переделу сфер влияния, после того как региональная держава исчерпает свои ресурсы. Кроме того что Китай уже получает от России часть ее территории, Пекин готовится занять место Москвы в G8. Интрига ведущейся большой игры даже не в сроках &ndash; когда РФ в ее нынешних границах прекратит существование, а в том, кто заполнит вакансию регионального лидера</h3>\r\n<p>В группу G7, которая представляет собой закрытый клуб, входят США, Канада, Великобритания, Германия, Франция, Италия и Япония. Примечательно, что впервые лидеры шести государств западного мира (в отсутствие Канады, присоединившейся годом позже) собрались осенью 1975 года, через несколько месяцев после подписания 1 августа Хельсинкского заключительного акта, закрепляющего политические и территориальные итоги Второй мировой войны.</p>\r\n<p>В 1997 году, через шесть лет после поражения <abbr class="tooltip"><u>СССР</u></abbr> в холодной войне, в целях неформального подтверждения новой геополитической реальности, состоялось оформление G7 в G8. Российская Федерация получила одновременно моральную компенсацию за потерю 12 союзных республик и аванс на будущее &ndash; ни на что не влиять, но набирать политический вес для решения экономических вопросов в кругу ведущих мировых держав. Когда весной 2014 года РФ нарушила целый ряд международных договоров аннексией Крыма, ее членство в Группе восьми было прекращено.</p>\r\n<p>Собственно, ничто не мешает G7 сохранять статус-кво и не принимать новое государство в свой узкий круг. Однако с учетом новой реальности, в которой одну из ведущих ролей играет Китай, существует вероятность того, что в обиход вернется формат G8. Ожидаются два информационных повода: 40-летие первой встречи в верхах Группы шести, прошедшей 15-17 ноября 1975 года, или 40-летие образования Группы семи 27-28 июня 1976 года. Но можно обойтись и без соблюдения таких формальностей. Тем более что специально, по случаю годовщины, саммит проводиться не будет.</p>\r\n<p>Достаточно задаться вопросом, какая страна входит в число трех крупнейших экономик мира, но не является членом закрытого клуба ведущих мировых держав. И получить ответ &ndash; это Китай, который находится в компании США и Японии. Кроме того, зафиксировать статус G7 в ее классическом варианте &ndash; это значит не воспользоваться шансом лишний раз показать Кремлю его место на карте. А сейчас от таких предложений не принято отказываться.</p>\r\n<p>И еще: так как G7 &ndash; это не Лига чемпионов, где право играть нужно подтверждать каждый год, в &laquo;Большой семерке&raquo; по-прежнему находится Италия, которую по большинству экономических показателей превзошла Бразилия, а Индия наступает на пятки. Так что G8 &ndash; быть. С Китаем. Но не G10 с Бразилией и Индией.</p>\r\n<p>Еще одно место, которое освободит РФ в международных организациях, &ndash; это Совет Безопасности <abbr class="tooltip"><u>ООН</u></abbr>. План создания Организации Объединенных Наций рождался в ходе Второй мировой войны и является пережитком послевоенного раздела мира, в котором принимал участие один диктатор &ndash; Иосиф Сталин, а другой &ndash; Адольф Гитлер &ndash; при всем желании не смог бы потребовать для Германии кресло, занимаемое Францией. Великобритания, Китай, РФ, Франция, США &ndash; так выглядит состав постоянных членов Совбеза <abbr class="tooltip"><u>ООН</u></abbr>, который отвечает в мировом масштабе за поддержание международного мира и безопасности. Но именно бездеятельность <abbr class="tooltip"><u>ООН</u></abbr> и его Совета Безопасности в исполнении уставных задач &ndash; поддержание мира во всем мире &ndash; позволяет мировому сообществу так же считаться с ним, как он учитывает мнения пострадавшей стороны во многих конфликтах послевоенной истории. Потому <abbr class="tooltip"><u>ООН</u></abbr> и продолжает существовать, но ни на что не влияет, а представляет собой дискуссионную площадку для обмена мнениями на самом высоком уровне.</p>\r\n<p>Так что если реформа <abbr class="tooltip"><u>ООН</u></abbr> состоится или организация будет распущена и на ее руинах будет формироваться новая, соответствующая вызовам времени и отвечающая требованиям текущего момента, то она учтет не только ошибки, допущенные в прошлом, но и ближайшие перспективы развития человечества. Ни одному из этих гипотетических требований Россия не отвечает. А с учетом поражения <abbr class="tooltip"><u>СССР</u></abbr> в холодной войне вообще трудно объяснить место РФ в Совбезе <abbr class="tooltip"><u>ООН</u></abbr>, которое никак не соответствует ее реальному весу. Если брать в учет наличие ядерного оружия, то в таком случае почему не Пакистан или Индия, а Россия занимает кресло в одном из шести главных органов <abbr class="tooltip"><u>ООН</u></abbr>?</p>\r\n<p>Впрочем, и без роспуска <abbr class="tooltip"><u>ООН</u></abbr> может провести реформы своих руководящих органов. Раз уж зашла речь о ядерном статусе, логично было бы отдать одно место в Совбезе государству, которое добровольно отказалось от оружия массового поражения: Беларусь, Казахстан, Украина. Тем более что и Беларусь, и Украина находятся среди основателей <abbr class="tooltip"><u>ООН</u></abbr>, в отличие от РФ.</p>\r\n<p>Место и статус РФ в современном мире точно определил президент США Барак Обама, в марте 2015 года назвавший Россию &laquo;региональной державой&raquo;, влияние которой распространяется только на ближайшие страны региона.</p>\r\n<p>В контексте юридического отказа России от части территорий под видом передачи в аренду Китаю 200 тыс. гектаров земли сельскохозяйственного назначения, невозможности содержать аннексированный Крым без ущерба для пенсионеров и работников бюджетной сферы, а также всегда готовых взорваться очагов напряженности на Кавказе не представляется фантастической картина будущего распада РФ на неизвестное количество государств. Но еще в период существования России в ее нынешних границах можно делать прогнозы, какая страна займет вакансию региональной державы. К западу от Урала, разумеется. С восточной частью РФ все ясно.</p>\r\n<p>Таких кандидатов на сегодня два.</p>\r\n<p>18 октября германский канцлер Ангела Меркель в ходе переговоров с турецким премьер-министром Ахметом Давутоглу обещала ускорить процесс вступления Турции в Европейский союз. Среди множества последствий, к которым может привести такое усиление Турции, &ndash; это готовность Анкары заполнить вакансию регионального лидера, которая освободится после того, как несостоятельность России перестанет вызывать сомнения, но породит многие вопросы.</p>\r\n<p>А так как турецкое влияние не сможет распространиться на все страны Восточной Европы &ndash; опять же, в силу целого ряда причин, то свое слово будет готова сказать Польша. Страна, имевшая опыт существования в составе Российской империи, имеющая границы с государствами, возникшими на месте бывшего <abbr class="tooltip"><u>СССР</u></abbr>, демонстрирующая внятную внешнюю политику, получает исторический шанс вернуть себе возможности, которых у нее не было со времен Речи Посполитой.</p>\r\n<p>Что с Украиной будет, когда Россия прекратит существование &ndash; зависит от Киева и Вашингтона. Самостоятельно Украина не сможет достичь уровня развития, позволяющего претендовать на роль регионального лидера. Хотя бы потому, что без внешнего вмешательства не в состоянии выработать ни внутреннюю политику, ни построить эффективную экономику.</p>', '/web/templates/Site/assets/images/Posts/_ksp9kytuf0_8398912823.jpg', '/web/templates/Site/assets/images/Posts/thumb__ksp9kytuf0_8398912823.jpg', 1, 1, '2015-11-19 00:11:52', '2015-11-19 00:11:59', 1, 'asdasdas', 'adsasd', 0, 'asdasdas', 1, 2),
(22, 'LastTest', '<p>фывывфывфывф</p>', '/web/templates/Site/assets/images/Posts/jpxkymwmw-0_4590556160.jpg', '/web/templates/Site/assets/images/Posts/thumb_jpxkymwmw-0_4590556160.jpg', 1, 1, '2015-11-21 18:11:41', '2015-11-21 18:11:48', 1, 'Новая тестовая опять Новая тестовая опять', 'LastTest', 0, 'Новая тестовая опять Новая тестовая опять', 1, 2),
(23, 'Ну давай', '', '/web/templates/Site/assets/images/Posts/', '', 1, 1, '2015-11-24 22:11:19', '2015-11-24 22:11:19', 0, 'Ну давай', 'Ну давай', 0, 'Ну давай', 1, 2),
(24, 'Ну давай', '<ul>\r\n<li>dfsfsdfdsfsd</li>\r\n</ul>', '/web/templates/Site/assets/images/Posts/', '', 1, 1, '2015-11-24 23:11:10', '2015-11-24 23:11:50', 1, 'Ну давай', 'Ну давай', 0, 'Ну давай', 1, 2),
(25, 'Ну давай', '', '/web/templates/Site/assets/images/Posts/', '', 1, 1, '2015-11-24 23:11:23', '2015-11-24 23:11:23', 0, 'Ну давай', 'Ну давай', 0, 'Ну давай', 1, 2),
(26, 'asdasdasd', '<ol>\r\n<li style="text-align: center;">asdasdasdasd</li>\r\n</ol>', '/web/templates/Site/assets/images/Posts/', '', 1, 1, '2015-11-24 23:11:39', '2015-11-24 23:11:39', 0, 'asdasda', 'asdasdasd', 0, 'asdasda', 1, 2),
(27, 'test on the servert', '<p><a style="color: greenyellow;" href="http://dev.moonstore.it/admin/post/edit/28">test on the servert</a></p>\r\n<p><a style="color: greenyellow;" href="http://dev.moonstore.it/admin/post/edit/28">test on the servert</a></p>', '/web/templates/Site/assets/images/bebinka_add.png', '/web/templates/Site/assets/images/bebinka_add.png', 1, 1, '2015-12-13 12:12:23', '2015-12-20 20:12:19', 1, 'test on the servert', 'test on the servert', 0, 'test on the servert', 4, 3),
(28, 'test on the servert2', '<p><a style="color: greenyellow;" href="http://dev.moonstore.it/admin/post/edit/28">test on the servert</a></p>', '/web/templates/Site/assets/images/bebinka_add.png', '/web/templates/Site/assets/images/bebinka_add.png', 6, 1, '2015-12-13 12:12:10', '2015-12-13 12:12:10', 0, 'test on the servert', 'test on the servert2', 0, 'test on the servert', 1, 3),
(29, 'testttttttttestttttttttestttttttt', '<p>testttttttttestttttttttestttttttt</p>', '/web/templates/Site/assets/images/bebinka_add.png', '/web/templates/Site/assets/images/bebinka_add.png', 1, 1, '2015-12-22 21:12:36', '2015-12-22 21:12:36', 0, 'testttttttttestttttttttestttttttt', 'testttttttttestttttttttestttttttt', 0, 'testttttttttestttttttttestttttttt', 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `powerplay_roles`
--

CREATE TABLE IF NOT EXISTS `powerplay_roles` (
  `role_id` int(11) NOT NULL,
  `role_code` varchar(4) CHARACTER SET utf8 DEFAULT NULL,
  `role_name` varchar(35) CHARACTER SET utf8 NOT NULL,
  `isLocked` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `powerplay_roles`
--

INSERT INTO `powerplay_roles` (`role_id`, `role_code`, `role_name`, `isLocked`, `parent_id`, `group_id`) VALUES
(1, '1A', 'Administrator', 0, 0, 1),
(2, '1S', 'Support', 0, 1, 1),
(4, '2G', 'Guest', 0, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `powerplay_users`
--

CREATE TABLE IF NOT EXISTS `powerplay_users` (
  `user_id` int(11) NOT NULL,
  `user_first_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `user_last_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `user_email` varchar(30) CHARACTER SET utf8 NOT NULL,
  `user_password` text CHARACTER SET utf8 NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `last_visit` datetime NOT NULL,
  `cell_phone` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '0',
  `isLocked` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `powerplay_users`
--

INSERT INTO `powerplay_users` (`user_id`, `user_first_name`, `user_last_name`, `user_email`, `user_password`, `language_id`, `last_visit`, `cell_phone`, `parent_id`, `role_id`, `isLocked`) VALUES
(1, 'Vladyslav', 'Pogorelov', 'ftpserver@mail.ru', 'f6fdffe48c908deb0f4c3bd36c032e72', 1, '2015-12-13 12:12:09', '380508203824', 1, 1, 0),
(2, 'Дмитрий', 'Андреев', 'dima@andreev.ru', '21232f297a57a5a743894a0e4a801fc3', 1, '0000-00-00 00:00:00', '380508203821', 1, 1, 0),
(8, 'Test1', 'Test', 'test@mail.ru', '21232f297a57a5a743894a0e4a801fc3', 1, '0000-00-00 00:00:00', '12312-3123', 1, 2, 1),
(9, 'vlad1', 'Wer', 'bw@livinginternet.deas', 'c84258e9c39059a89ab77d846ddab909', 1, '0000-00-00 00:00:00', '0', 1, 2, 1),
(10, 'Test', 'Test', 'test@mail.ru', '21232f297a57a5a743894a0e4a801fc3', 1, '0000-00-00 00:00:00', '12312-3123', 1, 2, 1),
(11, 'Vladyslav', 'TestPogorelov', 'nema@mail.ru', '21232f297a57a5a743894a0e4a801fc3', 1, '2015-11-10 21:11:07', '380508203824', 1, 2, 0),
(12, 'test', 'test', 'asdas@asd.as', '21232f297a57a5a743894a0e4a801fc3', 1, '0000-00-00 00:00:00', '1231232', 1, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `setting_id` int(11) NOT NULL,
  `category_count` int(11) NOT NULL,
  `pagination` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`setting_id`, `category_count`, `pagination`) VALUES
(1, 6, 14);

-- --------------------------------------------------------

--
-- Table structure for table `slider-setting`
--

CREATE TABLE IF NOT EXISTS `slider-setting` (
  `is_id` int(11) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `post_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider-setting`
--

INSERT INTO `slider-setting` (`is_id`, `image_path`, `post_id`) VALUES
(2, '/web/templates/Site/assets/images/Slider/1.jpg', NULL),
(8, '/web/templates/Site/assets/images/Slider/8589130591340-evil-angel-the-white-rose-wallpaper-hd.jpg', 1),
(9, '/web/templates/Site/assets/images/Slider/Dragon-Wallpaper-dragons-13975568-1280-800.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `tag_id` int(11) NOT NULL,
  `tag_name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`tag_id`, `tag_name`) VALUES
(1, 'test'),
(2, 'probuem');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ip_block`
--
ALTER TABLE `ip_block`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `moonstore_aligns`
--
ALTER TABLE `moonstore_aligns`
  ADD PRIMARY KEY (`align_id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`position_id`);

--
-- Indexes for table `posts_category`
--
ALTER TABLE `posts_category`
  ADD PRIMARY KEY (`posts_category_id`);

--
-- Indexes for table `post_comments`
--
ALTER TABLE `post_comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `post_types`
--
ALTER TABLE `post_types`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `powerplay_groups`
--
ALTER TABLE `powerplay_groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `powerplay_language`
--
ALTER TABLE `powerplay_language`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `powerplay_posts`
--
ALTER TABLE `powerplay_posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `powerplay_roles`
--
ALTER TABLE `powerplay_roles`
  ADD PRIMARY KEY (`role_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `powerplay_users`
--
ALTER TABLE `powerplay_users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `language_id` (`language_id`,`role_id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `language_id_2` (`language_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `slider-setting`
--
ALTER TABLE `slider-setting`
  ADD PRIMARY KEY (`is_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tag_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ip_block`
--
ALTER TABLE `ip_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `moonstore_aligns`
--
ALTER TABLE `moonstore_aligns`
  MODIFY `align_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `position_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `posts_category`
--
ALTER TABLE `posts_category`
  MODIFY `posts_category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `post_comments`
--
ALTER TABLE `post_comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `post_types`
--
ALTER TABLE `post_types`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `powerplay_groups`
--
ALTER TABLE `powerplay_groups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `powerplay_language`
--
ALTER TABLE `powerplay_language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `powerplay_posts`
--
ALTER TABLE `powerplay_posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `powerplay_roles`
--
ALTER TABLE `powerplay_roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `powerplay_users`
--
ALTER TABLE `powerplay_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `slider-setting`
--
ALTER TABLE `slider-setting`
  MODIFY `is_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `powerplay_roles`
--
ALTER TABLE `powerplay_roles`
  ADD CONSTRAINT `powerplay_roles_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `powerplay_groups` (`group_id`);

--
-- Constraints for table `powerplay_users`
--
ALTER TABLE `powerplay_users`
  ADD CONSTRAINT `powerplay_users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `powerplay_roles` (`role_id`),
  ADD CONSTRAINT `powerplay_users_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `powerplay_language` (`language_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
