<?php

namespace Override\Vlad;

use Modules\Vlad\Vlad as b;

class Vlad extends b{
    
    
    public function actionMain($arg){
        echo 'Hello from overriding module Vlad' . $arg;
    }
    public function actionTest($args){
        echo 'My name is ' . $args['name'] . ". My lastname is " . $args['lastname'] . ". <br> I am " . $args['age'] . " years old.<br><p style='color: green'>I am an override module";
    }
  
}
