<?php
/*%%SmartyHeaderCode:56140298656902ab5772708_90606276%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1b6b9dfe0be1cb1221aa98e9e4bc1435359babb1' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Admin/tables.tpl',
      1 => 1452217778,
      2 => 'file',
    ),
    'dcacc86d8945c492ff69ebe43b730721f2886f02' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Admin/menu.tpl',
      1 => 1452217778,
      2 => 'file',
    ),
    '43d8831b9b380800422352d9577b84e1de0d4150' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Admin/footer.tpl',
      1 => 1452217779,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '56140298656902ab5772708_90606276',
  'tpl_function' => 
  array (
  ),
  'variables' => 
  array (
    'posts' => 0,
    'post' => 0,
    'page' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56902ab588b587_61501079',
  'cache_lifetime' => 3600,
),true);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56902ab588b587_61501079')) {
function content_56902ab588b587_61501079 ($_smarty_tpl) {
?>
<!doctype html>
<html><head>
        <meta charset="utf-8">
        <title>Moonstore - Posts</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link href="/web/templates/Admin/css/tingle.css" rel="stylesheet">
        <link rel="stylesheet" href="/web/templates/Admin/css/normalize.css">
        <link rel="stylesheet" href="/web/templates/Admin/css/posts-style.css">

        <script src="/web/templates/Admin/js/prefixfree.min.js"></script>

        <link href="/web/templates/Admin/css/bootstrap.min.css" rel="stylesheet">

        <link href="/web/templates/Admin/css/main.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/font-style.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/flexslider.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/component.css" />
        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/normalize.css" />
        <script type="text/javascript" src="/web/templates/Admin/js/jquery-1.10.2.min.js"></script>

        <!-- Placed at the end of the document so the pages load faster -->
        <script type="text/javascript" src="/web/templates/Admin/js/bootstrap.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/lineandbars.js"></script>


        <!-- NOTY JAVASCRIPT -->
        <script type="text/javascript" src="/web/templates/Admin/js/jquery.noty.js"></script>
        <!-- You can add more layouts if you want -->
        <script type="text/javascript" src="/web/templates/Admin/js/default.js"></script>
        <script src="/web/templates/Admin/js/jquery.flexslider.js" type="text/javascript"></script>

        <script type="text/javascript" src="/web/templates/Admin/js/admin.js"></script>
        <script src="/web/templates/Admin/js/menu/js/modernizr.custom.25376.js"></script>
        <style type="text/css">
            body {
                padding-top: 60px;
            }
        </style>

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Google Fonts call. Font Used Open Sans -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

        <!-- DataTables Initialization -->
        <script type="text/javascript" src="/web/templates/Admin/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {
                $('#dt1').dataTable();
            });
        </script>


    </head>
    <body>

        <!-- NAVIGATION MENU -->

        <link rel="stylesheet" href="/web/templates/Admin/css/demo.css">
<link rel="stylesheet" href="/web/templates/Admin/css/forkit.css">
<link href="/web/templates/Admin/css/style-slider.css" rel="stylesheet" />
<link href="/web/templates/Admin/css/menu/css/jquery-ui.css" rel="stylesheet" />
<link href="/web/templates/Admin/js/menu/js/image-picker/image-picker/image-picker.css" rel="stylesheet" />


<script src="/web/templates/Admin/js/menu/js/jquery-ui.js "></script>
<script src="/web/templates/Admin/js/menu/js/image-picker/image-picker/image-picker.js"></script>
<div class="navbar-nav navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin"><img src="/web/templates/Admin/img/Moon-Store-transparent-bg.png" width="50px" height="50px" alt="Moonstore"></a>
        </div> 
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Сайт</a></li>                            
                <li><a href="/admin/posts">Статьи</a></li>
                <li><a href="/admin/post/add">Создать статью</a></li>
                                          
                    <li><a href="/admin/profile/usermanagement"> Пользователи</a></li>
                                    <li><a href="#" id="showMenu">Расширенное меню</a></li>

            </ul>
        </div><!--/.nav-collapse -->
    </div>    
    <!-- The contents (if there's no contents the ribbon acts as a link) -->
            <div class="forkit-curtain">
            <div class="close-button"></div>

            
            <div class="col-sm-3 col-lg-3">
                <div class="dash-unit">
                    <dtitle>Настройки сайта </dtitle>
                    <hr>

                    <div class="text">

                        <label class="checkbox inline">
                            Из одной категории
                            <div class="switch">
                                <input type="radio" class="switch-input" name="isLocked" value="true" id="on" checked="" >
                                <label for="on" class="switch-label switch-label-off">On</label>
                                <input type="radio" class="switch-input" name="isLocked" value="false" id="off"  >
                                <label for="off" class="switch-label switch-label-on">Off</label>
                                <span class="switch-selection"></span><br>

                                <div class="count">
                                    <label for="number">Количество</label>
                                    <div class="number">
                                        <input type="number" value="10" id="number"/>
                                        <br>

                                    </div>
                                </div>
                                <a href="#" class="save">Сохранить</a>

                                
                                    <script type="text/javascript">
                                        $("input.switch-input").on("click", function () {
                                            if ($("input:checked").val() == 'true') {

                                                $(".count").fadeIn(400);
                                            } else {
                                                $(".count").fadeOut(400);
                                                $("input#number").val('0');
                                            }
                                        });
                                        $(".save").click(function () {
                                            $.ajax({
                                                url: '/admin/pagination/save/',
                                                method: 'POST',
                                                data: {count: $("#number").val()},
                                                success: function () {
                                                    alert('Сохранено');
                                                },
                                                error: function () {
                                                    alert('Не удачно');
                                                }
                                            });

                                        });</script>
                                    
                            </div> 
                        </label>
                    </div>
                    <br>
                    <div class="info-user">

                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-lg-3">
                <div class="dash-unit">
                    <dtitle>Пагинация </dtitle>
                    <hr>
                    <div class="text">

                        <div class="count-pagination">
                            <label for="number">Количество статей на странице</label>
                            <div class="number">
                                <input type="number" value="14" id="number-pagination"/>
                                <br>
                            </div>
                        </div>
                        <a href="#" class="save-pagination">Сохранить</a>
                        <script type="text/javascript">
                            
                                $(".save-pagination").click(function () {
                                    $.ajax({
                                        url: '/admin/pagination/save/pagination/',
                                        method: 'POST',
                                        data: {count: $("#number-pagination").val()},
                                        success: function () {
                                            alert('Сохранено');
                                        },
                                        error: function () {
                                            alert('Не удачно');
                                        }
                                    });
                                    //$.post("/admin/pagination/save/pagination/", $("#number-pagination").val());
                                    //                                   alert("Сохранено");
                                });
                            </script>
                        </div>
                        <br>
                    </div>
                </div>
                <div class="col-sm-3 col-lg-3">
                    <div class="dash-unit">
                        <dtitle>Слайдер</dtitle>
                        <hr>
                        <div class="thumbnail">
                            Количество слайдов
                        </div><!-- /thumbnail -->
                        <div class="text">
                            <form id="upload" method="post" action="/admin/slider/save/slider/" enctype="multipart/form-data">
                                <div id="drop">
                                    Перетащи сюда

                                    <a>Выбрать</a>
                                    <input type="file" name="upl" multiple />
                                </div>

                                <ul>
                                    <!-- The file uploads will be shown here -->
                                </ul>

                            </form>

                        </div>
                        <br>

                    </div>
                </div>
                <div class="col-sm-3 col-lg-3">
                    <div class="dash-unit">
                        <dtitle>Настройки сайта </dtitle>
                        <hr>
                        <div class="text" style="overflow: scroll; height:244px;">
                            <form id="slider-settings" method="get" action="#">
                                <select class="image-picker show-html">
                                                                            <option data-img-src="/web/templates/Site/assets/images/Slider/1.jpg" value="2">2</option>
                                                                            <option data-img-src="/web/templates/Site/assets/images/Slider/8589130591340-evil-angel-the-white-rose-wallpaper-hd.jpg" value="8">8</option>
                                                                            <option data-img-src="/web/templates/Site/assets/images/Slider/Dragon-Wallpaper-dragons-13975568-1280-800.jpg" value="9">9</option>
                                                                            <option data-img-src="/web/templates/Site/assets/images/Slider/mhmzmdfyibbqqhglqrvx.jpg" value="25">25</option>
                                                                    </select>
                                <br>
                                <input type="text" class="post" name="post" placeholder="Статья">
                            </form>
                            <br>
                            <a class="save-slider">Сохранить настройки</a><br>
                            <a class="delete-slider" style="display: none">Удалить настройки</a>
                        </div>
                    </div>
                    <br>

                </div>
            </div>
            
            
                <script type="text/javascript">
                    ;
                    $("select").imagepicker();
                    $(".thumbnail").click(function () {
                        var imgsrc = $(this).find('img').attr('src');
                        $.ajax({
                            type: 'get',
                            url: '/admin/find/post/slider/',
                            data: {'imgsrc': imgsrc},
                            success: function (data) {
                                $('.post').val(data);
                            }
                        });
                        $(".delete-slider").show(300);
                    });
                    $(".delete-slider").click(function () {
                        var frm = $('#slider-settings');
                        $.ajax({
                            type: 'get',
                            url: '/admin/keepost/',
                            data: {'image_id': frm.find(".selected").find("img").attr('src'), 'post': frm.find('.post').val(), 'delete': 1},
                            success: function (data) {
                                //alert('ok');
                                frm.find(".selected").hide(700);
                               // frm.find(".selected").remove();
                            }
                        });
                    });
                    $(".save-slider").click(function () {
                        var frm = $('#slider-settings');

                        $.ajax({
                            type: 'get',
                            url: '/admin/keepost/',
                            data: {'image_id': frm.find(".selected").find("img").attr('src'), 'post': frm.find('.post').val()},
                            success: function (data) {
                                alert('ok');
                            }
                        });
                    });


                    //$(function () {
                    $(".post").autocomplete({
                        source: function (request, response) {
                            $.ajax({
                                url: '/admin/getposts/',
                                method: 'GET',
                                data: {'q': request.term},
                                dataType: 'json',
                                success: function (data) {
//                                            console.log(data);
                                    response(data);
                                }
                            });
                        },
                        minLength: 1,
                        open: function () {
                            $(".ui-autocomplete").insertAfter(".post");
                            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                        },
                        close: function () {
                            $(".ui-autocomplete").hide();
                            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                        }
                    });
                    //});
                </script>  
            
        </div>

        <!-- The ribbon -->
        <a class="forkit" data-text="Настройки" data-text-detached="Протяни вниз >" href="#"></a>

        <script src="/web/templates/Admin/js/forkit.js"></script>
                </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="/web/templates/Admin/js/sliderSettings/assets/js/jquery.knob.js"></script>

        <!-- jQuery File Upload Dependencies -->
        <script src="/web/templates/Admin/js/sliderSettings/assets/js/jquery.ui.widget.js"></script>
        <script src="/web/templates/Admin/js/sliderSettings/assets/js/jquery.iframe-transport.js"></script>
        <script src="/web/templates/Admin/js/sliderSettings/assets/js/jquery.fileupload.js"></script>

        <!-- Our main JS file -->
        <script src="/web/templates/Admin/js/sliderSettings/assets/js/script.js"></script>

        



        <div id="perspective" class="perspective effect-moveleft">
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <!-- Top Navigation -->

                    <div class="main clearfix">
                        <!-- CONTENT -->       
                        <div class="container">

                            <div class="row main-row">
                                                                                                            <div class="col-sm-3 col-lg-3" id="post-35807" >

                                            <!-- LAST USER BLOCK -->     
                                            <div class="half-unit"  >
                                                <div class="text">
                                                    <a href="/admin/post/edit/35807"  style="color: greenyellow" data-block="0">#35807  asdasdasd</a>
                                                           <a href="#" class="remove" data-id='35807' style="float: right; margin-right: 5px">X</a>
                                                       </div>
                                                       <hr>
                                                       <div class="cont2">
                                                           <div class="text">
                                                           </div>
                                                           <br>
                                                           <br>
                                                           <div class="left_img">
                                                               <a href="/admin/post/edit/35807"><img src="/web/templates/Site/assets/images/Posts/thumb_1_3132687397.jpg"></a>
                                                           </div>
                                                           <div class="after_img">
                                                               <p><bold>Дата редактирования: 2016-01-08 21:01:33</bold></p>
                                                               <p><bold>Мода</bold></p>
                                                           </div>
                                                       </div>
                                                    </div>

                                                </div>
                                                                                        <div class="col-sm-3 col-lg-3" id="post-1792" >

                                            <!-- LAST USER BLOCK -->     
                                            <div class="half-unit"  >
                                                <div class="text">
                                                    <a href="/admin/post/edit/1792"  style="color: greenyellow" data-block="0">#1792  Первые кадры осенне-зимней кампании PRADA</a>
                                                           <a href="#" class="remove" data-id='1792' style="float: right; margin-right: 5px">X</a>
                                                       </div>
                                                       <hr>
                                                       <div class="cont2">
                                                           <div class="text">
                                                           </div>
                                                           <br>
                                                           <br>
                                                           <div class="left_img">
                                                               <a href="/admin/post/edit/1792"><img src="/web/templates/Site/assets/images/Posts/bebinka_add.png"></a>
                                                           </div>
                                                           <div class="after_img">
                                                               <p><bold>Дата редактирования: 2016-01-08 21:01:16</bold></p>
                                                               <p><bold>Мода</bold></p>
                                                           </div>
                                                       </div>
                                                    </div>

                                                </div>
                                                                                        <div class="col-sm-3 col-lg-3" id="post-6400" >

                                            <!-- LAST USER BLOCK -->     
                                            <div class="half-unit"  >
                                                <div class="text">
                                                    <a href="/admin/post/edit/6400"  style="color: greenyellow" data-block="0">#6400  Посмотрите на штаб-квартиру  MSGM в Милане</a>
                                                           <a href="#" class="remove" data-id='6400' style="float: right; margin-right: 5px">X</a>
                                                       </div>
                                                       <hr>
                                                       <div class="cont2">
                                                           <div class="text">
                                                           </div>
                                                           <br>
                                                           <br>
                                                           <div class="left_img">
                                                               <a href="/admin/post/edit/6400"><img src="/web/templates/Site/assets/images/Posts/bebinka_add.png"></a>
                                                           </div>
                                                           <div class="after_img">
                                                               <p><bold>Дата редактирования: </bold></p>
                                                               <p><bold>Мода</bold></p>
                                                           </div>
                                                       </div>
                                                    </div>

                                                </div>
                                                                                        <div class="col-sm-3 col-lg-3" id="post-19712" >

                                            <!-- LAST USER BLOCK -->     
                                            <div class="half-unit"  >
                                                <div class="text">
                                                    <a href="/admin/post/edit/19712"  style="color: greenyellow" data-block="0">#19712  Jeremy Scott выпустит документальный фильм о своей работе</a>
                                                           <a href="#" class="remove" data-id='19712' style="float: right; margin-right: 5px">X</a>
                                                       </div>
                                                       <hr>
                                                       <div class="cont2">
                                                           <div class="text">
                                                           </div>
                                                           <br>
                                                           <br>
                                                           <div class="left_img">
                                                               <a href="/admin/post/edit/19712"><img src="/web/templates/Site/assets/images/Posts/image7.jpg"></a>
                                                           </div>
                                                           <div class="after_img">
                                                               <p><bold>Дата редактирования: </bold></p>
                                                               <p><bold>Мода</bold></p>
                                                           </div>
                                                       </div>
                                                    </div>

                                                </div>
                                                                                        <div class="col-sm-3 col-lg-3" id="post-25856" >

                                            <!-- LAST USER BLOCK -->     
                                            <div class="half-unit"  >
                                                <div class="text">
                                                    <a href="/admin/post/edit/25856"  style="color: greenyellow" data-block="0">#25856  В Лондоне поставят пьесу, посвященную Alexander McQueen</a>
                                                           <a href="#" class="remove" data-id='25856' style="float: right; margin-right: 5px">X</a>
                                                       </div>
                                                       <hr>
                                                       <div class="cont2">
                                                           <div class="text">
                                                           </div>
                                                           <br>
                                                           <br>
                                                           <div class="left_img">
                                                               <a href="/admin/post/edit/25856"><img src="/web/templates/Site/assets/images/Posts/image.jpg"></a>
                                                           </div>
                                                           <div class="after_img">
                                                               <p><bold>Дата редактирования: </bold></p>
                                                               <p><bold>Мода</bold></p>
                                                           </div>
                                                       </div>
                                                    </div>

                                                </div>
                                                                                        <div class="col-sm-3 col-lg-3" id="post-27136" >

                                            <!-- LAST USER BLOCK -->     
                                            <div class="half-unit"  >
                                                <div class="text">
                                                    <a href="/admin/post/edit/27136"  style="color: greenyellow" data-block="0">#27136  Boss Hugo Boss выпустил полную версию рекламной кампании, осень-зима 2015/16</a>
                                                           <a href="#" class="remove" data-id='27136' style="float: right; margin-right: 5px">X</a>
                                                       </div>
                                                       <hr>
                                                       <div class="cont2">
                                                           <div class="text">
                                                           </div>
                                                           <br>
                                                           <br>
                                                           <div class="left_img">
                                                               <a href="/admin/post/edit/27136"><img src="/web/templates/Site/assets/images/Posts/hugo-boss-fall-2015-ad-moonstore.jpg"></a>
                                                           </div>
                                                           <div class="after_img">
                                                               <p><bold>Дата редактирования: </bold></p>
                                                               <p><bold>Мода</bold></p>
                                                           </div>
                                                       </div>
                                                    </div>

                                                </div>
                                                                                        <div class="col-sm-3 col-lg-3" id="post-29184" >

                                            <!-- LAST USER BLOCK -->     
                                            <div class="half-unit"  >
                                                <div class="text">
                                                    <a href="/admin/post/edit/29184"  style="color: greenyellow" data-block="0">#29184  Burberry Prorsum презентовал рекламную кампанию, осень-зима 2015/16</a>
                                                           <a href="#" class="remove" data-id='29184' style="float: right; margin-right: 5px">X</a>
                                                       </div>
                                                       <hr>
                                                       <div class="cont2">
                                                           <div class="text">
                                                           </div>
                                                           <br>
                                                           <br>
                                                           <div class="left_img">
                                                               <a href="/admin/post/edit/29184"><img src="/web/templates/Site/assets/images/Posts/29184.jpg"></a>
                                                           </div>
                                                           <div class="after_img">
                                                               <p><bold>Дата редактирования: </bold></p>
                                                               <p><bold>Мода</bold></p>
                                                           </div>
                                                       </div>
                                                    </div>

                                                </div>
                                                                                        <div class="col-sm-3 col-lg-3" id="post-29952" >

                                            <!-- LAST USER BLOCK -->     
                                            <div class="half-unit"  >
                                                <div class="text">
                                                    <a href="/admin/post/edit/29952"  style="color: greenyellow" data-block="0">#29952  Michael Kors представил рекламную кампанию, осень-зима 2015/16</a>
                                                           <a href="#" class="remove" data-id='29952' style="float: right; margin-right: 5px">X</a>
                                                       </div>
                                                       <hr>
                                                       <div class="cont2">
                                                           <div class="text">
                                                           </div>
                                                           <br>
                                                           <br>
                                                           <div class="left_img">
                                                               <a href="/admin/post/edit/29952"><img src="/web/templates/Site/assets/images/Posts/michael-kors-fall-2015-16-ad-moonstore.jpg"></a>
                                                           </div>
                                                           <div class="after_img">
                                                               <p><bold>Дата редактирования: </bold></p>
                                                               <p><bold>Мода</bold></p>
                                                           </div>
                                                       </div>
                                                    </div>

                                                </div>
                                                                                        <div class="col-sm-3 col-lg-3" id="post-30976" >

                                            <!-- LAST USER BLOCK -->     
                                            <div class="half-unit"  >
                                                <div class="text">
                                                    <a href="/admin/post/edit/30976"  style="color: greenyellow" data-block="0">#30976  Yohji Yamamoto откроет экспозицию в Лондоне</a>
                                                           <a href="#" class="remove" data-id='30976' style="float: right; margin-right: 5px">X</a>
                                                       </div>
                                                       <hr>
                                                       <div class="cont2">
                                                           <div class="text">
                                                           </div>
                                                           <br>
                                                           <br>
                                                           <div class="left_img">
                                                               <a href="/admin/post/edit/30976"><img src="/web/templates/Site/assets/images/Posts/image107-960x600.jpg"></a>
                                                           </div>
                                                           <div class="after_img">
                                                               <p><bold>Дата редактирования: </bold></p>
                                                               <p><bold>Мода</bold></p>
                                                           </div>
                                                       </div>
                                                    </div>

                                                </div>
                                                                                        <div class="col-sm-3 col-lg-3" id="post-35072" >

                                            <!-- LAST USER BLOCK -->     
                                            <div class="half-unit"  >
                                                <div class="text">
                                                    <a href="/admin/post/edit/35072"  style="color: greenyellow" data-block="0">#35072  За что мы любим Maison Martin Margiela</a>
                                                           <a href="#" class="remove" data-id='35072' style="float: right; margin-right: 5px">X</a>
                                                       </div>
                                                       <hr>
                                                       <div class="cont2">
                                                           <div class="text">
                                                           </div>
                                                           <br>
                                                           <br>
                                                           <div class="left_img">
                                                               <a href="/admin/post/edit/35072"><img src="/web/templates/Site/assets/images/Posts/maison-martin-margiela-archives-moonstore.jpg"></a>
                                                           </div>
                                                           <div class="after_img">
                                                               <p><bold>Дата редактирования: </bold></p>
                                                               <p><bold>Мода</bold></p>
                                                           </div>
                                                       </div>
                                                    </div>

                                                </div>
                                                                                        <div class="col-sm-3 col-lg-3" id="post-7425" >

                                            <!-- LAST USER BLOCK -->     
                                            <div class="half-unit"  >
                                                <div class="text">
                                                    <a href="/admin/post/edit/7425"  style="color: greenyellow" data-block="0">#7425  Рекламная кампания Dior Homme осень-зима 2014/15</a>
                                                           <a href="#" class="remove" data-id='7425' style="float: right; margin-right: 5px">X</a>
                                                       </div>
                                                       <hr>
                                                       <div class="cont2">
                                                           <div class="text">
                                                           </div>
                                                           <br>
                                                           <br>
                                                           <div class="left_img">
                                                               <a href="/admin/post/edit/7425"><img src="/web/templates/Site/assets/images/Posts/dior-homme-fall-winter-2014-ad-campaign-7-.jpg"></a>
                                                           </div>
                                                           <div class="after_img">
                                                               <p><bold>Дата редактирования: </bold></p>
                                                               <p><bold>Мода</bold></p>
                                                           </div>
                                                       </div>
                                                    </div>

                                                </div>
                                                                                        <div class="col-sm-3 col-lg-3" id="post-8705" >

                                            <!-- LAST USER BLOCK -->     
                                            <div class="half-unit"  >
                                                <div class="text">
                                                    <a href="/admin/post/edit/8705"  style="color: greenyellow" data-block="0">#8705  Первая женская коллекция бренда Daily Paper осень-зима 2014/15</a>
                                                           <a href="#" class="remove" data-id='8705' style="float: right; margin-right: 5px">X</a>
                                                       </div>
                                                       <hr>
                                                       <div class="cont2">
                                                           <div class="text">
                                                           </div>
                                                           <br>
                                                           <br>
                                                           <div class="left_img">
                                                               <a href="/admin/post/edit/8705"><img src="/web/templates/Site/assets/images/Posts/daily-paper-2014-fall-winter-womens-capsule-collection-8.jpg"></a>
                                                           </div>
                                                           <div class="after_img">
                                                               <p><bold>Дата редактирования: </bold></p>
                                                               <p><bold>Мода</bold></p>
                                                           </div>
                                                       </div>
                                                    </div>

                                                </div>
                                                                                        <div class="col-sm-3 col-lg-3" id="post-9729" >

                                            <!-- LAST USER BLOCK -->     
                                            <div class="half-unit"  >
                                                <div class="text">
                                                    <a href="/admin/post/edit/9729"  style="color: greenyellow" data-block="0">#9729  Релиз совместной коллекции Pharrell Williams x adidas Originals </a>
                                                           <a href="#" class="remove" data-id='9729' style="float: right; margin-right: 5px">X</a>
                                                       </div>
                                                       <hr>
                                                       <div class="cont2">
                                                           <div class="text">
                                                           </div>
                                                           <br>
                                                           <br>
                                                           <div class="left_img">
                                                               <a href="/admin/post/edit/9729"><img src="/web/templates/Site/assets/images/Posts/pharrell-adidas-solid-stan-smith-06.jpg"></a>
                                                           </div>
                                                           <div class="after_img">
                                                               <p><bold>Дата редактирования: </bold></p>
                                                               <p><bold>Мода</bold></p>
                                                           </div>
                                                       </div>
                                                    </div>

                                                </div>
                                                                                        <div class="col-sm-3 col-lg-3" id="post-17153" >

                                            <!-- LAST USER BLOCK -->     
                                            <div class="half-unit"  >
                                                <div class="text">
                                                    <a href="/admin/post/edit/17153"  style="color: greenyellow" data-block="0">#17153  Провокационный бренд KTZ показал коллекцию осень-зима 2015/16 вместе с Лениным</a>
                                                           <a href="#" class="remove" data-id='17153' style="float: right; margin-right: 5px">X</a>
                                                       </div>
                                                       <hr>
                                                       <div class="cont2">
                                                           <div class="text">
                                                           </div>
                                                           <br>
                                                           <br>
                                                           <div class="left_img">
                                                               <a href="/admin/post/edit/17153"><img src="/web/templates/Site/assets/images/Posts/ktz-2015-fall-winter-collection-15.jpg"></a>
                                                           </div>
                                                           <div class="after_img">
                                                               <p><bold>Дата редактирования: </bold></p>
                                                               <p><bold>Мода</bold></p>
                                                           </div>
                                                       </div>
                                                    </div>

                                                </div>
                                                
                                                    
                                                        <div class="col-sm-3 col-lg-3 add">
                                                            <a href="/admin/post/add"><div class="half-unit-add-list">
                                                                </div></a>
                                                        </div>
                                                        <input type="checkbox" id="load_more" role="button">
                                                         
                                                            <label  class="page-next" data-page="2 " for="load_more" onclick=""><span>Загрузить еще</span>
                                                                                                                        
                                                                <script type="text/javascript">
                                                                    $(".page-next").click(function () {
                                                                        // alert('asdasdas');
                                                                        $.ajax({
                                                                            url: '/admin/posts/',
                                                                            method: 'get',
                                                                            data: {'page': $(this).data('page')},
                                                                            success: function (response) {
                                                                                insertData(response);
                                                                            },
                                                                            error: function () {
                                                                                alert('error');
                                                                            }
                                                                        });
                                                                    });

                                                                    function insertData(response) {

                                                                        $(".page-next").remove();
                                                                        $(".add").remove();
                                                                        var html = $(response).find('.row').html();
                                                                        $(".main-row").append(html);

                                                                    }
                                                                </script>
                                                            
                                                    </div><!-- /row -->

                                                </div><!-- /container -->
                                                <br>
                                                <!-- FOOTER -->	

                                            </div>
                                        </div>
                                    </div>


                                    
                                        <script type="text/javascript">
                                            $(".remove").click(function () {
                                                var id = $(this).data('id');
                                                var url = '/admin/post/remove/';
                                                $.ajax({
                                                    url: url,
                                                    data: {'post_id': id},
                                                    success: function () {
                                                        console.log('success');
                                                        removeBlock(id)
                                                    },
                                                    error: function () {
                                                        alert('Error! Try latter');
                                                    }
                                                });
                                            });

                                            function removeBlock(id) {
                                                var block = $('#post-' + id).find('.text').children('a');
                                                if (block.data('block') == 1) {
                                                    block.data('block', '0');
                                                    block.attr('title', '');
                                                    block.css('color', 'greenyellow');
                                                } else {
                                                    block.data('block', '1');
                                                    block.attr('title', 'Заблокированно');
                                                    block.css('color', 'red');
                                                }
                                            }
                                        </script>
                                    
                                    <!-- FOURTH ROW OF BLOCKS -->     
<div id="footerwrap">
    <footer class="clearfix"></footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <p>Moonstore - Copyright 2015</p>
            </div>

        </div><!-- /row -->
    </div><!-- /container -->		
</div><!-- /footerwrap -->

<div class="tingle-demo"  style="display: none">
    <form method="post"  id="category-add" >
        <input name="module" class="input-huge" value="posts" type="hidden">
        <input name="action" class="input-huge" value="category/add" type="hidden">

        <label class="post-name"> Название категории </label>
        <input name="category_name" type="text" placeholder="Название категории">
        <label class="post-cat"> Описание категоррии </label>
        <input name="post_" type="text" placeholder="Описание категоррии">
        <label class="cat-seo-title"> Seo title: </label>
        <input name="category_seo_title" type="text" placeholder="SEO title" >
        <label class="category_seo_desc"> Seo description: </label>
        <input name="category_seo_desc" type="text" placeholder="SEO описание">


        <a href="#" class="btn-done">Готово</a>
    </form>
</div>


<nav class="outer-nav right vertical">
    <a href="/admin" class="icon-home">Статистика</a>
    <a href="#" class="icon-image trigger-button">Создать категорию</a>
    <a href="/admin/gallery" class="icon-image">Создать галерею</a>
          
        <a href="/admin/user/add" class="icon-star">Создать пользователя</a>

    
</nav>
</div>


    <script type="text/javascript" src="/web/templates/Admin/js/tingle/tingle.js"></script>

    <script type="text/javascript">
        tingle.modal.init();
        $(".trigger-button").click(function () {
            tingle.modal.setContent($(".tingle-demo").html());
            tingle.modal.open();
            $(".btn-done").click(function () {
                $.post('/admin/profile/', $('#category-add').serialize());
                alert('Категория была созданна');
                tingle.modal.close();
            });
        });
   
    </script>

    <style>

        .pbfooter {
            width:100%; 
            height:50px; 
            background-color:#F58723; 
            color:white; 
            top:0; 
            left:0; 
            z-index:1200; 
            padding-top:15px;
        }

    </style>

    <script type="text/javascript">
        jQuery(function ($) {
            // put the pbfooter on the bottom of the page
            var height = ($(document).height() - $("#pbafooter").height()) + "px";
            $("#pbafooter").css("top", ($(document).height() - $("#pbafooter").height()) + "px");
        });
    </script>

    <script src="/web/templates/Admin/js/menu/js/classie.js"></script>
    <script src="/web/templates/Admin/js/menu/js/menu.js"></script>

                            </body></html><?php }
}
?>