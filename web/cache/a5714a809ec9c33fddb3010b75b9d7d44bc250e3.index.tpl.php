<?php
/*%%SmartyHeaderCode:114857172856902a4c3329f1_95115403%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a5714a809ec9c33fddb3010b75b9d7d44bc250e3' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Site/index.tpl',
      1 => 1452288446,
      2 => 'file',
    ),
    '59d4f3fcf8d371f962e0b1181c025ab8c72f2abb' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Site/header.tpl',
      1 => 1452217779,
      2 => 'file',
    ),
    'ee68c1633c30e984dbccf12b8109c83b4545aa54' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Site/footer.tpl',
      1 => 1452217779,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '114857172856902a4c3329f1_95115403',
  'tpl_function' => 
  array (
  ),
  'variables' => 
  array (
    'user' => 0,
    'sliders' => 0,
    'slider' => 0,
    'posts' => 0,
    'i' => 0,
    'post' => 0,
    'keys' => 0,
    'key' => 0,
    'j' => 0,
    'keys2' => 0,
    'key2' => 0,
    'tagsfinder' => 0,
    'page' => 0,
    'category' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56902a4c4a09e5_26100658',
  'cache_lifetime' => 3600,
),true);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56902a4c4a09e5_26100658')) {
function content_56902a4c4a09e5_26100658 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<html class="no-js" lang="en" id="html-element">
    <head>

        <meta charset="utf-8" />

        <title>Moon Store</title>
        <meta name="author" content="Pogorelov Vlad" />
        <meta name="description" content="Moonstore desc" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="canonical" href="" />

        <meta property="fb:app_id" content="id"/>
        <link href="/web/templates/Site/assets/css/site.css" type="text/css" rel="stylesheet" id="style1" />
        <link href="/web/templates/Site/assets/css/home-with-carousel.css" type="text/css" rel="stylesheet" id="style1" />
                <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <link rel="stylesheet" href="/web/templates/Site/assets/css/slider/superslides.css">
        <script src='/web/templates/Site/assets/js/jquery.min.js'></script>

    </head>
    <body class="document-ready window-load" data-facebooknamespace="moonstore">

        
        <div id="slides">
            <div class="slides-container">
                                    <a href=''>
                        <img src="/web/templates/Site/assets/images/Slider/1.jpg"></a>
                                        <a href='/posts/show/1'>
                        <img src="/web/templates/Site/assets/images/Slider/8589130591340-evil-angel-the-white-rose-wallpaper-hd.jpg"></a>
                                        <a href='/posts/show/3'>
                        <img src="/web/templates/Site/assets/images/Slider/Dragon-Wallpaper-dragons-13975568-1280-800.jpg"></a>
                                        <a href='/posts/show/29'>
                        <img src="/web/templates/Site/assets/images/Slider/mhmzmdfyibbqqhglqrvx.jpg"></a>
                                </div>

            <nav class="slides-navigation">
                <a href="#" class="next carousel-btn" id="carousel-next"><span class="icon-angle-right"></span></a>
                <a href="#" class="prev carousel-btn" id="carousel-prev"><span class="icon-angle-left"></span></a>
            </nav>

        </div>

        
<header id="header" class="clearfix">

    <div class="container clearfix">

        <div itemscope >
            <a href="/" id="logo" >
                <img class="logo" src="/web/templates/Site/assets/images/logo.png" alt="MoonStore">
            </a>
        </div>

        <nav class="menus">

            <div class="menu">
                                    <a href="/posts/category/1" class="menu-link category-link transition">Мода</a>
                                    <a href="/posts/category/2" class="menu-link category-link transition">Искусство</a>
                                    <a href="/posts/category/6" class="menu-link category-link transition">Партнеры</a>
                                    <a href="/posts/category/9" class="menu-link category-link transition">Moon Store studio</a>
                            </div>

            <div class="menu social-menu hidden-tablet">
                <a href="https://www.facebook.com/moonstore.it/" class="menu-link social-link" target="_blank"><span class="icon icon-facebook"></span></a>
                <a href="https://twitter.com/moon__store" class="menu-link social-link" target="_blank"><span class="icon icon-twitter"></span></a>
                <a href="https://www.instagram.com/moonstore.it/" class="menu-link social-link" target="_blank"><span class="icon icon-instagram"></span></a>
                <button id="search-btn" class="menu-link social-link hidden-phone"><span class="icon icon-search"></span></button>
            </div>

        </nav>

        <button id="menu-btn" class="visible-phone menu-link"><span class="icon icon-menu"></span></button>

    </div>

    <div id="header-search-container">
        <form class="header-search" action="/posts/search/" method="GET">
            <input type="search" id="search-input" name="q" data-search=" " required="" value="" />
            <button id="header-search-btn" type="submit">Поиск</button>
        </form>
    </div>

</header>




        <div id="main" role="main" class="container clearfix">

            <section class="article-grid">
                <div class="articles clearfix">

                                            <!-- row 1 -->
                        <div class="row halfpage clearfix">
                                                                                                                                                        
                                    <article class="article">
                                        <a href="/posts/show/35806" class="article-link clearfix">
                                            <div class="img-wrapper">
                                                <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                     data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                     data-responsive-widths="375,480"
                                                     src="/web/templates/Site/assets/images/Posts/best-brands-2015.jpg" style="max-width: 100%; width:480px; " />
                                                                                                    <h6 class="category">
                                                        Мода
                                                    </h6>
                                                                                            </div>
                                            <div class="text">
                                                <h4 class="title">Лучшие бренды 2015 года</h4>
                                                <h5 class="description">Перед тем, как полностью погрузиться в 2016 год, мы решили подвести итог и рассказать о брендах, которые по разным причине стали лучшими в 2015 году</h5>
                                            </div>
                                        </a>
                                    </article>
                                                                                                            
                                                                                                                                
                                    <article class="article">
                                        <a href="/posts/show/35785" class="article-link clearfix">
                                            <div class="img-wrapper">
                                                <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                     data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                     data-responsive-widths="375,480"
                                                     src="/web/templates/Site/assets/images/Posts/11214064_886397368106593_5614781332794543993_n.jpg" style="max-width: 100%; width:480px; " />
                                                                                                    <h6 class="category">
                                                        Мода
                                                    </h6>
                                                                                            </div>
                                            <div class="text">
                                                <h4 class="title">Киевский ЦУМ. Перерождение легенды</h4>
                                                <h5 class="description">ЦУМ всегда был самой престижной и элитной торговой площадкой любого крупного города СНГ</h5>
                                            </div>
                                        </a>
                                    </article>
                                                                                                            
                                                                                                                                
                                    <article class="article">
                                        <a href="/posts/show/35762" class="article-link clearfix">
                                            <div class="img-wrapper">
                                                <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                     data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                     data-responsive-widths="375,480"
                                                     src="/web/templates/Site/assets/images/Posts/moon-store-man.jpg" style="max-width: 100%; width:480px; " />
                                                                                                    <h6 class="category">
                                                        Мода
                                                    </h6>
                                                                                            </div>
                                            <div class="text">
                                                <h4 class="title">Чем запомнились мужчины в 2015</h4>
                                                <h5 class="description">В этом году мужская одежда смело, а может даже и нагло, украла долю рынка моды у женщин</h5>
                                            </div>
                                        </a>
                                    </article>
                                                                                                            
                                                                                                                                
                                    <article class="article">
                                        <a href="/posts/show/35744" class="article-link clearfix">
                                            <div class="img-wrapper">
                                                <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                     data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                     data-responsive-widths="375,480"
                                                     src="/web/templates/Site/assets/images/Posts/DC_Portrait_newMAISON-kopiya.jpg" style="max-width: 100%; width:480px; " />
                                                                                                    <h6 class="category">
                                                        Мода
                                                    </h6>
                                                                                            </div>
                                            <div class="text">
                                                <h4 class="title">Дизайнеры Valentino собираются покорить мир</h4>
                                                <h5 class="description">Когда вышла первая часть фильма "Zoolander" (“Образцовый самец”), Пьерпаоло Пиччоли и Мария Кюри были лишь дизайнерами аксессуаров  в ателье империи Валентино Гаравани, но в один прекрасный день, по их предложению, вся команда оставила работу, что бы пойт</h5>
                                            </div>
                                        </a>
                                    </article>
                                                                                                            
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            

                        </div>
                        <div class="subscribe-widget">
                            <h4 class="title">Подписаться</h4>
                            <h5 class="description">Подпишись на обновления</h5>

                            <form method="POST" class="subscribe-form" action="">
                                <input type="text" name="email" class="email-input" placeholder="you@email.com" required />
                                <a href="#"  class="subscribe-btn"><span class="icon icon-angle-right"></span></a>
                            </form>
                            <div class="success-subscribe" style="display: none">
                                <p>Вы успешно подписались на рассылку новостей.</p>
                            </div>
                            <div class="social-links">
                                <a href="https://www.facebook.com/moonstore.it/" class="social-link" target="_blank">
                                    <span class="icon icon-facebook"></span></a>
                                <a href="https://twitter.com/moon__store" class="social-link" target="_blank">
                                    <span class="icon icon-twitter"></span></a>
                                <a href="https://www.instagram.com/moonstore.it/" class="social-link" target="_blank">
                                    <span class="icon icon-instagram"></span></a>
                            </div>
                        </div>
                        <!-- /row 1 -->
                        <!-- row 2 -->
                        <div class="row featured clearfix">
                                                                                                                                                                                                <article class="article">
                                            <a href="/posts/show/35735" class="article-link clearfix">
                                                <div class="img-wrapper">
                                                    <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                         data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                         data-responsive-widths="375,480"
                                                         src="/web/templates/Site/assets/images/Posts/moon-store-kak-nas-zastavlyaut-pokupat.jpg" style="max-width: 100%; width:480px; " />
                                                                                                            <h6 class="category">
                                                            Мода
                                                        </h6>
                                                                                                    </div>
                                                <div class="text">
                                                    <h4 class="title">Как нас заставляют покупать?</h4>
                                                    <h5 class="description">
                                                        Сегодняшний день, для большинства современных людей, начинается, как говорится, не с кофе    
                                                    </h5>
                                                </div>
                                            </a>
                                        </article>
                                                                                                                                                                                                                                                                                            <article class="article">
                                            <a href="/posts/show/35721" class="article-link clearfix">
                                                <div class="img-wrapper">
                                                    <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                         data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                         data-responsive-widths="375,480"
                                                         src="/web/templates/Site/assets/images/Posts/pierre-cardin_2413573-moonstore.jpg" style="max-width: 100%; width:480px; " />
                                                                                                            <h6 class="category">
                                                            Мода
                                                        </h6>
                                                                                                    </div>
                                                <div class="text">
                                                    <h4 class="title">Пьер Карден или "Король" патентов</h4>
                                                    <h5 class="description">
                                                        Для каждого бренда, который мечтает изобрести и запатентовать что-либо, Пьер Карден – весьма поучительный пример того, как нужно придумывать и создавать    
                                                    </h5>
                                                </div>
                                            </a>
                                        </article>
                                                                                                                                                                                                                                                                                            <article class="article">
                                            <a href="/posts/show/35664" class="article-link clearfix">
                                                <div class="img-wrapper">
                                                    <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                         data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                         data-responsive-widths="375,480"
                                                         src="/web/templates/Site/assets/images/Posts/Armani-Hotel-Spa-in-Dubai-960x600.jpg" style="max-width: 100%; width:480px; " />
                                                                                                            <h6 class="category">
                                                            Мода
                                                        </h6>
                                                                                                    </div>
                                                <div class="text">
                                                    <h4 class="title">10 отелей, созданных известными модельерами </h4>
                                                    <h5 class="description">
                                                        У каждого из нас наступает такой момент в жизни, когда стараешься превзойти самого себя, стремясь быть лучшим во всем    
                                                    </h5>
                                                </div>
                                            </a>
                                        </article>
                                                                                                                                                                                                                                                                                            <article class="article">
                                            <a href="/posts/show/35647" class="article-link clearfix">
                                                <div class="img-wrapper">
                                                    <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                         data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                         data-responsive-widths="375,480"
                                                         src="/web/templates/Site/assets/images/Posts/Istoriya-e`volyutsii-damskogo-byustgaltera-.jpg" style="max-width: 100%; width:480px; " />
                                                                                                            <h6 class="category">
                                                            Мода
                                                        </h6>
                                                                                                    </div>
                                                <div class="text">
                                                    <h4 class="title">История эволюции дамского бюстгальтера </h4>
                                                    <h5 class="description">
                                                        От 20-х годов и туго затянутых корсетов, вплоть до “свободных” 90-х мы изучаем элемент одежды, который изменил жизнь и силуэт женщин    
                                                    </h5>
                                                </div>
                                            </a>
                                        </article>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     

                        </div>


                        <!-- /row 2 -->
                        <!-- row 3 -->
                        <div class="row multi-line clearfix">
                            
                                                            

                                <article class="article">
                                    <a href="/posts/show/35626" class="article-link clearfix">
                                        <div class="img-wrapper">
                                            <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                 data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                 data-responsive-widths="375,480"
                                                 src="/web/templates/Site/assets/images/Posts/01-960x600.jpg" style="max-width: 100%; width:480px;" />
                                                                                            <h6 class="category">
                                                    Мода
                                                </h6>
                                                                                    </div>
                                        <div class="text">
                                            <h4 class="title">Burberry обогнал по продажам в интернете Louis Vuitton</h4>
                                            <h5 class="description">
                                                Если вы считаете, что продажи через интернет, это бесполезное изобретение для ленивых, то вы глубоко ошибаетесь    
                                            </h5>
                                        </div>
                                    </a>
                                </article>
                                


                                                            

                                <article class="article">
                                    <a href="/posts/show/35598" class="article-link clearfix">
                                        <div class="img-wrapper">
                                            <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                 data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                 data-responsive-widths="375,480"
                                                 src="/web/templates/Site/assets/images/Posts/miuccia-prada-photo-guido-harari1999-828x1024-1748x984.jpg" style="max-width: 100%; width:480px;" />
                                                                                            <h6 class="category">
                                                    Мода
                                                </h6>
                                                                                    </div>
                                        <div class="text">
                                            <h4 class="title">Миучча Прада рассказала о своем "тревожном" творчестве</h4>
                                            <h5 class="description">
                                                Совсем недавно, младшая из внучек Марио Прады — синьора Миучча, которая является дизайнером Prada, а так же основательницей бренда Miu Miu, поделилась своими мыслями о работе в семейном бизнесе, а так же о современной модной индустрии в интервью для Docum    
                                            </h5>
                                        </div>
                                    </a>
                                </article>
                                


                                                            

                                <article class="article">
                                    <a href="/posts/show/35508" class="article-link clearfix">
                                        <div class="img-wrapper">
                                            <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                 data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                 data-responsive-widths="375,480"
                                                 src="/web/templates/Site/assets/images/Posts/demnagvasalia-2-960x600-kopiya.jpg" style="max-width: 100%; width:480px;" />
                                                                                            <h6 class="category">
                                                    Мода
                                                </h6>
                                                                                    </div>
                                        <div class="text">
                                            <h4 class="title">Почему грузин Демна Гвасалия занял главный пост в Balenciaga?</h4>
                                            <h5 class="description">
                                                Во время международного марафона недель мод, многое количество коллекций проходят приблизительно через такой путь: какое-нибудь музыкальное произведение, фильм, картина, личность или историческая эпоха вдохновляют некую творческую личность    
                                            </h5>
                                        </div>
                                    </a>
                                </article>
                                


                                                            

                                <article class="article">
                                    <a href="/posts/show/35578" class="article-link clearfix">
                                        <div class="img-wrapper">
                                            <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                 data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                 data-responsive-widths="375,480"
                                                 src="/web/templates/Site/assets/images/Posts/IMG_2322-960x600.jpg" style="max-width: 100%; width:480px;" />
                                                                                            <h6 class="category">
                                                    Искусство
                                                </h6>
                                                                                    </div>
                                        <div class="text">
                                            <h4 class="title">Открытие выставки Serge Payet "33/34»</h4>
                                            <h5 class="description">
                                                В преддверии дня рождения и персональной экспозиции «33/34», которая представлена киевской публике с 01    
                                            </h5>
                                        </div>
                                    </a>
                                </article>
                                


                                                            

                                <article class="article">
                                    <a href="/posts/show/35526" class="article-link clearfix">
                                        <div class="img-wrapper">
                                            <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                 data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                 data-responsive-widths="375,480"
                                                 src="/web/templates/Site/assets/images/Posts/.jpg" style="max-width: 100%; width:480px;" />
                                                                                            <h6 class="category">
                                                    Мода
                                                </h6>
                                                                                    </div>
                                        <div class="text">
                                            <h4 class="title">Уход с prêt-a-porter или как Жан-Поль Готье решил стать богом высокой моды</h4>
                                            <h5 class="description">
                                                Год назад всех потрясла внезапная новость: «Жан-Поль Готье уходит из повседневной моды», так пестрили все заголовки мировой прессы    
                                            </h5>
                                        </div>
                                    </a>
                                </article>
                                


                                                            

                                <article class="article">
                                    <a href="/posts/show/35479" class="article-link clearfix">
                                        <div class="img-wrapper">
                                            <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                 data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                 data-responsive-widths="375,480"
                                                 src="/web/templates/Site/assets/images/Posts/moon-store-1.jpg" style="max-width: 100%; width:480px;" />
                                                                                            <h6 class="category">
                                                    Мода
                                                </h6>
                                                                                    </div>
                                        <div class="text">
                                            <h4 class="title">5 фактов про модные дома, которые изменили их судьбу</h4>
                                            <h5 class="description">
                                                Если вы поклонник того или иного дома моды, то вы наверняка знаете о нем все, но достаточно большое количество людей даже не задумывалось какое у них прошлое и что хранится за плотно закрытой дверью    
                                            </h5>
                                        </div>
                                    </a>
                                </article>
                                


                                                        <!-- /row 3 -->
                            <!-- row 6 -->

                            <!-- /row 6 -->

                        </div>
                                                    <input type="checkbox" id="load_more" role="button">
                             
                                <label  class="page-next" data-page="2 " for="load_more" ><span>Загрузить еще</span></label>
                                                        <script type="text/javascript" class="script-page">
                                $(".page-next").click(function () {
                                                                    $.ajax({
                                        url: '/posts/page/',
                                        method: 'get',
                                        data: {
                                            'page': $(this).data('page'),
                                        },
                                        success: function (response) {
                                            insertData(response);
                                        },
                                        error: function () {
                                            alert('error');
                                        }
                                    });
                                                                    //var y = window.pageYOffset;
                                    //var x = window.pageXOffset;
                                    //window.scroll(x, y);
                                });
                                function insertData(response) {
                                    $(".page-next").remove();
                                    //console.log($(response).find('.article-grid').html());
                                    var html = $(response).find('.articles').html();
                                    $(html).insertAfter("#load_more");
                                    $("#load_more").first().remove();
                                    $(".script-page").first().remove();

                                    console.log('done');
                                                                    //$('head').append('<link rel="stylesheet" href="/web/templates/Site/assets/css/site.css" type="text/css" />');
                                }
                            </script>
                        
                </section>

            </div>
        


        <script src="/web/templates/Site/assets/js/home-with-carousel.js" async="" type="text/javascript"></script>

        <div id="fb-root">

            <footer id="footer">

            <div class="container">

                <div class="primary-nav">
                    <a href="/contact" class="footer-link">Связаться с нами</a>
                    <a href="/aboutus" class="footer-link">О нас</a>
                </div>

                <div class="social-menu">
                  <a href="https://www.facebook.com/moonstore.it/" class="social-link" target="blank">
                                        <span class="icon icon-facebook"></span></a>
                                    <a href="https://twitter.com/moon__store" class="social-link" target="blank">
                                        <span class="icon icon-twitter"></span></a>
                                    <a href="https://www.instagram.com/moonstore.it/" class="social-link" target="blank">
                                        <span class="icon icon-instagram"></span></a>
                </div>

                <div class="copyright">&copy; 2016 - 2017 Moon Store.</div>

            </div>

        </footer>
        </div>
        <script src="/web/templates/Site/assets/js/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript">
                                var safari = $.browser.safari;

                                $(document).ready(function () {
                                    if ($(window).width() <= 767) {
                                        $(".page-next").css('width', '200px');
                                        $(".page-next").css('height', '50px');
                                        $(".page-next").css('margin-top', '-30px');
                                        $(".page-next").css('margin-left', '10px');
                                        $(".page-next").children('span').css('margin-top', '-20px');
                                    }

                                    $("body").fadeIn('slow');
                                    $("a").click(function (event) {
                                        if ($(this).parent('nav').attr('class') != 'slides-navigation' && $(this).attr('class') != 'subscribe-btn' && !safari) {
                                            event.preventDefault();
                                            linkLocation = this.href;
                                            $("body").fadeOut(1500, redirectPage);
                                        }
                                    });
                                    function redirectPage() {
                                        window.location = linkLocation;
                                    }

                                });</script>

        <script src="/web/templates/Site/assets/js/slider/jquery.superslides.js" type="text/javascript" charset="utf-8"></script>
                <script>
            $(function () {
                $('#slides').superslides({
                    hashchange: true,
                    play: 4000
                });
                $(".slides-pagination").insertBefore("#main");
                console.log('moved');
                $('#slides').on('mouseenter', function () {
                    $(this).superslides('stop');
                    console.log('Stopped')
                });
                $('#slides').on('mouseleave', function () {
                    $(this).superslides('start');
                    console.log('Started')
                });
                $(".subscribe-btn").click(function () {
                    var email = $(".email-input").val();
                    $.ajax({
                        url: '/subscribe/add/',
                        method: 'post',
                        data: {'email': email},
                        dataType: 'json',
                        success: function () {
                            console.log('Success');
                            changeSubscribe();
                        },
                        error: function () {
                            alert('Error')
                        }
                    });
                });
                function changeSubscribe() {
                    $(".subscribe-form").fadeOut(300);
                    $(".success-subscribe").fadeIn(300);
                    var subscribed = localStorage.getItem('subscribe');
                    if (subscribed != '1') {
                        localStorage.setItem('subscribe', '1');
                    }
                }

            });
            
            </script>

        </body>
    </html>
<?php }
}
?>