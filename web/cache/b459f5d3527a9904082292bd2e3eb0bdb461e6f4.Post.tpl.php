<?php
/*%%SmartyHeaderCode:1203541256902a62cd3c31_45198574%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b459f5d3527a9904082292bd2e3eb0bdb461e6f4' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Site/Post.tpl',
      1 => 1452287251,
      2 => 'file',
    ),
    '59d4f3fcf8d371f962e0b1181c025ab8c72f2abb' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Site/header.tpl',
      1 => 1452217779,
      2 => 'file',
    ),
    'ee68c1633c30e984dbccf12b8109c83b4545aa54' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Site/footer.tpl',
      1 => 1452217779,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1203541256902a62cd3c31_45198574',
  'tpl_function' => 
  array (
  ),
  'variables' => 
  array (
    'post' => 0,
    'postOne' => 0,
    'user' => 0,
    'tags' => 0,
    'tag' => 0,
    'last' => 0,
    'news' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56902a62e375b4_45198259',
  'cache_lifetime' => 3600,
),true);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56902a62e375b4_45198259')) {
function content_56902a62e375b4_45198259 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<html class="no-js" lang="en" id="html-element">
    <head>

        <meta charset="utf-8" />

        <title>Как нас заставляют покупать?</title>
        <meta name="author" content="Pogorelov Vlad" />
        <meta name="description" content="Сегодняшний день, для большинства современных людей, начинается, как говорится, не с кофе" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta property="fb:app_id" content="id"/>
        <link rel="canonical" href="" />
        <meta property="og:site_name" content="MoonStore"/><meta property="og:title" content="Как нас заставляют покупать?"/>
        <meta property="og:type" content="article"/>
        <link href="/web/templates/Site/assets/css/site.css" type="text/css" rel="stylesheet"/>
        <link href="/web/templates/Site/assets/css/article.css" type="text/css" rel="stylesheet"/>  
        <link href="/web/templates/Site/assets/css/home-with-carousel.css" type="text/css" rel="stylesheet" id="style1" />

                <script type="text/javascript" src="/web/templates/Site/assets/js/jquery.min.js"></script>
        <script src="/web/templates/Admin/js/jquery-ui.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/video.js"></script>

        <script type="text/javascript" src="/web/templates/Site/gallery/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="/web/templates/Site/gallery/jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="http://vk.com/js/api/share.js?93" charset="windows-1251"></script>
        <style type="text/css">

            .hovergallery img{
                -webkit-transform:scale(0.8); /*Webkit: уменьшаем размер до 0.8*/
                -moz-transform:scale(0.8); /*Mozilla: масштабирование*/
                -o-transform:scale(0.8); /*Opera: масштабирование*/
                -webkit-transition-duration: 0.5s; /*Webkit: длительность анимации*/
                -moz-transition-duration: 0.5s; /*Mozilla: длительность анимации*/
                -o-transition-duration: 0.5s; /*Opera: длительность анимации*/
                opacity: 0.7; /*Начальная прозрачность изображений*/
                margin: 0 10px 5px 0; /*Интервалы между изображениями*/
            }

            .hovergallery img:hover{
                -webkit-transform:scale(1.1); /*Webkit: увеличиваем размер до 1.2x*/
                -moz-transform:scale(1.1); /*Mozilla: масштабирование*/
                -o-transform:scale(1.1); /*Opera: масштабирование*/
                box-shadow:0px 0px 30px gray; /*CSS3 тени: 30px размытая тень вокруг всего изображения*/
                -webkit-box-shadow:0px 0px 30px gray; /*Webkit: тени*/
                -moz-box-shadow:0px 0px 30px gray; /*Mozilla: тени*/
                opacity: 1;
            }

        </style>
        <script src="/web/templates/Site/assets/js/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript">
            var safari = $.browser.safari;

            $("#player").fitVids();

            // Target your .container, .wrapper, .post, etc.
            $(document).ready(function () {
                $("#search-btn").click(function () {
                    if ($('body').hasClass('search-open')) {
                        $('body').removeClass('search-open');
                    } else {
                        $('body').addClass('search-open');
                    }
                });
                if ($(window).width() < 767) {
                    $(".article-body").css('margin', '0 auto');
                }
                $("body").fadeIn('slow');
                $("a").click(function (event) {
                    if ($(this).attr('class') != 'show-gallery' && !safari && $(this).attr('class') != 'subscribe-btn' && $(this).attr('class') != 'close-gallery') {
                        event.preventDefault();
                        linkLocation = this.href;
                        $("body").fadeOut(1500, redirectPage);
                    }
                });
                $("#menu-btn").click(function () {
                    if ($(".menu-open").length > 0) {
                        $('body').removeClass('menu-open');
                    } else {
                        $("body").addClass('menu-open');
                    }
                });
                function redirectPage() {
                    window.location = linkLocation;
                }
                function changeSubscribe() {
                    $(".subscribe-form").fadeOut(300);
                    $(".success-subscribe").fadeIn(300);
                    var subscribed = localStorage.getItem('subscribe');
                    if (subscribed != '1') {
                        localStorage.setItem('subscribe', '1');
                    }
                }
                $(".hidden-gallery").hide();
            });
        </script>


    </head>
    <body class="document-ready window-load" data-facebooknamespace="moonstore">
                <div class="hidden-gallery">
                            
                    </div>
        
<header id="header" class="clearfix">

    <div class="container clearfix">

        <div itemscope >
            <a href="/" id="logo" >
                <img class="logo" src="/web/templates/Site/assets/images/logo.png" alt="MoonStore">
            </a>
        </div>

        <nav class="menus">

            <div class="menu">
                                    <a href="/posts/category/1" class="menu-link category-link transition">Мода</a>
                                    <a href="/posts/category/2" class="menu-link category-link transition">Искусство</a>
                                    <a href="/posts/category/6" class="menu-link category-link transition">Партнеры</a>
                                    <a href="/posts/category/9" class="menu-link category-link transition">Moon Store studio</a>
                            </div>

            <div class="menu social-menu hidden-tablet">
                <a href="https://www.facebook.com/moonstore.it/" class="menu-link social-link" target="_blank"><span class="icon icon-facebook"></span></a>
                <a href="https://twitter.com/moon__store" class="menu-link social-link" target="_blank"><span class="icon icon-twitter"></span></a>
                <a href="https://www.instagram.com/moonstore.it/" class="menu-link social-link" target="_blank"><span class="icon icon-instagram"></span></a>
                <button id="search-btn" class="menu-link social-link hidden-phone"><span class="icon icon-search"></span></button>
            </div>

        </nav>

        <button id="menu-btn" class="visible-phone menu-link"><span class="icon icon-menu"></span></button>

    </div>

    <div id="header-search-container">
        <form class="header-search" action="/posts/search/" method="GET">
            <input type="search" id="search-input" name="q" data-search=" " required="" value="" />
            <button id="header-search-btn" type="submit">Поиск</button>
        </form>
    </div>

</header>
        <div id="main" role="main" class="container clearfix">

            <article class="article clearfix" id="main-article" itemscope="itemscope">



                <div class="headings">
                    <h3 class="column-headings">
                        <a class="column-title" href="#" itemprop="articleSection">Мода</a> 
                    </h3>
                    <h2 class="title">Как нас заставляют покупать?</h2>
                    <time class="date" datetime="2015-12-16 13:17:17" itemprop="datePublished" style="color: #c9aa90">2015-12-16 13:17:17</time>

                </div>
                <div class="article-hero landscape">
                    <figure class="main-img">
                                                    <img alt="Как нас заставляют покупать?" data-aspect-ratio="1.57" data-aspect-ratio-type="landscape" 
                                 data-max-height="1000" data-max-width="1571" 
                                 data-responsive-widths="375,480,1257" 
                                 itemprop="image" src="/web/templates/Site/assets/images/Posts/moon-store-kak-nas-zastavlyaut-pokupat.jpg" style="max-width: 100%; width:1257px" />
                                            </figure>

                </div>

                <div class="article-container clearfix" itemprop="articleBody">

                    <div class="sidebar-social-wrapper">

                        <aside class="sidebar-social social-btns hidden-phone hidden-tablet">

                            <div class="social-btns">
                                <div class="social-btn">
                                    <script type="text/javascript">
                                        document.write(VK.Share.button({
                                            url: 'dev.moonstore.it/posts/show/35735',
                                            title: 'Как нас заставляют покупать?',
                                            description: 'Сегодняшний день, для большинства современных людей, начинается, как говорится, не с кофе',
                                            image: '/web/templates/Site/assets/images/Posts/moon-store-kak-nas-zastavlyaut-pokupat.jpg',
                                            noparse: true
                                        }));</script>
                                </div>

                                <div class="social-btn">
                                    <a name="fb_share" type="button_count"
                                       share_url="35735">Facebook</a> 
                                    <script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share"
                                            type="text/javascript">
                                    </script>
                                </div><br>
                            </div>

                        </aside>
                    </div>


                    <div class="article-body" style="margin: 0 210px 40px;">
                                                Сегодняшний день, для большинства современных людей, начинается, как говорится, не с кофе.

Технологии и социальные медиа превратили нас в цифровых кочевников, способных перемещаться по миру и общаться практически в любой стране с помощью нового, простого международного “языка” – смайлов на вашем смартфоне. Для этого вам не нужно покидать пределы квартиры или даже кровати.

Это дает безграничные возможности для маркетологов. Мы невольно попадаем в их сети и приманки всего открыв Instagram чтобы посмотреть новые фото друзей. Все больше и больше сервисов, которые, казалось бы, никак не относятся к покупкам, становятся “shopable”. Если посмотреть на Pinterest, где пользователи делятся стильными дизайнерскими идеями, собранными со всей сети, то можно заметить, как от стратегии “смотри и любуйся” он эволюционировал к стратегии “увидел и купил”. Даже в Shazam’е, который был создан для распознавания музыки, сегодня можно купить… Джинсы. Так что появление кнопки “купить сейчас” на профилях модных брендов в YouTube и Twitter это только вопрос времени.

Сегодня существует масса различных доступных сервисов, которыми мы пользуемся каждый день и из которых мы можем узнавать о моде быстрее, чем из любого печатного издания в газетном киоске.

К примеру, Тейлор Свифт загружает в свой Instagram фото с очередной церемонии награждения. На ней шикарное платье, которое невольно увидят ее 59 миллионов подписчиков листающих ленту. И очень многим оно понравится, после чего они станут искать его в интернете с желанием купить.

<blockquote class="instagram-media" data-instgrm-version="6" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/1r4sDGDvKv/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">Фото опубликовано Taylor Swift (@taylorswift)</a> <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2015-04-20T05:53:36+00:00">Апр 19 2015 в 10:53 PDT</time></p></div></blockquote> <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>

Для того чтобы посмотреть показ Burberry в первых рядах сегодня достаточно лишь открыть Periscope.

Томми Тон – влиятельный канадский стрит-стайл фотограф и блоггер мира моды, загружает новое фото, на котором изображен крутой парень в бейсбольной футболке. Моментально после этого, все мужчины побегут осматривать свой гардероб в поиске такой же. А если не найдут, то будут готовы купить ее здесь и сейчас.

Вы даже можете получить бесплатные уроки макияжа, снявшись в одном из видео блоггеров-подростков.

<strong>Кто теперь нуждается в редакторе Vogue?</strong>

Последние пару лет все стали разбираться в стиле и моде, даже те, кто совершенно не интересовался этим раньше (мужчины). Но и те, кто был гиком моды, открыли новые возможности для себя. К примеру, девушке больше не нужно спрашивать совет насчет ее нового платья у завистливых коллег или совершенно не разбирающегося в платьях любимого парня. Она заходит в Google с соответствующим запросом и получает ответы на все вопросы, при этом никто об этом не догадается и ей не придется смущаться, услышав критику ее наряда.

Наша привычка "покупать" меняется вместе с нашим сумасшедшим ритмом жизни. Мы следим за покупками все больше и больше на наших смартфонах. Недавно, американцы, любители статистики и цифр, сообщили, что треть(!) сделок электронной коммерции на рынке моды и роскоши США в этом году было совершено через мобильные устройства. В то время как в Японии и Южной Корее этот показатель уже превышает 50%!

Именно поэтому технологии стали частью модной индустрии как никогда ранее. Вот почему французский бренд Hermes сотрудничает с Apple, а Intel с ее чувствительными датчиками встроенными в одежду, была главной компанией на неделе моды в Нью-Йорке.

Многие вовсе не считают, что наш мир меняется к лучшему, не всем по душе новые реалии. И это не потому, что эти люди консервативны, или живут с убеждением, что все новое – это плохо.

Через фото-фильтры наша жизнь выглядит лучше, чем она есть. И это делается не для того, чтобы романтизировать и воспеть красоту нашей жизни, а зачастую лишь для того, чтобы заработать. Конечно, так бывает далеко не всегда и подобное обязательно существовало бы, даже если прогресс стоял на месте, такова природа человека, но иногда это действительно огорчает. Особенно когда топовые модные дома создают коллекцию так, лишь бы она получше смотрелась на экране смартфона, и как можно больше людей купили ее. А то, что в жизни она выглядит, мягко говоря, не так жизнерадостно - не важно. Конечно, всем нужны деньги, талант и работа всегда должны быть оплачена. Мода – это бизнес, в конце концов, но, увы, многие забывают, что это еще и искусство.

Однако эти тенденции выходят далеко за пределы моды. Вот, к примеру, в приютах для животных очень много добрых, милых, и хороших котов и собак. Люди любят животных, многие не прочь бы завести себе питомца. Так почему же им так трудно найти новый дом? Наверное, потому, что их не рекламируют в Instagram…

                    </div>


                </div>




                <div class="article-footer">

                    <div class="tags-list">
                        <strong>Теги:</strong> 
                                                                                    <a href="/posts/find/tag/мужская мода" class="tag-btn">#мужская мода</a> 
                                                                                                                <a href="/posts/find/tag/Instagram" class="tag-btn">#Instagram</a> 
                                                                        </div>


                    <div class="more-articles-container">

                        <h3 class="cat-title"><a href="/where-on-earth">Последнее в категории</a></h3>

                        <ul class="articles more-articles grid col-5 clearfix">
                                                            <li class="grid-item article">
                                    <a href="/posts/show/35806" data-track="event" data-title=""
                                       data-value="Click">
                                        <img alt="Лучшие бренды 2015 года" class="img"
                                             data-aspect-ratio="1.77" data-aspect-ratio-type="landscape" 
                                             data-max-height="1000" data-max-width="1773" 
                                             src="/web/templates/Site/assets/images/Posts/best-brands-2015.jpg" />
                                        
                                        <h4 class="title">Лучшие бренды 2015 года</h4>
                                    </a>
                                </li>
                                                            <li class="grid-item article">
                                    <a href="/posts/show/35785" data-track="event" data-title=""
                                       data-value="Click">
                                        <img alt="Киевский ЦУМ. Перерождение легенды" class="img"
                                             data-aspect-ratio="1.77" data-aspect-ratio-type="landscape" 
                                             data-max-height="1000" data-max-width="1773" 
                                             src="/web/templates/Site/assets/images/Posts/11214064_886397368106593_5614781332794543993_n.jpg" />
                                        
                                        <h4 class="title">Киевский ЦУМ. Перерождение легенды</h4>
                                    </a>
                                </li>
                                                            <li class="grid-item article">
                                    <a href="/posts/show/35762" data-track="event" data-title=""
                                       data-value="Click">
                                        <img alt="Чем запомнились мужчины в 2015" class="img"
                                             data-aspect-ratio="1.77" data-aspect-ratio-type="landscape" 
                                             data-max-height="1000" data-max-width="1773" 
                                             src="/web/templates/Site/assets/images/Posts/moon-store-man.jpg" />
                                        
                                        <h4 class="title">Чем запомнились мужчины в 2015</h4>
                                    </a>
                                </li>
                                                            <li class="grid-item article">
                                    <a href="/posts/show/35744" data-track="event" data-title=""
                                       data-value="Click">
                                        <img alt="Дизайнеры Valentino собираются покорить мир" class="img"
                                             data-aspect-ratio="1.77" data-aspect-ratio-type="landscape" 
                                             data-max-height="1000" data-max-width="1773" 
                                             src="/web/templates/Site/assets/images/Posts/DC_Portrait_newMAISON-kopiya.jpg" />
                                        
                                        <h4 class="title">Дизайнеры Valentino собираются покорить мир</h4>
                                    </a>
                                </li>
                                                            <li class="grid-item article">
                                    <a href="/posts/show/35735" data-track="event" data-title=""
                                       data-value="Click">
                                        <img alt="Как нас заставляют покупать?" class="img"
                                             data-aspect-ratio="1.77" data-aspect-ratio-type="landscape" 
                                             data-max-height="1000" data-max-width="1773" 
                                             src="/web/templates/Site/assets/images/Posts/moon-store-kak-nas-zastavlyaut-pokupat.jpg" />
                                        
                                        <h4 class="title">Как нас заставляют покупать?</h4>
                                    </a>
                                </li>
                                                            <li class="grid-item article">
                                    <a href="/posts/show/35721" data-track="event" data-title=""
                                       data-value="Click">
                                        <img alt="Пьер Карден или "Король" патентов" class="img"
                                             data-aspect-ratio="1.77" data-aspect-ratio-type="landscape" 
                                             data-max-height="1000" data-max-width="1773" 
                                             src="/web/templates/Site/assets/images/Posts/pierre-cardin_2413573-moonstore.jpg" />
                                        
                                        <h4 class="title">Пьер Карден или "Король" патентов</h4>
                                    </a>
                                </li>
                                                            <li class="grid-item article">
                                    <a href="/posts/show/35664" data-track="event" data-title=""
                                       data-value="Click">
                                        <img alt="10 отелей, созданных известными модельерами " class="img"
                                             data-aspect-ratio="1.77" data-aspect-ratio-type="landscape" 
                                             data-max-height="1000" data-max-width="1773" 
                                             src="/web/templates/Site/assets/images/Posts/Armani-Hotel-Spa-in-Dubai-960x600.jpg" />
                                        
                                        <h4 class="title">10 отелей, созданных известными модельерами </h4>
                                    </a>
                                </li>
                                                            <li class="grid-item article">
                                    <a href="/posts/show/35647" data-track="event" data-title=""
                                       data-value="Click">
                                        <img alt="История эволюции дамского бюстгальтера " class="img"
                                             data-aspect-ratio="1.77" data-aspect-ratio-type="landscape" 
                                             data-max-height="1000" data-max-width="1773" 
                                             src="/web/templates/Site/assets/images/Posts/Istoriya-e`volyutsii-damskogo-byustgaltera-.jpg" />
                                        
                                        <h4 class="title">История эволюции дамского бюстгальтера </h4>
                                    </a>
                                </li>
                                                            <li class="grid-item article">
                                    <a href="/posts/show/35626" data-track="event" data-title=""
                                       data-value="Click">
                                        <img alt="Burberry обогнал по продажам в интернете Louis Vuitton" class="img"
                                             data-aspect-ratio="1.77" data-aspect-ratio-type="landscape" 
                                             data-max-height="1000" data-max-width="1773" 
                                             src="/web/templates/Site/assets/images/Posts/01-960x600.jpg" />
                                        
                                        <h4 class="title">Burberry обогнал по продажам в интернете Louis Vuitton</h4>
                                    </a>
                                </li>
                                                            <li class="grid-item article">
                                    <a href="/posts/show/35598" data-track="event" data-title=""
                                       data-value="Click">
                                        <img alt="Миучча Прада рассказала о своем "тревожном" творчестве" class="img"
                                             data-aspect-ratio="1.77" data-aspect-ratio-type="landscape" 
                                             data-max-height="1000" data-max-width="1773" 
                                             src="/web/templates/Site/assets/images/Posts/miuccia-prada-photo-guido-harari1999-828x1024-1748x984.jpg" />
                                        
                                        <h4 class="title">Миучча Прада рассказала о своем "тревожном" творчестве</h4>
                                    </a>
                                </li>
                                                    </ul>
                    </div>
                </div>
            </article>
        </div>


        <div class='foot'>
            <footer id="footer">

            <div class="container">

                <div class="primary-nav">
                    <a href="/contact" class="footer-link">Связаться с нами</a>
                    <a href="/aboutus" class="footer-link">О нас</a>
                </div>

                <div class="social-menu">
                  <a href="https://www.facebook.com/moonstore.it/" class="social-link" target="blank">
                                        <span class="icon icon-facebook"></span></a>
                                    <a href="https://twitter.com/moon__store" class="social-link" target="blank">
                                        <span class="icon icon-twitter"></span></a>
                                    <a href="https://www.instagram.com/moonstore.it/" class="social-link" target="blank">
                                        <span class="icon icon-instagram"></span></a>
                </div>

                <div class="copyright">&copy; 2016 - 2017 Moon Store.</div>

            </div>

        </footer>
        </div>
        <script type="text/javascript">
            $(".show-gallery").click(function () {
                var ids = $(this).data('id');
                var id = $(".hidden-gallery").find('span');
                $.each(id, function (i, span) {
                    if (ids == $(span).data('id')) {
                        var bg = $('.hidden-gallery').find('#bgimg');
                        $(bg).attr('src', $(span).parent('a').attr('href'));
                    }
                });
                $("#main").hide();
                $("#header").hide();
                $(".foot").hide();
                $(".hidden-gallery").show();
            });
            $(".close-gallery").click(function () {
                //$("#outer_container").css('z-index', '-1000');
                $("#main").show();
                $("#header").show();
                $(".foot").show();
                $(".hidden-gallery").hide();
            });

        </script>
        
            <script>
                //set default view mode
                $defaultViewMode = "original"; //full (fullscreen background), fit (fit to window), original (no scale)
                //cache vars
                $bg = $("#bg");
                $bgimg = $("#bg #bgimg");
                $preloader = $("#preloader");
                $outer_container = $("#outer_container");
                $outer_container_a = $("#outer_container a.thumb_link");
                $toolbar = $("#toolbar");
                $nextimage_tip = $("#nextimage_tip");

                $(window).load(function () {
                    $(".hidden-gallery").show();
                    $toolbar.data("imageViewMode", $defaultViewMode); //default view mode
                    ImageViewMode($toolbar.data("imageViewMode"));
                    //cache vars
                    $customScrollBox = $("#customScrollBox");
                    $customScrollBox_container = $("#customScrollBox .container");

                    $customScrollBox.height($customScrollBox_container.height());

                    //resize browser window functions
                    $(window).resize(function () {
                        FullScreenBackground("#bgimg"); //scale bg image
                    });

                    LargeImageLoad($bgimg);
                    $(".hidden-gallery").hide();
                });

                //loading bg image
                $bgimg.load(function () {
                    LargeImageLoad($(this));
                });

                function LargeImageLoad($this) {
                    $preloader.fadeOut("fast"); //hide preloader
                    $this.removeAttr("width").removeAttr("height").css({width: "", height: ""}); //lose all previous dimensions in order to rescale new image data
                    $bg.data("originalImageWidth", $this.width()).data("originalImageHeight", $this.height());
                    if ($bg.data("newTitle")) {
                        $this.attr("title", $bg.data("newTitle")); //set new image title attribute
                    }
                    FullScreenBackground($this); //scale new image
                    $bg.data("nextImage", $($outer_container.data("selectedThumb")).next().attr("href")); //get and store next image
                    if (typeof itemIndex != "undefined") {
                        if (itemIndex == lastItemIndex) { //check if it is the last image
                            $bg.data("lastImageReached", "Y");
                            $bg.data("nextImage", $outer_container_a.first().attr("href")); //get and store next image
                        } else {
                            $bg.data("lastImageReached", "N");
                        }
                    } else {
                        $bg.data("lastImageReached", "N");
                    }
                    $this.fadeIn("slow"); //fadein background image
                    if ($bg.data("nextImage") || $bg.data("lastImageReached") == "Y") { //don't close thumbs pane on 1st load
                        SlidePanels("close"); //close the left pane
                    }
                    NextImageTip();
                }

                //slide in/out left pane
                $outer_container.hover(
                        function () { //mouse over
                            //SlidePanels("open");
                        },
                        function () { //mouse out
                            SlidePanels("close");
                        }
                );

                $("#arrow_indicator").click(
                        function () { //mouse over
                            SlidePanels("open");
                        }
                );

                //Clicking on thumbnail changes the background image
                $outer_container_a.click(function (event) {
                    event.preventDefault();
                    var $this = this;
                    $bgimg.css("display", "none");
                    $preloader.fadeIn("fast"); //show preloader
                    //style clicked thumbnail
                    $outer_container_a.each(function () {
                        $(this).children(".selected").css("display", "none");
                    });
                    $(this).children(".selected").css("display", "block");
                    //get and store next image and selected thumb 
                    $outer_container.data("selectedThumb", $this);
                    $bg.data("nextImage", $(this).next().attr("href"));
                    $bg.data("newTitle", $(this).children("img").attr("title")); //get and store new image title attribute
                    itemIndex = getIndex($this); //get clicked item index
                    lastItemIndex = ($outer_container_a.length) - 1; //get last item index
                    $bgimg.attr("src", "").attr("src", $this); //switch image
                });

                //clicking on large image loads the next one
                $bgimg.click(function (event) {
                    var $this = $(this);
                    if ($bg.data("nextImage")) { //if next image data is stored
                        $this.css("display", "none");
                        $preloader.fadeIn("fast"); //show preloader
                        $($outer_container.data("selectedThumb")).children(".selected").css("display", "none"); //deselect thumb
                        if ($bg.data("lastImageReached") != "Y") {
                            $($outer_container.data("selectedThumb")).next().children(".selected").css("display", "block"); //select new thumb
                        } else {
                            $outer_container_a.first().children(".selected").css("display", "block"); //select new thumb - first
                        }
                        //store new selected thumb
                        var selThumb = $outer_container.data("selectedThumb");
                        if ($bg.data("lastImageReached") != "Y") {
                            $outer_container.data("selectedThumb", $(selThumb).next());
                        } else {
                            $outer_container.data("selectedThumb", $outer_container_a.first());
                        }
                        $bg.data("newTitle", $($outer_container.data("selectedThumb")).children("img").attr("title")); //get and store new image title attribute
                        if ($bg.data("lastImageReached") != "Y") {
                            itemIndex++;
                        } else {
                            itemIndex = 0;
                        }
                        $this.attr("src", "").attr("src", $bg.data("nextImage")); //switch image
                    }
                });

                //function to get element index (fuck you IE!)
                function getIndex(theItem) {
                    for (var i = 0, length = $outer_container_a.length; i < length; i++) {
                        if ($outer_container_a[i] === theItem) {
                            return i;
                        }
                    }
                }

                //toolbar (image view mode button) hover
                $toolbar.hover(
                        function () { //mouse over
                            $(this).stop().fadeTo("fast", 1);
                        },
                        function () { //mouse out
                            $(this).stop().fadeTo("fast", 0.8);
                        }
                );
                $toolbar.stop().fadeTo("fast", 0.8); //set its original state

                //Clicking on toolbar changes the image view mode
                $toolbar.click(function (event) {
                    if ($toolbar.data("imageViewMode") == "full") {
                        ImageViewMode("fit");
                    } else if ($toolbar.data("imageViewMode") == "fit") {
                        ImageViewMode("original");
                    } else if ($toolbar.data("imageViewMode") == "original") {
                        ImageViewMode("full");
                    }
                });

                //next image balloon tip
                function NextImageTip() {
                    if ($bg.data("nextImage")) { //check if this is the first image
                        $nextimage_tip.stop().css("right", 20).fadeIn("fast").fadeOut(2000, "easeInExpo", function () {
                            $nextimage_tip.css("right", $(window).width());
                        });
                    }
                }

                //slide in/out left pane function
                function SlidePanels(action) {
                    var speed = 900;
                    var easing = "easeInOutExpo";
                    if (action == "open") {
                        $("#arrow_indicator").fadeTo("fast", 0);
                        $outer_container.stop().animate({left: 0}, speed, easing);
                        $bg.stop().animate({left: 585}, speed, easing);
                    } else {
                        $outer_container.stop().animate({left: -710}, speed, easing);
                        $bg.stop().animate({left: 0}, speed, easing, function () {
                            $("#arrow_indicator").fadeTo("fast", 1);
                        });
                    }
                }

                //Image scale function
                function FullScreenBackground(theItem) {
                    var winWidth = $(window).width();
                    var winHeight = $(window).height();
                    var imageWidth = $(theItem).width();
                    var imageHeight = $(theItem).height();
                    if ($toolbar.data("imageViewMode") != "original") { //scale
                        $(theItem).removeClass("with_border").removeClass("with_shadow"); //remove extra styles of orininal view mode
                        var picHeight = imageHeight / imageWidth;
                        var picWidth = imageWidth / imageHeight;
                        if ($toolbar.data("imageViewMode") != "fit") { //image view mode: full
                            if ((winHeight / winWidth) < picHeight) {
                                $(theItem).css("width", winWidth).css("height", picHeight * winWidth);
                            } else {
                                $(theItem).css("height", winHeight).css("width", picWidth * winHeight);
                            }
                            ;
                        } else { //image view mode: fit
                            if ((winHeight / winWidth) > picHeight) {
                                $(theItem).css("width", winWidth).css("height", picHeight * winWidth);
                            } else {
                                $(theItem).css("height", winHeight).css("width", picWidth * winHeight);
                            }
                            ;
                        }
                        //center it
                        $(theItem).css("margin-left", ((winWidth - $(theItem).width()) / 2)).css("margin-top", ((winHeight - $(theItem).height()) / 2));
                    } else { //no scale
                        //add extra styles for orininal view mode
                        $(theItem).addClass("with_border").addClass("with_shadow");
                        //set original dimensions
                        $(theItem).css("width", $bg.data("originalImageWidth")).css("height", $bg.data("originalImageHeight"));
                        //center it
                        $(theItem).css("margin-left", ((winWidth - $(theItem).outerWidth()) / 2)).css("margin-top", ((winHeight - $(theItem).outerHeight()) / 2));
                    }
                }

                //image view mode function - full or fit
                function ImageViewMode(theMode) {

                }

                //preload script images
                var images = ["/web/templates/Site/gallery/ajax-loader_dark.gif", "/web/templates/Site/gallery/round_custom_scrollbar_bg_over.png"];
                $.each(images, function (i) {
                    images[i] = new Image();
                    images[i].src = this;
                });
            </script>
        
    </body>
</html>
<?php }
}
?>