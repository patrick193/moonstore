<?php
/*%%SmartyHeaderCode:168963449756902a9b8a5016_78801946%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c97406a8c53460ac4f666a81103d2f9bc1dc5aae' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Admin/postAdd.tpl',
      1 => 1452217778,
      2 => 'file',
    ),
    'dcacc86d8945c492ff69ebe43b730721f2886f02' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Admin/menu.tpl',
      1 => 1452217778,
      2 => 'file',
    ),
    '43d8831b9b380800422352d9577b84e1de0d4150' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Admin/footer.tpl',
      1 => 1452217779,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '168963449756902a9b8a5016_78801946',
  'tpl_function' => 
  array (
  ),
  'variables' => 
  array (
    'post_edit' => 0,
    'category' => 0,
    'tags' => 0,
    'tag' => 0,
    'tagsp' => 0,
    'positions' => 0,
    'position' => 0,
    'aligns' => 0,
    'align' => 0,
    'i' => 0,
    'comment' => 0,
    'cat' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56902a9b9dab74_58984051',
  'cache_lifetime' => 3600,
),true);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56902a9b9dab74_58984051')) {
function content_56902a9b9dab74_58984051 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<html>
    <head>
        <link href="/web/templates/Admin/css/bootstrap.min.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/main.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/font-style.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/flexslider.css" rel="stylesheet">
        <link href="/web/templates/Admin/js/multiselect/jquery.tokenize.css" rel="stylesheet">
        <script type="text/javascript" src="/web/templates/Admin/js/jquery-1.10.2.min.js"></script>
        <link href="/web/templates/Admin/css/tingle.css" rel="stylesheet">
        <script type="text/javascript" src="/web/templates/Admin/js/bootstrap.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/lineandbars.js"></script>
        <!-- You can add more layouts if you want -->
        <script src="/web/templates/Admin/js/bootstrap-multiselect.js" type="text/javascript"></script>

        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/component.css" />
        <script type="text/javascript" src="/web/templates/Admin/js/tinymce/tinymce.min.js"></script>
        <script src="/web/templates/Admin/js/menu/js/modernizr.custom.25376.js"></script>
        <script src="/web/templates/Admin/js/video.js"></script>

        <!-- Our main JS file -->
        <script type="text/javascript">
            tinymce.init({
                height: '750px',
                width: '100%',
                selector: "textarea",
                plugins: [
                    "advlist autolink lists link charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime table contextmenu paste imagetools jbimages",
                    "youtube",
                ],
                language: "ru",
                toolbar: "preview | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | jbimages | youtube",
                image_advtab: true,
            });
            $(document).ready(function () {
                $(".video-lb").hide();
                $(".vimeo-player").hide();
            });

        </script>

        <style>
            p {
                font-family: sans-serif;
            }

            label.custom-select {
                position: relative;
                display: inline-block;
            }

            .custom-select select {
                display: inline-block;
                border: 2px solid #bbb;
                padding: 4px 3px 3px 5px;
                margin: 0;
                font: inherit;
                outline:none; /* remove focus ring from Webkit */
                line-height: 1.2;
                background: #f8f8f8;

                -webkit-appearance:none; /* remove the strong OSX influence from Webkit */

                -webkit-border-radius: 6px;
                -moz-border-radius: 6px;
                border-radius: 6px;
            }

            /* for Webkit's CSS-only solution */
            @media screen and (-webkit-min-device-pixel-ratio:0) { 
                .custom-select select {
                    padding-right:30px;    
                }
            }

            /* Since we removed the default focus styles, we have to add our own */
            .custom-select select:focus {
                -webkit-box-shadow: 0 0 3px 1px #c00;
                -moz-box-shadow: 0 0 3px 1px #c00;
                box-shadow: 0 0 3px 1px #c00;
            }

            /* Select arrow styling */
            .custom-select:after {
                content: "▼";
                position: absolute;
                top: 0;
                right: 0;
                bottom: 0;
                font-size: 60%;
                line-height: 30px;
                padding: 0 7px;
                background: #bbb;
                color: white;

                pointer-events:none;

                -webkit-border-radius: 0 6px 6px 0;
                -moz-border-radius: 0 6px 6px 0;
                border-radius: 0 6px 6px 0;
            }

            .no-pointer-events .custom-select:after {
                content: none;
            }
            @media (max-width: 1210px){
                .post_comments{
                    display: none;
                }
            }
        </style>

    </head>
    <body>

        <link rel="stylesheet" href="/web/templates/Admin/css/demo.css">
<link rel="stylesheet" href="/web/templates/Admin/css/forkit.css">
<link href="/web/templates/Admin/css/style-slider.css" rel="stylesheet" />
<link href="/web/templates/Admin/css/menu/css/jquery-ui.css" rel="stylesheet" />
<link href="/web/templates/Admin/js/menu/js/image-picker/image-picker/image-picker.css" rel="stylesheet" />


<script src="/web/templates/Admin/js/menu/js/jquery-ui.js "></script>
<script src="/web/templates/Admin/js/menu/js/image-picker/image-picker/image-picker.js"></script>
<div class="navbar-nav navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin"><img src="/web/templates/Admin/img/Moon-Store-transparent-bg.png" width="50px" height="50px" alt="Moonstore"></a>
        </div> 
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Сайт</a></li>                            
                <li><a href="/admin/posts">Статьи</a></li>
                <li><a href="/admin/post/add">Создать статью</a></li>
                                          
                    <li><a href="/admin/profile/usermanagement"> Пользователи</a></li>
                                    <li><a href="#" id="showMenu">Расширенное меню</a></li>

            </ul>
        </div><!--/.nav-collapse -->
    </div>    
    <!-- The contents (if there's no contents the ribbon acts as a link) -->
            <div class="forkit-curtain">
            <div class="close-button"></div>

            
            <div class="col-sm-3 col-lg-3">
                <div class="dash-unit">
                    <dtitle>Настройки сайта </dtitle>
                    <hr>

                    <div class="text">

                        <label class="checkbox inline">
                            Из одной категории
                            <div class="switch">
                                <input type="radio" class="switch-input" name="isLocked" value="true" id="on" checked="" >
                                <label for="on" class="switch-label switch-label-off">On</label>
                                <input type="radio" class="switch-input" name="isLocked" value="false" id="off"  >
                                <label for="off" class="switch-label switch-label-on">Off</label>
                                <span class="switch-selection"></span><br>

                                <div class="count">
                                    <label for="number">Количество</label>
                                    <div class="number">
                                        <input type="number" value="10" id="number"/>
                                        <br>

                                    </div>
                                </div>
                                <a href="#" class="save">Сохранить</a>

                                
                                    <script type="text/javascript">
                                        $("input.switch-input").on("click", function () {
                                            if ($("input:checked").val() == 'true') {

                                                $(".count").fadeIn(400);
                                            } else {
                                                $(".count").fadeOut(400);
                                                $("input#number").val('0');
                                            }
                                        });
                                        $(".save").click(function () {
                                            $.ajax({
                                                url: '/admin/pagination/save/',
                                                method: 'POST',
                                                data: {count: $("#number").val()},
                                                success: function () {
                                                    alert('Сохранено');
                                                },
                                                error: function () {
                                                    alert('Не удачно');
                                                }
                                            });

                                        });</script>
                                    
                            </div> 
                        </label>
                    </div>
                    <br>
                    <div class="info-user">

                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-lg-3">
                <div class="dash-unit">
                    <dtitle>Пагинация </dtitle>
                    <hr>
                    <div class="text">

                        <div class="count-pagination">
                            <label for="number">Количество статей на странице</label>
                            <div class="number">
                                <input type="number" value="14" id="number-pagination"/>
                                <br>
                            </div>
                        </div>
                        <a href="#" class="save-pagination">Сохранить</a>
                        <script type="text/javascript">
                            
                                $(".save-pagination").click(function () {
                                    $.ajax({
                                        url: '/admin/pagination/save/pagination/',
                                        method: 'POST',
                                        data: {count: $("#number-pagination").val()},
                                        success: function () {
                                            alert('Сохранено');
                                        },
                                        error: function () {
                                            alert('Не удачно');
                                        }
                                    });
                                    //$.post("/admin/pagination/save/pagination/", $("#number-pagination").val());
                                    //                                   alert("Сохранено");
                                });
                            </script>
                        </div>
                        <br>
                    </div>
                </div>
                <div class="col-sm-3 col-lg-3">
                    <div class="dash-unit">
                        <dtitle>Слайдер</dtitle>
                        <hr>
                        <div class="thumbnail">
                            Количество слайдов
                        </div><!-- /thumbnail -->
                        <div class="text">
                            <form id="upload" method="post" action="/admin/slider/save/slider/" enctype="multipart/form-data">
                                <div id="drop">
                                    Перетащи сюда

                                    <a>Выбрать</a>
                                    <input type="file" name="upl" multiple />
                                </div>

                                <ul>
                                    <!-- The file uploads will be shown here -->
                                </ul>

                            </form>

                        </div>
                        <br>

                    </div>
                </div>
                <div class="col-sm-3 col-lg-3">
                    <div class="dash-unit">
                        <dtitle>Настройки сайта </dtitle>
                        <hr>
                        <div class="text" style="overflow: scroll; height:244px;">
                            <form id="slider-settings" method="get" action="#">
                                <select class="image-picker show-html">
                                                                            <option data-img-src="/web/templates/Site/assets/images/Slider/1.jpg" value="2">2</option>
                                                                            <option data-img-src="/web/templates/Site/assets/images/Slider/8589130591340-evil-angel-the-white-rose-wallpaper-hd.jpg" value="8">8</option>
                                                                            <option data-img-src="/web/templates/Site/assets/images/Slider/Dragon-Wallpaper-dragons-13975568-1280-800.jpg" value="9">9</option>
                                                                            <option data-img-src="/web/templates/Site/assets/images/Slider/mhmzmdfyibbqqhglqrvx.jpg" value="25">25</option>
                                                                    </select>
                                <br>
                                <input type="text" class="post" name="post" placeholder="Статья">
                            </form>
                            <br>
                            <a class="save-slider">Сохранить настройки</a><br>
                            <a class="delete-slider" style="display: none">Удалить настройки</a>
                        </div>
                    </div>
                    <br>

                </div>
            </div>
            
            
                <script type="text/javascript">
                    ;
                    $("select").imagepicker();
                    $(".thumbnail").click(function () {
                        var imgsrc = $(this).find('img').attr('src');
                        $.ajax({
                            type: 'get',
                            url: '/admin/find/post/slider/',
                            data: {'imgsrc': imgsrc},
                            success: function (data) {
                                $('.post').val(data);
                            }
                        });
                        $(".delete-slider").show(300);
                    });
                    $(".delete-slider").click(function () {
                        var frm = $('#slider-settings');
                        $.ajax({
                            type: 'get',
                            url: '/admin/keepost/',
                            data: {'image_id': frm.find(".selected").find("img").attr('src'), 'post': frm.find('.post').val(), 'delete': 1},
                            success: function (data) {
                                //alert('ok');
                                frm.find(".selected").hide(700);
                               // frm.find(".selected").remove();
                            }
                        });
                    });
                    $(".save-slider").click(function () {
                        var frm = $('#slider-settings');

                        $.ajax({
                            type: 'get',
                            url: '/admin/keepost/',
                            data: {'image_id': frm.find(".selected").find("img").attr('src'), 'post': frm.find('.post').val()},
                            success: function (data) {
                                alert('ok');
                            }
                        });
                    });


                    //$(function () {
                    $(".post").autocomplete({
                        source: function (request, response) {
                            $.ajax({
                                url: '/admin/getposts/',
                                method: 'GET',
                                data: {'q': request.term},
                                dataType: 'json',
                                success: function (data) {
//                                            console.log(data);
                                    response(data);
                                }
                            });
                        },
                        minLength: 1,
                        open: function () {
                            $(".ui-autocomplete").insertAfter(".post");
                            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                        },
                        close: function () {
                            $(".ui-autocomplete").hide();
                            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                        }
                    });
                    //});
                </script>  
            
        </div>

        <!-- The ribbon -->
        <a class="forkit" data-text="Настройки" data-text-detached="Протяни вниз >" href="#"></a>

        <script src="/web/templates/Admin/js/forkit.js"></script>
                </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="/web/templates/Admin/js/sliderSettings/assets/js/jquery.knob.js"></script>

        <!-- jQuery File Upload Dependencies -->
        <script src="/web/templates/Admin/js/sliderSettings/assets/js/jquery.ui.widget.js"></script>
        <script src="/web/templates/Admin/js/sliderSettings/assets/js/jquery.iframe-transport.js"></script>
        <script src="/web/templates/Admin/js/sliderSettings/assets/js/jquery.fileupload.js"></script>

        <!-- Our main JS file -->
        <script src="/web/templates/Admin/js/sliderSettings/assets/js/script.js"></script>

        




        <div id="perspective" class="perspective effect-moveleft">
                        <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <!-- Top Navigation -->

                    <div class="main clearfix">
                        <!-- FIRST ROW OF BLOCKS -->     
                        <div class="container">

                            <div class="row">

                                                                    <div class="post_add">
                                        <form method="post" action="/admin/profile/posts/add/" enctype="multipart/form-data" class="edit-form" style="margin-top: 100px">

                                            <label class="post-name"> Изображение статьи 

                                                <input name="image_file" id="imageInput" type="file" />
                                            </label>
                                            <label for="video_url" class="video-lb">
                                                Видео: 
                                                <input name="video_url" class="video" type="text"
                                                       style="width: 500px"
                                                       placeholder="Вставить ссылку на видео">
                                            </label>
                                            <div class="player">
                                                <iframe src=""
                                                        width="500" height="281" frameborder="0" 
                                                        webkitallowfullscreen mozallowfullscreen 
                                                        allowfullscreen
                                                        class="vimeo-player">
                                                </iframe> 
                                                <p>
                                            </div>
                                            <br><br>
                                            <input type="checkbox" name="chackbox-video" value="1" class="video-check"><br>Видео<br>
                                            <br>
                                            <label class="post-name"> Название статьи: </label>
                                            <input name="post_name" type="text" placeholder="Название статьи">


                                            <label class="custom-select">
                                                Категории: 

                                                <select name="category_id">
                                                                                                            <option value="1">Мода</option>
                                                                                                            <option value="2">Искусство</option>
                                                                                                            <option value="6">Партнеры</option>
                                                                                                            <option value="9">Moon Store studio</option>
                                                                                                    </select>
                                            </label>
                                            <br>
                                            <label class="post-seo-desc"> Краткое описание: </label>
                                            <input name="short_desc" type="text" placeholder="Краткое описание" value="">

                                            <textarea name="content" style="width:50%"></textarea>
                                            <label class="post-seo-desc"> Seo description: </label>
                                            <input name="post_seo_desc" type="text" placeholder="SEO описание">

                                            <label class="post-seo-title"> Seo title: </label>
                                            <input name="post_seo_desc" type="text" placeholder="SEO описание">

                                            <label class="posttags"> Tags:
                                                <select id="tokenize" name="tags[]" multiple="multiple" class="tokenize-sample">
                                                                                                            <option value="1">test</option>
                                                                                                            <option value="2">probuem</option>
                                                                                                            <option value="3">tetetetet</option>
                                                                                                            <option value="4">ajaxtagtest</option>
                                                                                                            <option value="5">tag1</option>
                                                                                                            <option value="6">tag2</option>
                                                                                                            <option value="7">moonstore</option>
                                                                                                            <option value="8">moonstore2</option>
                                                                                                            <option value="9">testmoon</option>
                                                                                                            <option value="10">mooooooo</option>
                                                                                                            <option value="11">tetetetetetet</option>
                                                                                                            <option value="12">dfadsdasa</option>
                                                                                                            <option value="13">dfadsdasaSSASdfvefrrt5trtsts</option>
                                                                                                            <option value="14">Saint Laurent</option>
                                                                                                            <option value="15">MOSCHINO</option>
                                                                                                            <option value="16">Джереми Скотт</option>
                                                                                                            <option value="17">MARC JACOBS</option>
                                                                                                            <option value="18">Обувь</option>
                                                                                                            <option value="19">кроссовки</option>
                                                                                                            <option value="20">jil sander</option>
                                                                                                            <option value="21">Maison Martin Margiela</option>
                                                                                                            <option value="22">alexander mcqueen</option>
                                                                                                            <option value="23">PRADA</option>
                                                                                                            <option value="24">Hermès</option>
                                                                                                            <option value="25">givenchy</option>
                                                                                                            <option value="26">DAZED</option>
                                                                                                            <option value="27">Reebok</option>
                                                                                                            <option value="28">Эди Слиман</option>
                                                                                                            <option value="29">Фото</option>
                                                                                                            <option value="30">adidas</option>
                                                                                                            <option value="31">balenciaga</option>
                                                                                                            <option value="32">raf simons</option>
                                                                                                            <option value="33">Polanski</option>
                                                                                                            <option value="34">Emporio Armani</option>
                                                                                                            <option value="35">abajour</option>
                                                                                                            <option value="36">Marcelo Burlon</option>
                                                                                                            <option value="37">Sarah Burton</option>
                                                                                                            <option value="38">CIVILIZED</option>
                                                                                                            <option value="39">мужская мода</option>
                                                                                                            <option value="40">тренд сезона</option>
                                                                                                            <option value="41">игры</option>
                                                                                                            <option value="42">panorama lounge</option>
                                                                                                            <option value="43">Grovemade</option>
                                                                                                            <option value="44">Boo George</option>
                                                                                                            <option value="45">a.p.c.</option>
                                                                                                            <option value="46">bape</option>
                                                                                                            <option value="47">fifa</option>
                                                                                                            <option value="48">rick owens</option>
                                                                                                            <option value="49">supreme</option>
                                                                                                            <option value="50">world cup</option>
                                                                                                            <option value="51">Музыка</option>
                                                                                                            <option value="52">County of Milan</option>
                                                                                                            <option value="53">Jet Black</option>
                                                                                                            <option value="54">Max Snow</option>
                                                                                                            <option value="55">Ari Marcopoulos</option>
                                                                                                            <option value="56">Craig Redman</option>
                                                                                                            <option value="57">BeoPlay A9</option>
                                                                                                            <option value="58">Kellerstöckel</option>
                                                                                                            <option value="59">Джеймс МакЭвой</option>
                                                                                                            <option value="60">Christopher Kane</option>
                                                                                                            <option value="61">Nasir Mazhar</option>
                                                                                                            <option value="62">Louis Vuitton</option>
                                                                                                            <option value="63">Palmer // Harding</option>
                                                                                                            <option value="64">Menswear Hub</option>
                                                                                                            <option value="65">Sanahunt</option>
                                                                                                            <option value="66">Florence Hometown of Fashion</option>
                                                                                                            <option value="67">Tom Ford</option>
                                                                                                            <option value="68">Yves Saint Laurent</option>
                                                                                                            <option value="69">Hedi Slimane</option>
                                                                                                            <option value="70">Apple iWatch</option>
                                                                                                            <option value="71">Cristi</option>
                                                                                                            <option value="72">Sa nder Dekker</option>
                                                                                                            <option value="73">C-Heads</option>
                                                                                                            <option value="74">DKNY</option>
                                                                                                            <option value="75">Billionaire Boys</option>
                                                                                                            <option value="76">Ricardo Tisci</option>
                                                                                                            <option value="77">Versace</option>
                                                                                                            <option value="78">Donatella Versace</option>
                                                                                                            <option value="79">Dolce Gabbana</option>
                                                                                                            <option value="80">Domenico Dolce</option>
                                                                                                            <option value="81">Stefano Gabbana</option>
                                                                                                            <option value="82">к Stefano Gabbana</option>
                                                                                                            <option value="83">Vivienne Westwood</option>
                                                                                                            <option value="84">Miuccia Prada</option>
                                                                                                            <option value="85">Milano Fashion Week</option>
                                                                                                            <option value="86">Neil Barrett</option>
                                                                                                            <option value="87">MSGM</option>
                                                                                                            <option value="88">Marni</option>
                                                                                                            <option value="89">john varvatos</option>
                                                                                                            <option value="90">les hommes</option>
                                                                                                            <option value="91">MFW</option>
                                                                                                            <option value="92">ICEBERG</option>
                                                                                                            <option value="93">Giuseppe Zanotti</option>
                                                                                                            <option value="94">Bottega Veneta</option>
                                                                                                            <option value="95">Sterling Ruby</option>
                                                                                                            <option value="96">mareel</option>
                                                                                                            <option value="97">Citroën</option>
                                                                                                            <option value="98">Nicolas Ghesquiere</option>
                                                                                                            <option value="99">Annie Leibovitz</option>
                                                                                                            <option value="100">Charlotte Gainsbourg</option>
                                                                                                            <option value="101">Emily Ratajkowski</option>
                                                                                                            <option value="102">GQ</option>
                                                                                                            <option value="103">Фонд Louis Vuitton</option>
                                                                                                            <option value="104">Walter Van Beirendonck</option>
                                                                                                            <option value="105">PFW</option>
                                                                                                            <option value="106">DSQUARED2</option>
                                                                                                            <option value="107">Марина Абрамович</option>
                                                                                                            <option value="108">riccardo tisci</option>
                                                                                                            <option value="109">Paris Fashion Week</option>
                                                                                                            <option value="110">Dries Van Noten</option>
                                                                                                            <option value="111">Haider Ackermann</option>
                                                                                                            <option value="112">Dior</option>
                                                                                                            <option value="113">spirit of buddha</option>
                                                                                                            <option value="114">Redlight</option>
                                                                                                            <option value="115">COMME des GARÇONS</option>
                                                                                                            <option value="116">Nike</option>
                                                                                                            <option value="117">Boris Bidjan Saberi</option>
                                                                                                            <option value="118">Bruno Walpoth</option>
                                                                                                            <option value="119">Yohji Yamamoto</option>
                                                                                                            <option value="120">BALMAIN</option>
                                                                                                            <option value="121">Junya Watanabe</option>
                                                                                                            <option value="122">Rei Kawakubo</option>
                                                                                                            <option value="123">KENZO</option>
                                                                                                            <option value="124">Dior Homme</option>
                                                                                                            <option value="125">Kris Van Assche</option>
                                                                                                            <option value="126">Acne Studios</option>
                                                                                                            <option value="127">Lanvin</option>
                                                                                                            <option value="128">VALENTINO</option>
                                                                                                            <option value="129">y-3</option>
                                                                                                            <option value="130">Risa Fukui</option>
                                                                                                            <option value="131">Thome Browne</option>
                                                                                                            <option value="132">Veronique Nishanyan</option>
                                                                                                            <option value="133">MARC by MARC JACOBS</option>
                                                                                                            <option value="134">David Sims</option>
                                                                                                            <option value="135">innocad architecture</option>
                                                                                                            <option value="136">Stone Island</option>
                                                                                                            <option value="137">Josh Olins</option>
                                                                                                            <option value="138">Salvatore Ferragamo</option>
                                                                                                            <option value="139">Mert Alas</option>
                                                                                                            <option value="140">Marcus Piggott</option>
                                                                                                            <option value="141">Michele Lamy</option>
                                                                                                            <option value="142">Givenchy Robotics</option>
                                                                                                            <option value="143">Simeon Georgiev</option>
                                                                                                            <option value="144">Jean Paul Gaultier</option>
                                                                                                            <option value="145">GUCCI</option>
                                                                                                            <option value="146">LEVI"S</option>
                                                                                                            <option value="147">Notes of a day</option>
                                                                                                            <option value="148">Willy Vanderperre</option>
                                                                                                            <option value="149">visvim</option>
                                                                                                            <option value="150">iOS 8</option>
                                                                                                            <option value="151">40s &amp; Shorties</option>
                                                                                                            <option value="152">Lorenzo Vitturi</option>
                                                                                                            <option value="153">Dazed and Confused</option>
                                                                                                            <option value="154">James McAvoy</option>
                                                                                                            <option value="155">TROFYMENKO</option>
                                                                                                            <option value="156">Haute Couture</option>
                                                                                                            <option value="157">PC</option>
                                                                                                            <option value="158">Damir Doma</option>
                                                                                                            <option value="159">SILENT</option>
                                                                                                            <option value="160">fashion pre-party restaurant</option>
                                                                                                            <option value="161">house traffic group</option>
                                                                                                            <option value="162">perception collective</option>
                                                                                                            <option value="163">miu miu</option>
                                                                                                            <option value="164">Christian Dior</option>
                                                                                                            <option value="165">ALI SAULIDI</option>
                                                                                                            <option value="166">CHANEL</option>
                                                                                                            <option value="167">VANS</option>
                                                                                                            <option value="168">ESPN the Magazine</option>
                                                                                                            <option value="169">Pitti Uomo</option>
                                                                                                            <option value="170">Artisanal</option>
                                                                                                            <option value="171">рюкзак</option>
                                                                                                            <option value="172">HUGO by HUGO BOSS</option>
                                                                                                            <option value="173">Burberry</option>
                                                                                                            <option value="174">Christopher Bailey</option>
                                                                                                            <option value="175">diesel</option>
                                                                                                            <option value="176">Miranda Kerr</option>
                                                                                                            <option value="177">Calvin Klein</option>
                                                                                                            <option value="178">Terry Richardson</option>
                                                                                                            <option value="179">KRISVANASSCHE</option>
                                                                                                            <option value="180">PUMA</option>
                                                                                                            <option value="181">Christophe Lemaire</option>
                                                                                                            <option value="182">Katia Selinger</option>
                                                                                                            <option value="183">Gracie Hans</option>
                                                                                                            <option value="184">Jonathan Saunders</option>
                                                                                                            <option value="185">FROLOV</option>
                                                                                                            <option value="186">barneys</option>
                                                                                                            <option value="187">yasmin sewell</option>
                                                                                                            <option value="188">Olaf Hussein</option>
                                                                                                            <option value="189">Farewell Footwear</option>
                                                                                                            <option value="190">Molto</option>
                                                                                                            <option value="191">Fred Perry</option>
                                                                                                            <option value="192">LEVI"S</option>
                                                                                                            <option value="193">Levi’s</option>
                                                                                                            <option value="194">Carven</option>
                                                                                                            <option value="195">Guillaume Henry</option>
                                                                                                            <option value="196">Viviane Sassen</option>
                                                                                                            <option value="197">Gustaaf Wassink</option>
                                                                                                            <option value="198">David Gandy</option>
                                                                                                            <option value="199">The Glass Magazine</option>
                                                                                                            <option value="200">Roger Rich</option>
                                                                                                            <option value="201">Topshop</option>
                                                                                                            <option value="202">Cara Delevingne</option>
                                                                                                            <option value="203">Alasdair McLellan</option>
                                                                                                            <option value="204">ASOS</option>
                                                                                                            <option value="205">People Tree</option>
                                                                                                            <option value="206">Net-a-porter</option>
                                                                                                            <option value="207">MyWardrobe</option>
                                                                                                            <option value="208">Thom Browne</option>
                                                                                                            <option value="209">рубашка</option>
                                                                                                            <option value="210">Charlotte McKinney</option>
                                                                                                            <option value="211">Pirelli Calendar</option>
                                                                                                            <option value="212">STELLA MCCARTNEY</option>
                                                                                                            <option value="213">Kate Moss</option>
                                                                                                            <option value="214">Just Cavalli</option>
                                                                                                            <option value="215">Agent Provocateur</option>
                                                                                                            <option value="216">Mishka</option>
                                                                                                            <option value="217">ZARA</option>
                                                                                                            <option value="218">Filson</option>
                                                                                                            <option value="219">ALEXANDER WANG</option>
                                                                                                            <option value="220">ADIDAS Originals</option>
                                                                                                            <option value="221">Rita Ora</option>
                                                                                                            <option value="222">Pantherella</option>
                                                                                                            <option value="223">Beats by Dre</option>
                                                                                                            <option value="224">ColorWare</option>
                                                                                                            <option value="225">i-D Magazine</option>
                                                                                                            <option value="226">Polaroid Cube</option>
                                                                                                            <option value="227">HENRIKSILVIUS</option>
                                                                                                            <option value="228">SENSE Magazine</option>
                                                                                                            <option value="229">"W" Magazine</option>
                                                                                                            <option value="230">Supernormal Supermodel</option>
                                                                                                            <option value="231">Ferrante</option>
                                                                                                            <option value="232">Opening Ceremony</option>
                                                                                                            <option value="233">North West</option>
                                                                                                            <option value="234">Tom’s</option>
                                                                                                            <option value="235">AllSaints</option>
                                                                                                            <option value="236">MADE Kids</option>
                                                                                                            <option value="237">A$AP Rocky</option>
                                                                                                            <option value="238">Daniel Jackson</option>
                                                                                                            <option value="239">Tod’s</option>
                                                                                                            <option value="240">skinny jeans</option>
                                                                                                            <option value="241">J.Crew</option>
                                                                                                            <option value="242">Frank Muytjens</option>
                                                                                                            <option value="243">Christopher Raeburn</option>
                                                                                                            <option value="244">Gap</option>
                                                                                                            <option value="245">SHOWstudio</option>
                                                                                                            <option value="246">Armani Exchange</option>
                                                                                                            <option value="247">River Island</option>
                                                                                                            <option value="248">VICTIM</option>
                                                                                                            <option value="249">Chanel Iman</option>
                                                                                                            <option value="250">Hood by Air</option>
                                                                                                            <option value="251">Mai Gidah</option>
                                                                                                            <option value="252">Jeremy Scott</option>
                                                                                                            <option value="253">Miley Cyrus</option>
                                                                                                            <option value="254">Dr. Martens</option>
                                                                                                            <option value="255">Ronnie Fieg</option>
                                                                                                            <option value="256">Somebody</option>
                                                                                                            <option value="257">Beyoncé</option>
                                                                                                            <option value="258">Atelier Scotch</option>
                                                                                                            <option value="259">LVMH</option>
                                                                                                            <option value="260">Filippa K</option>
                                                                                                            <option value="261">Pepe Jeans</option>
                                                                                                            <option value="262">Line Up</option>
                                                                                                            <option value="263">NYFW</option>
                                                                                                            <option value="264">BLACKBARRETT</option>
                                                                                                            <option value="265">BMW R nine</option>
                                                                                                            <option value="266">Kim Kardashian</option>
                                                                                                            <option value="267">Gaastra</option>
                                                                                                            <option value="268">Rutger Hauer</option>
                                                                                                            <option value="269">Barbour</option>
                                                                                                            <option value="270">Freaks</option>
                                                                                                            <option value="271">Amanda Cerny</option>
                                                                                                            <option value="272">Георгий Ростовщиков</option>
                                                                                                            <option value="273">Gore-Tex</option>
                                                                                                            <option value="274">Lacoste</option>
                                                                                                            <option value="275">Eniko Mihalik</option>
                                                                                                            <option value="276">Prabal Gurung</option>
                                                                                                            <option value="277">Victoria Beckham</option>
                                                                                                            <option value="278">MARCHI</option>
                                                                                                            <option value="279">Tommy Hilfiger</option>
                                                                                                            <option value="280">Modscape</option>
                                                                                                            <option value="281">Harley</option>
                                                                                                            <option value="282">3.1 Phillip Lim</option>
                                                                                                            <option value="283">Gypsy One</option>
                                                                                                            <option value="284">Delphine Arnault</option>
                                                                                                            <option value="285">Ralph Lauren</option>
                                                                                                            <option value="286">Diesel Black Gold</option>
                                                                                                            <option value="287">Mat Abad</option>
                                                                                                            <option value="288">Tianna Gregory</option>
                                                                                                            <option value="289">J.W. Anderson</option>
                                                                                                            <option value="290">Nike + R.T. Air Force 1</option>
                                                                                                            <option value="291">Moon hoon</option>
                                                                                                            <option value="292">Karl Lagerfeld</option>
                                                                                                            <option value="293">Christian Louboutin</option>
                                                                                                            <option value="294">Frank Gehry</option>
                                                                                                            <option value="295">Cindy Sherman</option>
                                                                                                            <option value="296">Mark Newson</option>
                                                                                                            <option value="297">BURBERRY Prorsum</option>
                                                                                                            <option value="298">Y-3 Qasa</option>
                                                                                                            <option value="299">Dorian Grinspan</option>
                                                                                                            <option value="300">Gordon von Steiner</option>
                                                                                                            <option value="301">Tom Van Dorpe</option>
                                                                                                            <option value="302">Robert Storey</option>
                                                                                                            <option value="303">Brian Buenaventura</option>
                                                                                                            <option value="304">Шкаф моего парня</option>
                                                                                                            <option value="305">Naomi Yasuda</option>
                                                                                                            <option value="306">Marla Belt</option>
                                                                                                            <option value="307">Maurizio Bavutti</option>
                                                                                                            <option value="308">Lauren Boyle</option>
                                                                                                            <option value="309">Marco Roso</option>
                                                                                                            <option value="310">David Toro</option>
                                                                                                            <option value="311">Solomon Chase</option>
                                                                                                            <option value="312">Piergiorgio Del Moro и Samuel Ellis Scheinman</option>
                                                                                                            <option value="313">LeBron 12</option>
                                                                                                            <option value="314">Стрит-Стайл</option>
                                                                                                            <option value="315">FENDI</option>
                                                                                                            <option value="316">Kruz Co</option>
                                                                                                            <option value="317">Woolrich</option>
                                                                                                            <option value="318">Gemma Ward</option>
                                                                                                            <option value="319">Berlin Art Week</option>
                                                                                                            <option value="320">Spora</option>
                                                                                                            <option value="321">Buscemi</option>
                                                                                                            <option value="322">EVISU</option>
                                                                                                            <option value="323">Акио Такацука</option>
                                                                                                            <option value="324">James Jebbia</option>
                                                                                                            <option value="325">Anna Wintour</option>
                                                                                                            <option value="326">Adrian Joffe</option>
                                                                                                            <option value="327">Chris Kyvetos</option>
                                                                                                            <option value="328">JACQUEMUS</option>
                                                                                                            <option value="329">Apple Watch</option>
                                                                                                            <option value="330">Milan Fashion Week</option>
                                                                                                            <option value="331">G by Givenchy</option>
                                                                                                            <option value="332">Monster</option>
                                                                                                            <option value="333">sneaker melts</option>
                                                                                                            <option value="334">Dead Dilly</option>
                                                                                                            <option value="335">Ukrainian Fashion Week</option>
                                                                                                            <option value="336">Lexus NX</option>
                                                                                                            <option value="337">David Murray</option>
                                                                                                            <option value="338">John Galliano</option>
                                                                                                            <option value="339">Skylar Diggins</option>
                                                                                                            <option value="340">Hunter</option>
                                                                                                            <option value="341">Urban Outfitters</option>
                                                                                                            <option value="342">новости</option>
                                                                                                            <option value="343">Дарья Шаповалова</option>
                                                                                                            <option value="344">Branislav Simoncik</option>
                                                                                                            <option value="345">Frederik Muka</option>
                                                                                                            <option value="346">GARAGE Magazine</option>
                                                                                                            <option value="347">TONYS TOYS</option>
                                                                                                            <option value="348">Esquire</option>
                                                                                                            <option value="349">Celine</option>
                                                                                                            <option value="350">Jun Takahashi</option>
                                                                                                            <option value="351">Opel</option>
                                                                                                            <option value="352">Savage Beauty</option>
                                                                                                            <option value="353">Cola Cola</option>
                                                                                                            <option value="354">ELENA BURBA</option>
                                                                                                            <option value="355">CONVERSE</option>
                                                                                                            <option value="356">POUSTOVIT</option>
                                                                                                            <option value="357">Victoria Gres</option>
                                                                                                            <option value="358">LAKE</option>
                                                                                                            <option value="359">ALONOVA</option>
                                                                                                            <option value="360">ZALEVSKIY</option>
                                                                                                            <option value="361">Jean Gritsfeldt</option>
                                                                                                            <option value="362">GOLETS</option>
                                                                                                            <option value="363">ELENAREVA</option>
                                                                                                            <option value="364">bobkova</option>
                                                                                                            <option value="365">ARTEMKLIMCHUK</option>
                                                                                                            <option value="366">Sasha Kanevski</option>
                                                                                                            <option value="367">I Love Ugly</option>
                                                                                                            <option value="368">PODOLYAN</option>
                                                                                                            <option value="369">BURENINA</option>
                                                                                                            <option value="370">WHATEVER</option>
                                                                                                            <option value="371">Giselle Bundchen</option>
                                                                                                            <option value="372">Anisimov</option>
                                                                                                            <option value="373">ANDRE TAN</option>
                                                                                                            <option value="374">Anya Hindmarch</option>
                                                                                                            <option value="375">IDoL</option>
                                                                                                            <option value="376">Harper"s Bazaar Ukraine</option>
                                                                                                            <option value="377">Mary Katrantzou</option>
                                                                                                            <option value="378">Vорожбит-Zемскова весна-лето 2015</option>
                                                                                                            <option value="379">Larisa Lobanova</option>
                                                                                                            <option value="380">Atelier Kikala</option>
                                                                                                            <option value="381">EDWIN</option>
                                                                                                            <option value="382">Berlin fashion Film Festival</option>
                                                                                                            <option value="383">Иван Дорн</option>
                                                                                                            <option value="384">Natalie Westling</option>
                                                                                                            <option value="385">VOGUE</option>
                                                                                                            <option value="386">Palais de Tokyo</option>
                                                                                                            <option value="387">Zegna</option>
                                                                                                            <option value="388">David Armstrong</option>
                                                                                                            <option value="389">Missoni</option>
                                                                                                            <option value="390">Marc Lagrange</option>
                                                                                                            <option value="391">Pharrell Williams</option>
                                                                                                            <option value="392">Playboy</option>
                                                                                                            <option value="393">Telegraph</option>
                                                                                                            <option value="394">"POP" Magazine</option>
                                                                                                            <option value="395">CoSTUME NATIONAL</option>
                                                                                                            <option value="396">Saskia de Brauw</option>
                                                                                                            <option value="397">Très Bien</option>
                                                                                                            <option value="398">Rihanna</option>
                                                                                                            <option value="399">Анна Винтур</option>
                                                                                                            <option value="400">"SYSTEM" Magazine</option>
                                                                                                            <option value="401">Lara Stone</option>
                                                                                                            <option value="402">Facebook</option>
                                                                                                            <option value="403">Gosha Rubchinskiy</option>
                                                                                                            <option value="404">Nicola Formichetti</option>
                                                                                                            <option value="405">Уголь</option>
                                                                                                            <option value="406">Pradasphere</option>
                                                                                                            <option value="407">Men`s Shoes UA</option>
                                                                                                            <option value="408">"PAPER" Magazine</option>
                                                                                                            <option value="409">Quentin Tarantino</option>
                                                                                                            <option value="410">Art.Casual</option>
                                                                                                            <option value="411">Versus Versace</option>
                                                                                                            <option value="412">Sorel</option>
                                                                                                            <option value="413">EASTPACK</option>
                                                                                                            <option value="414">Domanoff</option>
                                                                                                            <option value="415">Symbol</option>
                                                                                                            <option value="416">MICHAEL KORS</option>
                                                                                                            <option value="417">Kofta</option>
                                                                                                            <option value="418">Sanoma</option>
                                                                                                            <option value="419">Fat Moose</option>
                                                                                                            <option value="420">Olympia Le-Tan</option>
                                                                                                            <option value="421">Disney</option>
                                                                                                            <option value="422">Moon Store studio</option>
                                                                                                            <option value="423">buro 24/7</option>
                                                                                                            <option value="424">Harper’s Bazaar</option>
                                                                                                            <option value="425">Canada Goose</option>
                                                                                                            <option value="426">Matthieu Blazy</option>
                                                                                                            <option value="427">Topman</option>
                                                                                                            <option value="428">"EXIT" Magazine</option>
                                                                                                            <option value="429">Marchesa</option>
                                                                                                            <option value="430">Партнеры</option>
                                                                                                            <option value="431">Sofia Sanchez</option>
                                                                                                            <option value="432">Mauro Mongiello</option>
                                                                                                            <option value="433">Wood Wood</option>
                                                                                                            <option value="434">Sneakerboy</option>
                                                                                                            <option value="435">"LUI" Magazine</option>
                                                                                                            <option value="436">Laetitia Casta</option>
                                                                                                            <option value="437">FUCKING YOUNG</option>
                                                                                                            <option value="438">Madonna</option>
                                                                                                            <option value="439">Interview</option>
                                                                                                            <option value="440">Chanel Salzburg</option>
                                                                                                            <option value="441">Kendall Jenner</option>
                                                                                                            <option value="442">Victoria"s Secret</option>
                                                                                                            <option value="443">British Fashion Awards 2014</option>
                                                                                                            <option value="444">Fashion Move On</option>
                                                                                                            <option value="445">Anna Ewers</option>
                                                                                                            <option value="446">FOTT</option>
                                                                                                            <option value="447">Ermenegildo Zegna</option>
                                                                                                            <option value="448">Etro</option>
                                                                                                            <option value="449">"VOGUE" Ukraine</option>
                                                                                                            <option value="450">витрины</option>
                                                                                                            <option value="451">Дмитрий Тубелец</option>
                                                                                                            <option value="452">American Apparel</option>
                                                                                                            <option value="453">Brendan Jordan</option>
                                                                                                            <option value="454">Julia Roberts</option>
                                                                                                            <option value="455">Rick Owens и Saint Laurent</option>
                                                                                                            <option value="456">Dsquared²</option>
                                                                                                            <option value="457">Style.com</option>
                                                                                                            <option value="458">тренды 2014</option>
                                                                                                            <option value="459">Roberto Cavalli</option>
                                                                                                            <option value="460">Nicki Minaj</option>
                                                                                                            <option value="461">Mykita</option>
                                                                                                            <option value="462">The Farm Company</option>
                                                                                                            <option value="463">MaxMara</option>
                                                                                                            <option value="464">032c</option>
                                                                                                            <option value="465">Kanye West</option>
                                                                                                            <option value="466">Алёна Рубан</option>
                                                                                                            <option value="467">POSHCLUB</option>
                                                                                                            <option value="468">Pink</option>
                                                                                                            <option value="469">John Hardy</option>
                                                                                                            <option value="470">buro247</option>
                                                                                                            <option value="471">Victoria Secret</option>
                                                                                                            <option value="472">Justin Bieber</option>
                                                                                                            <option value="473">L" officiel</option>
                                                                                                            <option value="474">Chloé</option>
                                                                                                            <option value="475">KTZ</option>
                                                                                                            <option value="476">Maison Margiela</option>
                                                                                                            <option value="477">Peter Lindbergh</option>
                                                                                                            <option value="478">VOGUE Italy</option>
                                                                                                            <option value="479">L"OFFICIEL</option>
                                                                                                            <option value="480">Воины Philipp Plein</option>
                                                                                                            <option value="481">коллекция осень-зима 2015</option>
                                                                                                            <option value="482">UFW</option>
                                                                                                            <option value="483">Johanna Schneider</option>
                                                                                                            <option value="484">Yuketen</option>
                                                                                                            <option value="485">BUTTERO</option>
                                                                                                            <option value="486">Ylati</option>
                                                                                                            <option value="487">Vibram</option>
                                                                                                            <option value="488">Diadora</option>
                                                                                                            <option value="489">Eastpak</option>
                                                                                                            <option value="490">Atelier Versace</option>
                                                                                                            <option value="491">MULBERRY</option>
                                                                                                            <option value="492">LUISAVIAROMA</option>
                                                                                                            <option value="493">Proenza Schouler</option>
                                                                                                            <option value="494">Bally</option>
                                                                                                            <option value="495">H&amp;M</option>
                                                                                                            <option value="496">ALEXANDER WANG x H&amp;M</option>
                                                                                                            <option value="497">Diane von FurstenbergDiane von Furstenberg</option>
                                                                                                            <option value="498">коллекция Diane von Furstenberg</option>
                                                                                                            <option value="499">Diane von Furstenberg 2015</option>
                                                                                                            <option value="500">Diane von Furstenberg весна-лето 2015</option>
                                                                                                            <option value="501">рекламная кампания Diane von Furstenberg</option>
                                                                                                            <option value="502">рекламная кампания Diane von Furstenberg весна-лето 2015</option>
                                                                                                            <option value="503">CFDA</option>
                                                                                                            <option value="504">ASICS</option>
                                                                                                            <option value="505">Amazon</option>
                                                                                                            <option value="506">Мирослав Мельник</option>
                                                                                                            <option value="507">"LOVE" Magazine</option>
                                                                                                            <option value="508">Scarlett Johansson</option>
                                                                                                            <option value="509">kiev</option>
                                                                                                            <option value="510">guess</option>
                                                                                                            <option value="511">Jason Wu</option>
                                                                                                            <option value="512">Moncler</option>
                                                                                                            <option value="513">новости моды</option>
                                                                                                            <option value="514">онлайн-продажи</option>
                                                                                                            <option value="515">"Another" Magazine</option>
                                                                                                            <option value="516">Tim Coppens</option>
                                                                                                            <option value="517">"Interview" Magazine</option>
                                                                                                            <option value="518">Kristen Stewart</option>
                                                                                                            <option value="519">Simone Rocha</option>
                                                                                                            <option value="520">Equipment</option>
                                                                                                            <option value="521">Наоми Кэмпбелл</option>
                                                                                                            <option value="522">Vivienne Westwood Red Label</option>
                                                                                                            <option value="523">экономика моды</option>
                                                                                                            <option value="524">политика моды</option>
                                                                                                            <option value="525">Giles</option>
                                                                                                            <option value="526">Philip Plein</option>
                                                                                                            <option value="527">Carlo Pazolini</option>
                                                                                                            <option value="528">MM6 Maison Margiela</option>
                                                                                                            <option value="529">Anthony Vaccarello</option>
                                                                                                            <option value="530">Farfetch</option>
                                                                                                            <option value="531">Gloria Jeans</option>
                                                                                                            <option value="532">Giambattista Valli</option>
                                                                                                            <option value="533">Giorgio Armani</option>
                                                                                                            <option value="534">Серж Смолин</option>
                                                                                                            <option value="535">Adidas Originals YEEZY</option>
                                                                                                            <option value="536">Oodji</option>
                                                                                                            <option value="537">Love Republic</option>
                                                                                                            <option value="538">Incity</option>
                                                                                                            <option value="539">Ostin</option>
                                                                                                            <option value="540">Ksenia Schnaider</option>
                                                                                                            <option value="541">Sacai</option>
                                                                                                            <option value="542">Жизель Бюндхен</option>
                                                                                                            <option value="543">Николя Формичетти</option>
                                                                                                            <option value="544">Dazed SXSW</option>
                                                                                                            <option value="545">Charlotte Olympia</option>
                                                                                                            <option value="546">Jimmy Choo</option>
                                                                                                            <option value="547">Франция</option>
                                                                                                            <option value="548">Модельный бизнес</option>
                                                                                                            <option value="549">Elena Burenina</option>
                                                                                                            <option value="550">VIKTORANISIMOV</option>
                                                                                                            <option value="551">Питер Дундас</option>
                                                                                                            <option value="552">Кара Дельвинь</option>
                                                                                                            <option value="553">Кендалл Дженнер</option>
                                                                                                            <option value="554">Emilio Pucci</option>
                                                                                                            <option value="555">Николя Жескьер</option>
                                                                                                            <option value="556">Rybalko</option>
                                                                                                            <option value="557">NUMBER15СONCEPT</option>
                                                                                                            <option value="558">"Odda" Magazine</option>
                                                                                                            <option value="559">Modemethode</option>
                                                                                                            <option value="560">ритейлер</option>
                                                                                                            <option value="561">Miu Miu Aoyama</option>
                                                                                                            <option value="562">McDonalds</option>
                                                                                                            <option value="563">Мэри Катранзу</option>
                                                                                                            <option value="564">Omelya</option>
                                                                                                            <option value="565">Anton Belinskiy</option>
                                                                                                            <option value="566">Paul Smith</option>
                                                                                                            <option value="567">Anna K</option>
                                                                                                            <option value="568">Numéro</option>
                                                                                                            <option value="569">Yoox</option>
                                                                                                            <option value="570">Хлои Вайз</option>
                                                                                                            <option value="571">Nika Medisan</option>
                                                                                                            <option value="572">Канье Уэст</option>
                                                                                                            <option value="573">Adidas Originals x Jeremy Scott</option>
                                                                                                            <option value="574">PRZHONSKAYA</option>
                                                                                                            <option value="575">Лара Стоун</option>
                                                                                                            <option value="576">Marie Claire</option>
                                                                                                            <option value="577">Schön‬! Magazine</option>
                                                                                                            <option value="578">Элла Задавысвичка</option>
                                                                                                            <option value="579">L’Officiel Online</option>
                                                                                                            <option value="580">Domino Eight</option>
                                                                                                            <option value="581">Фаррелл Уильямс</option>
                                                                                                            <option value="582">Неделя моды в Нью-Йорке</option>
                                                                                                            <option value="583">Woolworths</option>
                                                                                                            <option value="584">Benetton</option>
                                                                                                            <option value="585">Uniqlo</option>
                                                                                                            <option value="586">Екатерина Луговая</option>
                                                                                                            <option value="587">Валерия Руденко</option>
                                                                                                            <option value="588">Ульяна Сергеенко</option>
                                                                                                            <option value="589">Карин Ройтфельд</option>
                                                                                                            <option value="590">Александр Ван</option>
                                                                                                            <option value="591">Диана фон Фюрстенберг</option>
                                                                                                            <option value="592">Артем Кривда</option>
                                                                                                            <option value="593">Фрида Джаннини</option>
                                                                                                            <option value="594">Алессандро Микеле</option>
                                                                                                            <option value="595">Марио Тестино</option>
                                                                                                            <option value="596">Symbol News</option>
                                                                                                            <option value="597">Nina Ricci</option>
                                                                                                            <option value="598">Кейт Мосс</option>
                                                                                                            <option value="599">Gents" Club</option>
                                                                                                            <option value="600">Clessidra</option>
                                                                                                            <option value="601">Роби Родригес</option>
                                                                                                            <option value="602">How Old Do I Look</option>
                                                                                                            <option value="603">Chanel Cruise Seoul</option>
                                                                                                            <option value="604">Elsewhere magazine</option>
                                                                                                            <option value="605">Богдан Романович</option>
                                                                                                            <option value="606">Оливье Йохан</option>
                                                                                                            <option value="607">London Collections Men</option>
                                                                                                            <option value="608">"Wonderland" Magazine</option>
                                                                                                            <option value="609">HM</option>
                                                                                                            <option value="610">Клемент Шаберно</option>
                                                                                                            <option value="611">Marks&amp;Spencer</option>
                                                                                                            <option value="612">Дэвид Ганди</option>
                                                                                                            <option value="613">Рианна</option>
                                                                                                            <option value="614">Calvin Klein Jeans</option>
                                                                                                            <option value="615">Isabel Marant</option>
                                                                                                            <option value="616">Jourdan Dunn</option>
                                                                                                            <option value="617"> Olivier Rousteing</option>
                                                                                                            <option value="618">Джон Гальяно</option>
                                                                                                            <option value="619">T by Alexander Wang</option>
                                                                                                            <option value="620">Alibaba</option>
                                                                                                            <option value="621">Kering</option>
                                                                                                            <option value="622">Max Mara</option>
                                                                                                            <option value="623">Александр МакКуин</option>
                                                                                                            <option value="624">Burberry Black Label</option>
                                                                                                            <option value="625">Coco de Mer</option>
                                                                                                            <option value="626">Bain &amp; Co</option>
                                                                                                            <option value="627">Mango</option>
                                                                                                            <option value="628">Крис Ван Аш</option>
                                                                                                            <option value="629">"Polanski" magazine</option>
                                                                                                            <option value="630">Boss Hugo Boss</option>
                                                                                                            <option value="631">Cartier</option>
                                                                                                            <option value="632">Mittelmoda</option>
                                                                                                            <option value="633">ANDAM Fashion Award</option>
                                                                                                            <option value="634">Oscar de la Renta</option>
                                                                                                            <option value="635">AVTANDIL</option>
                                                                                                            <option value="636">Frankie Morello</option>
                                                                                                            <option value="637">Ренцо Россо</option>
                                                                                                            <option value="638">Амансио Ортега</option>
                                                                                                            <option value="639">Inditex</option>
                                                                                                            <option value="640">Дмитрий Андреев</option>
                                                                                                            <option value="641">Viberg</option>
                                                                                                            <option value="642">3sixteen</option>
                                                                                                            <option value="643">The Hill-Side</option>
                                                                                                            <option value="644">Knickerbockermfg</option>
                                                                                                            <option value="645">Pitti Uomo 88</option>
                                                                                                            <option value="646">Inis Mean</option>
                                                                                                            <option value="647">Nudie Jeans</option>
                                                                                                            <option value="648">John Smedley</option>
                                                                                                            <option value="649">Sammy Icon</option>
                                                                                                            <option value="650">Филипп Литвинов</option>
                                                                                                            <option value="651">Фредерико Маркетти</option>
                                                                                                            <option value="652">Haversack</option>
                                                                                                            <option value="653">Satorisan</option>
                                                                                                            <option value="654">Риккардо Тиши</option>
                                                                                                            <option value="655">Рикардо Тиши</option>
                                                                                                            <option value="656">Украинская мода</option>
                                                                                                            <option value="657">Peter Soubbotnik</option>
                                                                                                            <option value="658">Катя Марасина</option>
                                                                                                            <option value="659">Alberta Ferretti</option>
                                                                                                            <option value="660">Schiaparelli</option>
                                                                                                            <option value="661">Alta Moda</option>
                                                                                                            <option value="662">Escada</option>
                                                                                                            <option value="663">Такаси Мураками</option>
                                                                                                            <option value="664">Parke &amp; Ronen</option>
                                                                                                            <option value="665">Марк Паркер</option>
                                                                                                            <option value="666">дендизм</option>
                                                                                                            <option value="667">Marques" Almeida</option>
                                                                                                            <option value="668">Calvin Klein Collection</option>
                                                                                                            <option value="669">Clan P</option>
                                                                                                            <option value="670">футболки</option>
                                                                                                            <option value="671">streetwear</option>
                                                                                                            <option value="672">Украина</option>
                                                                                                            <option value="673">t-shirts</option>
                                                                                                            <option value="674">Китч</option>
                                                                                                            <option value="675">Kendall + Kylie</option>
                                                                                                            <option value="676">Уиллоу Смит</option>
                                                                                                            <option value="677">«Nylon»</option>
                                                                                                            <option value="678">Кристен Стюарт</option>
                                                                                                            <option value="679">Доменико Дольче</option>
                                                                                                            <option value="680">MTV</option>
                                                                                                            <option value="681">Theory</option>
                                                                                                            <option value="682">Наталья Водянова</option>
                                                                                                            <option value="683">Рик Оуэнс</option>
                                                                                                            <option value="684">Hillier Bartley</option>
                                                                                                            <option value="685">Mercedes-Benz Kiev Fashion Days</option>
                                                                                                            <option value="686">Ким Кардашьян</option>
                                                                                                            <option value="687">Birkin</option>
                                                                                                            <option value="688">Юлия Монахова</option>
                                                                                                            <option value="689">Periscope</option>
                                                                                                            <option value="690">Острів</option>
                                                                                                            <option value="691">Don"t Take Fake</option>
                                                                                                            <option value="692">Circul</option>
                                                                                                            <option value="693">WeAreAble</option>
                                                                                                            <option value="694">Evian</option>
                                                                                                            <option value="695">Терри Ричардсон</option>
                                                                                                            <option value="696">ЦУМ</option>
                                                                                                            <option value="697">MAX&amp;Co</option>
                                                                                                            <option value="698">Topshop Unique</option>
                                                                                                            <option value="699">Kanye West’s</option>
                                                                                                            <option value="700">Ник Фенсом</option>
                                                                                                            <option value="701">Джо Ла Пума</option>
                                                                                                            <option value="702">Эдуардо Гарсиа и Адриано Батиста</option>
                                                                                                            <option value="703">Георг Кох</option>
                                                                                                            <option value="704">Райан Виллмс</option>
                                                                                                            <option value="705">Имран Амед</option>
                                                                                                            <option value="706">Такахиро Киношита</option>
                                                                                                            <option value="707">Тим Бланкс</option>
                                                                                                            <option value="708">Тим Нокс</option>
                                                                                                            <option value="709">Лоуренс Шлёссман</option>
                                                                                                            <option value="710">Balmain x H&amp;M</option>
                                                                                                            <option value="711">SAVKA</option>
                                                                                                            <option value="712">Snapchat</option>
                                                                                                            <option value="713">Марк Джейкобс</option>
                                                                                                            <option value="714">Альбер Эльбаз</option>
                                                                                                            <option value="715">Пьер Берже</option>
                                                                                                            <option value="716">Саския де Брау</option>
                                                                                                            <option value="717">Леди Гага</option>
                                                                                                            <option value="718">Дон Ланг</option>
                                                                                                            <option value="719">Барбра Стрейзанд</option>
                                                                                                            <option value="720">Энни Леннокс</option>
                                                                                                            <option value="721">Дорис Дэй</option>
                                                                                                            <option value="722">Жанна д"Арк</option>
                                                                                                            <option value="723">Леди Трубридж</option>
                                                                                                            <option value="724">Марлен Дитрих</option>
                                                                                                            <option value="725">Dazed &amp; Confused</option>
                                                                                                            <option value="726">Жан Поль Готье</option>
                                                                                                            <option value="727">Armani Collezioni</option>
                                                                                                            <option value="728">Ольга Ломака</option>
                                                                                                            <option value="729">Майли Сайрус</option>
                                                                                                            <option value="730">BFA</option>
                                                                                                            <option value="731">Стефано Пилати</option>
                                                                                                            <option value="732">Дмитрий Рыбаков</option>
                                                                                                            <option value="733">Swarovski</option>
                                                                                                            <option value="734">Rosemanclub</option>
                                                                                                            <option value="735">Дмитрий Дряев</option>
                                                                                                            <option value="736">Оливье Рустен</option>
                                                                                                            <option value="737">Демна Гвасалия</option>
                                                                                                            <option value="738">Vetements</option>
                                                                                                            <option value="739">Serge Payet</option>
                                                                                                            <option value="740">Миучча Прада</option>
                                                                                                            <option value="741">Бюстгалтер</option>
                                                                                                            <option value="742">Bvlgari</option>
                                                                                                            <option value="743">Christian Lacroix</option>
                                                                                                            <option value="744">Pierre Cardin</option>
                                                                                                            <option value="745">Пьер Карден</option>
                                                                                                            <option value="746">Instagram</option>
                                                                                                            <option value="747">Валентино Гаравани</option>
                                                                                                            <option value="748">Пьерпаоло Пиччоли</option>
                                                                                                            <option value="749">Мария Грация Кьюри</option>
                                                                                                            <option value="750">Boggi</option>
                                                                                                            <option value="751">Lancôme</option>
                                                                                                            <option value="752">Estée Lauder</option>
                                                                                                            <option value="753">Palace</option>
                                                                                                            <option value="754">Stussy</option>
                                                                                                    </select>
                                            </label>
                                            <a class="icon-image trigger-button-tags">Создать Tag</a>
                                            <br><br>
                                            <label class="position">
                                                Type:
                                                <select name="position">
                                                                                                            <option value="2" 
                                                                >
                                                            Полный</option>
                                                                                                                <option value="3" 
                                                                >
                                                            Не полный</option>
                                                                                                        </select>
                                            </label>
                                            <label class="align">
                                                Позиция:
                                                <select name="align">
                                                                                                            <option value="3" >
                                                            Центр</option>
                                                                                                                <option value="4" >
                                                            По левой стороне</option>
                                                                                                        </select>
                                            </label>
                                            <input type="submit" value="Сохранить ">

                                        </form>


                                    </div>
                                    <div class="post_comments">
                                        Комментарии
                                        <div class="acc-container">
                                            <article class="accordion">
                                                <section id="acc1">
                                                    <h2><a href="#acc1">
                                                            Нет комментариев </a>
                                                    </h2>
                                                    <p style="color: green">Опубликованно</p>
                                                    <hr>
                                                    <p>Здесь будут комментарии</p>

                                                </section>
                                            </article>
                                        </div>
                                    </div>
                                                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tingle-demo-tag"  style="display: none">
                <form method="post"  id="tag-add" >
                    <label class="tag"> Название tag: </label>
                    <input name="tag" class="tag-name" type="text" placeholder="Название tag">


                    <a href="#" class="btn-done-tag">Готово</a>
                </form>
            </div>

            <!-- FOURTH ROW OF BLOCKS -->     
<div id="footerwrap">
    <footer class="clearfix"></footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <p>Moonstore - Copyright 2015</p>
            </div>

        </div><!-- /row -->
    </div><!-- /container -->		
</div><!-- /footerwrap -->

<div class="tingle-demo"  style="display: none">
    <form method="post"  id="category-add" >
        <input name="module" class="input-huge" value="posts" type="hidden">
        <input name="action" class="input-huge" value="category/add" type="hidden">

        <label class="post-name"> Название категории </label>
        <input name="category_name" type="text" placeholder="Название категории">
        <label class="post-cat"> Описание категоррии </label>
        <input name="post_" type="text" placeholder="Описание категоррии">
        <label class="cat-seo-title"> Seo title: </label>
        <input name="category_seo_title" type="text" placeholder="SEO title" >
        <label class="category_seo_desc"> Seo description: </label>
        <input name="category_seo_desc" type="text" placeholder="SEO описание">


        <a href="#" class="btn-done">Готово</a>
    </form>
</div>


<nav class="outer-nav right vertical">
    <a href="/admin" class="icon-home">Статистика</a>
    <a href="#" class="icon-image trigger-button">Создать категорию</a>
    <a href="/admin/gallery" class="icon-image">Создать галерею</a>
          
        <a href="/admin/user/add" class="icon-star">Создать пользователя</a>

    
</nav>
</div>


    <script type="text/javascript" src="/web/templates/Admin/js/tingle/tingle.js"></script>

    <script type="text/javascript">
        tingle.modal.init();
        $(".trigger-button").click(function () {
            tingle.modal.setContent($(".tingle-demo").html());
            tingle.modal.open();
            $(".btn-done").click(function () {
                $.post('/admin/profile/', $('#category-add').serialize());
                alert('Категория была созданна');
                tingle.modal.close();
            });
        });
   
    </script>

    <style>

        .pbfooter {
            width:100%; 
            height:50px; 
            background-color:#F58723; 
            color:white; 
            top:0; 
            left:0; 
            z-index:1200; 
            padding-top:15px;
        }

    </style>

    <script type="text/javascript">
        jQuery(function ($) {
            // put the pbfooter on the bottom of the page
            var height = ($(document).height() - $("#pbafooter").height()) + "px";
            $("#pbafooter").css("top", ($(document).height() - $("#pbafooter").height()) + "px");
        });
    </script>

    <script src="/web/templates/Admin/js/menu/js/classie.js"></script>
    <script src="/web/templates/Admin/js/menu/js/menu.js"></script>

            <link rel="stylesheet" type="text/css" href="/web/templates/Admin/js/multiselect/jquery.tokenize.css" />
            <script type="text/javascript" src="/web/templates/Admin/js/multiselect/jquery.tokenize.js"></script>

            <script>
                //                $(document).ready(function (request, response) {

                $(function () {
                                    $('#tokenize').tokenize().toArray();
                    $(".video-check").click(function () {
                        switcher();
                    });
                    $(".video").change(function () {
                        changeState();
                    });
                    function changeState() {
                        var value = $(".video").val().split("/");
                        var code = value[value.length - 1];
                        console.log(code);
                        $(".vimeo-player").show();
                        var url = '';
                        if (value.indexOf('vimeo.com') !== -1) {
                            console.log('vimeo');
                            url = "https://player.vimeo.com/video/" + code;
                        } else {
                            console.log('youtube');
                            var youtubeCode = code.split('=');
                            url = 'https://www.youtube.com/embed/' + youtubeCode[youtubeCode.length - 1];
                        }
                        $('.chackbox-video').val($(".video").val());
                        $(".vimeo-player").attr('src', url);
                    }

                    function switcher() {
                        if ($(".video-check").is(':checked')) {
                            $(".post-name").hide();
                            $("#imageInput").hide();
                            $(".video-lb").show();
                            $(".player").show();
                            $(".vimeo-player").show();
                        } else {
                            $(".post-name").show();
                            $("#imageInput").show();
                            $(".video-lb").hide();
                            $(".player").hide();
                            $(".vimeo-player").hide();
                        }
                    }
                }
                );
                $(".trigger-button-tags").click(function () {
                    console.log('yes');
                    tingle.modal.setContent($(".tingle-demo-tag").html());
                    tingle.modal.open();
                    $(".btn-done-tag").click(function () {
                        var tagname = $(".tag-name").val();
                        console.log(tagname);
                        $.ajax({
                            url: '/admin/tag/add/',
                            dataType: 'json',
                            data: {'tag': tagname},
                            success: function (data) {
                                updateTags(data.tagname, data.tagid)
                            },
                            error: function () {
                                alert('Error. Somthing wrong. Please contact with developer')
                            }
                        });
//                tingle.modal.close();
                    });
                });
                function updateTags(tag, tagId) {
                    $("#tokenize").append("<option value='" + tagId + "'>" + tag + "</option>")
                }
                
                </script>
        </body>
    </html>


<?php }
}
?>