<?php
/*%%SmartyHeaderCode:206857130256902a942fb745_93328748%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f5d61b0a533f49ffdc6786687d937408b6107686' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Admin/index.login.tpl',
      1 => 1452217779,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '206857130256902a942fb745_93328748',
  'tpl_function' => 
  array (
  ),
  'variables' => 
  array (
    'err' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56902a9439e049_03353990',
  'cache_lifetime' => 3600,
),true);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56902a9439e049_03353990')) {
function content_56902a9439e049_03353990 ($_smarty_tpl) {
?>

<!doctype html>
<html><head>
        <meta charset="utf-8">
        <title>MoonStore - добро пожаловать</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Pohorielov Vlad">

        <link href="/web/templates/Admin/css/bootstrap.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/bootstrap.min.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/styles.css" rel="stylesheet">
        <link rel="stylesheet" href="/web/templates/Admin/css/register.css">

        <script type="text/javascript" src="/web/templates/Admin/js/jquery-1.10.2.min.js"></script>    
        <script src="/web/templates/Admin/js/bootstrap.js"></script>


        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Google Fonts call. Font Used Open Sans & Raleway -->
        <link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
        <!-- Le styles -->

        <link href="/web/templates/Admin/css/login.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/bootstrap.min.css" />

        <style type="text/css">
            body {
                padding-top: 30px;
            }

            pbfooter {
                position:relative;
            }
        </style>

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Google Fonts call. Font Used Open Sans & Raleway -->
        <link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

        <!-- Jquery Validate Script -->
        <script type="text/javascript" src="/web/templates/Admin/js/jquery.validate.js"></script>

        <!-- Jquery Validate Script - Validation Fields -->
        <script type="text/javascript">


            $().ready(function () {
                // validate the comment form when it is submitted
                $("#commentForm").validate();

                // validate signup form on keyup and submit
                $("#signupForm").validate({
                    rules: {
                        firstname: "required",
                        lastname: "required",
                        username: {
                            required: true,
                            minlength: 1
                        },
                        password: {
                            required: true,
                            minlength: 1
                        },
                        confirm_password: {
                            required: true,
                            minlength: 2,
                            equalTo: "#password"
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        topic: {
                            required: "#newsletter:checked",
                            minlength: 2
                        },
                        agree: "required"
                    },
                    messages: {
                        firstname: "Please enter your firstname",
                        lastname: "Please enter your lastname",
                        username: {
                            required: "Please enter a username",
                            minlength: "Your username must consist of at least 1 character"
                        },
                        password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 1 character long"
                        },
                        confirm_password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long",
                            equalTo: "Please enter the same password as above"
                        },
                        email: "Please enter a valid email address",
                        agree: "Please accept our policy"
                    }
                });

                // propose username by combining first- and lastname
                $("#username").focus(function () {
                    var firstname = $("#firstname").val();
                    var lastname = $("#lastname").val();
                    if (firstname && lastname && !this.value) {
                        this.value = firstname + "." + lastname;
                    }
                });

                //code to hide topic selection, disable for demo
                var newsletter = $("#newsletter");
                // newsletter topics are optional, hide at first
                var inital = newsletter.is(":checked");
                var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
                var topicInputs = topics.find("input").attr("disabled", !inital);
                // show when newsletter is checked
                newsletter.click(function () {
                    topics[this.checked ? "removeClass" : "addClass"]("gray");
                    topicInputs.attr("disabled", !this.checked);
                });
            });
        </script>

    </head>

    <style>

        .pbfooter {
            position:relative;
        }

    </style>

    <body style="background:url('web/templates/Admin/img/bg.jpg') no-repeat center center; height:100%;">

        <!-- NAVIGATION MENU -->

        <div class="navbar-nav navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div> 

            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-offset-4 col-lg-4" style="margin-top:100px">
                    <div class="block-unit" style="text-align:center; padding:8px 8px 8px 8px;">
                        <img src="web/templates/Admin/img/face80x80.jpg" alt="" class="img-circle">
                        <br>
                        <br>
                        <form class="cmxform" id="signupForm" method="post" action="/moonstoreadmin/login">
                            <fieldset>
                                <p>
                                                                        <input id="username" name="username" type="text" placeholder="Username">
                                    <input id="password" name="password" type="password" placeholder="Password">
                                </p>
                                <input class="submit btn-success btn btn-large" type="submit" value="Login">
                            </fieldset>
                        </form>
                    </div>

                </div>


            </div>
        </div>

    </body></html><?php }
}
?>