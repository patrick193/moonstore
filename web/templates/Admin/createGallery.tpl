<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Moonstore - Gallery</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="/web/templates/Admin/css/tingle.css" rel="stylesheet">
    <link rel="stylesheet" href="/web/templates/Admin/css/normalize.css">
    <link rel="stylesheet" href="/web/templates/Admin/css/posts-style.css">

    <script src="/web/templates/Admin/js/prefixfree.min.js"></script>

    <link href="/web/templates/Admin/css/bootstrap.min.css" rel="stylesheet">

    <link href="/web/templates/Admin/css/main.css" rel="stylesheet">
    <link href="/web/templates/Admin/css/font-style.css" rel="stylesheet">
    <link href="/web/templates/Admin/css/flexslider.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/component.css"/>
    <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/normalize.css"/>
    {*        <link href="/web/templates/Admin/js/menu/js/image-picker/image-picker/image-picker.css" rel="stylesheet" />*}
    <script type="text/javascript" src="/web/templates/Admin/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/web/templates/Admin/js/jquery-ui.js"></script>
    {*        <script src="/web/templates/Admin/js/menu/js/image-picker/image-picker/image-picker.js"></script>*}


    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="/web/templates/Admin/js/bootstrap.js"></script>


    <!-- NOTY JAVASCRIPT -->
    <script type="text/javascript" src="/web/templates/Admin/js/jquery.noty.js"></script>
    <!-- You can add more layouts if you want -->
    <script type="text/javascript" src="/web/templates/Admin/js/default.js"></script>

    <script src="/web/templates/Admin/js/menu/js/modernizr.custom.25376.js"></script>
    <style type="text/css">
        body {
            padding-top: 60px;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Google Fonts call. Font Used Open Sans -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

    <!-- DataTables Initialization -->
    <script type="text/javascript" src="/web/templates/Admin/js/jquery.dataTables.js"></script>
    {literal}
        <script type="text/javascript">

            ;
            $(function () {
                var enable = false;
                $(".check-name").click(function () {
                    var data = $(".gallery-name").val();
                    $.ajax({
                        url: '/admin/gallery/check/',
                        method: 'get',
                        data: {'gallery_name': data},
                        dataType: 'json',
                        success: function (response) {
                            console.log(response);
                            console.log("success");
                            enable = true;
                            endis();
                        },
                        error: function (ex) {
                            console.log(ex.error());
                            console.log("error");
                            enable = false;
                            endis();
                        }
                    });
                });

                function endis() {
                    if (enable === true) {
                        $("#upload").find('input').removeProp('disabled');
                        $("#upload").fadeIn();
                        $(".save").fadeIn();

                    }
                }

                $(".save").click(function () {
                    var img = '';
                    var data = $(".gallery-name").val();
                    $("#upload").find('ul').find('li').find('p').find('i').remove()
                    ;
                    var a = $("#upload").find('ul').find('li').find('p');

                    $.each(a, function (i, v) {
                        img += $(v).text() + " ";
                    });

                    $.ajax({
                        url: '/admin/gallery/upload/img/',
                        data: {'save': data, 'img': img},
                        dataType: 'json',
                        method: 'post',
                        success: function (response) {
                            console.log(response);
                            console.log("success");
                            window.location.replace('/admin');
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    alert("Галлерея успешно создана! Вставте код в статью:\ngallery((" + data + "))");
                    window.location.href = '/admin/gallery';
                    return false;
                });

            });
        </script>
    {/literal}
</head>
<body>
<div id="perspective" class="perspective effect-moveleft">
    <div class="container">
        <div class="wrapper"><!-- wrapper needed for scroll -->
            <!-- Top Navigation -->

            <div class="main clearfix">
                <!-- CONTENT -->
                <div class="container">

                    <div class="row main-row">
                        <label for="gallery-name" style="color: #fff"> Имя галлереи
                            <input type="text" class="gallery-name" placeholder="Имя галереи должно быть уникальным"
                                   style="width: 400px">
                            <a href="#" class="check-name">Проверить</a>
                        </label>

                        <form id="upload" method="post" action="/admin/gallery/upload/img/"
                              enctype="multipart/form-data">
                            <div id="drop">
                                Перетащи сюда

                                <a id="select">Выбрать</a>
                                <input disabled type="file" name="upl" multiple/>
                            </div>

                            <ul>
                                <!-- The file uploads will be shown here -->
                            </ul>

                        </form>
                        <h2><a class="save btn-success">Save</a></h2>
                    </div><br><br>
                    <div class="row main-row">
                        {foreach from=$galleries key='key' item='gallery'}
                            <div class="photopile-wrapper" style="border: medium dashed; margin: 10px; padding: 10px;">
                                <h2 style="color: #fff; float: right" class="gallery-name">{$key}</h2>

                                <p class="photopile">
                                    {foreach from=$gallery item='g'}
                                        {*<a href="{$g.image_path}">*}
                                        <a href="#">
                                            <img src="{$g.image_path}" alt="" style="max-width: 200px; height: auto"/>
                                        </a>
                                    {/foreach}
                                </p>
                            </div>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{*<link href="/web/templates/Admin/css/photopile.css" rel="stylesheet">*}
{*<script src="/web/templates/Admin/js/photopile.js"></script>*}
<script>

</script>
<script>

    $(document).ready(function () {
        $("#upload").fadeOut();
        $(".save").fadeOut();
    });
    $(".gallery-name").click(function () {
        if ($(this).text().indexOf('gallery=') < 0) {
            $(this).text('gallery=((' + $(this).text() + "))");
        }
    });

</script>
{include file="./menu.tpl"}
{*        {include file="./footer.tpl"}*}
<script>
    $(".outer-nav").css('visibility', 'hidden');
</script>
</body>
</html>