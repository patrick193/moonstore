<!-- FOURTH ROW OF BLOCKS -->     
<div id="footerwrap">
    <footer class="clearfix"></footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <p>Moonstore - Copyright 2015</p>
            </div>

        </div><!-- /row -->
    </div><!-- /container -->		
</div><!-- /footerwrap -->

<div class="tingle-demo"  style="display: none">
    <form method="post"  id="category-add" >
        <input name="module" class="input-huge" value="posts" type="hidden">
        <input name="action" class="input-huge" value="category/add" type="hidden">

        <label class="post-name"> Название категории </label>
        <input name="category_name" type="text" placeholder="Название категории">
        <label class="post-cat"> Описание категоррии </label>
        <input name="post_" type="text" placeholder="Описание категоррии">
        <label class="cat-seo-title"> Seo title: </label>
        <input name="category_seo_title" type="text" placeholder="SEO title" >
        <label class="category_seo_desc"> Seo description: </label>
        <input name="category_seo_desc" type="text" placeholder="SEO описание">


        <a href="#" class="btn-done">Готово</a>
    </form>
</div>


<nav class="outer-nav right vertical">
    <a href="/admin" class="icon-home">Статистика</a>
    <a href="#" class="icon-image trigger-button">Создать категорию</a>
    <a href="/admin/gallery" class="icon-image">Создать галерею</a>
    {assign var='user' value=$smarty.session.register.user_auth|base64_decode|unserialize}
    {if $user->getRoleId() == 1}  
        <a href="/admin/user/add" class="icon-star">Создать пользователя</a>

    {/if}

</nav>
</div>

{literal}
    <script type="text/javascript" src="/web/templates/Admin/js/tingle/tingle.js"></script>

    <script type="text/javascript">
        tingle.modal.init();
        $(".trigger-button").click(function () {
            tingle.modal.setContent($(".tingle-demo").html());
            tingle.modal.open();
            $(".btn-done").click(function () {
                $.post('/admin/profile/', $('#category-add').serialize());
                alert('Категория была созданна');
                tingle.modal.close();
            });
        });
   
    </script>

    <style>

        .pbfooter {
            width:100%; 
            height:50px; 
            background-color:#F58723; 
            color:white; 
            top:0; 
            left:0; 
            z-index:1200; 
            padding-top:15px;
        }

    </style>

    <script type="text/javascript">
        jQuery(function ($) {
            // put the pbfooter on the bottom of the page
            var height = ($(document).height() - $("#pbafooter").height()) + "px";
            $("#pbafooter").css("top", ($(document).height() - $("#pbafooter").height()) + "px");
        });
    </script>

    <script src="/web/templates/Admin/js/menu/js/classie.js"></script>
    <script src="/web/templates/Admin/js/menu/js/menu.js"></script>
{/literal}