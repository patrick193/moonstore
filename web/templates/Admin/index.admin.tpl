<!doctype html>
<html><head>
        <meta charset="utf-8">
        <title>Moonstore администратор</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Pohorielov Vlad">


        <link href="/web/templates/Admin/css/bootstrap.min.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/tingle.css" rel="stylesheet">

        <link href="/web/templates/Admin/css/main.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/font-style.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/flexslider.css" rel="stylesheet">
        <script type="text/javascript" src="/web/templates/Admin/js/jquery-1.10.2.min.js"></script>

        <!-- Placed at the end of the document so the pages load faster -->
        <script type="text/javascript" src="/web/templates/Admin/js/bootstrap.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/lineandbars.js"></script>

        <script type="text/javascript" src="/web/templates/Admin/js/dash-charts.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/gauge.js"></script>

        <!-- NOTY JAVASCRIPT -->
        <script type="text/javascript" src="/web/templates/Admin/js/jquery.noty.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/top.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/topLeft.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/topRight.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/topCenter.js"></script>

        <!-- You can add more layouts if you want -->
        <script type="text/javascript" src="/web/templates/Admin/js/default.js"></script>
        <!-- <script type="text/javascript" src="assets/js/dash-noty.js"></script> This is a Noty bubble when you init the theme-->
        <script type="text/javascript" src="/web/templates/Admin/js/highcharts.js"></script>
        <script src="/web/templates/Admin/js/jquery.flexslider.js" type="text/javascript"></script>

        <script type="text/javascript" src="/web/templates/Admin/js/admin.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/ping.js"></script>

        <style type="text/css">
            body {
                padding-top: 60px;
            }
        </style>

        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/component.css" />
        <script src="/web/templates/Admin/js/menu/js/modernizr.custom.25376.js"></script>
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->


        <!-- Google Fonts call. Font Used Open Sans & Raleway -->
        <link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

        <script type="text/javascript">

            {*$(document).ready(function () {

            $("#btn-blog-next").click(function () {
            $('#blogCarousel').carousel('next')
            });
            $("#btn-blog-prev").click(function () {
            $('#blogCarousel').carousel('prev')
            });

            $("#btn-client-next").click(function () {
            $('#clientCarousel').carousel('next')
            });
            $("#btn-client-prev").click(function () {
            $('#clientCarousel').carousel('prev')
            });

            });

            $(window).load(function () {

            $('.flexslider').flexslider({
            animation: "slide",
            slideshow: true,
            start: function (slider) {
            $('body').removeClass('loading');
            }
            });
            });*}

        </script>  
        <script src="/web/templates/Admin/js/ping.js" type="text/javascript"></script>

        <script>
            var do_ping = function () {
                ping(document.getElementById('pingurl').value).then(function (delta) {
                    alert(delta);
                }).catch(function (error) {
                    alert(String(error));
                });
            };</script>
    </head>
    <body>

        <!-- NAVIGATION MENU -->

        {include file='./menu.tpl'}
        <div id="perspective" class="perspective effect-moveleft">
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <!-- Top Navigation -->

                    <div class="main clearfix">
                        <div class="container">

                            <!-- FIRST ROW OF BLOCKS -->     
                            <div class="row">

                                <!-- USER PROFILE BLOCK -->
                                <div class="col-sm-3 col-lg-3">
                                    <div class="dash-unit">
                                        <dtitle>Пользователь</dtitle>
                                        <hr>
                                        <div class="thumbnail">
                                            <img src="/web/templates/Admin/img/face80x80.jpg" class="img-circle">
                                        </div><!-- /thumbnail -->
                                        <h1>{user->getUserFirstName} {user->getUserLastName} </h1>
                                        <h3>{user->getUserEmail}</h3>
                                        <br>
                                        <div class="info-user">
                                            {if {user->getRoleId} == 1}  
                                                <a href="/admin/profile/usermanagement"<span aria-hidden="true" class="li_user fs1"></span></a>
                                                {/if}
                                            <a href="/admin/change/{user->getUserId}"><span aria-hidden="true" class="li_settings fs1"></span></a>
                                            <a href="/moonstoreadmin/logout" title="Выход"><span aria-hidden="true" class="li_key fs1"></span></a>
                                        </div>
                                    </div>
                                </div>

                                <!-- DONUT CHART BLOCK -->
                                <div class="col-sm-3 col-lg-3">
                                    <div class="dash-unit">
                                        <dtitle>Последняя новость</dtitle>
                                        <hr>
                                        <div class="info-user">
                                            <span aria-hidden="true" class="li_news fs2"></span>
                                        </div>
                                        <br>
                                        <div class="text">
                                            <p><a href="/admin/post/edit/{lastNews->post_id}">{lastNews->post_name}</a></p>
                                            <p><grey>Обновленна: {lastNews->update} дней назад</grey></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-lg-3">
                                    <div class="dash-unit">
                                        <dtitle>Всего статей</dtitle>
                                        <hr>
                                        <div class="cont">
                                            <p><bold>{$posts_statistic.all}</bold> | <ok>Всего</ok></p>
                                            <br>
                                            <p><bold>{$posts_statistic.this_month}</bold> | В этом месяце</p>
                                            <br>
                                            <p><bold>{$posts_statistic.blocked}</bold> | <bad> Отклонены</bad></p>
                                            <br>
                                            <p><img src="/web/templates/Admin/img/up-small.png" alt=""> 12% с прошлым месяцем</p>

                                        </div>

                                    </div>
                                </div>
                                <!-- DONUT CHART BLOCK -->


                                <div class="col-sm-3 col-lg-3">

                                    <!-- LOCAL TIME BLOCK -->
                                    <div class="half-unit">
                                        <dtitle>Позледний вход</dtitle>
                                        <hr>
                                        <div class="clockcenter">
                                            <digiclock>{user->getLastVisit}</digiclock>
                                        </div>
                                    </div>

                                    <!-- SERVER UPTIME -->
                                    <div class="half-unit">
                                        <dtitle>Server Uptime</dtitle>
                                        <hr>
                                        <div class="cont">
                                            <p><img src="/web/templates/Admin/img/up.png" alt=""> <bold>Up</bold> |  <span id="ping-me"></span>ms.</p>
                                        </div>
                                    </div>
                                    <script>
                                        var p = new Ping();
                                        p.ping(document.location.origin, function (data) {
                                            document.getElementById("ping-me").innerHTML = data;
                                        });
                                    </script>
                                </div>
                            </div><!-- /row -->


                            <!-- SECOND ROW OF BLOCKS -->     
                            <div class="row">




                                <!-- INFORMATION BLOCK -->     
                                <div class="col-sm-3 col-lg-3">
                                    <div class="dash-unit">
                                        <dtitle>Инвертация цветов</dtitle>
                                        <hr>
                                        <div class="info-user">
                                            <span aria-hidden="true" class="li_display fs2"></span>
                                        </div>
                                        <br>
                                        <div class="text">
                                            <p>Список категорий</p>
                                            <p id="categories">
                                                {foreach from = $categories item =category}
                                                    {$category->category_name}&nbsp;&nbsp;
                                                    <input type="checkbox" class="category-id" {if $category->invert == 1} checked="" {/if}value="{$category->posts_category_id}">
                                                    <br>
                                                {/foreach}
                                            </p>
                                            <script type="text/javascript">
                                                $(".category-id").change(function () {
                                                    var id = $(this).val();
                                                    $.ajax({
                                                        url: '/admin/invert/',
                                                        dataType: 'json',
                                                        method: 'post',
                                                        data: {
                                                            'id': id
                                                        },
                                                        success: function () {
                                                            console.log('success');
                                                        },
                                                        error: function () {
                                                            console.log('error');
                                                        }
                                                    });
                                                });
                                            </script>
                                        </div>

                                    </div>
                                </div>

                                <!-- TWITTER WIDGET BLOCK -->     
                                <div class="col-sm-3 col-lg-3">
                                    <div class="dash-unit">
                                        <dtitle>Последнии комментарии</dtitle>
                                        <hr>
                                        <div class="info-user">
                                            <span aria-hidden="true" class="li_megaphone fs2"></span>
                                        </div>
                                        <div class="accordion" id="accordion2">
                                            {foreach from = $comments key=i item = comment}
                                                <div class="accordion-group">
                                                    <div class="accordion-heading">
                                                        {if $i == 0}
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                                                {$comment->date}
                                                            </a>
                                                        </div>
                                                        <div id="collapseOne" class="accordion-body collapse in">
                                                            <div class="accordion-inner">
                                                                {$comment->comment}
                                                            </div>
                                                        </div>
                                                    {else}
                                                        <a class="accordion-toggle-colapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse{$i}">
                                                            {$comment->date}
                                                        </a>
                                                    </div>
                                                    <div id="collapse{$i}" class="accordion-body collapse">
                                                        <div class="accordion-inner">
                                                            {$comment->comment}
                                                        </div>
                                                    </div>

                                                {/if}


                                            </div>



                                        {/foreach}
                                    </div><!--/accordion -->
                                </div>


                            </div>

                            <div class="col-sm-3 col-lg-3">

                                <div class="dash-unit">
                                    <dtitle>Последнии подписщики</dtitle>
                                    <hr>
                                    <div class="flexslider">
                                        {foreach from=$subscribers item=s}
                                            {$s->email} {$s->date}<br/>
                                        {/foreach}
                                    </div>
                                    <a href="/admin/subscribe">Создать рассылку</a>
                                </div>
                            </div>
                            <div class="col-sm-3 col-lg-3">
                                <!-- TOTAL SUBSCRIBERS BLOCK -->     
                                <div class="half-unit">
                                    <dtitle>Facebook</dtitle>
                                    <hr>
                                    <div class="cont">
                                        <p><bold>1.744</bold></p>
                                        <p>98 сегодня</p>
                                    </div>
                                </div>

                                <!-- FOLLOWERS BLOCK -->     
                                <div class="half-unit">
                                    <dtitle>Twitter</dtitle>
                                    <hr>
                                    <div class="cont">
                                        <p><bold>1.833</bold></p>
                                        <p>45 сегодня</p>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /row -->
                        <div class="row">

                            <div class="col-sm-3 col-lg-3">
                                <div class="dash-unit">
                                    <dtitle>Настройки почты</dtitle>
                                    <hr>
                                    <div class="info-user">
                                        <div class="email-settings">
                                            <label class="email_host"> Email host: 
                                                <input class="emailhosts" name="email_host" type="text" placeholder="Host" value="{$owner[0]->Host}">
                                            </label>
                                            <label class="email_owner"> Email: 
                                                <input class="email_email" name="email_owner" type="text" placeholder="Email" value="{$owner[0]->Username}">
                                            </label>
                                            <label class="email_password"> Email пароль: 
                                                <input class="email_pass" name="email_password" type="text" placeholder="Пароль от почты" value="{$owner[0]->Password}">
                                            </label>
                                            <label class="email_port"> Email port: 
                                                <input name="email_port" class="email_ports" type="text" placeholder="Порт" value="{$owner[0]->Port}">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- THIRD ROW OF BLOCKS -->     
                        <div class="row">

                            <!-- 30 DAYS STATS - CAROUSEL FLEXSLIDER -->     



                            {*<div class="col-sm-3 col-lg-3">

                                <!-- LIVE VISITORS BLOCK -->     
                                <div class="half-unit">
                                    <dtitle>Сейчас на сайте</dtitle>
                                    <hr>
                                    <div class="cont">
                                        <p><bold>388</bold></p>
                                        <p>чеоловек</p>
                                    </div>
                                </div>

                                <!-- PAGE VIEWS BLOCK -->     
                                <div class="half-unit">
                                    <dtitle>Просмотренно страниц</dtitle>
                                    <hr>
                                    <div class="cont">
                                        <p><bold>145.0K</bold></p>
                                        <p><img src="/web/templates/Admin/img/up-small.png" alt=""> 23.88%</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-lg-3">

                                <!-- LAST USER BLOCK -->     
                                <div class="half-unit">
                                    <dtitle>Последний пользователь</dtitle>
                                    <hr>
                                    <div class="cont2">
                                        <br>
                                        <br>
                                        <p>Вася Пупкин</p>
                                        <p><bold>vasiya@pupkin.com</bold></p>
                                    </div>
                                </div>

                            </div>*}
                        </div>      
                    </div><!-- /container -->
                </div><!-- wrapper -->
            </div> <!-- /container -->
        </div><!-- /main -->

        {include file='./footer.tpl'}

        <script type="text/javascript">
            $(".email-settings").change(function () {
                var email = $(".email_email").val();
                var emailHost = $(".emailhosts").val();
                var emailPass = $(".email_pass").val();
                var emailPort = $(".email_ports").val();
                console.log(emailHost);
                $.ajax({
                    url: '/admin/email/save/',
                    dataType: 'json',
                    method: 'post',
                    data: {
                        'email': email,
                        'email_host': emailHost,
                        'email_pass': emailPass,
                        'email_port': emailPort
                    },
                    success: function () {
                        console.log('success');
                    },
                    error: function () {
                        console.log('error');
                        alert('We have an error.');
                    }

                });
            });
        </script>

    </body></html>