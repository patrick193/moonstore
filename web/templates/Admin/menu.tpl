<link rel="stylesheet" href="/web/templates/Admin/css/demo.css">
<link rel="stylesheet" href="/web/templates/Admin/css/forkit.css">
<link href="/web/templates/Admin/css/style-slider.css" rel="stylesheet" />
<link href="/web/templates/Admin/css/menu/css/jquery-ui.css" rel="stylesheet" />
<link href="/web/templates/Admin/js/menu/js/image-picker/image-picker/image-picker.css" rel="stylesheet" />


<script src="/web/templates/Admin/js/menu/js/jquery-ui.js "></script>
<script src="/web/templates/Admin/js/menu/js/image-picker/image-picker/image-picker.js"></script>
<div class="navbar-nav navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin"><img src="/web/templates/Admin/img/Moon-Store-transparent-bg.png" width="50px" height="50px" alt="Moonstore"></a>
        </div> 
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Сайт</a></li>                            
                <li><a href="/admin/posts">Статьи</a></li>
                <li><a href="/admin/post/add">Создать статью</a></li>
                    {assign var='user' value=$smarty.session.register.user_auth|base64_decode|unserialize}
                    {if $user->getRoleId() == 1}  
                    <li><a href="/admin/profile/usermanagement"> Пользователи</a></li>
                    {/if}
                <li><a href="#" id="showMenu">Расширенное меню</a></li>

            </ul>
        </div><!--/.nav-collapse -->
    </div>    
    <!-- The contents (if there's no contents the ribbon acts as a link) -->
    {if isset($lastpost)}
        <div class="forkit-curtain">
            <div class="close-button"></div>

            {*            {foreach from = $lastpost item = post}*}
            <div class="col-sm-3 col-lg-3">
                <div class="dash-unit">
                    <dtitle>Настройки сайта </dtitle>
                    <hr>

                    <div class="text">

                        <label class="checkbox inline">
                            Из одной категории
                            <div class="switch">
                                <input type="radio" class="switch-input" name="isLocked" value="true" id="on" checked="" >
                                <label for="on" class="switch-label switch-label-off">On</label>
                                <input type="radio" class="switch-input" name="isLocked" value="false" id="off"  >
                                <label for="off" class="switch-label switch-label-on">Off</label>
                                <span class="switch-selection"></span><br>

                                <div class="count">
                                    <label for="number">Количество</label>
                                    <div class="number">
                                        <input type="number" value="{$settings[0]->category_count}" id="number"/>
                                        <br>

                                    </div>
                                </div>
                                <a href="#" class="save">Сохранить</a>

                                {literal}
                                    <script type="text/javascript">
                                        $("input.switch-input").on("click", function () {
                                            if ($("input:checked").val() == 'true') {

                                                $(".count").fadeIn(400);
                                            } else {
                                                $(".count").fadeOut(400);
                                                $("input#number").val('0');
                                            }
                                        });
                                        $(".save").click(function () {
                                            $.ajax({
                                                url: '/admin/pagination/save/',
                                                method: 'POST',
                                                data: {count: $("#number").val()},
                                                success: function () {
                                                    alert('Сохранено');
                                                },
                                                error: function () {
                                                    alert('Не удачно');
                                                }
                                            });

                                        });</script>
                                    {/literal}
                            </div> 
                        </label>
                    </div>
                    <br>
                    <div class="info-user">

                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-lg-3">
                <div class="dash-unit">
                    <dtitle>Пагинация </dtitle>
                    <hr>
                    <div class="text">

                        <div class="count-pagination">
                            <label for="number">Количество статей на странице</label>
                            <div class="number">
                                <input type="number" value="{$settings[0]->pagination}" id="number-pagination"/>
                                <br>
                            </div>
                        </div>
                        <a href="#" class="save-pagination">Сохранить</a>
                        <script type="text/javascript">
                            {literal}
                                $(".save-pagination").click(function () {
                                    $.ajax({
                                        url: '/admin/pagination/save/pagination/',
                                        method: 'POST',
                                        data: {count: $("#number-pagination").val()},
                                        success: function () {
                                            alert('Сохранено');
                                        },
                                        error: function () {
                                            alert('Не удачно');
                                        }
                                    });
                                    //$.post("/admin/pagination/save/pagination/", $("#number-pagination").val());
                                    //                                   alert("Сохранено");
                                });{/literal}
                            </script>
                        </div>
                        <br>
                    </div>
                </div>
                <div class="col-sm-3 col-lg-3">
                    <div class="dash-unit">
                        <dtitle>Слайдер</dtitle>
                        <hr>
                        <div class="thumbnail">
                            Количество слайдов
                        </div><!-- /thumbnail -->
                        <div class="text">
                            <form id="upload" method="post" action="/admin/slider/save/slider/" enctype="multipart/form-data">
                                <div id="drop">
                                    Перетащи сюда

                                    <a>Выбрать</a>
                                    <input type="file" name="upl" multiple />
                                </div>

                                <ul>
                                    <!-- The file uploads will be shown here -->
                                </ul>

                            </form>

                        </div>
                        <br>

                    </div>
                </div>
                <div class="col-sm-3 col-lg-3">
                    <div class="dash-unit">
                        <dtitle>Настройки сайта </dtitle>
                        <hr>
                        <div class="text" style="overflow: scroll; height:244px;">
                            <form id="slider-settings" method="get" action="#">
                                <select class="image-picker show-html">
                                    {foreach from=$slider item='point'}
                                        <option data-img-src="{$point->image_path}" value="{$point->is_id}">{$point->is_id}</option>
                                    {/foreach}
                                </select>
                                <br>
                                <input type="text" class="post" name="post" placeholder="Статья">
                            </form>
                            <br>
                            <a class="save-slider">Сохранить настройки</a><br>
                            <a class="delete-slider" style="display: none">Удалить настройки</a>
                        </div>
                    </div>
                    <br>

                </div>
            </div>
            {*            {/foreach}*}
            {literal}
                <script type="text/javascript">
                    ;
                    $("select").imagepicker();
                    $(".thumbnail").click(function () {
                        var imgsrc = $(this).find('img').attr('src');
                        $.ajax({
                            type: 'get',
                            url: '/admin/find/post/slider/',
                            data: {'imgsrc': imgsrc},
                            success: function (data) {
                                $('.post').val(data);
                            }
                        });
                        $(".delete-slider").show(300);
                    });
                    $(".delete-slider").click(function () {
                        var frm = $('#slider-settings');
                        $.ajax({
                            type: 'get',
                            url: '/admin/keepost/',
                            data: {'image_id': frm.find(".selected").find("img").attr('src'), 'post': frm.find('.post').val(), 'delete': 1},
                            success: function (data) {
                                //alert('ok');
                                frm.find(".selected").hide(700);
                               // frm.find(".selected").remove();
                            }
                        });
                    });
                    $(".save-slider").click(function () {
                        var frm = $('#slider-settings');

                        $.ajax({
                            type: 'get',
                            url: '/admin/keepost/',
                            data: {'image_id': frm.find(".selected").find("img").attr('src'), 'post': frm.find('.post').val()},
                            success: function (data) {
                                alert('ok');
                            }
                        });
                    });


                    //$(function () {
                    $(".post").autocomplete({
                        source: function (request, response) {
                            $.ajax({
                                url: '/admin/getposts/',
                                method: 'GET',
                                data: {'q': request.term},
                                dataType: 'json',
                                success: function (data) {
//                                            console.log(data);
                                    response(data);
                                }
                            });
                        },
                        minLength: 1,
                        open: function () {
                            $(".ui-autocomplete").insertAfter(".post");
                            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                        },
                        close: function () {
                            $(".ui-autocomplete").hide();
                            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                        }
                    });
                    //});
                </script>  
            {/literal}
        </div>

        <!-- The ribbon -->
        <a class="forkit" data-text="Настройки" data-text-detached="Протяни вниз >" href="#"></a>

        <script src="/web/templates/Admin/js/forkit.js"></script>
        {/if}
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="/web/templates/Admin/js/sliderSettings/assets/js/jquery.knob.js"></script>

        <!-- jQuery File Upload Dependencies -->
        <script src="/web/templates/Admin/js/sliderSettings/assets/js/jquery.ui.widget.js"></script>
        <script src="/web/templates/Admin/js/sliderSettings/assets/js/jquery.iframe-transport.js"></script>
        <script src="/web/templates/Admin/js/sliderSettings/assets/js/jquery.fileupload.js"></script>

        <!-- Our main JS file -->
        <script src="/web/templates/Admin/js/sliderSettings/assets/js/script.js"></script>

        {* $(".save-slider").click(function () {
        $.post("/admin/slider/save/slider/", $("form-slider").serialize());
        alert("Сохранено");
        });*}


