<!DOCTYPE html>
<html>
<head>
    <link href="/web/templates/Admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="/web/templates/Admin/css/main.css" rel="stylesheet">
    <link href="/web/templates/Admin/css/font-style.css" rel="stylesheet">
    <link href="/web/templates/Admin/css/flexslider.css" rel="stylesheet">
    <link href="/web/templates/Admin/js/multiselect/jquery.tokenize.css" rel="stylesheet">
    <link href="/web/templates/Admin/datetime/jquery.datetimepicker.css" rel="stylesheet">

    <script type="text/javascript" src="/web/templates/Admin/js/jquery-1.10.2.min.js"></script>
    <link href="/web/templates/Admin/css/tingle.css" rel="stylesheet">
    <script type="text/javascript" src="/web/templates/Admin/js/bootstrap.js"></script>
    <script type="text/javascript" src="/web/templates/Admin/js/lineandbars.js"></script>
    <!— You can add more layouts if you want —>
    <script src="/web/templates/Admin/js/bootstrap-multiselect.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/component.css"/>
    <script type="text/javascript" src="/web/templates/Admin/js/tinymce/tinymce.min.js"></script>
    <script src="/web/templates/Admin/js/menu/js/modernizr.custom.25376.js"></script>
    <script src="/web/templates/Admin/js/video.js"></script>

    <!— Our main JS file —>
    <script type="text/javascript">
        tinymce.init({
            height: '750px',
            width: '100%',
            selector: "textarea",
            plugins: [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime table contextmenu paste imagetools image responsivefilemanager",
                "youtube",
                "videoembed"
            ],
            language: "ru",
            toolbar: "preview | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager | youtube | videoembed",
            content_css: "/web/templates/Site/assets/css/article.css",
            image_advtab: true,
            relative_urls: false,
            browser_spellcheck: true,
            filemanager_title: "Filemanager",
            external_filemanager_path: "/web/templates/Admin/js/filemanager/",
            external_plugins: {
                "filemanager": "./filemanager/plugin.min.js"
            },
        });

        $(document).ready(function () {
            $(".video-lb").hide();
            $(".vimeo-player").hide();
        });

        window.onload = function () {
            globalSwitch()
        };
        function globalSwitch(selected = null) {
            if (selected === null) {
                {if $post_edit[0]->img_type}
                selected = {$post_edit[0]->img_type};
                {else}
                selected = 0;
                {/if}
            }
            console.log(selected);
            if (selected == 0) {
                $(".post-name").show();
                $("#imageInput").show();
                $(".video-lb").hide();
                $(".player").hide();
                $(".vimeo-player").hide();
            } else if (selected == 1) {
                $(".post-name").hide();
                $("#imageInput").hide();
                $(".video-lb").show();
                $(".player").show();
                $(".vimeo-player").show();
            } else {
                $(".post-name").show();
                $("#imageInput").show();
                $(".video-lb").show();
                $(".player").show();
                $(".vimeo-player").show();
            }
        }

    </script>

    <style>
        p {
            font-family: sans-serif;
        }

        label.custom-select {
            position: relative;
            display: inline-block;
        }

        .custom-select select {
            display: inline-block;
            border: 2px solid #bbb;
            padding: 4px 3px 3px 5px;
            margin: 0;
            font: inherit;
            outline: none; /* remove focus ring from Webkit */
            line-height: 1.2;
            background: #f8f8f8;

            -webkit-appearance: none; /* remove the strong OSX influence from Webkit */

            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            border-radius: 6px;
        }

        /* for Webkit's CSS-only solution */
        @media screen and (-webkit-min-device-pixel-ratio: 0) {
            .custom-select select {
                padding-right: 30px;
            }
        }

        /* Since we removed the default focus styles, we have to add our own */
        .custom-select select:focus {
            -webkit-box-shadow: 0 0 3px 1px #c00;
            -moz-box-shadow: 0 0 3px 1px #c00;
            box-shadow: 0 0 3px 1px #c00;
        }

        /* Select arrow styling */
        .custom-select:after {
            content: "▼";
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            font-size: 60%;
            line-height: 30px;
            padding: 0 7px;
            background: #bbb;
            color: white;

            pointer-events: none;

            -webkit-border-radius: 0 6px 6px 0;
            -moz-border-radius: 0 6px 6px 0;
            border-radius: 0 6px 6px 0;
        }

        .no-pointer-events .custom-select:after {
            content: none;
        }

        @media (max-width: 1210px) {
            .post_comments {
                display: none;
            }
        }
    </style>
    {literal}
        <script type="text/javascript">

        </script>
    {/literal}
</head>
<body>

{include file='./menu.tpl'}

<div id="perspective" class="perspective effect-moveleft">

    <div class="container" style="margin-top: 75px;">
        <div class="wrapper"><!— wrapper needed for scroll —>
            <!— Top Navigation —>

            <div class="main clearfix">
                <!— FIRST ROW OF BLOCKS —>
                <div class="container">

                    <div class="row">

                        {if isset($post_edit)}
                            <div class="post_add edit">

                                <form method="post" action="/admin/profile/posts/edit/" enctype="multipart/form-data"
                                      class="form-edit">
                                    <input name="post_id" class="input-huge" value="{$post_edit[0]->post_id}"
                                           type="hidden">

                                    <label for="img_type" class="custom-select">
                                        Изображение статьи:

                                        <select name="img_type" class="img_type">
                                            <option value="0" {if $post_edit[0]->img_type eq 0} selected="" {/if}>
                                                Изображение
                                            </option>
                                            <option value="1" {if $post_edit[0]->img_type eq 1} selected="" {/if}>
                                                Видео
                                            </option>
                                            <option value="2" {if $post_edit[0]->img_type eq 2} selected="" {/if}>Микс
                                            </option>
                                        </select>
                                    </label>
                                    <br><br><br>
                                    <label class="post-name"> Изображение
                                        <input name="image_file" id="imageInput" type="file"/>
                                    </label>
                                    <br>

                                    <label for="video_url" class="video-lb">
                                        Видео:
                                        <input name="video_url" class="video" type="text"
                                               style="width: 500px"
                                               placeholder="Вставить ссылку на видео"
                                                {if $post_edit[0]->vimeo != '' }
                                                    value="{$post_edit[0]->vimeo}"
                                                {/if}
                                        >
                                    </label>
                                    <div class="player">
                                        <iframe src="{if $post_edit[0]->vimeo != '' }{$post_edit[0]->vimeo}{/if}"
                                                width="500" height="281" frameborder="0"
                                                webkitallowfullscreen mozallowfullscreen
                                                allowfullscreen
                                                class="vimeo-player">
                                        </iframe>
                                        <p>
                                    </div>
                                    <br><br>

                                    <br>
                                    {*<div class="img-prew">
                                    <img src="{$post_edit[0]->post_img}" ><br>
                                    </div>*}

                                    <label class="post-name"> Название статьи </label>
                                    <input name="post_name" class="post-name-input" type="text"
                                           value="{$post_edit[0]->post_name}">

                                    <label class="custom-select">
                                        Категория:
                                        <select name="category_id">
                                            {foreach from = $post_edit[1] item = category}
                                                <option value="{$category->posts_category_id}"
                                                        {if {$post_edit[0]->post_category_id} == {$category->category_name}}
                                                    selected="selected" {/if}>
                                                    {$category->category_name}</option>
                                            {/foreach}
                                        </select>
                                    </label>
                                    <br>
                                    <label class="post-seo-desc"> Краткое описание: </label>
                                    <input name="short_desc" type="text" placeholder="Краткое описание"
                                           value="{$post_edit[0]->short_desc}">

<textarea name="content" id="my_editor"
          style="width:50%; height: 75%">{$post_edit[0]->post_text}</textarea>

                                    <label class="post-seo-desc"> Seo description: </label>
                                    <input name="post_seo_desc" type="text" placeholder="SEO описание"
                                           value="{$post_edit[0]->post_seo_desc}">
                                    <label class="post-seo-title"> Seo title: </label>
                                    <input name="post_seo_title" type="text" placeholder="SEO title"
                                           value="{$post_edit[0]->post_seo_title}">
                                    <label class="posttags"> Tags:
                                        <select id="tokenize" name="tags[]" multiple class="tokenize-sample">
                                            {foreach from=$tags item='tag'}
                                                <option value="{$tag->tag_id}" {if $tag->tag_id|in_array:$tagsp} selected{/if}>{$tag->tag_name}</option>
                                            {/foreach}
                                        </select>
                                    </label>
                                    <a class="icon-image trigger-button-tags">Создать Tag</a>
                                    <br><br>
                                    <label class="position">
                                        Type:
                                        <select name="position">
                                            {foreach from = $positions item = 'position'}
                                                <option value="{$position->position_id}"
                                                        {if {$post_edit[0]->position_id} == {$position->position_id}}
                                                    selected="selected" {/if}>
                                                    {$position->position_name}</option>
                                            {/foreach}
                                        </select>
                                    </label>
                                    <label class="align">
                                        Позиция:
                                        <select name="align">
                                            {foreach from = $aligns item = 'align'}
                                                <option value="{$align->align_id}"
                                                        {if {$post_edit[0]->type_id} == {$align->align_id}} selected="" {/if}>
                                                    {$align->align_name}</option>
                                            {/foreach}
                                        </select>
                                    </label>
                                    <input type="submit" value="Сохранить ">

                                </form>

                            </div>
                            <div class="post_comments">
                                Комментарии
                                <div class="acc-container">
                                    <article class="accordion">
                                        {foreach from = $post_edit[0]->comments key=i item = comment}
                                            <section id="acc{$i}">
                                                <h2><a href="#acc{$i}">
                                                        {if $comment === 1} Нет комментариев {else}От {$comment->ip} в {$comment->date}{/if}</a>
                                                </h2>
                                                {if $comment->published == 1}
                                                    <p style="color: green">Опубликованно</p>
                                                {else} <p
                                                        style="color: red"> {if $comment == 1} Здесь будет коментарий {else}Заблокированно{/if} </p>{/if}
                                                <hr>
                                                <p>{$comment->comment}</p>
                                                {if $comment->published == 1}
                                                    <a href="/admin/profile/comments/unpublish/{$comment->comment_id}"
                                                       style="color: red">
                                                        Заблокировать</a>
                                                {/if}
                                            </section>
                                        {/foreach}
                                    </article>
                                </div>
                                <div class="img-fon" style="margin-top: 50px;">
                                    Изображение статьи
                                    {if isset($post_edit)}
                                        <img src="{$post_edit[0]->post_img}"
                                             style="max-width: 100%; max-height: 100%" alt="{$post_edit[0]->post_name}"
                                             title="{$post_edit[0]->post_name}">
                                    {/if}
                                </div>
                            </div>
                            <div>
                                <a href="#" class="preview" target="_blank">Просмотреть</a>

                            </div>
                            <script>
                                $(".preview").click(function () {
                                    var text = $("#content_ifr").contents().find('#tinymce').html();
                                    var postName = $(".post_name-input").val();
                                    $.ajax({
                                        url: '/posts/preview/save/',
                                        method: 'post',
                                        data: {
                                            'post_id': {$post_edit[0]->post_id},
                                            'post_name': postName,
                                            'content': text
                                        }
                                    });
                                    window.open('/posts/previewtrue/show/{$post_edit[0]->post_id}', '_blank');
                                    return false;
                                });

                            </script>
                            <!-- ---------------------------    -->{else}<!-- ---------------------------    -->
                            <div class="post_add">
                                <form method="post" action="/admin/profile/posts/add/" enctype="multipart/form-data"
                                      class="edit-form" style="margin-top: 100px">

                                    <label for="img_type" class="custom-select">
                                        Изображение статьи:

                                        <select name="img_type" class="img_type">
                                            <option value="0">Изображение</option>
                                            <option value="1">Видео</option>
                                            <option value="2">Микс</option>
                                        </select>
                                    </label>
                                    <br><br>
                                    <label class="post-name"> Выбрать изображение

                                        <input name="image_file" id="imageInput" type="file"/>
                                    </label>
                                    <br>
                                    <label for="video_url" class="video-lb">
                                        Видео:
                                        <input name="video_url" class="video" type="text"
                                               style="width: 500px"
                                               placeholder="Вставить ссылку на видео">
                                    </label>

                                    <div class="player">
                                        <iframe src=""
                                                width="500" height="281" frameborder="0"
                                                webkitallowfullscreen mozallowfullscreen
                                                allowfullscreen
                                                class="vimeo-player">
                                        </iframe>
                                        <p>
                                    </div>

                                    <label class="post-name"> Название статьи: </label>
                                    <input name="post_name" type="text" placeholder="Название статьи">

                                    <label class="custom-select">
                                        Категории:

                                        <select name="category_id">
                                            {foreach from = $cat item = category}
                                                <option value="{$category->posts_category_id}">{$category->category_name}</option>
                                            {/foreach}
                                        </select>
                                    </label><br><br>
                                    {*<label for="tiem">
                                    Время публикации
                                    <input id="datetimepicker" name="date" type="text" >
                                    </label>
                                    <script src="/web/templates/Admin/datetime/build/jquery.datetimepicker.full.min.js"></script>
                                    <script type="text/javascript">
                                    $('#datetimepicker').datetimepicker({
                                    format: 'Y-m-d H:i:s',
                                    lang: 'ru'
                                    });
                                    </script>*}
                                    <br>
                                    <label class="post-seo-desc"> Краткое описание: </label>
                                    <input name="short_desc" type="text" placeholder="Краткое описание"
                                           value="{$post_edit[0]->short_desc}">

                                    <textarea name="content" style="width:50%"></textarea>
                                    <label class="post-seo-desc"> Seo description: </label>
                                    <input name="post_seo_desc" type="text" placeholder="SEO описание">

                                    <label class="post-seo-title"> Seo title: </label>
                                    <input name="post_seo_desc" type="text" placeholder="SEO описание">

                                    <label class="posttags"> Tags:
                                        <select id="tokenize" name="tags[]" multiple class="tokenize-sample">
                                            {foreach from=$tags item='tag'}
                                                <option value="{$tag->tag_id}">{$tag->tag_name}</option>
                                            {/foreach}
                                        </select>
                                    </label>
                                    <a class="icon-image trigger-button-tags">Создать Tag</a>
                                    <br><br>
                                    <label class="position">
                                        Type:
                                        <select name="position">
                                            {foreach from = $positions item = 'position'}
                                                <option value="{$position->position_id}"
                                                        {if {$post_edit[0]->position_id} == {$position->position_id}} selected="" {/if}>
                                                    {$position->position_name}</option>
                                            {/foreach}
                                        </select>
                                    </label>
                                    <label class="align">
                                        Позиция:
                                        <select name="align">
                                            {foreach from = $aligns item = 'align'}
                                                <option value="{$align->align_id}">
                                                    {$align->align_name}</option>
                                            {/foreach}
                                        </select>
                                    </label>
                                    <input type="submit" value="Сохранить ">

                                </form>

                            </div>
                            <div class="post_comments">
                                Комментарии
                                <div class="acc-container">
                                    <article class="accordion">
                                        <section id="acc1">
                                            <h2><a href="#acc1">
                                                    Нет комментариев </a>
                                            </h2>

                                            <p style="color: green">Опубликованно</p>
                                            <hr>
                                            <p>Здесь будут комментарии</p>

                                        </section>
                                    </article>
                                </div>
                            </div>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tingle-demo-tag" style="display: none">
        <form method="post" id="tag-add">
            <label class="tag"> Название tag: </label>
            <input name="tag" class="tag-name" type="text" placeholder="Название tag">

            <a href="#" class="btn-done-tag">Готово</a>
        </form>
    </div>

    {include file='./footer.tpl'}
    <link rel="stylesheet" type="text/css" href="/web/templates/Admin/js/multiselect/jquery.tokenize.css"/>
    <script type="text/javascript" src="/web/templates/Admin/js/multiselect/jquery.tokenize.js"></script>

    <script>
        // $(document).ready(function (request, response) {

        $(function () {
                    {if $post_edit[0]->vimeo != '' }
                    switcher();
                    {/if}
                    $('#tokenize').tokenize().toArray();
                    $(".video-check").click(function () {
                        switcher();
                    });
                    $(".img_type").change(function () {
                        var selected = $(this).val();
                        globalSwitch(selected);
                    });
                    $(".video").change(function () {
                        changeState();
                    });
                    function changeState() {
                        var value = $(".video").val().split("/");
                        var code = value[value.length - 1];
                        console.log(code);
                        $(".vimeo-player").show();
                        var url = '';
                        if (value.indexOf('vimeo.com') !== -1) {
                            console.log('vimeo');
                            url = "https://player.vimeo.com/video/" + code;
                        } else {
                            console.log('youtube');
                            var youtubeCode = code.split('=');
                            url = 'https://www.youtube.com/embed/' + youtubeCode[youtubeCode.length - 1];
                        }
                        $('.chackbox-video').val($(".video").val());
                        $(".vimeo-player").attr('src', url);
                    }

                    function switcher() {
                        if ($(".video-check").is(':checked')) {
                            $(".post-name").hide();
                            $("#imageInput").hide();
                            $(".video-lb").show();
                            $(".player").show();
                            $(".vimeo-player").show();
                        } else {
                            $(".post-name").show();
                            $("#imageInput").show();
                            $(".video-lb").hide();
                            $(".player").hide();
                            $(".vimeo-player").hide();
                        }
                    }
                }
        );
        {literal}
        $(".trigger-button-tags").click(function () {
            console.log('yes');
            tingle.modal.setContent($(".tingle-demo-tag").html());
            tingle.modal.open();
            $(".btn-done-tag").click(function () {
                var tagname = $(".tag-name").val();
                console.log(tagname);
                $.ajax({
                    url: '/admin/tag/add/',
                    dataType: 'json',
                    data: {'tag': tagname},
                    success: function (data) {
                        updateTags(data.tagname, data.tagid)
                    },
                    error: function () {
                        alert('Error. Somthing wrong. Please contact with developer')
                    }
                });

                tingle.modal.close();
                return false;
            });
        });
        function updateTags(tag, tagId) {
            $("#tokenize").append("<option value='" + tagId + "'>" + tag + "</option>")
        }
        {/literal}
    </script>
</body>
</html>