
<!doctype html>
<html><head>
        <meta charset="utf-8">
        <title>Moonstore - profile</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="/web/templates/Admin/css/bootstrap.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/main.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/font-style.css" rel="stylesheet">
        <link rel="stylesheet" href="/web/templates/Admin/css/register.css">
        <link href="/web/templates/Admin/css/tingle.css" rel="stylesheet">

        <script type="text/javascript" src="/web/templates/Admin/js/jquery-1.10.2.min.js"></script>    
        <script src="/web/templates/Admin/js/bootstrap.js"></script>


        <style type="text/css">
            body {
                padding-top: 60px;
            }
        </style>
        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/component.css" />
        <script src="/web/templates/Admin/js/menu/js/modernizr.custom.25376.js"></script>
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Google Fonts call. Font Used Open Sans & Raleway -->
        <link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">


    </head>
    <body>

        <!-- NAVIGATION MENU -->

        {include file='./menu.tpl'}
        <div id="perspective" class="perspective effect-moveleft">
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <!-- Top Navigation -->

                    <div class="main clearfix">
                        <div class="container">
                            <div class="row">

                                <div class="col-lg-6">

                                    <div class="register-info-wraper">
                                        <div id="register-info">
                                            <div class="cont2">
                                                <div class="thumbnail">
                                                    <img src="/web/templates/Admin/img/face80x80.jpg" alt="{user->getUserFirstName}" class="img-circle">
                                                </div><!-- /thumbnail -->
                                                <h2 {if {user->getIsLocked} != 0}style="color:red" title="Заблокироавн"{/if}>{user->getUserFirstName} {user->getUserLastName}</h2>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <div class="cont3">
                                                        <p><ok>Имя:</ok> {user->getUserFirstName}</p>
                                                        <p><ok>Mail:</ok> {user->getUserEmail}</p>
                                                        <p><ok>Телефон:</ok> {user->getCellPhone}</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="cont3">
                                                        <p><ok>Роль:</ok> {role->getRoleName}</p>
                                                        <p><ok>Последний визит:</ok> {user->getLastVisit}</p>

                                                    </div>
                                                </div>
                                            </div><!-- /inner row -->
                                            <hr>
                                            <div class="cont2">
                                                <h2>Доступные действия</h2>
                                            </div>
                                            <br>
                                            <div class="info-user2">
                                                <a href="/admin/profile/usermanagement/" title="Все пользователи"><span aria-hidden="true" class="li_user fs1"></span></a> 
                                                    {assign var='user_auth' value=$smarty.session.register.user_auth|base64_decode|unserialize}
                                                    {if $user_auth->getRoleId() == 1}
                                                    <a href="/admin/profile/usermanagement/admin/user_id={user->getUserId}"><span aria-hidden="true" class="li_key fs1" {if {user->getRoleId} ==1}title="Техподдержка"{else}title="Администратор"{/if}></span></a>
                                                    <a href="/admin/profile/usermanagement/delete/user_id={user->getUserId}" title="{if {user->getIsLocked} == 0}Заблокировать{else}Разблокировать{/if}"><span aria-hidden="true" class="li_lock fs1"></span></a>
                                                    {/if}
                                            </div>


                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-6 col-lg-6">
                                    <div id="register-wraper">
                                        {if isset($add)}
                                            <form id="register-form" class="form" method="post" action="/admin/profile/usermanagement/add/">
                                                <legend>Регистрация пользователя</legend>
                                                <input name="module" class="input-huge" value="usermanagement" type="hidden">
                                                <input name="action" class="input-huge" value="add" type="hidden">

                                                <div class="body">
                                                    <!-- first name -->
                                                    <label for="name">Имя</label>
                                                    <input name="name" class="input-huge" placeholder="First Name" type="text">
                                                    <!-- last name -->
                                                    <label for="lastname">Фамилия</label>
                                                    <input name="lastname" class="input-huge" placeholder="Last Name" type="text">
                                                    <label>Телефон</label>
                                                    <input name="cell_phone" class="input-huge" placeholder="Cell Phone" type="text">
                                                    <!-- email -->
                                                    <label>E-mail</label>
                                                    <input name="email" class="input-huge" placeholder="Email" type="text">
                                                    <!-- password -->
                                                    <label>Пароль</label>
                                                    <input name="password" class="input-huge" placeholder="password" type="text">

                                                </div>

                                                <div class="footer">
                                                    <label class="checkbox inline">
                                                        <div class="switch">
                                                            <input type="radio" class="switch-input" name="isLocked" value="true" id="on" checked="" >
                                                            <label for="on" class="switch-label switch-label-off">On</label>
                                                            <input type="radio" class="switch-input" name="isLocked" value="false" id="off" >
                                                            <label for="off" class="switch-label switch-label-on">Off</label>
                                                            <span class="switch-selection"></span>
                                                        </div> 
                                                    </label>
                                                    <button type="submit" class="btn btn-success">Зарегестрировать</button>
                                                </div>
                                            </form>
                                        {else}
                                            <form id="register-form" class="form" action="/admin/profile/usermanagement/edit/" method="post">
                                                <legend>Изменить данные</legend>
                                               
                                                <input name="user_id" class="input-huge" value="{user->getUserId}" type="hidden">

                                                <div class="body">
                                                    <!-- first name -->
                                                    <label for="name">Имя</label>
                                                    <input name="name" class="input-huge" placeholder="{user->getUserFirstName}" value="{user->getUserFirstName}" type="text">
                                                    <!-- last name -->
                                                    <label for="lastname">Фамилия</label>
                                                    <input name="lastname" class="input-huge" value="{user->getUserLastName}" placeholder="{user->getUserLastName}" type="text">
                                                    <label>Телефон</label>
                                                    <input name="cell_phone" class="input-huge" placeholder="{user->getCellPhone}" value="{user->getCellPhone}" type="text">
                                                    <!-- email -->
                                                    <label>E-mail</label>
                                                    <input name="email" class="input-huge" placeholder="{user->getUserEmail}" value="{user->getUserEmail}" type="text">
                                                    <!-- password -->
                                                    <label>Пароль</label>
                                                    <input name="password" class="input-huge" placeholder="password" type="text">

                                                </div>

                                                <div class="footer">
                                                    <label class="checkbox inline">
                                                        <div class="switch">
                                                            <input type="radio" class="switch-input" name="isLocked" value="true" id="on" {if {user->getIsLocked} != 1 } checked="" {/if} >
                                                            <label for="on" class="switch-label switch-label-off">On</label>
                                                            <input type="radio" class="switch-input" name="isLocked" value="false" id="off" {if {user->getIsLocked} == 1 } checked="" {/if} >
                                                            <label for="off" class="switch-label switch-label-on">Off</label>
                                                            <span class="switch-selection"></span>
                                                        </div> 
                                                    </label>
                                                    <button type="submit" class="btn btn-success">Обновить</button>
                                                </div>
                                            </form>
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {include file='./footer.tpl'}
    </body></html>