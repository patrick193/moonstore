<!doctype html>
<html><head>
        <meta charset="utf-8">
        <title>Moonstore - Send emails</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link href="/web/templates/Admin/css/bootstrap.min.css" rel="stylesheet">

        <link href="/web/templates/Admin/css/main.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/flexslider.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/component.css" />
        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/normalize.css" />
        <script type="text/javascript" src="/web/templates/Admin/js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/jquery-ui.js"></script>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="/web/templates/Admin/js/bootstrap-multiselect.js" type="text/javascript"></script>

        <script type="text/javascript" src="/web/templates/Admin/js/default.js"></script>
        <script src="/web/templates/Admin/js/menu/js/modernizr.custom.25376.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/tinymce/tinymce.min.js"></script>

        <script type="text/javascript">
            tinymce.init({
                height: '750px',
                width: '100%',
                selector: "textarea",
                plugins: [
                    "advlist autolink lists link charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime table contextmenu paste imagetools jbimages",
                    "youtube",
                ],
                language: "ru",
                toolbar: "preview | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | jbimages | youtube",
                image_advtab: true,
            });
            $(document).ready(function () {
{*                $(".navbar-nav").children('li').last().remove();
                $(".outer-nav").remove();
*}            });

        </script>
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!-- Google Fonts call. Font Used Open Sans -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

        <!-- DataTables Initialization -->
        <script type="text/javascript" src="/web/templates/Admin/js/jquery.dataTables.js"></script>
    </head>
    <body>
        {include file='./menu.tpl'}
        <div id="perspective" class="perspective effect-moveleft">
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <!-- Top Navigation -->

                    <div class="main clearfix">
                        <!-- CONTENT -->       
                        <div class="container">
                            <div class="row main-row">
                                <!-- TODO -->
                                <div class="email">

                                    <form method="post" action="/admin/subscribe/send/" enctype="multipart/form-data" style="margin-top: 100px">
                                        <label class="email_name"> От кого: 
                                            <input name="email_name" type="text" placeholder="Имя пользователя" value="{$owner[0]->From}">
                                        </label>
                                        <label class="email_subject"> Тема: 
                                            <input name="email_subject" type="text" placeholder="Email subject">
                                        </label>
                                        <br>
                                        <br>
                                        <label for="customers" class="customers"> Кому:<br>
                                            <select id="tokenize" name="customers[]" multiple class="tokenize-sample" >
                                                {foreach from = $customers item = customer}
                                                    <option value="{$customer->subscriber_id}" selected="">
                                                        {$customer->email}
                                                    </option>

                                                {/foreach}
                                            </select>
                                        </label>
                                        <textarea name="content" style="width:50%; height: 75%"></textarea>

                                        <br>
                                        <input type="submit" value="Отправить">

                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {include file='./footer.tpl'}
        </div>
        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/js/multiselect/jquery.tokenize.css" />
        <script type="text/javascript" src="/web/templates/Admin/js/multiselect/jquery.tokenize.js"></script>
        <script type="text/javascript">
            $('#tokenize').tokenize().toArray();
        </script>
    </body>
</html>