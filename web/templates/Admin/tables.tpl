<!doctype html>
<html><head>
        <meta charset="utf-8">
        <title>Moonstore - Posts</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link href="/web/templates/Admin/css/tingle.css" rel="stylesheet">
        <link rel="stylesheet" href="/web/templates/Admin/css/normalize.css">
        <link rel="stylesheet" href="/web/templates/Admin/css/posts-style.css">

        <script src="/web/templates/Admin/js/prefixfree.min.js"></script>

        <link href="/web/templates/Admin/css/bootstrap.min.css" rel="stylesheet">

        <link href="/web/templates/Admin/css/main.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/font-style.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/flexslider.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/component.css" />
        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/normalize.css" />
        <script type="text/javascript" src="/web/templates/Admin/js/jquery-1.10.2.min.js"></script>

        <!-- Placed at the end of the document so the pages load faster -->
        <script type="text/javascript" src="/web/templates/Admin/js/bootstrap.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/lineandbars.js"></script>


        <!-- NOTY JAVASCRIPT -->
        <script type="text/javascript" src="/web/templates/Admin/js/jquery.noty.js"></script>
        <!-- You can add more layouts if you want -->
        <script type="text/javascript" src="/web/templates/Admin/js/default.js"></script>
        <script src="/web/templates/Admin/js/jquery.flexslider.js" type="text/javascript"></script>

        <script type="text/javascript" src="/web/templates/Admin/js/admin.js"></script>
        <script src="/web/templates/Admin/js/menu/js/modernizr.custom.25376.js"></script>
        <style type="text/css">
            body {
                padding-top: 60px;
            }
        </style>

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Google Fonts call. Font Used Open Sans -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

        <!-- DataTables Initialization -->
        <script type="text/javascript" src="/web/templates/Admin/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {
                $('#dt1').dataTable();
            });
        </script>


    </head>
    <body>

        <!-- NAVIGATION MENU -->

        {include file='./menu.tpl'}
        <div id="perspective" class="perspective effect-moveleft">
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <!-- Top Navigation -->

                    <div class="main clearfix">
                        <!-- CONTENT -->       
                        <div class="container">

                            <div class="row main-row">
                                {if isset($posts)}
                                    {foreach from = $posts.query item = post}
                                        <div class="col-sm-3 col-lg-3" id="post-{$post->post_id}" >

                                            <!-- LAST USER BLOCK -->     
                                            <div class="half-unit"  >
                                                <div class="text">
                                                    <a href="/admin/post/edit/{$post->post_id}" {if {$post->isLocked} != 0} style="color: red" data-block="1" title="Заблокированно" 
                                                       {else} style="color: greenyellow" data-block="0"{/if}>#{$post->post_id}  {$post->post_name}</a>
                                                           <a href="#" class="remove" data-id='{$post->post_id}' style="float: right; margin-right: 5px">
                                                                {if $smarty.now|date_format: "Y-m-d H:i:s" >= $post->date_created}X{/if}</a>
                                                       </div>
                                                       <hr>
                                                       <div class="cont2">
                                                           <div class="text">
                                                           </div>
                                                           <br>
                                                           <br>
                                                           <div class="left_img">
                                                               <a href="/admin/post/edit/{$post->post_id}"><img src="{$post->post_img_th}"></a>
                                                           </div>
                                                           <div class="after_img">
                                                               {if $smarty.now|date_format: "Y-m-d H:i:s" < $post->date_created}
                                                                   Запланированная
                                                               {/if}
                                                               <p><bold>Дата редактирования: {$post->date_redaction}</bold></p>
                                                               <p><bold>{$post->post_category_id}</bold></p>

                                                           </div>
                                                       </div>
                                                    </div>

                                                </div>
                                                {/foreach}

                                                    {/if}

                                                        <div class="col-sm-3 col-lg-3 add">
                                                            <a href="/admin/post/add"><div class="half-unit-add-list">
                                                                </div></a>
                                                        </div>
                                                        <input type="checkbox" id="load_more" role="button">
                                                        {if ($posts.page_num - $page) >0} 
                                                            <label  class="page-next" data-page="{$page +1} " for="load_more" onclick=""><span>Загрузить еще</span>
                                                            {/if}
                                                            {literal}
                                                                <script type="text/javascript">
                                                                    $(".page-next").click(function () {
                                                                        // alert('asdasdas');
                                                                        $.ajax({
                                                                            url: '/admin/posts/',
                                                                            method: 'get',
                                                                            data: {'page': $(this).data('page')},
                                                                            success: function (response) {
                                                                                insertData(response);
                                                                            },
                                                                            error: function () {
                                                                                alert('error');
                                                                            }
                                                                        });
                                                                    });

                                                                    function insertData(response) {

                                                                        $(".page-next").remove();
                                                                        $(".add").remove();
                                                                        var html = $(response).find('.row').html();
                                                                        $(".main-row").append(html);

                                                                    }
                                                                </script>
                                                            {/literal}
                                                    </div><!-- /row -->

                                                </div><!-- /container -->
                                                <br>
                                                <!-- FOOTER -->	

                                            </div>
                                        </div>
                                    </div>


                                    {literal}
                                        <script type="text/javascript">
                                            $(".remove").click(function () {
                                                var id = $(this).data('id');
                                                var url = '/admin/post/remove/';
                                                $.ajax({
                                                    url: url,
                                                    data: {'post_id': id},
                                                    success: function () {
                                                        console.log('success');
                                                        removeBlock(id)
                                                    },
                                                    error: function () {
                                                        alert('Error! Try latter');
                                                    }
                                                });
                                            });

                                            function removeBlock(id) {
                                                var block = $('#post-' + id).find('.text').children('a');
                                                if (block.data('block') == 1) {
                                                    block.data('block', '0');
                                                    block.attr('title', '');
                                                    block.css('color', 'greenyellow');
                                                } else {
                                                    block.data('block', '1');
                                                    block.attr('title', 'Заблокированно');
                                                    block.css('color', 'red');
                                                }
                                            }
                                        </script>
                                    {/literal}
                                    {include file='./footer.tpl'}
                            </body></html>