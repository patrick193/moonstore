<!doctype html>
<html><head>
        <meta charset="utf-8">
        <title>Moonstore - Users</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Pohorielov Vlad">


        <link href="/web/templates/Admin/css/bootstrap.min.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/tingle.css" rel="stylesheet">

        <link href="/web/templates/Admin/css/main.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/font-style.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/flexslider.css" rel="stylesheet">
        <script type="text/javascript" src="/web/templates/Admin/js/jquery-1.10.2.min.js"></script>

        <!-- Placed at the end of the document so the pages load faster -->
        <script type="text/javascript" src="/web/templates/Admin/js/bootstrap.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/lineandbars.js"></script>

        <script type="text/javascript" src="/web/templates/Admin/js/dash-charts.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/gauge.js"></script>

        <!-- NOTY JAVASCRIPT -->
        <script type="text/javascript" src="/web/templates/Admin/js/jquery.noty.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/top.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/topLeft.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/topRight.js"></script>
        <script type="text/javascript" src="/web/templates/Admin/js/topCenter.js"></script>

        <!-- You can add more layouts if you want -->
        <script type="text/javascript" src="/web/templates/Admin/js/default.js"></script>
        <!-- <script type="text/javascript" src="assets/js/dash-noty.js"></script> This is a Noty bubble when you init the theme-->
        <script type="text/javascript" src="/web/templates/Admin/js/highcharts.js"></script>
        <script src="/web/templates/Admin/js/jquery.flexslider.js" type="text/javascript"></script>

        <script type="text/javascript" src="/web/templates/Admin/js/admin.js"></script>

        <style type="text/css">
            body {
                padding-top: 60px;
            }
        </style>

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/component.css" />
        <script src="/web/templates/Admin/js/menu/js/modernizr.custom.25376.js"></script>
        <!-- Google Fonts call. Font Used Open Sans & Raleway -->
        <link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

        <script type="text/javascript">
            $(document).ready(function () {

                $("#btn-blog-next").click(function () {
                    $('#blogCarousel').carousel('next')
                });
                $("#btn-blog-prev").click(function () {
                    $('#blogCarousel').carousel('prev')
                });

                $("#btn-client-next").click(function () {
                    $('#clientCarousel').carousel('next')
                });
                $("#btn-client-prev").click(function () {
                    $('#clientCarousel').carousel('prev')
                });

            });

            $(window).load(function () {

                $('.flexslider').flexslider({
                    animation: "slide",
                    slideshow: true,
                    start: function (slider) {
                        $('body').removeClass('loading');
                    }
                });
            });

        </script>  

    </head>
    <body>

        <!-- NAVIGATION MENU -->

        {include file='./menu.tpl'}
        <div id="perspective" class="perspective effect-moveleft">
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <!-- Top Navigation -->

                    <div class="main clearfix">

                        <div class="container">

                            <!-- FIRST ROW OF BLOCKS -->     
                            <div class="row">
                                {if isset($users)}

                                    {foreach from = $users key= i item = user}

                                        <!-- USER PROFILE BLOCK -->
                                        <div class="col-sm-3 col-lg-3">
                                            <div class="dash-unit" {if $user->getIsLocked() == 1} title="Заблокирован" {/if}>
                                                <dtitle {if $user->getIsLocked() == 1} style="color: red"{elseif $user->getRoleId() ==1}style="color:greenyellow"{/if}>{if $user->getRoleId() == 1}Администратор{else}Техподдрежка{/if}</dtitle>
                                                <hr>
                                                <div class="thumbnail">
                                                    <img src="/web/templates/Admin/img/face80x80.jpg" class="img-circle">
                                                </div><!-- /thumbnail -->
                                                <h1>{$user->getUserFirstName()} {$user->getUserLastName()} </h1>
                                                <h3>{$user->getUserEmail()}</h3>
                                                <br>
                                                <div class="info-user">
                                                    <a href="/admin/profile/usermanagement/delete/user_id={$user->getUserId()}" title="{if $user->getIsLocked() == 0}Заблокировать{else}Разблокировать{/if}"><span aria-hidden="true" class="li_lock fs1"></span></a>
                                                    <a href="/admin/change/{$user->getUserId()}"><span aria-hidden="true" class="li_pen fs1"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    {/foreach}
                                {/if}

                                <div class="col-sm-3 col-lg-3">
                                    <a href="/admin/user/add"><div class="dash-unit-add">
                                        </div></a>
                                </div>

                            </div><!-- /row -->

                            <!-- FOURTH ROW OF BLOCKS -->     
                        </div> <!-- /container -->

                    </div>
                </div>
            </div>
            {include file='./footer.tpl'}




    </body></html>