<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!--[if IE]>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <![endif]-->
        <title>MoonStore скоро новый сайт</title>
        <!--REQUIRED STYLE SHEETS-->   
        <!-- BOOTSTRAP CORE STYLE CSS -->
        <link href="/web/templates/Main/assets/css/bootstrap.css" rel="stylesheet" />
        <!-- FONTAWESOME STYLE CSS -->
        <link href="/web/templates/Main/assets/css/font-awesome.min.css" rel="stylesheet" />
        <!-- VEGAS STYLE CSS -->
        <link href="/web/templates/Main/assets/scripts/vegas/jquery.vegas.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLE CSS -->
        <link href="/web/templates/Main/assets/css/style.css" rel="stylesheet" />
        <!-- GOOGLE FONT -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div class="loader"></div>

        <!-- MAIN CONTAINER -->
        <div class="container-fluid">
            <!-- NAVBAR SECTION -->
            <div class="navbar navbar-inverse navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li ><a data-toggle="modal" data-target="#mAbout" href="#mHome"> Кто мы </a></li>
                            <li><a data-toggle="modal" data-target="#mContact" href="#myModal">Контакты</a></li>
                        </ul>
                    </div>         
                </div>
            </div>
            <!-- END NAVBAR SECTION -->

            <!-- MAIN ROW SECTION -->
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-12">
                    <div id="movingicon">
                        <i class="fa fa-flask fa-spin icon-color"></i>
                        <br />   
                        <div id="headLine"></div>   
                    </div>              
                </div>
                <!--/. HEAD LINE DIV-->
                <div class="col-md-8 col-md-offset-2" >
                    <div id="counter"></div>
                    <div id="counter-default" class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="inner">
                                <div id="day-number" class="timer-number"></div>
                                <div class="timer-text">ДНЕЙ</div>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="inner">
                                <div id="hour-number" class="timer-number"></div>
                                <div class="timer-text">ЧАСОВ</div>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="inner">
                                <div id="minute-number" class="timer-number"></div>
                                <div class="timer-text">МИНУТ</div>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="inner">
                                <div id="second-number" class="timer-number"></div>
                                <div class="timer-text">СЕКУНД</div>



                            </div>
                        </div>
                    </div>

                </div>
                <!--/. COUNTER DIV-->
                <div class="col-md-6 col-md-offset-3">    

                </div>
                <!--/. SUBSCRIBE DIV-->
                <div class="col-md-6 col-md-offset-3">
                    <div class="social-links" >
                        <a href="https://www.facebook.com/moonstore.it/" >  <i class="fa fa-facebook fa-4x"></i> </a>
                        <a href="https://twitter.com/moon__store" >  <i class="fa fa-twitter fa-4x"></i> </a>

                    </div>
                </div>
                <!--/. SOCIAL DIV-->
            </div>
            <!--END MAIN ROW SECTION -->
        </div>
        <!-- MAIN CONTAINER END -->
        <div class="modal fade" id="mAbout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel1"> <i class="fa fa-plus"></i> КТО МЫ </h4>
                    </div>
                    <div class="modal-body">
                        <div class="mian-popup-body" >
                            <h1> <span class="fa fa-flask "></span> КТО МЫ ?</h1>
                            <div class="row">
                                <p>
                                    MoonStore —  это ежедневное онлайн-издание , 
                                    освещающее своих читателей грядущими тенденциями и самыми последними новостями из мира моды. 
                                    Журналисты команды Moon Store находятся в Украине, России и Европе. 
                                    Это позволяет нам доносить информацию быстрее, чем кто-либо.

                                </p>
                                <h3> <span class="fa fa-cog fa-spin "></span> САЙТ?</h3>
                                <p>
                                    На данный момент сайт в разработке. Но Вы можете посетить наш сайт работающий на старой версии
                                    <strong><a href="http://moonstore.it">moonstore.it</a></strong>

                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary back-btn" data-dismiss="modal">ЗАКРЫТЬ</button>
                    </div>
                </div>
            </div>
        </div>
        <!--/. ABOUT MODAL POPUP DIV-->

        <div class="modal fade" id="mContact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel"> <i class="fa fa-plus"></i> КОНТАКТЫ</h4>
                    </div>
                    <div class="modal-body">
                        <div class="mian-popup-body">
                            <h1> <span class="fa fa-book "></span> Телефон</h1>
                            <div class="row">
                                <h3> <span class="fa fa-cog fa-spin "></span> Как связаться</h3>
                                <p>
                                    <br />
                                    Телефон: +38(050) 820-38-24.

                                </p>
                                <br/><br/>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary back-btn" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <!--/. CONTACT MODAL POPUP DIV-->
        <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
        <!-- CORE JQUERY  -->
        <script src="/web/templates/Main/assets/plugins/jquery-1.10.2.js"></script>
        <!-- BOOTSTRAP CORE SCRIPT   -->
        <script src="/web/templates/Main/assets/plugins/bootstrap.js"></script>
        <!-- COUNTDOWN SCRIPTS -->
        <script src="/web/templates/Main/assets/plugins/jquery.countdown.js"></script>
        <script src="/web/templates/Main/assets/js/countdown.js"></script>
        <!-- VEGAS SLIDESHOW SCRIPTS -->
        <script src="/web/templates/Main/assets/plugins/vegas/jquery.vegas.min.js"></script>
        <!-- CUSTOM SCRIPTS -->
        <script src="/web/templates/Main/assets/js/custom-image.js"></script>

    </body>
</html>
