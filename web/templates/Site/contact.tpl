{assign var='user' value=$smarty.session.register.user_auth|base64_decode|unserialize}
<!DOCTYPE html>
<html class="no-js" lang="en" id="html-element">
    <head>

        <meta charset="utf-8" />

        <title>{post->post_seo_title}</title>
        <meta name="author" content="Pogorelov Vlad" />
        <meta name="description" content="{post->post_seo_desc}" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta property="fb:app_id" content="id"/>
        <link rel="canonical" href="" />
        <meta property="og:site_name" content="MoonStore"/><meta property="og:title" content="{post->post_seo_title}"/>
        <meta property="og:type" content="article"/>
        <link href="/web/templates/Site/assets/css/site.css" type="text/css" rel="stylesheet"/>
        <link href="/web/templates/Site/assets/css/article.css" type="text/css" rel="stylesheet"/>  
        <link href="/web/templates/Site/assets/css/home-with-carousel.css" type="text/css" rel="stylesheet" id="style1" />

        {if $user|is_object}
            <link media="screen" href="/web/templates/Site/leftblock/css/custom.css" type="text/css" rel="stylesheet" />
            <link rel="stylesheet" href="/web/templates/Site/leftblock/css/style.css" media="screen" type="text/css" />
        {/if}
        <script type="text/javascript" src="/web/templates/Site/assets/js/jquery.min.js"></script>

        <script type="text/javascript">
            // Target your .container, .wrapper, .post, etc.
            $(document).ready(function () {
                $("body").fadeIn(1500);
                $("a").click(function (event) {
                    if ($(this).attr('class' != 'show-gallery')) {
                        event.preventDefault();
                        linkLocation = this.href;
                        $("body").fadeOut(1500, redirectPage);
                    }
                });
                $("#menu-btn").click(function () {
                    if ($(".menu-open").length > 0) {
                        $('body').removeClass('menu-open');
                    } else {
                        $("body").addClass('menu-open');
                    }
                });
                function redirectPage() {
                    window.location = linkLocation;
                }
            })
        </script>


    </head>
    <body class="document-ready window-load" data-facebooknamespace="moonstore">
        {if $user|is_object}
            {include file='./leftblock/index.tpl'}
        {/if}
        
        {include file='./header.tpl'}
        <div id="main" role="main" class="container clearfix">

            <article class="article clearfix" id="main-article" itemscope="itemscope">



                <div class="headings">
                    <h2 class="title">{post->post_name}</h2>
                    <time class="date" datetime="{post->date_created}" itemprop="datePublished" style="color: #c9aa90">{post->date_created}</time>

                </div>
                <div class="article-container clearfix" itemprop="articleBody">

                    <div class="article-body"{if {post->type_id} != 4} style="margin: 0 210px 40px;"{/if}>
                        {post->post_text}
                    </div>
                </div>
            </article>
        </div>
        {include file='./footer.tpl'}   
    </body>
</html>
