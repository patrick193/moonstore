<footer id="footer">

            <div class="container">

                <div class="primary-nav">
                    <a href="/contact" class="footer-link">Связаться с нами</a>
                    <a href="/aboutus" class="footer-link">О нас</a>
                </div>

                <div class="social-menu">
                  <a href="https://www.facebook.com/moonstore.it/" class="social-link" target="blank">
                                        <span class="icon icon-facebook"></span></a>
                                    <a href="https://twitter.com/moon__store" class="social-link" target="blank">
                                        <span class="icon icon-twitter"></span></a>
                                    <a href="https://www.instagram.com/moonstore.it/" class="social-link" target="blank">
                                        <span class="icon icon-instagram"></span></a>
                <div class="copyright"><span class="coop">&copy;</span> 2014 - 2016 Moon Store.</div>
                </div>


            </div>

        </footer>