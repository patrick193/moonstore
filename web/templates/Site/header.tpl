
<header id="header" class="clearfix">

    <div class="container clearfix">

        <div itemscope >
            <a href="/" id="logo" >
                <img class="logo" src="/web/templates/Site/assets/images/logo.png" alt="MoonStore">
            </a>
        </div>

        <nav class="menus">

            <div class="menu">
                {foreach from=$categoties item='category1'}
                    {if isset($category[0]) and  $category[0]|is_object and $category[0]->posts_category_id eq 9 and $category1->posts_category_id eq 9}
                        <a href="/posts/category/{$category1->posts_category_id}" 
                           class="menu-link category-link transition"
                           style=" border:1px solid #000; background-color: #000; color: #fff"
                           >{$category1->category_name}</a>
                    {else}
                        <a href="/posts/category/{$category1->posts_category_id}" class="menu-link category-link transition">{$category1->category_name}</a>
                    {/if}
                {/foreach}
            </div>

            <div class="menu social-menu hidden-tablet">
                <a href="https://www.facebook.com/moonstore.it/" class="menu-link social-link" target="_blank"><span class="icon icon-facebook"></span></a>
                <a href="https://twitter.com/moon__store" class="menu-link social-link" target="_blank"><span class="icon icon-twitter"></span></a>
                <a href="https://www.instagram.com/moonstore.it/" class="menu-link social-link" target="_blank"><span class="icon icon-instagram"></span></a>
                <button id="search-btn" class="menu-link social-link hidden-phone"><span class="icon icon-search"></span></button>
            </div>

        </nav>

        <button id="menu-btn" class="visible-phone menu-link"><span class="icon icon-menu"></span></button>

    </div>

    <div id="header-search-container">
        <form class="header-search" action="/posts/search/" method="GET">
            <input type="search" id="search-input" name="q" data-search=" " required="" value="" />
            <button id="header-search-btn" type="submit">Поиск</button>
        </form>
    </div>

</header>