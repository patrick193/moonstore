{assign var='user' value=$smarty.session.register.user_auth|base64_decode|unserialize}
{assign var='postsss' value='1'}
<!DOCTYPE html>
<html class="no-js" lang="en" id="html-element">
    <head>
        <meta charset="utf-8" />
        <title>Moon Store - знак качества ваших знаний в мире моды.</title>
        <meta name="author" content="Moonstore" />
        <meta name="description" content="Команда MoonStore ежедневно освещает события в мире моды, искусства, архитектуры, музыки, видео и т.д. Мы хотим, что бы Вы знали больше." />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="canonical" href="" />

        <meta property="fb:297581050442076" content="id"/>
        <link href="/web/templates/Site/assets/css/site.css" type="text/css" rel="stylesheet" id="style1" />
        <link href="/web/templates/Site/assets/css/home-with-carousel.css" type="text/css" rel="stylesheet" id="style1" />
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <link rel="stylesheet" href="/web/templates/Site/assets/css/slider/superslides.css">
        {if  $user|is_object }
            <link media="screen" href="/web/templates/Site/leftblock/css/custom.css" type="text/css" rel="stylesheet" />
            <link rel="stylesheet" href="/web/templates/Site/leftblock/css/style.css" media="screen" type="text/css" />
        {/if}
        <script src='/web/templates/Site/assets/js/jquery.min.js'></script>
        <script src="/web/templates/Site/assets/js/slider/jquery.superslides.js" type="text/javascript" charset="utf-8"></script>
        {literal}
            <script>
                (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                ga('create', 'UA-51230191-1', 'moonstore.it');
                ga('send', 'pageview');



            </script>
        {/literal}
    </head>
    <body class="document-ready window-load" data-facebooknamespace="moonstore">
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=297581050442076";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>

        {if $user|is_object }
            {include file='./leftblock/index.tpl'}
        {/if}

        <div id="slides">
            <div class="slides-container">
                {foreach from=$sliders item='slider'}
                    <a href='{if !$slider->post_id|is_null && $slider->post_id !=0 }/posts/show/{$slider->post_id}{/if}'>
                        <img src="{$slider->image_path}"></a>
                    {/foreach}
            </div>

            <nav class="slides-navigation">
                <a href="#" class="next carousel-btn" id="carousel-next"><span class="icon-angle-right"></span></a>
                <a href="#" class="prev carousel-btn" id="carousel-prev"><span class="icon-angle-left"></span></a>
            </nav>

        </div>

        {include file='./header.tpl'}




        <div id="main" role="main" class="container clearfix">

            <section class="article-grid">
                <div class="articles clearfix">

                    {if $posts.query|is_array}
                        <!-- row 1 -->
                        <div class="row halfpage clearfix">
                            {assign var='i' value=0}
                            {assign var='keys' value=''}
                            {foreach from= $posts.query key='key' item='post'}
                                {if $i < 4}
                                    {if {$post->position_id} != 2}
                                        <article class="article">
                                            <a href="/posts/show/{$post->post_id}" class="article-link clearfix">
                                                <div class="img-wrapper">
                                                    <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                         data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                         data-responsive-widths="375,480"
                                                         src="{$post->post_img}" style="max-width: 100%; width:480px; " />
                                                    {if !{$post->vimeo}|is_null && {$post->display} eq 1}
                                                        <h6 class="category">
                                                            <span class="icon icon-instagram"></span>
                                                            {$post->post_category_id}
                                                        </h6>
                                                    {else}
                                                        <h6 class="category">
                                                            {$post->post_category_id}
                                                        </h6>
                                                    {/if}
                                                </div>
                                                <div class="text">
                                                    <h4 class="title">{$post->post_name}</h4>
                                                    <h5 class="description">{$post->short_desc}</h5>
                                                </div>
                                            </a>
                                        </article>
                                        {assign var='i' value=$i+1}
                                        {assign var='keys' value=$keys|cat: ","|cat:$key}
                                    {/if}
                                {/if}
                            {/foreach}
                            {$posts.query = $posts.query|unset:$keys}                            

                        </div>
                        <div class="subscribe-widget">
                            <div class="fb-page" data-href="https://www.facebook.com/moonstore.it"
                                 data-tabs="timeline" 
                                 data-small-header="false" 
                                 data-adapt-container-width="true" 
                                 data-hide-cover="false" data-show-facepile="true">
                                <div class="fb-xfbml-parse-ignore">
                                    <blockquote cite="https://www.facebook.com/moonstore.it">
                                        <a href="https://www.facebook.com/moonstore.it">Moonstore</a></blockquote></div></div>
                            <br><br><br><br><br>
                            <h5 class="description">Подписаться на обновления</h5>

                            <form method="POST" class="subscribe-form" action="">
                                <input type="text" name="email" class="email-input" placeholder="you@email.com" required />
                                <a href="#"  class="subscribe-btn"><span class="icon icon-angle-right"></span></a>
                            </form>
                            <div class="success-subscribe" style="display: none">
                                <p>Вы успешно подписались на рассылку новостей.</p>
                            </div>
                            <div class="social-links">
                                <a href="https://www.facebook.com/moonstore.it/" class="social-link" target="_blank">
                                    <span class="icon icon-facebook"></span></a>
                                <a href="https://twitter.com/moon__store" class="social-link" target="_blank">
                                    <span class="icon icon-twitter"></span></a>
                                <a href="https://www.instagram.com/moonstore.it/" class="social-link" target="_blank">
                                    <span class="icon icon-instagram"></span></a>
                            </div>
                        </div>
                        <!-- /row 1 -->
                        <!-- row 2 -->
                        <div class="row featured clearfix">
                            {assign var='j' value=0}
                            {assign var='keys2' value=''}
                            {foreach from= $posts.query key='key2' item='post'}
                                {if $j < 4}
                                    {if {$post->position_id} == 2}
                                        <article class="article">
                                            <a href="/posts/show/{$post->post_id}" class="article-link clearfix">
                                                <div class="img-wrapper">
                                                    <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                         data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                         data-responsive-widths="375,480"
                                                         src="{$post->post_img}" style="max-width: 100%; width:480px; " />
                                                    {if !{$post->vimeo}|is_null && {$post->display} eq 1}
                                                        <h6 class="category">
                                                            <span class="fa fa-video-camera"></span>
                                                            {$post->post_category_id}
                                                        </h6>
                                                    {else}
                                                        <h6 class="category">
                                                            {$post->post_category_id}
                                                        </h6>
                                                    {/if}
                                                </div>
                                                <div class="text">
                                                    <h4 class="title">{$post->post_name}</h4>
                                                    <h5 class="description">
                                                        {$post->short_desc}    
                                                    </h5>
                                                </div>
                                            </a>
                                        </article>
                                        {assign var='keys2' value=$keys2|cat: ","|cat:$key2}
                                        {assign var='j' value = $j+1}
                                    {/if}
                                {/if}
                            {/foreach}
                            {$posts.query = $posts.query|unset:$keys2} 

                        </div>


                        <!-- /row 2 -->
                        <!-- row 3 -->
                        <div class="row multi-line clearfix">
                            {*                        {assign var='ij' value=2}*}
                            {foreach from= $posts.query key='key3' item='post'}
                                {*                            {if {$post->position_id} == 2}*}

                                <article class="article">
                                    <a href="/posts/show/{$post->post_id}" class="article-link clearfix">
                                        <div class="img-wrapper">
                                            <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                 data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                 data-responsive-widths="375,480"
                                                 src="{$post->post_img}" style="max-width: 100%; width:480px;" />
                                            {if !{$post->vimeo}|is_null && {$post->display} eq 1}

                                                <h6 class="category">
                                                    <span class="fa fa-video-camera"></span>
                                                    {$post->post_category_id}
                                                </h6>
                                            {else}
                                                <h6 class="category">
                                                    {$post->post_category_id}
                                                </h6>
                                            {/if}
                                        </div>
                                        <div class="text">
                                            <h4 class="title">{$post->post_name}</h4>
                                            <h5 class="description">
                                                {$post->short_desc}    
                                            </h5>
                                        </div>
                                    </a>
                                </article>
                                {*                            {$posts.query = $posts.query|unset:$key3} *}


                            {/foreach}
                            <!-- /row 3 -->
                            <!-- row 6 -->

                            <!-- /row 6 -->

                        </div>
                        {if isset($tagsfinder)}
                        {else}
                            <input type="checkbox" id="load_more" role="button">
                            {if ($posts.page_num - $page) >0} 
                                <label  class="page-next" data-page="{$page +1} " for="load_more" ><span>Загрузить еще</span></label>
                            {/if}
                            <script type="text/javascript" class="script-page">
                                $(".page-next").click(function () {
                                {if $category[0]|is_object}
                                    $.ajax({
                                        url: '/posts/page/',
                                        method: 'get',
                                        data: {
                                            'page': $(this).data('page'),
                                            'category': {$category[0]->posts_category_id}
                                        },
                                        success: function (response) {
                                            insertData(response);
                                        },
                                        error: function () {
                                            alert('error');
                                        }
                                    });
                                {else}
                                    $.ajax({
                                        url: '/posts/page/',
                                        method: 'get',
                                        data: {
                                            'page': $(this).data('page'),
                                        },
                                        success: function (response) {
                                            insertData(response);
                                        },
                                        error: function () {
                                            alert('error');
                                        }
                                    });
                                {/if}
                                    //var y = window.pageYOffset;
                                    //var x = window.pageXOffset;
                                    //window.scroll(x, y);
                                });
                                function insertData(response) {
                                    $(".page-next").remove();
                                    //console.log($(response).find('.article-grid').html());
                                    var html = $(response).find('.articles').html();
                                    $(html).insertAfter("#load_more");
                                    $("#load_more").first().remove();
                                    $(".script-page").first().remove();

                                    console.log('done');
                                {if $category[0]|is_object and {$category[0]->invert} != 0}
                                    $("body").css({
                                        'background-color': '#000',
                                        'color': '#fff'
                                    });
                                    $("#main").css({
                                        'background-color': '#000',
                                        'color': '#fff'
                                    });
                                    $(".article").css({
                                        'background-color': '#000',
                                        'color': '#fff'
                                    });
                                    $(".text").css('color', '#fff');
                                    $("#footer .container, #footer").css({
                                        'background-color': '#000',
                                        'color': '#fff'
                                    });
                                    $("#footer a").css('color', '#fff');
                                    $(".subscribe-widget").css({
                                        'background-color': '#000',
                                        'color': '#fff'
                                    });
                                {/if}
                                    //$('head').append('<link rel="stylesheet" href="/web/templates/Site/assets/css/site.css" type="text/css" />');
                                }
                            </script>
                        {/if}

                </section>

            </div>
        {else}
            <div>
                Здесь еще нет статей
            </div>
        {/if}



        <script src="/web/templates/Site/assets/js/home-with-carousel.js" async="" type="text/javascript"></script>


        {include file='./footer.tpl'}

        {literal}
            <!-- Yandex.Metrika counter -->
            <script type="text/javascript">
                (function (d, w, c) {
                    (w[c] = w[c] || []).push(function () {
                        try {
                            w.yaCounter24852869 = new Ya.Metrika({id: 24852869,
                                webvisor: true,
                                clickmap: true,
                                trackLinks: true,
                                accurateTrackBounce: true});
                        } catch (e) {
                        }
                    });

                    var n = d.getElementsByTagName("script")[0],
                            s = d.createElement("script"),
                            f = function () {
                                n.parentNode.insertBefore(s, n);
                            };
                    s.type = "text/javascript";
                    s.async = true;
                    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                    if (w.opera == "[object Opera]") {
                        d.addEventListener("DOMContentLoaded", f, false);
                    } else {
                        f();
                    }
                })(document, window, "yandex_metrika_callbacks");
            </script>
            <noscript><div><img src="//mc.yandex.ru/watch/24852869" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
            <!-- /Yandex.Metrika counter -->


            <div id="error-notification-id" class="enp-35735"></div>
            <div id="error-notification-settings" style="display: none;"><p>Спасибо большое за вашу помощь. Оставайтесь с нами!</p></div>
            <div style="display:none">
            </div>

            <!-- Yandex.Metrika counter -->
            <script type="text/javascript">
                (function (d, w, c) {
                    (w[c] = w[c] || []).push(function () {
                        try {
                            w.yaCounter24852869 = new Ya.Metrika({id: 24852869,
                                webvisor: true,
                                clickmap: true,
                                trackLinks: true,
                                accurateTrackBounce: true});
                        } catch (e) {
                        }
                    });

                    var n = d.getElementsByTagName("script")[0],
                            s = d.createElement("script"),
                            f = function () {
                                n.parentNode.insertBefore(s, n);
                            };
                    s.type = "text/javascript";
                    s.async = true;
                    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                    if (w.opera == "[object Opera]") {
                        d.addEventListener("DOMContentLoaded", f, false);
                    } else {
                        f();
                    }
                })(document, window, "yandex_metrika_callbacks");
            </script>
            <noscript><div><img src="//mc.yandex.ru/watch/24852869" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
            <!-- /Yandex.Metrika counter -->
            <script type='text/javascript'>
                /* <![CDATA[ */
                var enp = {"ajaxurl": "http:\/\/moonstore.it\/wp-admin\/admin-ajax.php", "barBackground": "#cc3333", "barPosition": "bottom", "barTextColor": "#ffffff", "baseurl": "http:\/\/moonstore.it", "confirmation": "", "cbTitle": "Are you sure?", "cbError": "Error", "cbOK": "OK", "cbCancel": "Cancel"};
                /* ]]> */
            </script> 
        {/literal}
        <script src="/web/templates/Site/assets/js/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript">
                var safari = $.browser.safari;

                $(document).ready(function () {
                    $("body").fadeIn(1500);
                    $("#search-btn").mouseover(function () {
                        if ($('body').hasClass('search-open')) {
                            $('body').removeClass('search-open');
                        } else {
                            $('body').addClass('search-open');
                        }
                    });
                    $("#header-search-container").mouseleave(function () {
                        $('body').removeClass('search-open');
                    });
                    if ($(window).width() <= 767) {
                        $(".page-next").css('width', '200px');
                        $(".page-next").css('height', '50px');
                        $(".page-next").css('margin-top', '-30px');
                        $(".page-next").css('margin-left', '10px');
                        $(".page-next").children('span').css('margin-top', '-20px');
                    }

                    $("a").click(function (event) {
                        if ($(this).parent('nav').attr('class') != 'slides-navigation' && $(this).attr('class') != 'subscribe-btn' && !safari) {
                            event.preventDefault();
                            linkLocation = this.href;
                            $("body").fadeOut(1500, redirectPage);
                        }
                    });
                    function redirectPage() {
                        window.location = linkLocation;
                    }

                });</script>

        {if $category[0]|is_object and {$category[0]->invert} != 0}
            <script type="text/javascript">
                $("body").css({
                    'background-color': '#000',
                    'color': '#fff'
                });
                $("#main").css({
                    'background-color': '#000',
                    'color': '#fff'
                });
                $(".text").css('color', '#fff');
                $("#footer .container, #footer").css({
                    'background-color': '#000',
                    'color': '#fff'
                });
                $(".page-next").css('box-shadow', '1px 1px rgba(255, 255, 255, 254.1) inset, -1px -1px rgba(255, 255, 255, 254.1) inset');
                $("#footer a").css('color', '#fff');
                $(".subscribe-widget").css({
                    'background-color': '#000',
                    'color': '#fff'
                });</script>
            {/if}
        <script>
            $(function () {
                $('#slides').superslides({
                    hashchange: true,
                    play: 4000
                });
                $(".slides-pagination").insertBefore("#main");
                console.log('moved');
                $('#slides').on('mouseenter', function () {
                    $(this).superslides('stop');
                    console.log('Stopped')
                });
                $('#slides').on('mouseleave', function () {
                    $(this).superslides('start');
                    console.log('Started')
                });{literal}
                $(".subscribe-btn").click(function () {
                    var email = $(".email-input").val();
                    $.ajax({
                        url: '/subscribe/add/',
                        method: 'post',
                        data: {'email': email},
                        dataType: 'json',
                        success: function () {
                            console.log('Success');
                            changeSubscribe();
                        },
                        error: function () {
                            alert('Error')
                        }
                    });
                });
                function changeSubscribe() {
                    $(".subscribe-form").fadeOut(300);
                    $(".success-subscribe").fadeIn(300);
                    var subscribed = localStorage.getItem('subscribe');
                    if (subscribed != '1') {
                        localStorage.setItem('subscribe', '1');
                    }
                }

            });
            {/literal}
            </script>

        </body>
    </html>
