
<div id='snav' class='en'>

    <ul>

        <li>
            <a href='/admin'>
                <i class="fa fa-home"></i>
                <span>Админка</span>
            </a>
        </li>
        {if $postsss eq '1'}
        {else}
            <li>
                <a href='/admin/post/edit/{$postOne->post_id}'>
                    <i class="fa fa-camera"></i>
                    <span>Редактировать статью</span>
                </a>
            </li>
        {/if}
        <li>
            <a href='/admin/post/add'>
                <i class="fa fa-camera"></i>
                <span>Создать статью</span>
            </a>
        </li>

        <li>
            <a href='/admin/posts'>
                <i class="fa fa-circle-o-notch"></i>
                <span>Список статей</span>
            </a>
        </li> 
        {if $postsss eq 1}
        {else}
            <li>
                <a href='/admin/post/remove/{$postOne->post_id}'>
                    <i class="fa fa-trash-o"></i>
                    <span>Удалить</span>
                </a>
            </li>
        {/if}
        <li>
            <a href='/moonstoreadmin/logout'>
                <i class="fa fa-key"></i>
                <span>Выход</span>
            </a>
        </li>

    </ul>

</div>

<script src="/web/templates/Site/leftblock/js/index.js"></script>
