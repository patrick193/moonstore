<?php /* Smarty version 3.1.27, created on 2016-02-02 20:28:29
         compiled from "/var/www/moonstore/web/templates/Site/leftblock/index.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:74876191856b1116d5aab71_53523725%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0b28fd4a90f22e2a2e00728d96744c6ffd3f90bc' => 
    array (
      0 => '/var/www/moonstore/web/templates/Site/leftblock/index.tpl',
      1 => 1452428725,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '74876191856b1116d5aab71_53523725',
  'variables' => 
  array (
    'postsss' => 0,
    'postOne' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56b1116d5e5ac9_20206066',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56b1116d5e5ac9_20206066')) {
function content_56b1116d5e5ac9_20206066 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '74876191856b1116d5aab71_53523725';
?>

<div id='snav' class='en'>

    <ul>

        <li>
            <a href='/admin'>
                <i class="fa fa-home"></i>
                <span>Админка</span>
            </a>
        </li>
        <?php if ($_smarty_tpl->tpl_vars['postsss']->value == '1') {?>
        <?php } else { ?>
            <li>
                <a href='/admin/post/edit/<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_id;?>
'>
                    <i class="fa fa-camera"></i>
                    <span>Редактировать статью</span>
                </a>
            </li>
        <?php }?>
        <li>
            <a href='/admin/post/add'>
                <i class="fa fa-camera"></i>
                <span>Создать статью</span>
            </a>
        </li>

        <li>
            <a href='/admin/posts'>
                <i class="fa fa-circle-o-notch"></i>
                <span>Список статей</span>
            </a>
        </li> 
        <?php if ($_smarty_tpl->tpl_vars['postsss']->value == 1) {?>
        <?php } else { ?>
            <li>
                <a href='/admin/post/remove/<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_id;?>
'>
                    <i class="fa fa-trash-o"></i>
                    <span>Удалить</span>
                </a>
            </li>
        <?php }?>
        <li>
            <a href='/moonstoreadmin/logout'>
                <i class="fa fa-key"></i>
                <span>Выход</span>
            </a>
        </li>

    </ul>

</div>

<?php echo '<script'; ?>
 src="/web/templates/Site/leftblock/js/index.js"><?php echo '</script'; ?>
>
<?php }
}
?>