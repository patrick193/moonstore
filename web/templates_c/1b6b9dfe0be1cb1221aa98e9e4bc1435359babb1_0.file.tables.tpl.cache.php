<?php /* Smarty version 3.1.27, created on 2016-01-08 21:31:33
         compiled from "/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Admin/tables.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:56140298656902ab5772708_90606276%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1b6b9dfe0be1cb1221aa98e9e4bc1435359babb1' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Admin/tables.tpl',
      1 => 1452217778,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '56140298656902ab5772708_90606276',
  'variables' => 
  array (
    'posts' => 0,
    'post' => 0,
    'page' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56902ab585a041_87831236',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56902ab585a041_87831236')) {
function content_56902ab585a041_87831236 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '56140298656902ab5772708_90606276';
?>
<!doctype html>
<html><head>
        <meta charset="utf-8">
        <title>Moonstore - Posts</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link href="/web/templates/Admin/css/tingle.css" rel="stylesheet">
        <link rel="stylesheet" href="/web/templates/Admin/css/normalize.css">
        <link rel="stylesheet" href="/web/templates/Admin/css/posts-style.css">

        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/prefixfree.min.js"><?php echo '</script'; ?>
>

        <link href="/web/templates/Admin/css/bootstrap.min.css" rel="stylesheet">

        <link href="/web/templates/Admin/css/main.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/font-style.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/flexslider.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/component.css" />
        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/normalize.css" />
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/jquery-1.10.2.min.js"><?php echo '</script'; ?>
>

        <!-- Placed at the end of the document so the pages load faster -->
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/bootstrap.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/lineandbars.js"><?php echo '</script'; ?>
>


        <!-- NOTY JAVASCRIPT -->
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/jquery.noty.js"><?php echo '</script'; ?>
>
        <!-- You can add more layouts if you want -->
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/default.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/jquery.flexslider.js" type="text/javascript"><?php echo '</script'; ?>
>

        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/admin.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/menu/js/modernizr.custom.25376.js"><?php echo '</script'; ?>
>
        <style type="text/css">
            body {
                padding-top: 60px;
            }
        </style>

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <?php echo '<script'; ?>
 src="http://html5shim.googlecode.com/svn/trunk/html5.js"><?php echo '</script'; ?>
>
        <![endif]-->

        <!-- Google Fonts call. Font Used Open Sans -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

        <!-- DataTables Initialization -->
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/jquery.dataTables.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" charset="utf-8">
            $(document).ready(function () {
                $('#dt1').dataTable();
            });
        <?php echo '</script'; ?>
>


    </head>
    <body>

        <!-- NAVIGATION MENU -->

        <?php echo $_smarty_tpl->getSubTemplate ('./menu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        <div id="perspective" class="perspective effect-moveleft">
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <!-- Top Navigation -->

                    <div class="main clearfix">
                        <!-- CONTENT -->       
                        <div class="container">

                            <div class="row main-row">
                                <?php if (isset($_smarty_tpl->tpl_vars['posts']->value)) {?>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['posts']->value['query'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['post'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['post']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
$foreach_post_Sav = $_smarty_tpl->tpl_vars['post'];
?>
                                        <div class="col-sm-3 col-lg-3" id="post-<?php echo $_smarty_tpl->tpl_vars['post']->value->post_id;?>
" >

                                            <!-- LAST USER BLOCK -->     
                                            <div class="half-unit"  >
                                                <div class="text">
                                                    <a href="/admin/post/edit/<?php echo $_smarty_tpl->tpl_vars['post']->value->post_id;?>
" <?php ob_start();
echo $_smarty_tpl->tpl_vars['post']->value->isLocked;
$_tmp1=ob_get_clean();
if ($_tmp1 != 0) {?> style="color: red" data-block="1" title="Заблокированно" 
                                                       <?php } else { ?> style="color: greenyellow" data-block="0"<?php }?>>#<?php echo $_smarty_tpl->tpl_vars['post']->value->post_id;?>
  <?php echo $_smarty_tpl->tpl_vars['post']->value->post_name;?>
</a>
                                                           <a href="#" class="remove" data-id='<?php echo $_smarty_tpl->tpl_vars['post']->value->post_id;?>
' style="float: right; margin-right: 5px">X</a>
                                                       </div>
                                                       <hr>
                                                       <div class="cont2">
                                                           <div class="text">
                                                           </div>
                                                           <br>
                                                           <br>
                                                           <div class="left_img">
                                                               <a href="/admin/post/edit/<?php echo $_smarty_tpl->tpl_vars['post']->value->post_id;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['post']->value->post_img_th;?>
"></a>
                                                           </div>
                                                           <div class="after_img">
                                                               <p><bold>Дата редактирования: <?php echo $_smarty_tpl->tpl_vars['post']->value->date_redaction;?>
</bold></p>
                                                               <p><bold><?php echo $_smarty_tpl->tpl_vars['post']->value->post_category_id;?>
</bold></p>
                                                           </div>
                                                       </div>
                                                    </div>

                                                </div>
                                                <?php
$_smarty_tpl->tpl_vars['post'] = $foreach_post_Sav;
}
?>

                                                    <?php }?>

                                                        <div class="col-sm-3 col-lg-3 add">
                                                            <a href="/admin/post/add"><div class="half-unit-add-list">
                                                                </div></a>
                                                        </div>
                                                        <input type="checkbox" id="load_more" role="button">
                                                        <?php if (($_smarty_tpl->tpl_vars['posts']->value['page_num']-$_smarty_tpl->tpl_vars['page']->value) > 0) {?> 
                                                            <label  class="page-next" data-page="<?php echo $_smarty_tpl->tpl_vars['page']->value+1;?>
 " for="load_more" onclick=""><span>Загрузить еще</span>
                                                            <?php }?>
                                                            
                                                                <?php echo '<script'; ?>
 type="text/javascript">
                                                                    $(".page-next").click(function () {
                                                                        // alert('asdasdas');
                                                                        $.ajax({
                                                                            url: '/admin/posts/',
                                                                            method: 'get',
                                                                            data: {'page': $(this).data('page')},
                                                                            success: function (response) {
                                                                                insertData(response);
                                                                            },
                                                                            error: function () {
                                                                                alert('error');
                                                                            }
                                                                        });
                                                                    });

                                                                    function insertData(response) {

                                                                        $(".page-next").remove();
                                                                        $(".add").remove();
                                                                        var html = $(response).find('.row').html();
                                                                        $(".main-row").append(html);

                                                                    }
                                                                <?php echo '</script'; ?>
>
                                                            
                                                    </div><!-- /row -->

                                                </div><!-- /container -->
                                                <br>
                                                <!-- FOOTER -->	

                                            </div>
                                        </div>
                                    </div>


                                    
                                        <?php echo '<script'; ?>
 type="text/javascript">
                                            $(".remove").click(function () {
                                                var id = $(this).data('id');
                                                var url = '/admin/post/remove/';
                                                $.ajax({
                                                    url: url,
                                                    data: {'post_id': id},
                                                    success: function () {
                                                        console.log('success');
                                                        removeBlock(id)
                                                    },
                                                    error: function () {
                                                        alert('Error! Try latter');
                                                    }
                                                });
                                            });

                                            function removeBlock(id) {
                                                var block = $('#post-' + id).find('.text').children('a');
                                                if (block.data('block') == 1) {
                                                    block.data('block', '0');
                                                    block.attr('title', '');
                                                    block.css('color', 'greenyellow');
                                                } else {
                                                    block.data('block', '1');
                                                    block.attr('title', 'Заблокированно');
                                                    block.css('color', 'red');
                                                }
                                            }
                                        <?php echo '</script'; ?>
>
                                    
                                    <?php echo $_smarty_tpl->getSubTemplate ('./footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                            </body></html><?php }
}
?>