<?php /* Smarty version 3.1.27, created on 2016-01-18 16:27:57
         compiled from "/var/www/andreevd/data/www/moonstore.it/web/templates/Admin/profile.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:617015320569d128d54ddd4_92551970%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2eab621e34e57e151f5c67f574bfbde27be64788' => 
    array (
      0 => '/var/www/andreevd/data/www/moonstore.it/web/templates/Admin/profile.tpl',
      1 => 1452629071,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '617015320569d128d54ddd4_92551970',
  'variables' => 
  array (
    'user_auth' => 0,
    'add' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_569d128d689ae9_20498033',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_569d128d689ae9_20498033')) {
function content_569d128d689ae9_20498033 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '617015320569d128d54ddd4_92551970';
?>

<!doctype html>
<html><head>
        <meta charset="utf-8">
        <title>Moonstore - profile</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="/web/templates/Admin/css/bootstrap.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/main.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/font-style.css" rel="stylesheet">
        <link rel="stylesheet" href="/web/templates/Admin/css/register.css">
        <link href="/web/templates/Admin/css/tingle.css" rel="stylesheet">

        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/jquery-1.10.2.min.js"><?php echo '</script'; ?>
>    
        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/bootstrap.js"><?php echo '</script'; ?>
>


        <style type="text/css">
            body {
                padding-top: 60px;
            }
        </style>
        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/component.css" />
        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/menu/js/modernizr.custom.25376.js"><?php echo '</script'; ?>
>
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <?php echo '<script'; ?>
 src="http://html5shim.googlecode.com/svn/trunk/html5.js"><?php echo '</script'; ?>
>
        <![endif]-->

        <!-- Google Fonts call. Font Used Open Sans & Raleway -->
        <link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">


    </head>
    <body>

        <!-- NAVIGATION MENU -->

        <?php echo $_smarty_tpl->getSubTemplate ('./menu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        <div id="perspective" class="perspective effect-moveleft">
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <!-- Top Navigation -->

                    <div class="main clearfix">
                        <div class="container">
                            <div class="row">

                                <div class="col-lg-6">

                                    <div class="register-info-wraper">
                                        <div id="register-info">
                                            <div class="cont2">
                                                <div class="thumbnail">
                                                    <img src="/web/templates/Admin/img/face80x80.jpg" alt="<?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getUserFirstName(array(),$_smarty_tpl);?>
" class="img-circle">
                                                </div><!-- /thumbnail -->
                                                <h2 <?php ob_start();
echo $_smarty_tpl->smarty->registered_objects['user'][0]->getIsLocked(array(),$_smarty_tpl);
$_tmp1=ob_get_clean();
if ($_tmp1 != 0) {?>style="color:red" title="Заблокироавн"<?php }?>><?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getUserFirstName(array(),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getUserLastName(array(),$_smarty_tpl);?>
</h2>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <div class="cont3">
                                                        <p><ok>Имя:</ok> <?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getUserFirstName(array(),$_smarty_tpl);?>
</p>
                                                        <p><ok>Mail:</ok> <?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getUserEmail(array(),$_smarty_tpl);?>
</p>
                                                        <p><ok>Телефон:</ok> <?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getCellPhone(array(),$_smarty_tpl);?>
</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="cont3">
                                                        <p><ok>Роль:</ok> <?php echo $_smarty_tpl->smarty->registered_objects['role'][0]->getRoleName(array(),$_smarty_tpl);?>
</p>
                                                        <p><ok>Последний визит:</ok> <?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getLastVisit(array(),$_smarty_tpl);?>
</p>

                                                    </div>
                                                </div>
                                            </div><!-- /inner row -->
                                            <hr>
                                            <div class="cont2">
                                                <h2>Доступные действия</h2>
                                            </div>
                                            <br>
                                            <div class="info-user2">
                                                <a href="/admin/profile/usermanagement/" title="Все пользователи"><span aria-hidden="true" class="li_user fs1"></span></a> 
                                                    <?php $_smarty_tpl->tpl_vars['user_auth'] = new Smarty_Variable(unserialize(base64_decode($_SESSION['register']['user_auth'])), null, 0);?>
                                                    <?php if ($_smarty_tpl->tpl_vars['user_auth']->value->getRoleId() == 1) {?>
                                                    <a href="/admin/profile/usermanagement/admin/user_id=<?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getUserId(array(),$_smarty_tpl);?>
"><span aria-hidden="true" class="li_key fs1" <?php ob_start();
echo $_smarty_tpl->smarty->registered_objects['user'][0]->getRoleId(array(),$_smarty_tpl);
$_tmp2=ob_get_clean();
if ($_tmp2 == 1) {?>title="Техподдержка"<?php } else { ?>title="Администратор"<?php }?>></span></a>
                                                    <a href="/admin/profile/usermanagement/delete/user_id=<?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getUserId(array(),$_smarty_tpl);?>
" title="<?php ob_start();
echo $_smarty_tpl->smarty->registered_objects['user'][0]->getIsLocked(array(),$_smarty_tpl);
$_tmp3=ob_get_clean();
if ($_tmp3 == 0) {?>Заблокировать<?php } else { ?>Разблокировать<?php }?>"><span aria-hidden="true" class="li_lock fs1"></span></a>
                                                    <?php }?>
                                            </div>


                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-6 col-lg-6">
                                    <div id="register-wraper">
                                        <?php if (isset($_smarty_tpl->tpl_vars['add']->value)) {?>
                                            <form id="register-form" class="form" method="post" action="/admin/profile/usermanagement/add/">
                                                <legend>Регистрация пользователя</legend>
                                                <input name="module" class="input-huge" value="usermanagement" type="hidden">
                                                <input name="action" class="input-huge" value="add" type="hidden">

                                                <div class="body">
                                                    <!-- first name -->
                                                    <label for="name">Имя</label>
                                                    <input name="name" class="input-huge" placeholder="First Name" type="text">
                                                    <!-- last name -->
                                                    <label for="lastname">Фамилия</label>
                                                    <input name="lastname" class="input-huge" placeholder="Last Name" type="text">
                                                    <label>Телефон</label>
                                                    <input name="cell_phone" class="input-huge" placeholder="Cell Phone" type="text">
                                                    <!-- email -->
                                                    <label>E-mail</label>
                                                    <input name="email" class="input-huge" placeholder="Email" type="text">
                                                    <!-- password -->
                                                    <label>Пароль</label>
                                                    <input name="password" class="input-huge" placeholder="password" type="text">

                                                </div>

                                                <div class="footer">
                                                    <label class="checkbox inline">
                                                        <div class="switch">
                                                            <input type="radio" class="switch-input" name="isLocked" value="true" id="on" checked="" >
                                                            <label for="on" class="switch-label switch-label-off">On</label>
                                                            <input type="radio" class="switch-input" name="isLocked" value="false" id="off" >
                                                            <label for="off" class="switch-label switch-label-on">Off</label>
                                                            <span class="switch-selection"></span>
                                                        </div> 
                                                    </label>
                                                    <button type="submit" class="btn btn-success">Зарегестрировать</button>
                                                </div>
                                            </form>
                                        <?php } else { ?>
                                            <form id="register-form" class="form" action="/admin/profile/usermanagement/edit/" method="post">
                                                <legend>Изменить данные</legend>
                                               
                                                <input name="user_id" class="input-huge" value="<?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getUserId(array(),$_smarty_tpl);?>
" type="hidden">

                                                <div class="body">
                                                    <!-- first name -->
                                                    <label for="name">Имя</label>
                                                    <input name="name" class="input-huge" placeholder="<?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getUserFirstName(array(),$_smarty_tpl);?>
" value="<?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getUserFirstName(array(),$_smarty_tpl);?>
" type="text">
                                                    <!-- last name -->
                                                    <label for="lastname">Фамилия</label>
                                                    <input name="lastname" class="input-huge" value="<?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getUserLastName(array(),$_smarty_tpl);?>
" placeholder="<?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getUserLastName(array(),$_smarty_tpl);?>
" type="text">
                                                    <label>Телефон</label>
                                                    <input name="cell_phone" class="input-huge" placeholder="<?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getCellPhone(array(),$_smarty_tpl);?>
" value="<?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getCellPhone(array(),$_smarty_tpl);?>
" type="text">
                                                    <!-- email -->
                                                    <label>E-mail</label>
                                                    <input name="email" class="input-huge" placeholder="<?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getUserEmail(array(),$_smarty_tpl);?>
" value="<?php echo $_smarty_tpl->smarty->registered_objects['user'][0]->getUserEmail(array(),$_smarty_tpl);?>
" type="text">
                                                    <!-- password -->
                                                    <label>Пароль</label>
                                                    <input name="password" class="input-huge" placeholder="password" type="text">

                                                </div>

                                                <div class="footer">
                                                    <label class="checkbox inline">
                                                        <div class="switch">
                                                            <input type="radio" class="switch-input" name="isLocked" value="true" id="on" <?php ob_start();
echo $_smarty_tpl->smarty->registered_objects['user'][0]->getIsLocked(array(),$_smarty_tpl);
$_tmp4=ob_get_clean();
if ($_tmp4 != 1) {?> checked="" <?php }?> >
                                                            <label for="on" class="switch-label switch-label-off">On</label>
                                                            <input type="radio" class="switch-input" name="isLocked" value="false" id="off" <?php ob_start();
echo $_smarty_tpl->smarty->registered_objects['user'][0]->getIsLocked(array(),$_smarty_tpl);
$_tmp5=ob_get_clean();
if ($_tmp5 == 1) {?> checked="" <?php }?> >
                                                            <label for="off" class="switch-label switch-label-on">Off</label>
                                                            <span class="switch-selection"></span>
                                                        </div> 
                                                    </label>
                                                    <button type="submit" class="btn btn-success">Обновить</button>
                                                </div>
                                            </form>
                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $_smarty_tpl->getSubTemplate ('./footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

    </body></html><?php }
}
?>