<?php /* Smarty version 3.1.27, created on 2016-01-05 22:20:06
         compiled from "/var/www/moonstore/web/templates/Admin/subscribe.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1728123411568c41965ee268_51109797%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '37962a6487b3cfda31f947afc357cf4b5e4bfa4a' => 
    array (
      0 => '/var/www/moonstore/web/templates/Admin/subscribe.tpl',
      1 => 1452032405,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1728123411568c41965ee268_51109797',
  'variables' => 
  array (
    'owner' => 0,
    'customers' => 0,
    'customer' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_568c4196630b02_88442144',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_568c4196630b02_88442144')) {
function content_568c4196630b02_88442144 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1728123411568c41965ee268_51109797';
?>
<!doctype html>
<html><head>
        <meta charset="utf-8">
        <title>Moonstore - Send emails</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link href="/web/templates/Admin/css/bootstrap.min.css" rel="stylesheet">

        <link href="/web/templates/Admin/css/main.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/flexslider.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/component.css" />
        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/normalize.css" />
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/jquery-1.10.2.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/jquery-ui.js"><?php echo '</script'; ?>
>
        <!-- Placed at the end of the document so the pages load faster -->
        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/bootstrap-multiselect.js" type="text/javascript"><?php echo '</script'; ?>
>

        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/default.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/menu/js/modernizr.custom.25376.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/tinymce/tinymce.min.js"><?php echo '</script'; ?>
>

        <?php echo '<script'; ?>
 type="text/javascript">
            tinymce.init({
                height: '750px',
                width: '100%',
                selector: "textarea",
                plugins: [
                    "advlist autolink lists link charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime table contextmenu paste imagetools jbimages",
                    "youtube",
                ],
                language: "ru",
                toolbar: "preview | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | jbimages | youtube",
                image_advtab: true,
            });
            $(document).ready(function () {
            });

        <?php echo '</script'; ?>
>
        <!--[if lt IE 9]>
          <?php echo '<script'; ?>
 src="http://html5shim.googlecode.com/svn/trunk/html5.js"><?php echo '</script'; ?>
>
        <![endif]-->
        <!-- Google Fonts call. Font Used Open Sans -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

        <!-- DataTables Initialization -->
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/jquery.dataTables.js"><?php echo '</script'; ?>
>
    </head>
    <body>
        <?php echo $_smarty_tpl->getSubTemplate ('./menu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        <div id="perspective" class="perspective effect-moveleft">
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <!-- Top Navigation -->

                    <div class="main clearfix">
                        <!-- CONTENT -->       
                        <div class="container">
                            <div class="row main-row">
                                <!-- TODO -->
                                <div class="email">

                                    <form method="post" action="/admin/subscribe/send/" enctype="multipart/form-data" style="margin-top: 100px">
                                        <label class="email_name"> От кого: 
                                            <input name="email_name" type="text" placeholder="Имя пользователя" value="<?php echo $_smarty_tpl->tpl_vars['owner']->value[0]->From;?>
">
                                        </label>
                                        <label class="email_subject"> Тема: 
                                            <input name="email_subject" type="text" placeholder="Email subject">
                                        </label>
                                        <br>
                                        <br>
                                        <label for="customers" class="customers"> Кому:<br>
                                            <select id="tokenize" name="customers[]" multiple class="tokenize-sample" >
                                                <?php
$_from = $_smarty_tpl->tpl_vars['customers']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['customer'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['customer']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['customer']->value) {
$_smarty_tpl->tpl_vars['customer']->_loop = true;
$foreach_customer_Sav = $_smarty_tpl->tpl_vars['customer'];
?>
                                                    <option value="<?php echo $_smarty_tpl->tpl_vars['customer']->value->subscriber_id;?>
" selected="">
                                                        <?php echo $_smarty_tpl->tpl_vars['customer']->value->email;?>

                                                    </option>

                                                <?php
$_smarty_tpl->tpl_vars['customer'] = $foreach_customer_Sav;
}
?>
                                            </select>
                                        </label>
                                        <textarea name="content" style="width:50%; height: 75%"></textarea>

                                        <br>
                                        <input type="submit" value="Отправить">

                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php echo $_smarty_tpl->getSubTemplate ('./footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        </div>
        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/js/multiselect/jquery.tokenize.css" />
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/multiselect/jquery.tokenize.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript">
            $('#tokenize').tokenize().toArray();
        <?php echo '</script'; ?>
>
    </body>
</html><?php }
}
?>