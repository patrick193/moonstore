<?php /* Smarty version 3.1.27, created on 2016-02-04 19:13:12
         compiled from "/var/www/moonstore/web/templates/Admin/createGallery.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:174599708056b3a2c84e0983_88899297%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4e3cd7a3ec7826ed268405f4650b54eb13ff3485' => 
    array (
      0 => '/var/www/moonstore/web/templates/Admin/createGallery.tpl',
      1 => 1454613019,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '174599708056b3a2c84e0983_88899297',
  'variables' => 
  array (
    'galleries' => 0,
    'key' => 0,
    'gallery' => 0,
    'g' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56b3a2c85dcc20_25998924',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56b3a2c85dcc20_25998924')) {
function content_56b3a2c85dcc20_25998924 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '174599708056b3a2c84e0983_88899297';
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Moonstore - Gallery</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="/web/templates/Admin/css/tingle.css" rel="stylesheet">
    <link rel="stylesheet" href="/web/templates/Admin/css/normalize.css">
    <link rel="stylesheet" href="/web/templates/Admin/css/posts-style.css">

    <?php echo '<script'; ?>
 src="/web/templates/Admin/js/prefixfree.min.js"><?php echo '</script'; ?>
>

    <link href="/web/templates/Admin/css/bootstrap.min.css" rel="stylesheet">

    <link href="/web/templates/Admin/css/main.css" rel="stylesheet">
    <link href="/web/templates/Admin/css/font-style.css" rel="stylesheet">
    <link href="/web/templates/Admin/css/flexslider.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/component.css"/>
    <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/normalize.css"/>
    
    <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/jquery-1.10.2.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/jquery-ui.js"><?php echo '</script'; ?>
>
    


    <!-- Placed at the end of the document so the pages load faster -->
    <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/bootstrap.js"><?php echo '</script'; ?>
>


    <!-- NOTY JAVASCRIPT -->
    <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/jquery.noty.js"><?php echo '</script'; ?>
>
    <!-- You can add more layouts if you want -->
    <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/default.js"><?php echo '</script'; ?>
>

    <?php echo '<script'; ?>
 src="/web/templates/Admin/js/menu/js/modernizr.custom.25376.js"><?php echo '</script'; ?>
>
    <style type="text/css">
        body {
            padding-top: 60px;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <?php echo '<script'; ?>
 src="http://html5shim.googlecode.com/svn/trunk/html5.js"><?php echo '</script'; ?>
>
    <![endif]-->

    <!-- Google Fonts call. Font Used Open Sans -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

    <!-- DataTables Initialization -->
    <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/jquery.dataTables.js"><?php echo '</script'; ?>
>
    
        <?php echo '<script'; ?>
 type="text/javascript">

            ;
            $(function () {
                var enable = false;
                $(".check-name").click(function () {
                    var data = $(".gallery-name").val();
                    $.ajax({
                        url: '/admin/gallery/check/',
                        method: 'get',
                        data: {'gallery_name': data},
                        dataType: 'json',
                        success: function (response) {
                            console.log(response);
                            console.log("success");
                            enable = true;
                            endis();
                        },
                        error: function (ex) {
                            console.log(ex.error());
                            console.log("error");
                            enable = false;
                            endis();
                        }
                    });
                });

                function endis() {
                    if (enable === true) {
                        $("#upload").find('input').removeProp('disabled');
                        $("#upload").fadeIn();
                        $(".save").fadeIn();

                    }
                }

                $(".save").click(function () {
                    var img = '';
                    var data = $(".gallery-name").val();
                    $("#upload").find('ul').find('li').find('p').find('i').remove()
                    ;
                    var a = $("#upload").find('ul').find('li').find('p');

                    $.each(a, function (i, v) {
                        img += $(v).text() + " ";
                    });

                    $.ajax({
                        url: '/admin/gallery/upload/img/',
                        data: {'save': data, 'img': img},
                        dataType: 'json',
                        method: 'post',
                        success: function (response) {
                            console.log(response);
                            console.log("success");
                            window.location.replace('/admin');
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    alert("Галлерея успешно создана! Вставте код в статью:\ngallery((" + data + "))");
                    window.location.href = '/admin/gallery';
                    return false;
                });

            });
        <?php echo '</script'; ?>
>
    
</head>
<body>
<div id="perspective" class="perspective effect-moveleft">
    <div class="container">
        <div class="wrapper"><!-- wrapper needed for scroll -->
            <!-- Top Navigation -->

            <div class="main clearfix">
                <!-- CONTENT -->
                <div class="container">

                    <div class="row main-row">
                        <label for="gallery-name" style="color: #fff"> Имя галлереи
                            <input type="text" class="gallery-name" placeholder="Имя галереи должно быть уникальным"
                                   style="width: 400px">
                            <a href="#" class="check-name">Проверить</a>
                        </label>

                        <form id="upload" method="post" action="/admin/gallery/upload/img/"
                              enctype="multipart/form-data">
                            <div id="drop">
                                Перетащи сюда

                                <a id="select">Выбрать</a>
                                <input disabled type="file" name="upl" multiple/>
                            </div>

                            <ul>
                                <!-- The file uploads will be shown here -->
                            </ul>

                        </form>
                        <h2><a class="save btn-success">Save</a></h2>
                    </div><br><br>
                    <div class="row main-row">
                        <?php
$_from = $_smarty_tpl->tpl_vars['galleries']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['gallery'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['gallery']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['gallery']->value) {
$_smarty_tpl->tpl_vars['gallery']->_loop = true;
$foreach_gallery_Sav = $_smarty_tpl->tpl_vars['gallery'];
?>
                            <div class="photopile-wrapper" style="border: medium dashed; margin: 10px; padding: 10px;">
                                <h2 style="color: #fff; float: right" class="gallery-name"><?php echo $_smarty_tpl->tpl_vars['key']->value;?>
</h2>

                                <p class="photopile">
                                    <?php
$_from = $_smarty_tpl->tpl_vars['gallery']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['g'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['g']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['g']->value) {
$_smarty_tpl->tpl_vars['g']->_loop = true;
$foreach_g_Sav = $_smarty_tpl->tpl_vars['g'];
?>
                                        
                                        <a href="#">
                                            <img src="<?php echo $_smarty_tpl->tpl_vars['g']->value['image_path'];?>
" alt="" style="max-width: 200px; height: auto"/>
                                        </a>
                                    <?php
$_smarty_tpl->tpl_vars['g'] = $foreach_g_Sav;
}
?>
                                </p>
                            </div>
                        <?php
$_smarty_tpl->tpl_vars['gallery'] = $foreach_gallery_Sav;
}
?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php echo '<script'; ?>
>

<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>

    $(document).ready(function () {
        $("#upload").fadeOut();
        $(".save").fadeOut();
    });
    $(".gallery-name").click(function () {
        if ($(this).text().indexOf('gallery=') < 0) {
            $(this).text('gallery=((' + $(this).text() + "))");
        }
    });

<?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->getSubTemplate ("./menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<?php echo '<script'; ?>
>
    $(".outer-nav").css('visibility', 'hidden');
<?php echo '</script'; ?>
>
</body>
</html><?php }
}
?>