<?php /* Smarty version 3.1.27, created on 2016-02-02 20:19:00
         compiled from "/var/www/moonstore/web/templates/Site/header.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:75437869956b10f349379d6_49335687%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '537466392b62d10abd4152432c9ddb4515a16c0f' => 
    array (
      0 => '/var/www/moonstore/web/templates/Site/header.tpl',
      1 => 1452428725,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '75437869956b10f349379d6_49335687',
  'variables' => 
  array (
    'categoties' => 0,
    'category' => 0,
    'category1' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56b10f34961525_39190495',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56b10f34961525_39190495')) {
function content_56b10f34961525_39190495 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '75437869956b10f349379d6_49335687';
?>

<header id="header" class="clearfix">

    <div class="container clearfix">

        <div itemscope >
            <a href="/" id="logo" >
                <img class="logo" src="/web/templates/Site/assets/images/logo.png" alt="MoonStore">
            </a>
        </div>

        <nav class="menus">

            <div class="menu">
                <?php
$_from = $_smarty_tpl->tpl_vars['categoties']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['category1'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['category1']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['category1']->value) {
$_smarty_tpl->tpl_vars['category1']->_loop = true;
$foreach_category1_Sav = $_smarty_tpl->tpl_vars['category1'];
?>
                    <?php if (isset($_smarty_tpl->tpl_vars['category']->value[0]) && is_object($_smarty_tpl->tpl_vars['category']->value[0]) && $_smarty_tpl->tpl_vars['category']->value[0]->posts_category_id == 9 && $_smarty_tpl->tpl_vars['category1']->value->posts_category_id == 9) {?>
                        <a href="/posts/category/<?php echo $_smarty_tpl->tpl_vars['category1']->value->posts_category_id;?>
" 
                           class="menu-link category-link transition"
                           style=" border:1px solid #000; background-color: #000; color: #fff"
                           ><?php echo $_smarty_tpl->tpl_vars['category1']->value->category_name;?>
</a>
                    <?php } else { ?>
                        <a href="/posts/category/<?php echo $_smarty_tpl->tpl_vars['category1']->value->posts_category_id;?>
" class="menu-link category-link transition"><?php echo $_smarty_tpl->tpl_vars['category1']->value->category_name;?>
</a>
                    <?php }?>
                <?php
$_smarty_tpl->tpl_vars['category1'] = $foreach_category1_Sav;
}
?>
            </div>

            <div class="menu social-menu hidden-tablet">
                <a href="https://www.facebook.com/moonstore.it/" class="menu-link social-link" target="_blank"><span class="icon icon-facebook"></span></a>
                <a href="https://twitter.com/moon__store" class="menu-link social-link" target="_blank"><span class="icon icon-twitter"></span></a>
                <a href="https://www.instagram.com/moonstore.it/" class="menu-link social-link" target="_blank"><span class="icon icon-instagram"></span></a>
                <button id="search-btn" class="menu-link social-link hidden-phone"><span class="icon icon-search"></span></button>
            </div>

        </nav>

        <button id="menu-btn" class="visible-phone menu-link"><span class="icon icon-menu"></span></button>

    </div>

    <div id="header-search-container">
        <form class="header-search" action="/posts/search/" method="GET">
            <input type="search" id="search-input" name="q" data-search=" " required="" value="" />
            <button id="header-search-btn" type="submit">Поиск</button>
        </form>
    </div>

</header><?php }
}
?>