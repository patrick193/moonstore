<?php /* Smarty version 3.1.27, created on 2016-01-08 21:29:48
         compiled from "/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Site/header.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:39027057456902a4c443830_33771075%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '59d4f3fcf8d371f962e0b1181c025ab8c72f2abb' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Site/header.tpl',
      1 => 1452217779,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '39027057456902a4c443830_33771075',
  'variables' => 
  array (
    'categoties' => 0,
    'category' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56902a4c46a280_35895922',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56902a4c46a280_35895922')) {
function content_56902a4c46a280_35895922 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '39027057456902a4c443830_33771075';
?>

<header id="header" class="clearfix">

    <div class="container clearfix">

        <div itemscope >
            <a href="/" id="logo" >
                <img class="logo" src="/web/templates/Site/assets/images/logo.png" alt="MoonStore">
            </a>
        </div>

        <nav class="menus">

            <div class="menu">
                <?php
$_from = $_smarty_tpl->tpl_vars['categoties']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['category'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['category']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
$foreach_category_Sav = $_smarty_tpl->tpl_vars['category'];
?>
                    <a href="/posts/category/<?php echo $_smarty_tpl->tpl_vars['category']->value->posts_category_id;?>
" class="menu-link category-link transition"><?php echo $_smarty_tpl->tpl_vars['category']->value->category_name;?>
</a>
                <?php
$_smarty_tpl->tpl_vars['category'] = $foreach_category_Sav;
}
?>
            </div>

            <div class="menu social-menu hidden-tablet">
                <a href="https://www.facebook.com/moonstore.it/" class="menu-link social-link" target="_blank"><span class="icon icon-facebook"></span></a>
                <a href="https://twitter.com/moon__store" class="menu-link social-link" target="_blank"><span class="icon icon-twitter"></span></a>
                <a href="https://www.instagram.com/moonstore.it/" class="menu-link social-link" target="_blank"><span class="icon icon-instagram"></span></a>
                <button id="search-btn" class="menu-link social-link hidden-phone"><span class="icon icon-search"></span></button>
            </div>

        </nav>

        <button id="menu-btn" class="visible-phone menu-link"><span class="icon icon-menu"></span></button>

    </div>

    <div id="header-search-container">
        <form class="header-search" action="/posts/search/" method="GET">
            <input type="search" id="search-input" name="q" data-search=" " required="" value="" />
            <button id="header-search-btn" type="submit">Поиск</button>
        </form>
    </div>

</header><?php }
}
?>