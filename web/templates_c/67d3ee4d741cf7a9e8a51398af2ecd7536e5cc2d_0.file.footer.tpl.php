<?php /* Smarty version 3.1.27, created on 2016-02-02 20:19:00
         compiled from "/var/www/moonstore/web/templates/Site/footer.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:2407110456b10f34966910_85319406%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '67d3ee4d741cf7a9e8a51398af2ecd7536e5cc2d' => 
    array (
      0 => '/var/www/moonstore/web/templates/Site/footer.tpl',
      1 => 1452428729,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2407110456b10f34966910_85319406',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56b10f3496c1d1_54538085',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56b10f3496c1d1_54538085')) {
function content_56b10f3496c1d1_54538085 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '2407110456b10f34966910_85319406';
?>
<footer id="footer">

            <div class="container">

                <div class="primary-nav">
                    <a href="/contact" class="footer-link">Связаться с нами</a>
                    <a href="/aboutus" class="footer-link">О нас</a>
                </div>

                <div class="social-menu">
                  <a href="https://www.facebook.com/moonstore.it/" class="social-link" target="blank">
                                        <span class="icon icon-facebook"></span></a>
                                    <a href="https://twitter.com/moon__store" class="social-link" target="blank">
                                        <span class="icon icon-twitter"></span></a>
                                    <a href="https://www.instagram.com/moonstore.it/" class="social-link" target="blank">
                                        <span class="icon icon-instagram"></span></a>
                <div class="copyright"><span class="coop">&copy;</span> 2014 - 2016 Moon Store.</div>
                </div>


            </div>

        </footer><?php }
}
?>