<?php /* Smarty version 3.1.27, created on 2016-01-09 15:07:48
         compiled from "/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Admin/users.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1663965885569122442d9472_23223648%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6a66b0ab3160848baf335669605568fb45357a4c' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Admin/users.tpl',
      1 => 1452217778,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1663965885569122442d9472_23223648',
  'variables' => 
  array (
    'users' => 0,
    'user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5691224440bb48_80692108',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5691224440bb48_80692108')) {
function content_5691224440bb48_80692108 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1663965885569122442d9472_23223648';
?>
<!doctype html>
<html><head>
        <meta charset="utf-8">
        <title>Moonstore - Users</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Pohorielov Vlad">


        <link href="/web/templates/Admin/css/bootstrap.min.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/tingle.css" rel="stylesheet">

        <link href="/web/templates/Admin/css/main.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/font-style.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/flexslider.css" rel="stylesheet">
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/jquery-1.10.2.min.js"><?php echo '</script'; ?>
>

        <!-- Placed at the end of the document so the pages load faster -->
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/bootstrap.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/lineandbars.js"><?php echo '</script'; ?>
>

        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/dash-charts.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/gauge.js"><?php echo '</script'; ?>
>

        <!-- NOTY JAVASCRIPT -->
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/jquery.noty.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/top.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/topLeft.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/topRight.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/topCenter.js"><?php echo '</script'; ?>
>

        <!-- You can add more layouts if you want -->
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/default.js"><?php echo '</script'; ?>
>
        <!-- <?php echo '<script'; ?>
 type="text/javascript" src="assets/js/dash-noty.js"><?php echo '</script'; ?>
> This is a Noty bubble when you init the theme-->
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/highcharts.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/jquery.flexslider.js" type="text/javascript"><?php echo '</script'; ?>
>

        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/admin.js"><?php echo '</script'; ?>
>

        <style type="text/css">
            body {
                padding-top: 60px;
            }
        </style>

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <?php echo '<script'; ?>
 src="http://html5shim.googlecode.com/svn/trunk/html5.js"><?php echo '</script'; ?>
>
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/component.css" />
        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/menu/js/modernizr.custom.25376.js"><?php echo '</script'; ?>
>
        <!-- Google Fonts call. Font Used Open Sans & Raleway -->
        <link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

        <?php echo '<script'; ?>
 type="text/javascript">
            $(document).ready(function () {

                $("#btn-blog-next").click(function () {
                    $('#blogCarousel').carousel('next')
                });
                $("#btn-blog-prev").click(function () {
                    $('#blogCarousel').carousel('prev')
                });

                $("#btn-client-next").click(function () {
                    $('#clientCarousel').carousel('next')
                });
                $("#btn-client-prev").click(function () {
                    $('#clientCarousel').carousel('prev')
                });

            });

            $(window).load(function () {

                $('.flexslider').flexslider({
                    animation: "slide",
                    slideshow: true,
                    start: function (slider) {
                        $('body').removeClass('loading');
                    }
                });
            });

        <?php echo '</script'; ?>
>  

    </head>
    <body>

        <!-- NAVIGATION MENU -->

        <?php echo $_smarty_tpl->getSubTemplate ('./menu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        <div id="perspective" class="perspective effect-moveleft">
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <!-- Top Navigation -->

                    <div class="main clearfix">

                        <div class="container">

                            <!-- FIRST ROW OF BLOCKS -->     
                            <div class="row">
                                <?php if (isset($_smarty_tpl->tpl_vars['users']->value)) {?>

                                    <?php
$_from = $_smarty_tpl->tpl_vars['users']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['user'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['user']->_loop = false;
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['user']->value) {
$_smarty_tpl->tpl_vars['user']->_loop = true;
$foreach_user_Sav = $_smarty_tpl->tpl_vars['user'];
?>

                                        <!-- USER PROFILE BLOCK -->
                                        <div class="col-sm-3 col-lg-3">
                                            <div class="dash-unit" <?php if ($_smarty_tpl->tpl_vars['user']->value->getIsLocked() == 1) {?> title="Заблокирован" <?php }?>>
                                                <dtitle <?php if ($_smarty_tpl->tpl_vars['user']->value->getIsLocked() == 1) {?> style="color: red"<?php } elseif ($_smarty_tpl->tpl_vars['user']->value->getRoleId() == 1) {?>style="color:greenyellow"<?php }?>><?php if ($_smarty_tpl->tpl_vars['user']->value->getRoleId() == 1) {?>Администратор<?php } else { ?>Техподдрежка<?php }?></dtitle>
                                                <hr>
                                                <div class="thumbnail">
                                                    <img src="/web/templates/Admin/img/face80x80.jpg" class="img-circle">
                                                </div><!-- /thumbnail -->
                                                <h1><?php echo $_smarty_tpl->tpl_vars['user']->value->getUserFirstName();?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value->getUserLastName();?>
 </h1>
                                                <h3><?php echo $_smarty_tpl->tpl_vars['user']->value->getUserEmail();?>
</h3>
                                                <br>
                                                <div class="info-user">
                                                    <a href="/admin/profile/usermanagement/delete/user_id=<?php echo $_smarty_tpl->tpl_vars['user']->value->getUserId();?>
" title="<?php if ($_smarty_tpl->tpl_vars['user']->value->getIsLocked() == 0) {?>Заблокировать<?php } else { ?>Разблокировать<?php }?>"><span aria-hidden="true" class="li_lock fs1"></span></a>
                                                    <a href="/admin/change/<?php echo $_smarty_tpl->tpl_vars['user']->value->getUserId();?>
"><span aria-hidden="true" class="li_pen fs1"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
$_smarty_tpl->tpl_vars['user'] = $foreach_user_Sav;
}
?>
                                <?php }?>

                                <div class="col-sm-3 col-lg-3">
                                    <a href="/admin/user/add"><div class="dash-unit-add">
                                        </div></a>
                                </div>

                            </div><!-- /row -->

                            <!-- FOURTH ROW OF BLOCKS -->     
                        </div> <!-- /container -->

                    </div>
                </div>
            </div>
            <?php echo $_smarty_tpl->getSubTemplate ('./footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>





    </body></html><?php }
}
?>