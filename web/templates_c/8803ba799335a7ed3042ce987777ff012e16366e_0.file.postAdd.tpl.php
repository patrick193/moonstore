<?php /* Smarty version 3.1.27, created on 2016-02-04 19:21:17
         compiled from "/var/www/moonstore/web/templates/Admin/postAdd.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:209554956056b3a4adcee0c7_92449712%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8803ba799335a7ed3042ce987777ff012e16366e' => 
    array (
      0 => '/var/www/moonstore/web/templates/Admin/postAdd.tpl',
      1 => 1454613669,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '209554956056b3a4adcee0c7_92449712',
  'variables' => 
  array (
    'post_edit' => 0,
    'category' => 0,
    'tags' => 0,
    'tag' => 0,
    'tagsp' => 0,
    'positions' => 0,
    'position' => 0,
    'aligns' => 0,
    'align' => 0,
    'i' => 0,
    'comment' => 0,
    'cat' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56b3a4ade96953_00996745',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56b3a4ade96953_00996745')) {
function content_56b3a4ade96953_00996745 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '209554956056b3a4adcee0c7_92449712';
?>
<!DOCTYPE html>
<html>
<head>
    <link href="/web/templates/Admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="/web/templates/Admin/css/main.css" rel="stylesheet">
    <link href="/web/templates/Admin/css/font-style.css" rel="stylesheet">
    <link href="/web/templates/Admin/css/flexslider.css" rel="stylesheet">
    <link href="/web/templates/Admin/js/multiselect/jquery.tokenize.css" rel="stylesheet">
    <link href="/web/templates/Admin/datetime/jquery.datetimepicker.css" rel="stylesheet">

    <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/jquery-1.10.2.min.js"><?php echo '</script'; ?>
>
    <link href="/web/templates/Admin/css/tingle.css" rel="stylesheet">
    <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/bootstrap.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/lineandbars.js"><?php echo '</script'; ?>
>
    <!— You can add more layouts if you want —>
    <?php echo '<script'; ?>
 src="/web/templates/Admin/js/bootstrap-multiselect.js" type="text/javascript"><?php echo '</script'; ?>
>

    <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/menu/css/component.css"/>
    <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/tinymce/tinymce.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/web/templates/Admin/js/menu/js/modernizr.custom.25376.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/web/templates/Admin/js/video.js"><?php echo '</script'; ?>
>

    <!— Our main JS file —>
    <?php echo '<script'; ?>
 type="text/javascript">
        tinymce.init({
            height: '750px',
            width: '100%',
            selector: "textarea",
            plugins: [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime table contextmenu paste imagetools image responsivefilemanager",
                "youtube",
                "videoembed"
            ],
            language: "ru",
            toolbar: "preview | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager | youtube | videoembed",
            content_css: "/web/templates/Site/assets/css/article.css",
            image_advtab: true,
            relative_urls: false,
            browser_spellcheck: true,
            filemanager_title: "Filemanager",
            external_filemanager_path: "/web/templates/Admin/js/filemanager/",
            external_plugins: {
                "filemanager": "./filemanager/plugin.min.js"
            },
        });

        $(document).ready(function () {
            $(".video-lb").hide();
            $(".vimeo-player").hide();
        });

        window.onload = function () {
            globalSwitch()
        };
        function globalSwitch(selected = null) {
            if (selected === null) {
                <?php if ($_smarty_tpl->tpl_vars['post_edit']->value[0]->img_type) {?>
                selected = <?php echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->img_type;?>
;
                <?php } else { ?>
                selected = 0;
                <?php }?>
            }
            console.log(selected);
            if (selected == 0) {
                $(".post-name").show();
                $("#imageInput").show();
                $(".video-lb").hide();
                $(".player").hide();
                $(".vimeo-player").hide();
            } else if (selected == 1) {
                $(".post-name").hide();
                $("#imageInput").hide();
                $(".video-lb").show();
                $(".player").show();
                $(".vimeo-player").show();
            } else {
                $(".post-name").show();
                $("#imageInput").show();
                $(".video-lb").show();
                $(".player").show();
                $(".vimeo-player").show();
            }
        }

    <?php echo '</script'; ?>
>

    <style>
        p {
            font-family: sans-serif;
        }

        label.custom-select {
            position: relative;
            display: inline-block;
        }

        .custom-select select {
            display: inline-block;
            border: 2px solid #bbb;
            padding: 4px 3px 3px 5px;
            margin: 0;
            font: inherit;
            outline: none; /* remove focus ring from Webkit */
            line-height: 1.2;
            background: #f8f8f8;

            -webkit-appearance: none; /* remove the strong OSX influence from Webkit */

            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            border-radius: 6px;
        }

        /* for Webkit's CSS-only solution */
        @media screen and (-webkit-min-device-pixel-ratio: 0) {
            .custom-select select {
                padding-right: 30px;
            }
        }

        /* Since we removed the default focus styles, we have to add our own */
        .custom-select select:focus {
            -webkit-box-shadow: 0 0 3px 1px #c00;
            -moz-box-shadow: 0 0 3px 1px #c00;
            box-shadow: 0 0 3px 1px #c00;
        }

        /* Select arrow styling */
        .custom-select:after {
            content: "▼";
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            font-size: 60%;
            line-height: 30px;
            padding: 0 7px;
            background: #bbb;
            color: white;

            pointer-events: none;

            -webkit-border-radius: 0 6px 6px 0;
            -moz-border-radius: 0 6px 6px 0;
            border-radius: 0 6px 6px 0;
        }

        .no-pointer-events .custom-select:after {
            content: none;
        }

        @media (max-width: 1210px) {
            .post_comments {
                display: none;
            }
        }
    </style>
    
        <?php echo '<script'; ?>
 type="text/javascript">

        <?php echo '</script'; ?>
>
    
</head>
<body>

<?php echo $_smarty_tpl->getSubTemplate ('./menu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<div id="perspective" class="perspective effect-moveleft">

    <div class="container" style="margin-top: 75px;">
        <div class="wrapper"><!— wrapper needed for scroll —>
            <!— Top Navigation —>

            <div class="main clearfix">
                <!— FIRST ROW OF BLOCKS —>
                <div class="container">

                    <div class="row">

                        <?php if (isset($_smarty_tpl->tpl_vars['post_edit']->value)) {?>
                            <div class="post_add edit">

                                <form method="post" action="/admin/profile/posts/edit/" enctype="multipart/form-data"
                                      class="form-edit">
                                    <input name="post_id" class="input-huge" value="<?php echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->post_id;?>
"
                                           type="hidden">

                                    <label for="img_type" class="custom-select">
                                        Изображение статьи:

                                        <select name="img_type" class="img_type">
                                            <option value="0" <?php if ($_smarty_tpl->tpl_vars['post_edit']->value[0]->img_type == 0) {?> selected="" <?php }?>>
                                                Изображение
                                            </option>
                                            <option value="1" <?php if ($_smarty_tpl->tpl_vars['post_edit']->value[0]->img_type == 1) {?> selected="" <?php }?>>
                                                Видео
                                            </option>
                                            <option value="2" <?php if ($_smarty_tpl->tpl_vars['post_edit']->value[0]->img_type == 2) {?> selected="" <?php }?>>Микс
                                            </option>
                                        </select>
                                    </label>
                                    <br><br><br>
                                    <label class="post-name"> Изображение
                                        <input name="image_file" id="imageInput" type="file"/>
                                    </label>
                                    <br>

                                    <label for="video_url" class="video-lb">
                                        Видео:
                                        <input name="video_url" class="video" type="text"
                                               style="width: 500px"
                                               placeholder="Вставить ссылку на видео"
                                                <?php if ($_smarty_tpl->tpl_vars['post_edit']->value[0]->vimeo != '') {?>
                                                    value="<?php echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->vimeo;?>
"
                                                <?php }?>
                                        >
                                    </label>
                                    <div class="player">
                                        <iframe src="<?php if ($_smarty_tpl->tpl_vars['post_edit']->value[0]->vimeo != '') {
echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->vimeo;
}?>"
                                                width="500" height="281" frameborder="0"
                                                webkitallowfullscreen mozallowfullscreen
                                                allowfullscreen
                                                class="vimeo-player">
                                        </iframe>
                                        <p>
                                    </div>
                                    <br><br>

                                    <br>
                                    

                                    <label class="post-name"> Название статьи </label>
                                    <input name="post_name" class="post-name-input" type="text"
                                           value="<?php echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->post_name;?>
">

                                    <label class="custom-select">
                                        Категория:
                                        <select name="category_id">
                                            <?php
$_from = $_smarty_tpl->tpl_vars['post_edit']->value[1];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['category'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['category']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
$foreach_category_Sav = $_smarty_tpl->tpl_vars['category'];
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['category']->value->posts_category_id;?>
"
                                                        <?php ob_start();
echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->post_category_id;
$_tmp1=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['category']->value->category_name;
$_tmp2=ob_get_clean();
if ($_tmp1 == $_tmp2) {?>
                                                    selected="selected" <?php }?>>
                                                    <?php echo $_smarty_tpl->tpl_vars['category']->value->category_name;?>
</option>
                                            <?php
$_smarty_tpl->tpl_vars['category'] = $foreach_category_Sav;
}
?>
                                        </select>
                                    </label>
                                    <br>
                                    <label class="post-seo-desc"> Краткое описание: </label>
                                    <input name="short_desc" type="text" placeholder="Краткое описание"
                                           value="<?php echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->short_desc;?>
">

<textarea name="content" id="my_editor"
          style="width:50%; height: 75%"><?php echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->post_text;?>
</textarea>

                                    <label class="post-seo-desc"> Seo description: </label>
                                    <input name="post_seo_desc" type="text" placeholder="SEO описание"
                                           value="<?php echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->post_seo_desc;?>
">
                                    <label class="post-seo-title"> Seo title: </label>
                                    <input name="post_seo_title" type="text" placeholder="SEO title"
                                           value="<?php echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->post_seo_title;?>
">
                                    <label class="posttags"> Tags:
                                        <select id="tokenize" name="tags[]" multiple class="tokenize-sample">
                                            <?php
$_from = $_smarty_tpl->tpl_vars['tags']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['tag'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['tag']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['tag']->value) {
$_smarty_tpl->tpl_vars['tag']->_loop = true;
$foreach_tag_Sav = $_smarty_tpl->tpl_vars['tag'];
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['tag']->value->tag_id;?>
" <?php if (in_array($_smarty_tpl->tpl_vars['tag']->value->tag_id,$_smarty_tpl->tpl_vars['tagsp']->value)) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['tag']->value->tag_name;?>
</option>
                                            <?php
$_smarty_tpl->tpl_vars['tag'] = $foreach_tag_Sav;
}
?>
                                        </select>
                                    </label>
                                    <a class="icon-image trigger-button-tags">Создать Tag</a>
                                    <br><br>
                                    <label class="position">
                                        Type:
                                        <select name="position">
                                            <?php
$_from = $_smarty_tpl->tpl_vars['positions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['position'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['position']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['position']->value) {
$_smarty_tpl->tpl_vars['position']->_loop = true;
$foreach_position_Sav = $_smarty_tpl->tpl_vars['position'];
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['position']->value->position_id;?>
"
                                                        <?php ob_start();
echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->position_id;
$_tmp3=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['position']->value->position_id;
$_tmp4=ob_get_clean();
if ($_tmp3 == $_tmp4) {?>
                                                    selected="selected" <?php }?>>
                                                    <?php echo $_smarty_tpl->tpl_vars['position']->value->position_name;?>
</option>
                                            <?php
$_smarty_tpl->tpl_vars['position'] = $foreach_position_Sav;
}
?>
                                        </select>
                                    </label>
                                    <label class="align">
                                        Позиция:
                                        <select name="align">
                                            <?php
$_from = $_smarty_tpl->tpl_vars['aligns']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['align'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['align']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['align']->value) {
$_smarty_tpl->tpl_vars['align']->_loop = true;
$foreach_align_Sav = $_smarty_tpl->tpl_vars['align'];
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['align']->value->align_id;?>
"
                                                        <?php ob_start();
echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->type_id;
$_tmp5=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['align']->value->align_id;
$_tmp6=ob_get_clean();
if ($_tmp5 == $_tmp6) {?> selected="" <?php }?>>
                                                    <?php echo $_smarty_tpl->tpl_vars['align']->value->align_name;?>
</option>
                                            <?php
$_smarty_tpl->tpl_vars['align'] = $foreach_align_Sav;
}
?>
                                        </select>
                                    </label>
                                    <input type="submit" value="Сохранить ">

                                </form>

                            </div>
                            <div class="post_comments">
                                Комментарии
                                <div class="acc-container">
                                    <article class="accordion">
                                        <?php
$_from = $_smarty_tpl->tpl_vars['post_edit']->value[0]->comments;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['comment'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['comment']->_loop = false;
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['comment']->value) {
$_smarty_tpl->tpl_vars['comment']->_loop = true;
$foreach_comment_Sav = $_smarty_tpl->tpl_vars['comment'];
?>
                                            <section id="acc<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
                                                <h2><a href="#acc<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
                                                        <?php if ($_smarty_tpl->tpl_vars['comment']->value === 1) {?> Нет комментариев <?php } else { ?>От <?php echo $_smarty_tpl->tpl_vars['comment']->value->ip;?>
 в <?php echo $_smarty_tpl->tpl_vars['comment']->value->date;
}?></a>
                                                </h2>
                                                <?php if ($_smarty_tpl->tpl_vars['comment']->value->published == 1) {?>
                                                    <p style="color: green">Опубликованно</p>
                                                <?php } else { ?> <p
                                                        style="color: red"> <?php if ($_smarty_tpl->tpl_vars['comment']->value == 1) {?> Здесь будет коментарий <?php } else { ?>Заблокированно<?php }?> </p><?php }?>
                                                <hr>
                                                <p><?php echo $_smarty_tpl->tpl_vars['comment']->value->comment;?>
</p>
                                                <?php if ($_smarty_tpl->tpl_vars['comment']->value->published == 1) {?>
                                                    <a href="/admin/profile/comments/unpublish/<?php echo $_smarty_tpl->tpl_vars['comment']->value->comment_id;?>
"
                                                       style="color: red">
                                                        Заблокировать</a>
                                                <?php }?>
                                            </section>
                                        <?php
$_smarty_tpl->tpl_vars['comment'] = $foreach_comment_Sav;
}
?>
                                    </article>
                                </div>
                                <div class="img-fon" style="margin-top: 50px;">
                                    Изображение статьи
                                    <?php if (isset($_smarty_tpl->tpl_vars['post_edit']->value)) {?>
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->post_img;?>
"
                                             style="max-width: 100%; max-height: 100%" alt="<?php echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->post_name;?>
"
                                             title="<?php echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->post_name;?>
">
                                    <?php }?>
                                </div>
                            </div>
                            <div>
                                <a href="#" class="preview" target="_blank">Просмотреть</a>

                            </div>
                            <?php echo '<script'; ?>
>
                                $(".preview").click(function () {
                                    var text = $("#content_ifr").contents().find('#tinymce').html();
                                    var postName = $(".post_name-input").val();
                                    $.ajax({
                                        url: '/posts/preview/save/',
                                        method: 'post',
                                        data: {
                                            'post_id': <?php echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->post_id;?>
,
                                            'post_name': postName,
                                            'content': text
                                        }
                                    });
                                    window.open('/posts/previewtrue/show/<?php echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->post_id;?>
', '_blank');
                                    return false;
                                });

                            <?php echo '</script'; ?>
>
                            <!-- ---------------------------    --><?php } else { ?><!-- ---------------------------    -->
                            <div class="post_add">
                                <form method="post" action="/admin/profile/posts/add/" enctype="multipart/form-data"
                                      class="edit-form" style="margin-top: 100px">

                                    <label for="img_type" class="custom-select">
                                        Изображение статьи:

                                        <select name="img_type" class="img_type">
                                            <option value="0">Изображение</option>
                                            <option value="1">Видео</option>
                                            <option value="2">Микс</option>
                                        </select>
                                    </label>
                                    <br><br>
                                    <label class="post-name"> Выбрать изображение

                                        <input name="image_file" id="imageInput" type="file"/>
                                    </label>
                                    <br>
                                    <label for="video_url" class="video-lb">
                                        Видео:
                                        <input name="video_url" class="video" type="text"
                                               style="width: 500px"
                                               placeholder="Вставить ссылку на видео">
                                    </label>

                                    <div class="player">
                                        <iframe src=""
                                                width="500" height="281" frameborder="0"
                                                webkitallowfullscreen mozallowfullscreen
                                                allowfullscreen
                                                class="vimeo-player">
                                        </iframe>
                                        <p>
                                    </div>

                                    <label class="post-name"> Название статьи: </label>
                                    <input name="post_name" type="text" placeholder="Название статьи">

                                    <label class="custom-select">
                                        Категории:

                                        <select name="category_id">
                                            <?php
$_from = $_smarty_tpl->tpl_vars['cat']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['category'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['category']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
$foreach_category_Sav = $_smarty_tpl->tpl_vars['category'];
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['category']->value->posts_category_id;?>
"><?php echo $_smarty_tpl->tpl_vars['category']->value->category_name;?>
</option>
                                            <?php
$_smarty_tpl->tpl_vars['category'] = $foreach_category_Sav;
}
?>
                                        </select>
                                    </label><br><br>
                                    
                                    <br>
                                    <label class="post-seo-desc"> Краткое описание: </label>
                                    <input name="short_desc" type="text" placeholder="Краткое описание"
                                           value="<?php echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->short_desc;?>
">

                                    <textarea name="content" style="width:50%"></textarea>
                                    <label class="post-seo-desc"> Seo description: </label>
                                    <input name="post_seo_desc" type="text" placeholder="SEO описание">

                                    <label class="post-seo-title"> Seo title: </label>
                                    <input name="post_seo_desc" type="text" placeholder="SEO описание">

                                    <label class="posttags"> Tags:
                                        <select id="tokenize" name="tags[]" multiple class="tokenize-sample">
                                            <?php
$_from = $_smarty_tpl->tpl_vars['tags']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['tag'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['tag']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['tag']->value) {
$_smarty_tpl->tpl_vars['tag']->_loop = true;
$foreach_tag_Sav = $_smarty_tpl->tpl_vars['tag'];
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['tag']->value->tag_id;?>
"><?php echo $_smarty_tpl->tpl_vars['tag']->value->tag_name;?>
</option>
                                            <?php
$_smarty_tpl->tpl_vars['tag'] = $foreach_tag_Sav;
}
?>
                                        </select>
                                    </label>
                                    <a class="icon-image trigger-button-tags">Создать Tag</a>
                                    <br><br>
                                    <label class="position">
                                        Type:
                                        <select name="position">
                                            <?php
$_from = $_smarty_tpl->tpl_vars['positions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['position'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['position']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['position']->value) {
$_smarty_tpl->tpl_vars['position']->_loop = true;
$foreach_position_Sav = $_smarty_tpl->tpl_vars['position'];
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['position']->value->position_id;?>
"
                                                        <?php ob_start();
echo $_smarty_tpl->tpl_vars['post_edit']->value[0]->position_id;
$_tmp7=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['position']->value->position_id;
$_tmp8=ob_get_clean();
if ($_tmp7 == $_tmp8) {?> selected="" <?php }?>>
                                                    <?php echo $_smarty_tpl->tpl_vars['position']->value->position_name;?>
</option>
                                            <?php
$_smarty_tpl->tpl_vars['position'] = $foreach_position_Sav;
}
?>
                                        </select>
                                    </label>
                                    <label class="align">
                                        Позиция:
                                        <select name="align">
                                            <?php
$_from = $_smarty_tpl->tpl_vars['aligns']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['align'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['align']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['align']->value) {
$_smarty_tpl->tpl_vars['align']->_loop = true;
$foreach_align_Sav = $_smarty_tpl->tpl_vars['align'];
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['align']->value->align_id;?>
">
                                                    <?php echo $_smarty_tpl->tpl_vars['align']->value->align_name;?>
</option>
                                            <?php
$_smarty_tpl->tpl_vars['align'] = $foreach_align_Sav;
}
?>
                                        </select>
                                    </label>
                                    <input type="submit" value="Сохранить ">

                                </form>

                            </div>
                            <div class="post_comments">
                                Комментарии
                                <div class="acc-container">
                                    <article class="accordion">
                                        <section id="acc1">
                                            <h2><a href="#acc1">
                                                    Нет комментариев </a>
                                            </h2>

                                            <p style="color: green">Опубликованно</p>
                                            <hr>
                                            <p>Здесь будут комментарии</p>

                                        </section>
                                    </article>
                                </div>
                            </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tingle-demo-tag" style="display: none">
        <form method="post" id="tag-add">
            <label class="tag"> Название tag: </label>
            <input name="tag" class="tag-name" type="text" placeholder="Название tag">

            <a href="#" class="btn-done-tag">Готово</a>
        </form>
    </div>

    <?php echo $_smarty_tpl->getSubTemplate ('./footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

    <link rel="stylesheet" type="text/css" href="/web/templates/Admin/js/multiselect/jquery.tokenize.css"/>
    <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/multiselect/jquery.tokenize.js"><?php echo '</script'; ?>
>

    <?php echo '<script'; ?>
>
        // $(document).ready(function (request, response) {

        $(function () {
                    <?php if ($_smarty_tpl->tpl_vars['post_edit']->value[0]->vimeo != '') {?>
                    switcher();
                    <?php }?>
                    $('#tokenize').tokenize().toArray();
                    $(".video-check").click(function () {
                        switcher();
                    });
                    $(".img_type").change(function () {
                        var selected = $(this).val();
                        globalSwitch(selected);
                    });
                    $(".video").change(function () {
                        changeState();
                    });
                    function changeState() {
                        var value = $(".video").val().split("/");
                        var code = value[value.length - 1];
                        console.log(code);
                        $(".vimeo-player").show();
                        var url = '';
                        if (value.indexOf('vimeo.com') !== -1) {
                            console.log('vimeo');
                            url = "https://player.vimeo.com/video/" + code;
                        } else {
                            console.log('youtube');
                            var youtubeCode = code.split('=');
                            url = 'https://www.youtube.com/embed/' + youtubeCode[youtubeCode.length - 1];
                        }
                        $('.chackbox-video').val($(".video").val());
                        $(".vimeo-player").attr('src', url);
                    }

                    function switcher() {
                        if ($(".video-check").is(':checked')) {
                            $(".post-name").hide();
                            $("#imageInput").hide();
                            $(".video-lb").show();
                            $(".player").show();
                            $(".vimeo-player").show();
                        } else {
                            $(".post-name").show();
                            $("#imageInput").show();
                            $(".video-lb").hide();
                            $(".player").hide();
                            $(".vimeo-player").hide();
                        }
                    }
                }
        );
        
        $(".trigger-button-tags").click(function () {
            console.log('yes');
            tingle.modal.setContent($(".tingle-demo-tag").html());
            tingle.modal.open();
            $(".btn-done-tag").click(function () {
                var tagname = $(".tag-name").val();
                console.log(tagname);
                $.ajax({
                    url: '/admin/tag/add/',
                    dataType: 'json',
                    data: {'tag': tagname},
                    success: function (data) {
                        updateTags(data.tagname, data.tagid)
                    },
                    error: function () {
                        alert('Error. Somthing wrong. Please contact with developer')
                    }
                });

                tingle.modal.close();
                return false;
            });
        });
        function updateTags(tag, tagId) {
            $("#tokenize").append("<option value='" + tagId + "'>" + tag + "</option>")
        }
        
    <?php echo '</script'; ?>
>
</body>
</html><?php }
}
?>