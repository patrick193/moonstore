<?php /* Smarty version 3.1.27, created on 2016-02-02 20:19:00
         compiled from "/var/www/moonstore/web/templates/Site/404.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:51085732856b10f347cbbf9_09032703%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8e155ca0e81d89fdf3beee0c3540ad01ee5a8fea' => 
    array (
      0 => '/var/www/moonstore/web/templates/Site/404.tpl',
      1 => 1452441682,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '51085732856b10f347cbbf9_09032703',
  'variables' => 
  array (
    'user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56b10f34930572_30426235',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56b10f34930572_30426235')) {
function content_56b10f34930572_30426235 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '51085732856b10f347cbbf9_09032703';
$_smarty_tpl->tpl_vars['user'] = new Smarty_Variable(unserialize(base64_decode($_SESSION['register']['user_auth'])), null, 0);?>
<!DOCTYPE html>
<html class="no-js" lang="en" id="html-element">
    <head>

        <meta charset="utf-8" />

        <title>404</title>
        <meta name="author" content="Pogorelov Vlad" />
        <meta name="description" content="404" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta property="fb:app_id" content="id"/>
        <link rel="canonical" href="" />
        <meta property="og:site_name" content="MoonStore"/>
        <meta property="og:type" content="article"/>
        <link href="/web/templates/Site/assets/css/site.css" type="text/css" rel="stylesheet"/>
        <link href="/web/templates/Site/assets/css/article.css" type="text/css" rel="stylesheet"/>  
        <link href="/web/templates/Site/assets/css/home-with-carousel.css" type="text/css" rel="stylesheet" id="style1" />

        <?php if (is_object($_smarty_tpl->tpl_vars['user']->value)) {?>
            <link media="screen" href="/web/templates/Site/leftblock/css/custom.css" type="text/css" rel="stylesheet" />
            <link rel="stylesheet" href="/web/templates/Site/leftblock/css/style.css" media="screen" type="text/css" />
        <?php }?>
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Site/assets/js/jquery.min.js"><?php echo '</script'; ?>
>

        <?php echo '<script'; ?>
 type="text/javascript">
            // Target your .container, .wrapper, .post, etc.
            $(document).ready(function () {
                $("body").fadeIn(1500);
                $("a").click(function (event) {
                    if ($(this).attr('class' != 'show-gallery')) {
                        event.preventDefault();
                        linkLocation = this.href;
                        $("body").fadeOut(1500, redirectPage);
                    }
                });
                $("#menu-btn").click(function () {
                    if ($(".menu-open").length > 0) {
                        $('body').removeClass('menu-open');
                    } else {
                        $("body").addClass('menu-open');
                    }
                });
                function redirectPage() {
                    window.location = linkLocation;
                }
            })
        <?php echo '</script'; ?>
>


    </head>
    <body class="document-ready window-load" data-facebooknamespace="moonstore">
        <?php if (is_object($_smarty_tpl->tpl_vars['user']->value)) {?>
            <?php echo $_smarty_tpl->getSubTemplate ('./leftblock/index.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        <?php }?>

        <?php echo $_smarty_tpl->getSubTemplate ('./header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        <div id="main" role="main" class="container clearfix">

            <article class="article clearfix" id="main-article" itemscope="itemscope">



                <div class="headings">
                  
                </div>
                <div class="article-container clearfix" itemprop="articleBody">
                    <h2 > 404 Страница не найдена</h2>
                </div>
            </article>
        </div>
        <?php echo $_smarty_tpl->getSubTemplate ('./footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>
   
    </body>
</html>
<?php }
}
?>