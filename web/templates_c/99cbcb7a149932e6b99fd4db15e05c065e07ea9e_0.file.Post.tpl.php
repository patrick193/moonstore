<?php /* Smarty version 3.1.27, created on 2016-01-16 20:24:30
         compiled from "/var/www/andreevd/data/www/moonstore.it/web/templates/Site/Post.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:545625606569aa6fe8b6e76_78181940%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '99cbcb7a149932e6b99fd4db15e05c065e07ea9e' => 
    array (
      0 => '/var/www/andreevd/data/www/moonstore.it/web/templates/Site/Post.tpl',
      1 => 1452975822,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '545625606569aa6fe8b6e76_78181940',
  'variables' => 
  array (
    'post' => 0,
    'postOne' => 0,
    'tags' => 0,
    'tag' => 0,
    'user' => 0,
    'author' => 0,
    'last' => 0,
    'news' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_569aa6fecd2339_81856880',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_569aa6fecd2339_81856880')) {
function content_569aa6fecd2339_81856880 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '545625606569aa6fe8b6e76_78181940';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="https://www.w3.org/1999/xhtml" xmlns:og="https://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
    <?php $_smarty_tpl->tpl_vars['user'] = new Smarty_Variable(unserialize(base64_decode($_SESSION['register']['user_auth'])), null, 0);?>
    <?php $_smarty_tpl->tpl_vars['postOne'] = new Smarty_Variable($_smarty_tpl->tpl_vars['post']->value[0], null, 0);?>
    <head>
        <meta charset="utf-8" />
        <title><?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_seo_title;?>
</title>
        <meta name="author" content="MoonstoreIt" />
        <meta property="og:type" content="article"/>
        <meta property="og:site_name" content="MoonStore"/>
        <meta property="og:title" content="<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_seo_title;?>
"/>
        <meta property="og:image" content="http://moonstore.it<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_img;?>
"/>
        <meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_seo_desc;?>
" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
        <meta name="title" content="<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_name;?>
"/>
        <meta keywords="<?php
$_from = $_smarty_tpl->tpl_vars['tags']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars["tag"] = new Smarty_Variable;
$_smarty_tpl->tpl_vars["tag"]->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars["tag"]->value) {
$_smarty_tpl->tpl_vars["tag"]->_loop = true;
$foreach_tag_Sav = $_smarty_tpl->tpl_vars["tag"];
if (!is_null($_smarty_tpl->tpl_vars['tag']->value)) {
echo $_smarty_tpl->tpl_vars['tag']->value;?>
,<?php }
$_smarty_tpl->tpl_vars["tag"] = $foreach_tag_Sav;
}
?>"/>
        <link rel="canonical" href="" />
        <link href="/web/templates/Site/assets/css/site.css" type="text/css" rel="stylesheet"/>
        <link href="/web/templates/Site/assets/css/article.css" type="text/css" rel="stylesheet"/>  
        <link href="/web/templates/Site/assets/css/home-with-carousel.css" type="text/css" rel="stylesheet" id="style1" />

        <?php if (is_object($_smarty_tpl->tpl_vars['user']->value)) {?>
            <link media="screen" href="/web/templates/Site/leftblock/css/custom.css" type="text/css" rel="stylesheet" />
            <link rel="stylesheet" href="/web/templates/Site/leftblock/css/style.css" media="screen" type="text/css" />
        <?php }?>
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Site/assets/js/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/jquery-ui.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/video.js"><?php echo '</script'; ?>
>

        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Site/gallery/jquery.easing.1.3.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Site/gallery/jquery.mousewheel.min.js"><?php echo '</script'; ?>
>
        <style type="text/css">

            .hovergallery img{
                -webkit-transform:scale(0.8); /*Webkit: уменьшаем размер до 0.8*/
                -moz-transform:scale(0.8); /*Mozilla: масштабирование*/
                -o-transform:scale(0.8); /*Opera: масштабирование*/
                -webkit-transition-duration: 0.5s; /*Webkit: длительность анимации*/
                -moz-transition-duration: 0.5s; /*Mozilla: длительность анимации*/
                -o-transition-duration: 0.5s; /*Opera: длительность анимации*/
                opacity: 0.7; /*Начальная прозрачность изображений*/
                margin: 0 10px 5px 0; /*Интервалы между изображениями*/
            }

            .hovergallery img:hover{
                -webkit-transform:scale(1.1); /*Webkit: увеличиваем размер до 1.2x*/
                -moz-transform:scale(1.1); /*Mozilla: масштабирование*/
                -o-transform:scale(1.1); /*Opera: масштабирование*/
                box-shadow:0px 0px 30px gray; /*CSS3 тени: 30px размытая тень вокруг всего изображения*/
                -webkit-box-shadow:0px 0px 30px gray; /*Webkit: тени*/
                -moz-box-shadow:0px 0px 30px gray; /*Mozilla: тени*/
                opacity: 1;
            }

        </style>
        <?php echo '<script'; ?>
 src="/web/templates/Site/assets/js/jquery-migrate-1.2.1.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript">
            var safari = $.browser.safari;
            $("#player").fitVids();
            $(document).ready(function () {
                $("#search-btn").mouseover(function () {
                    if ($('body').hasClass('search-open')) {
                        $('body').removeClass('search-open');
                    } else {
                        $('body').addClass('search-open');
                    }
                });
                $("#header-search-container").mouseleave(function () {
                    $('body').removeClass('search-open');
                })
                if ($(window).width() < 767) {
                    $(".article-body").css('margin', '0 auto');
                }
                $("body").fadeIn(1500);
                $("a").click(function (event) {
                    if ($(this).attr('class') !== 'show-gallery'
                            && !safari && $(this).attr('class') !== 'subscribe-btn' && $(this).data('fade') !== 1
                            && $(this).attr('class') !== 'not-fade'
                            && $(this).attr('id') !== 'close-gallery') {
                        event.preventDefault();
                        linkLocation = this.href;
                        $("body").fadeOut(1500, redirectPage);
                    }
                });
                $("#menu-btn").click(function () {
                    if ($(".menu-open").length > 0) {
                        $('body').removeClass('menu-open');
                    } else {
                        $("body").addClass('menu-open');
                    }
                });
                function redirectPage() {
                    window.location = linkLocation;
                }
                function changeSubscribe() {
                    $(".subscribe-form").fadeOut(300);
                    $(".success-subscribe").fadeIn(300);
                    var subscribed = localStorage.getItem('subscribe');
                    if (subscribed != '1') {
                        localStorage.setItem('subscribe', '1');
                    }
                }
                $(".hidden-gallery").hide();


                function shareFB(title, url, desc, image) {
                    t = encodeURIComponent(title);
                    d = encodeURIComponent(desc);
                    u = encodeURIComponent(url);
                    i = encodeURIComponent(image);
                    share_url = 'http://www.facebook.com/sharer.php';
                    share_url += '?s=100&p[title]=' + t + '&p[summary]=' + d + '&p[url]=' + u;
                    share_url += '&p[images][0]=' + i;
                    share_url += '&t=' + t + '&e=' + d;
                    return share_url;
                }
                function shareVK(title, url, desc, image) {
                    t = encodeURIComponent(title);
                    d = encodeURIComponent(desc);
                    u = encodeURIComponent(url);
                    i = encodeURIComponent(image);
                    share_url = 'http://vkontakte.ru/share.php';
                    share_url += '?title=' + t + '&description=' + d + '&url=' + u;
                    share_url += '&image=' + i;
                    share_url += '&noparse=1'
                    return share_url;
                }
                function shareMM(title, url, desc, image) {
                    t = encodeURIComponent(title);
                    d = encodeURIComponent(desc);
                    u = encodeURIComponent(url);
                    i = encodeURIComponent(image);
                    share_url = 'http://connect.mail.ru/share';
                    share_url += '?title=' + t + '&description=' + d + '&url=' + u;
                    share_url += '&imageurl=' + i;
                    return share_url;
                }
                function shareOD(title, url, desc, image) {
                    t = encodeURIComponent(title);
                    d = encodeURIComponent(desc);
                    u = encodeURIComponent(url);
                    i = encodeURIComponent(image);
                    return 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=' + u;
                }
                function socialShare(type, title, url, desc, image) {
                    var u = '';
                    switch (type) {
                        case 'vk':
                            u = shareVK(title, url, desc, image);
                            break;
                        case 'fb':
                            u = shareFB(title, url, desc, image);
                            break;
                        case 'mm':
                            u = shareMM(title, url, desc, image);
                            break;
                        case 'od':
                            u = shareOD(title, url, desc, image);
                            break;
                    }
                    if (url != '') {
                        window.open(u, 'sharer', 'toolbar=0,status=0,width=626,height=436');
                    }
                    return false;
                }
                $("#share-button-vk").click(function (e) {
                    e.preventDefault();
                    socialShare('vk', '<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_name;?>
', 'http://moonstore.it/posts/show/<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_id;?>
', '<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_seo_desc;?>
', 'http://moonstore.it<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_img;?>
')
                });
                $('#share_button-facebook').click(function (e) {
                    e.preventDefault();
                    socialShare('fb', '<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_name;?>
', 'http://moonstore.it/posts/show/<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_id;?>
', '<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_seo_desc;?>
', 'http://moonstore.it<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_img;?>
')

            
                });
            });<?php echo '</script'; ?>
>
            
            <?php echo '<script'; ?>
>
                (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date()
                            ;
                    a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                ga('create', 'UA-51230191-1', 'moonstore.it');
                ga('send', 'pageview');



            <?php echo '</script'; ?>
>
        
        
    </head>
    <body class="document-ready window-load" data-facebooknamespace="moonstore">
        
        <?php if (is_object($_smarty_tpl->tpl_vars['user']->value)) {?>
            <?php echo $_smarty_tpl->getSubTemplate ('./leftblock/index.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        <?php }?>

        <div class="hidden-gallery">
            <?php ob_start();
echo $_smarty_tpl->tpl_vars['postOne']->value->gallery;
$_tmp1=ob_get_clean();
if (isset($_tmp1)) {?>
                <?php echo $_smarty_tpl->tpl_vars['postOne']->value->gallery;?>

            <?php }?>
        </div>

        <?php echo $_smarty_tpl->getSubTemplate ('./header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        <div class="sidebar-social-wrapper" style="margin-left: 100px; margin-top: 100px; position: fixed">

            <aside class="sidebar-social social-btns hidden-phone hidden-tablet">

                <div class="social-btns">
                    <div class="social-btn" id="share-button-vk">

                        <img src="/web/templates/Site/assets/images/icons/vk.png" title="Поделиться в VK" alt="Поделиться в VK" style="width: 50px; height: 50px;">
                    </div>

                    <div class="social-btn" id="share_button-facebook">
                        
                        <img src="/web/templates/Site/assets/images/icons/facebook.png" title="Поделиться в facebook" alt="Поделиться в facebook" style="width: 50px; height: 50px;">
                            
                    </div>
                    <div class="social-btn">
                        <a href="#"
                           class="not-fade"  onClick="window.open('https://twitter.com/share?url=moonstore.it/posts/show/<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_id;?>
&text=<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_seo_title;?>
&url=dev.moonstore.it/posts/show/<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_id;?>
', 'twitter', 'height=500px,width=500px')">
                            <img src="/web/templates/Site/assets/images/icons/twitter.png" class="twitter-share" title="Поделиться в twitter" alt="Поделиться в twitter" style="width: 50px; height: 50px;">
                        </a>
                    </div>
                    <div class="social-btn">
                        <a href="#" 
                           title="Поделиться в Tumblr" class="not-fade"  onClick="window.open('http://www.tumblr.com/share', 'tublr', 'height=500px,width=500px')" >
                            <img src="/web/templates/Site/assets/images/icons/tumblr.png" class="twitter-share" title="Поделиться в Tumblr" alt="Поделиться в twitter" style="width: 50px; height: 50px;">
                        </a>
                    </div>
                    <br>
                </div>

            </aside>
        </div>
        <div id="main" role="main" class="container clearfix">

            <article class="article clearfix" id="main-article" itemscope="itemscope">



                <div class="headings">
                    <h3 class="column-headings">
                        <a class="column-title" href="#" itemprop="articleSection"><?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_category_id;?>
</a> 
                    </h3>
                    <h2 class="title"><?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_name;?>
</h2>
                    <time class="date" datetime="<?php echo $_smarty_tpl->tpl_vars['postOne']->value->date_created;?>
" itemprop="datePublished" style="color: #c9aa90">
                        <?php echo $_smarty_tpl->tpl_vars['postOne']->value->date_created;?>
</time>

                </div>
                <div class="article-hero landscape">
                    <figure class="main-img">
                        <?php ob_start();
echo $_smarty_tpl->tpl_vars['postOne']->value->vimeo;
$_tmp2=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['postOne']->value->display;
$_tmp3=ob_get_clean();
if (!is_null($_tmp2) && $_tmp3 == 1) {?>
                            <div id="player" style="margin-left: 35px">
                                <iframe src="<?php echo $_smarty_tpl->tpl_vars['postOne']->value->vimeo;?>
"
                                        width="980" height="500" frameborder="0" 
                                        webkitallowfullscreen mozallowfullscreen 
                                        allowfullscreen
                                        class="fluid-width-video-wrapper">
                                </iframe> 
                                <p>
                            </div>
                        <?php } else { ?>
                            <img alt="<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_name;?>
" data-aspect-ratio="1.57" data-aspect-ratio-type="landscape" 
                                 data-max-height="1000" data-max-width="1571" 
                                 data-responsive-widths="375,480,1257" 
                                 itemprop="image" src="<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_img;?>
" style="max-width: 100%; width:1257px" />
                        <?php }?>
                    </figure>
                    <ul class="credits">
                        <li class="credit" itemprop="author" itemscope="">
                            <span class="credit-title" itemprop="jobTitle">Автор</span>
                            <span class="credit-name" itemprop="name">
                                <?php echo $_smarty_tpl->tpl_vars['author']->value[0]->getUserFirstName();?>
 <?php echo $_smarty_tpl->tpl_vars['author']->value[0]->getUserLastName();?>

                            </span>
                        </li>
                    </ul>

                </div>

                <div class="article-container clearfix" itemprop="articleBody">

                    <div class="article-body"<?php ob_start();
echo $_smarty_tpl->tpl_vars['postOne']->value->type_id;
$_tmp4=ob_get_clean();
if ($_tmp4 != 4) {?> style="margin: 0 210px 40px;"<?php }?>>
                        <?php ob_start();
echo $_smarty_tpl->tpl_vars['postOne']->value->type_id;
$_tmp5=ob_get_clean();
if ($_tmp5 == 4) {?>
                            <div class="subscribe-widget" style=" margin-right: -300px;">
                                <div class="fb-page" data-href="https://www.facebook.com/moonstore.it"
                                     data-tabs="timeline" 
                                     data-small-header="false" 
                                     data-adapt-container-width="true" 
                                     data-hide-cover="false" data-show-facepile="true">
                                    <div class="fb-xfbml-parse-ignore">
                                        <blockquote cite="https://www.facebook.com/moonstore.it">
                                            <a href="https://www.facebook.com/moonstore.it">Moonstore</a></blockquote></div></div>
                                <br><br><br><br><br>
                                                    <h5 class="description">Подписаться на обновления</h5>

                                                    <form method="POST" class="subscribe-form" action="">
                                                        <input type="text" name="email" class="email-input" placeholder="you@email.com" required />
                                                        <a href="#"  class="subscribe-btn"><span class="icon icon-angle-right"></span></a>
                                                    </form>
                                                    <div class="success-subscribe" style="display: none">
                                                        <p>Вы успешно подписались на рассылку новостей.</p>
                                                    </div>
                                                    <div class="social-links">
                                                        <a href="https://www.facebook.com/moonstore.it/" class="social-link" target="_blank">
                                                            <span class="icon icon-facebook"></span></a>
                                                        <a href="https://twitter.com/moon__store" class="social-link" target="_blank">
                                                            <span class="icon icon-twitter"></span></a>
                                                        <a href="https://www.instagram.com/moonstore.it/" class="social-link" target="_blank">
                                                            <span class="icon icon-instagram"></span></a>
                                                    </div>
                                                    </div>
                                                <?php }?>
                                                <?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_text;?>


                                                </div>


                                                </div>




                                                <div class="article-footer">

                                                    <div class="tags-list">
                                                        <strong>Теги:</strong> 
                                                        <?php
$_from = $_smarty_tpl->tpl_vars['tags']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars["tag"] = new Smarty_Variable;
$_smarty_tpl->tpl_vars["tag"]->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars["tag"]->value) {
$_smarty_tpl->tpl_vars["tag"]->_loop = true;
$foreach_tag_Sav = $_smarty_tpl->tpl_vars["tag"];
?>
                                                            <?php if (!is_null($_smarty_tpl->tpl_vars['tag']->value)) {?>
                                                                <a href="/posts/find/tag/<?php echo $_smarty_tpl->tpl_vars['tag']->value;?>
" class="tag-btn">#<?php echo $_smarty_tpl->tpl_vars['tag']->value;?>
</a> 
                                                            <?php }?>
                                                        <?php
$_smarty_tpl->tpl_vars["tag"] = $foreach_tag_Sav;
}
?>
                                                    </div>


                                                    <div class="more-articles-container">

                                                        <h3 class="cat-title"><a href="/where-on-earth">Последнее в категории</a></h3>

                                                        <ul class="articles more-articles grid col-5 clearfix">
                                                            <?php
$_from = $_smarty_tpl->tpl_vars['last']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['news'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['news']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['news']->value) {
$_smarty_tpl->tpl_vars['news']->_loop = true;
$foreach_news_Sav = $_smarty_tpl->tpl_vars['news'];
?>
                                                                <li class="grid-item article">
                                                                    <a href="/posts/show/<?php echo $_smarty_tpl->tpl_vars['news']->value->post_id;?>
" data-track="event" data-title="<?php echo $_smarty_tpl->tpl_vars['postOne']->value->category_id;?>
"
                                                                       data-value="Click">
                                                                        <img alt="<?php echo $_smarty_tpl->tpl_vars['news']->value->post_name;?>
" class="img"
                                                                             data-aspect-ratio="1.77" data-aspect-ratio-type="landscape" 
                                                                             data-max-height="1000" data-max-width="1773" 
                                                                             src="<?php echo $_smarty_tpl->tpl_vars['news']->value->post_img_th;?>
" />
                                                                        
                                                                        <h4 class="title"><?php echo $_smarty_tpl->tpl_vars['news']->value->post_name;?>
</h4>
                                                                    </a>
                                                                </li>
                                                            <?php
$_smarty_tpl->tpl_vars['news'] = $foreach_news_Sav;
}
?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                </article>
                                                </div>


                                                <div class='foot'>
                                                    <?php echo $_smarty_tpl->getSubTemplate ('./footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                                                </div>

                                                
                                                    <!-- Yandex.Metrika counter -->
                                                    <?php echo '<script'; ?>
 type="text/javascript">
                                                        (function (d, w, c) {
                                                            (w[c] = w[c] || []).push(function () {
                                                                try {
                                                                    w.yaCounter24852869 = new Ya.Metrika({id: 24852869,
                                                                        webvisor: true,
                                                                        clickmap: true,
                                                                        trackLinks: true,
                                                                        accurateTrackBounce: true});
                                                                } catch (e) {
                                                                }
                                                            });

                                                            var n = d.getElementsByTagName("script")[0],
                                                                    s = d.createElement("script"),
                                                                    f = function () {
                                                                        n.parentNode.insertBefore(s, n);
                                                                    };
                                                            s.type = "text/javascript";
                                                            s.async = true;
                                                            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                                                            if (w.opera == "[object Opera]") {
                                                                d.addEventListener("DOMContentLoaded", f, false);
                                                            } else {
                                                                f();
                                                            }
                                                        })(document, window, "yandex_metrika_callbacks");
                                                    <?php echo '</script'; ?>
>
                                                    <noscript><div><img src="//mc.yandex.ru/watch/24852869" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
                                                    <!-- /Yandex.Metrika counter -->

                                                    <div id="error-notification-id" class="enp-35735"></div>
                                                    <div id="error-notification-settings" style="display: none;"><p>Спасибо большое за вашу помощь. Оставайтесь с нами!</p></div>
                                                    <div style="display:none">
                                                    </div>

                                                    <!-- Yandex.Metrika counter -->
                                                    <?php echo '<script'; ?>
 type="text/javascript">
                                                        (function (d, w, c) {
                                                            (w[c] = w[c] || []).push(function () {
                                                                try {
                                                                    w.yaCounter24852869 = new Ya.Metrika({id: 24852869,
                                                                        webvisor: true,
                                                                        clickmap: true,
                                                                        trackLinks: true,
                                                                        accurateTrackBounce: true});
                                                                } catch (e) {
                                                                }
                                                            });

                                                            var n = d.getElementsByTagName("script")[0],
                                                                    s = d.createElement("script"),
                                                                    f = function () {
                                                                        n.parentNode.insertBefore(s, n);
                                                                    };
                                                            s.type = "text/javascript";
                                                            s.async = true;
                                                            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                                                            if (w.opera == "[object Opera]") {
                                                                d.addEventListener("DOMContentLoaded", f, false);
                                                            } else {
                                                                f();
                                                            }
                                                        })(document, window, "yandex_metrika_callbacks");
                                                    <?php echo '</script'; ?>
>
                                                    <noscript><div><img src="//mc.yandex.ru/watch/24852869" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
                                                    <!-- /Yandex.Metrika counter --><?php echo '<script'; ?>
 type='text/javascript'>
                                                        /* <![CDATA[ */
                                                        var enp = {"ajaxurl": "http:\/\/moonstore.it\/wp-admin\/admin-ajax.php", "barBackground": "#cc3333", "barPosition": "bottom", "barTextColor": "#ffffff", "baseurl": "http:\/\/moonstore.it", "confirmation": "", "cbTitle": "Are you sure?", "cbError": "Error", "cbOK": "OK", "cbCancel": "Cancel"};
                                                        /* ]]> */
                                                    <?php echo '</script'; ?>
>
                                                

                                                <?php echo '<script'; ?>
 type="text/javascript">
                                                    $(".show-gallery").click(function () {
                                                        var ids = $(this).find('img').attr('id');
                                                        console.log(ids)
                                                        var id = ids.split('this')[1];
                                                        $(".img").attr('src', $('#' + id).attr('src'));
                                                        $(".img").attr('id', id);
                                                        $("#current-slide").text(id + 1);
                                                        $("#main").hide();
                                                        $("#header").hide();
                                                        $(".foot").hide();
                                                        $(".hidden-gallery").fadeIn(1000);
                                                    });
                                                    $("#close-gallery").click(function () {
                                                        //$("#outer_container").css('z-index', '-1000');
                                                        $(".hidden-gallery").hide();
                                                        $("#header").show();
                                                        $(".foot").show();
                                                        $("#main").fadeIn(1000);
                                                    });<?php echo '</script'; ?>
>

                                                </body>
                                                </html>
<?php }
}
?>