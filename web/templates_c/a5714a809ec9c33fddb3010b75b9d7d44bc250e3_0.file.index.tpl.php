<?php /* Smarty version 3.1.27, created on 2016-01-10 00:13:29
         compiled from "/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Site/index.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:5785313625691a229d687a5_57120487%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a5714a809ec9c33fddb3010b75b9d7d44bc250e3' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Site/index.tpl',
      1 => 1452384668,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5785313625691a229d687a5_57120487',
  'variables' => 
  array (
    'user' => 0,
    'sliders' => 0,
    'slider' => 0,
    'posts' => 0,
    'i' => 0,
    'post' => 0,
    'keys' => 0,
    'key' => 0,
    'j' => 0,
    'keys2' => 0,
    'key2' => 0,
    'tagsfinder' => 0,
    'page' => 0,
    'category' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5691a22a366059_15186426',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5691a22a366059_15186426')) {
function content_5691a22a366059_15186426 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_unset')) require_once '/var/www/andreevd/data/www/dev.moonstore.it/PowerPlay/PPS/libs/plugins/modifier.unset.php';

$_smarty_tpl->properties['nocache_hash'] = '5785313625691a229d687a5_57120487';
$_smarty_tpl->tpl_vars['user'] = new Smarty_Variable(unserialize(base64_decode($_SESSION['register']['user_auth'])), null, 0);?>
<?php $_smarty_tpl->tpl_vars['postsss'] = new Smarty_Variable('1', null, 0);?>
<!DOCTYPE html>
<html class="no-js" lang="en" id="html-element">
    <head>
        <meta charset="utf-8" />
        <title>Moon Store</title>
        <meta name="author" content="Moonstore" />
        <meta name="description" content="Команда MoonStore ежедневно освещает события в мире моды, искусства, архитектуры, музыки, видео и т.д. Мы хотим, что бы Вы знали больше." />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="canonical" href="" />

        <meta property="fb:161383014049553" content="id"/>
        <link href="/web/templates/Site/assets/css/site.css" type="text/css" rel="stylesheet" id="style1" />
        <link href="/web/templates/Site/assets/css/home-with-carousel.css" type="text/css" rel="stylesheet" id="style1" />
        <!--[if lt IE 9]><?php echo '<script'; ?>
 src="http://html5shim.googlecode.com/svn/trunk/html5.js"><?php echo '</script'; ?>
><![endif]-->
        <link rel="stylesheet" href="/web/templates/Site/assets/css/slider/superslides.css">
        <?php if (is_object($_smarty_tpl->tpl_vars['user']->value)) {?>
            <link media="screen" href="/web/templates/Site/leftblock/css/custom.css" type="text/css" rel="stylesheet" />
            <link rel="stylesheet" href="/web/templates/Site/leftblock/css/style.css" media="screen" type="text/css" />
        <?php }?>
        <?php echo '<script'; ?>
 src='/web/templates/Site/assets/js/jquery.min.js'><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/web/templates/Site/assets/js/slider/jquery.superslides.js" type="text/javascript" charset="utf-8"><?php echo '</script'; ?>
>
    </head>
    <body class="document-ready window-load" data-facebooknamespace="moonstore">
        <div id="fb-root"></div>
        <?php echo '<script'; ?>
>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=126231547426086";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));<?php echo '</script'; ?>
>

        <?php if (is_object($_smarty_tpl->tpl_vars['user']->value)) {?>
            <?php echo $_smarty_tpl->getSubTemplate ('./leftblock/index.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        <?php }?>

        <div id="slides">
            <div class="slides-container">
                <?php
$_from = $_smarty_tpl->tpl_vars['sliders']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['slider'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['slider']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['slider']->value) {
$_smarty_tpl->tpl_vars['slider']->_loop = true;
$foreach_slider_Sav = $_smarty_tpl->tpl_vars['slider'];
?>
                    <a href='<?php if (!is_null($_smarty_tpl->tpl_vars['slider']->value->post_id) && $_smarty_tpl->tpl_vars['slider']->value->post_id != 0) {?>/posts/show/<?php echo $_smarty_tpl->tpl_vars['slider']->value->post_id;
}?>'>
                        <img src="<?php echo $_smarty_tpl->tpl_vars['slider']->value->image_path;?>
"></a>
                    <?php
$_smarty_tpl->tpl_vars['slider'] = $foreach_slider_Sav;
}
?>
            </div>

            <nav class="slides-navigation">
                <a href="#" class="next carousel-btn" id="carousel-next"><span class="icon-angle-right"></span></a>
                <a href="#" class="prev carousel-btn" id="carousel-prev"><span class="icon-angle-left"></span></a>
            </nav>

        </div>

        <?php echo $_smarty_tpl->getSubTemplate ('./header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>





        <div id="main" role="main" class="container clearfix">

            <section class="article-grid">
                <div class="articles clearfix">

                    <?php if (is_array($_smarty_tpl->tpl_vars['posts']->value['query'])) {?>
                        <!-- row 1 -->
                        <div class="row halfpage clearfix">
                            <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(0, null, 0);?>
                            <?php $_smarty_tpl->tpl_vars['keys'] = new Smarty_Variable('', null, 0);?>
                            <?php
$_from = $_smarty_tpl->tpl_vars['posts']->value['query'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['post'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['post']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
$foreach_post_Sav = $_smarty_tpl->tpl_vars['post'];
?>
                                <?php if ($_smarty_tpl->tpl_vars['i']->value < 4) {?>
                                    <?php ob_start();
echo $_smarty_tpl->tpl_vars['post']->value->position_id;
$_tmp1=ob_get_clean();
if ($_tmp1 != 2) {?>
                                        <article class="article">
                                            <a href="/posts/show/<?php echo $_smarty_tpl->tpl_vars['post']->value->post_id;?>
" class="article-link clearfix">
                                                <div class="img-wrapper">
                                                    <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                         data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                         data-responsive-widths="375,480"
                                                         src="<?php echo $_smarty_tpl->tpl_vars['post']->value->post_img;?>
" style="max-width: 100%; width:480px; " />
                                                    <?php ob_start();
echo $_smarty_tpl->tpl_vars['post']->value->vimeo;
$_tmp2=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['post']->value->display;
$_tmp3=ob_get_clean();
if (!is_null($_tmp2) && $_tmp3 == 1) {?>
                                                        <h6 class="category">
                                                            <span class="icon icon-instagram"></span>
                                                            <?php echo $_smarty_tpl->tpl_vars['post']->value->post_category_id;?>

                                                        </h6>
                                                    <?php } else { ?>
                                                        <h6 class="category">
                                                            <?php echo $_smarty_tpl->tpl_vars['post']->value->post_category_id;?>

                                                        </h6>
                                                    <?php }?>
                                                </div>
                                                <div class="text">
                                                    <h4 class="title"><?php echo $_smarty_tpl->tpl_vars['post']->value->post_name;?>
</h4>
                                                    <h5 class="description"><?php echo $_smarty_tpl->tpl_vars['post']->value->short_desc;?>
</h5>
                                                </div>
                                            </a>
                                        </article>
                                        <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
                                        <?php $_smarty_tpl->tpl_vars['keys'] = new Smarty_Variable((($_smarty_tpl->tpl_vars['keys']->value).(",")).($_smarty_tpl->tpl_vars['key']->value), null, 0);?>
                                    <?php }?>
                                <?php }?>
                            <?php
$_smarty_tpl->tpl_vars['post'] = $foreach_post_Sav;
}
?>
                            <?php $_smarty_tpl->createLocalArrayVariable('posts', null, 0);
$_smarty_tpl->tpl_vars['posts']->value['query'] = smarty_modifier_unset($_smarty_tpl->tpl_vars['posts']->value['query'],$_smarty_tpl->tpl_vars['keys']->value);?>                            

                        </div>
                        <div class="subscribe-widget">
                            <div class="fb-page" data-href="https://www.facebook.com/moonstore.it"
                                 data-tabs="timeline" 
                                 data-small-header="false" 
                                 data-adapt-container-width="true" 
                                 data-hide-cover="false" data-show-facepile="true">
                                <div class="fb-xfbml-parse-ignore">
                                    <blockquote cite="https://www.facebook.com/moonstore.it">
                                        <a href="https://www.facebook.com/moonstore.it">Moonstore</a></blockquote></div></div>
                            <br><br><br><br><br>
                            <h5 class="description">Подписаться на обновления</h5>

                            <form method="POST" class="subscribe-form" action="">
                                <input type="text" name="email" class="email-input" placeholder="you@email.com" required />
                                <a href="#"  class="subscribe-btn"><span class="icon icon-angle-right"></span></a>
                            </form>
                            <div class="success-subscribe" style="display: none">
                                <p>Вы успешно подписались на рассылку новостей.</p>
                            </div>
                            <div class="social-links">
                                <a href="https://www.facebook.com/moonstore.it/" class="social-link" target="_blank">
                                    <span class="icon icon-facebook"></span></a>
                                <a href="https://twitter.com/moon__store" class="social-link" target="_blank">
                                    <span class="icon icon-twitter"></span></a>
                                <a href="https://www.instagram.com/moonstore.it/" class="social-link" target="_blank">
                                    <span class="icon icon-instagram"></span></a>
                            </div>
                        </div>
                        <!-- /row 1 -->
                        <!-- row 2 -->
                        <div class="row featured clearfix">
                            <?php $_smarty_tpl->tpl_vars['j'] = new Smarty_Variable(0, null, 0);?>
                            <?php $_smarty_tpl->tpl_vars['keys2'] = new Smarty_Variable('', null, 0);?>
                            <?php
$_from = $_smarty_tpl->tpl_vars['posts']->value['query'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['post'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['post']->_loop = false;
$_smarty_tpl->tpl_vars['key2'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key2']->value => $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
$foreach_post_Sav = $_smarty_tpl->tpl_vars['post'];
?>
                                <?php if ($_smarty_tpl->tpl_vars['j']->value < 4) {?>
                                    <?php ob_start();
echo $_smarty_tpl->tpl_vars['post']->value->position_id;
$_tmp4=ob_get_clean();
if ($_tmp4 == 2) {?>
                                        <article class="article">
                                            <a href="/posts/show/<?php echo $_smarty_tpl->tpl_vars['post']->value->post_id;?>
" class="article-link clearfix">
                                                <div class="img-wrapper">
                                                    <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                         data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                         data-responsive-widths="375,480"
                                                         src="<?php echo $_smarty_tpl->tpl_vars['post']->value->post_img;?>
" style="max-width: 100%; width:480px; " />
                                                    <?php ob_start();
echo $_smarty_tpl->tpl_vars['post']->value->vimeo;
$_tmp5=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['post']->value->display;
$_tmp6=ob_get_clean();
if (!is_null($_tmp5) && $_tmp6 == 1) {?>
                                                        <h6 class="category">
                                                            <span class="fa fa-video-camera"></span>
                                                            <?php echo $_smarty_tpl->tpl_vars['post']->value->post_category_id;?>

                                                        </h6>
                                                    <?php } else { ?>
                                                        <h6 class="category">
                                                            <?php echo $_smarty_tpl->tpl_vars['post']->value->post_category_id;?>

                                                        </h6>
                                                    <?php }?>
                                                </div>
                                                <div class="text">
                                                    <h4 class="title"><?php echo $_smarty_tpl->tpl_vars['post']->value->post_name;?>
</h4>
                                                    <h5 class="description">
                                                        <?php echo $_smarty_tpl->tpl_vars['post']->value->short_desc;?>
    
                                                    </h5>
                                                </div>
                                            </a>
                                        </article>
                                        <?php $_smarty_tpl->tpl_vars['keys2'] = new Smarty_Variable((($_smarty_tpl->tpl_vars['keys2']->value).(",")).($_smarty_tpl->tpl_vars['key2']->value), null, 0);?>
                                        <?php $_smarty_tpl->tpl_vars['j'] = new Smarty_Variable($_smarty_tpl->tpl_vars['j']->value+1, null, 0);?>
                                    <?php }?>
                                <?php }?>
                            <?php
$_smarty_tpl->tpl_vars['post'] = $foreach_post_Sav;
}
?>
                            <?php $_smarty_tpl->createLocalArrayVariable('posts', null, 0);
$_smarty_tpl->tpl_vars['posts']->value['query'] = smarty_modifier_unset($_smarty_tpl->tpl_vars['posts']->value['query'],$_smarty_tpl->tpl_vars['keys2']->value);?> 

                        </div>


                        <!-- /row 2 -->
                        <!-- row 3 -->
                        <div class="row multi-line clearfix">
                            
                            <?php
$_from = $_smarty_tpl->tpl_vars['posts']->value['query'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['post'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['post']->_loop = false;
$_smarty_tpl->tpl_vars['key3'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key3']->value => $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
$foreach_post_Sav = $_smarty_tpl->tpl_vars['post'];
?>
                                

                                <article class="article">
                                    <a href="/posts/show/<?php echo $_smarty_tpl->tpl_vars['post']->value->post_id;?>
" class="article-link clearfix">
                                        <div class="img-wrapper">
                                            <img alt="5" class="img" data-aspect-ratio="1.78" 
                                                 data-aspect-ratio-type="landscape" data-max-height="800" data-max-width="1422" 
                                                 data-responsive-widths="375,480"
                                                 src="<?php echo $_smarty_tpl->tpl_vars['post']->value->post_img;?>
" style="max-width: 100%; width:480px;" />
                                            <?php ob_start();
echo $_smarty_tpl->tpl_vars['post']->value->vimeo;
$_tmp7=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['post']->value->display;
$_tmp8=ob_get_clean();
if (!is_null($_tmp7) && $_tmp8 == 1) {?>

                                                <h6 class="category">
                                                    <span class="fa fa-video-camera"></span>
                                                    <?php echo $_smarty_tpl->tpl_vars['post']->value->post_category_id;?>

                                                </h6>
                                            <?php } else { ?>
                                                <h6 class="category">
                                                    <?php echo $_smarty_tpl->tpl_vars['post']->value->post_category_id;?>

                                                </h6>
                                            <?php }?>
                                        </div>
                                        <div class="text">
                                            <h4 class="title"><?php echo $_smarty_tpl->tpl_vars['post']->value->post_name;?>
</h4>
                                            <h5 class="description">
                                                <?php echo $_smarty_tpl->tpl_vars['post']->value->short_desc;?>
    
                                            </h5>
                                        </div>
                                    </a>
                                </article>
                                


                            <?php
$_smarty_tpl->tpl_vars['post'] = $foreach_post_Sav;
}
?>
                            <!-- /row 3 -->
                            <!-- row 6 -->

                            <!-- /row 6 -->

                        </div>
                        <?php if (isset($_smarty_tpl->tpl_vars['tagsfinder']->value)) {?>
                        <?php } else { ?>
                            <input type="checkbox" id="load_more" role="button">
                            <?php if (($_smarty_tpl->tpl_vars['posts']->value['page_num']-$_smarty_tpl->tpl_vars['page']->value) > 0) {?> 
                                <label  class="page-next" data-page="<?php echo $_smarty_tpl->tpl_vars['page']->value+1;?>
 " for="load_more" ><span>Загрузить еще</span></label>
                            <?php }?>
                            <?php echo '<script'; ?>
 type="text/javascript" class="script-page">
                                $(".page-next").click(function () {
                                <?php if (is_object($_smarty_tpl->tpl_vars['category']->value[0])) {?>
                                    $.ajax({
                                        url: '/posts/page/',
                                        method: 'get',
                                        data: {
                                            'page': $(this).data('page'),
                                            'category': <?php echo $_smarty_tpl->tpl_vars['category']->value[0]->posts_category_id;?>

                                        },
                                        success: function (response) {
                                            insertData(response);
                                        },
                                        error: function () {
                                            alert('error');
                                        }
                                    });
                                <?php } else { ?>
                                    $.ajax({
                                        url: '/posts/page/',
                                        method: 'get',
                                        data: {
                                            'page': $(this).data('page'),
                                        },
                                        success: function (response) {
                                            insertData(response);
                                        },
                                        error: function () {
                                            alert('error');
                                        }
                                    });
                                <?php }?>
                                    //var y = window.pageYOffset;
                                    //var x = window.pageXOffset;
                                    //window.scroll(x, y);
                                });
                                function insertData(response) {
                                    $(".page-next").remove();
                                    //console.log($(response).find('.article-grid').html());
                                    var html = $(response).find('.articles').html();
                                    $(html).insertAfter("#load_more");
                                    $("#load_more").first().remove();
                                    $(".script-page").first().remove();

                                    console.log('done');
                                <?php ob_start();
echo $_smarty_tpl->tpl_vars['category']->value[0]->invert;
$_tmp9=ob_get_clean();
if (is_object($_smarty_tpl->tpl_vars['category']->value[0]) && $_tmp9 != 0) {?>
                                    $("body").css({
                                        'background-color': '#000',
                                        'color': '#fff'
                                    });
                                    $("#main").css({
                                        'background-color': '#000',
                                        'color': '#fff'
                                    });
                                    $(".article").css({
                                        'background-color': '#000',
                                        'color': '#fff'
                                    });
                                    $(".text").css('color', '#fff');
                                    $("#footer .container, #footer").css({
                                        'background-color': '#000',
                                        'color': '#fff'
                                    });
                                    $("#footer a").css('color', '#fff');
                                    $(".subscribe-widget").css({
                                        'background-color': '#000',
                                        'color': '#fff'
                                    });
                                <?php }?>
                                    //$('head').append('<link rel="stylesheet" href="/web/templates/Site/assets/css/site.css" type="text/css" />');
                                }
                            <?php echo '</script'; ?>
>
                        <?php }?>

                </section>

            </div>
        <?php } else { ?>
            <div>
                Здесь еще нет статей
            </div>
        <?php }?>



        <?php echo '<script'; ?>
 src="/web/templates/Site/assets/js/home-with-carousel.js" async="" type="text/javascript"><?php echo '</script'; ?>
>


        <?php echo $_smarty_tpl->getSubTemplate ('./footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        <?php echo '<script'; ?>
 src="/web/templates/Site/assets/js/jquery-migrate-1.2.1.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript">
                                var safari = $.browser.safari;

                                $(document).ready(function () {
                                    $("body").fadeIn(1500);
                                    $("#search-btn").mouseover(function () {
                                        if ($('body').hasClass('search-open')) {
                                            $('body').removeClass('search-open');
                                        } else {
                                            $('body').addClass('search-open');
                                        }
                                    });
                                    $("#header-search-container").mouseleave(function () {
                                        $('body').removeClass('search-open');
                                    });
                                    if ($(window).width() <= 767) {
                                        $(".page-next").css('width', '200px');
                                        $(".page-next").css('height', '50px');
                                        $(".page-next").css('margin-top', '-30px');
                                        $(".page-next").css('margin-left', '10px');
                                        $(".page-next").children('span').css('margin-top', '-20px');
                                    }

                                    $("a").click(function (event) {
                                        if ($(this).parent('nav').attr('class') != 'slides-navigation' && $(this).attr('class') != 'subscribe-btn' && !safari) {
                                            event.preventDefault();
                                            linkLocation = this.href;
                                            $("body").fadeOut(1500, redirectPage);
                                        }
                                    });
                                    function redirectPage() {
                                        window.location = linkLocation;
                                    }

                                });<?php echo '</script'; ?>
>

        <?php ob_start();
echo $_smarty_tpl->tpl_vars['category']->value[0]->invert;
$_tmp10=ob_get_clean();
if (is_object($_smarty_tpl->tpl_vars['category']->value[0]) && $_tmp10 != 0) {?>
            <?php echo '<script'; ?>
 type="text/javascript">
                $("body").css({
                    'background-color': '#000',
                    'color': '#fff'
                });
                $("#main").css({
                    'background-color': '#000',
                    'color': '#fff'
                });
                $(".text").css('color', '#fff');
                $("#footer .container, #footer").css({
                    'background-color': '#000',
                    'color': '#fff'
                });
                $(".page-next").css('box-shadow', '1px 1px rgba(255, 255, 255, 254.1) inset, -1px -1px rgba(255, 255, 255, 254.1) inset');
                $("#footer a").css('color', '#fff');
                $(".subscribe-widget").css({
                    'background-color': '#000',
                    'color': '#fff'
                });<?php echo '</script'; ?>
>
            <?php }?>
        <?php echo '<script'; ?>
>
            $(function () {
                $('#slides').superslides({
                    hashchange: true,
                    play: 4000
                });
                $(".slides-pagination").insertBefore("#main");
                console.log('moved');
                $('#slides').on('mouseenter', function () {
                    $(this).superslides('stop');
                    console.log('Stopped')
                });
                $('#slides').on('mouseleave', function () {
                    $(this).superslides('start');
                    console.log('Started')
                });
                $(".subscribe-btn").click(function () {
                    var email = $(".email-input").val();
                    $.ajax({
                        url: '/subscribe/add/',
                        method: 'post',
                        data: {'email': email},
                        dataType: 'json',
                        success: function () {
                            console.log('Success');
                            changeSubscribe();
                        },
                        error: function () {
                            alert('Error')
                        }
                    });
                });
                function changeSubscribe() {
                    $(".subscribe-form").fadeOut(300);
                    $(".success-subscribe").fadeIn(300);
                    var subscribed = localStorage.getItem('subscribe');
                    if (subscribed != '1') {
                        localStorage.setItem('subscribe', '1');
                    }
                }

            });
            
            <?php echo '</script'; ?>
>

        </body>
    </html>
<?php }
}
?>