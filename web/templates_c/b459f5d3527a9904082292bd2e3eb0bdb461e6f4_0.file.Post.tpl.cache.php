<?php /* Smarty version 3.1.27, created on 2016-01-08 21:30:10
         compiled from "/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Site/Post.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1203541256902a62cd3c31_45198574%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b459f5d3527a9904082292bd2e3eb0bdb461e6f4' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Site/Post.tpl',
      1 => 1452287251,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1203541256902a62cd3c31_45198574',
  'variables' => 
  array (
    'post' => 0,
    'postOne' => 0,
    'user' => 0,
    'tags' => 0,
    'tag' => 0,
    'last' => 0,
    'news' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56902a62dbed36_16947297',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56902a62dbed36_16947297')) {
function content_56902a62dbed36_16947297 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1203541256902a62cd3c31_45198574';
$_smarty_tpl->tpl_vars['user'] = new Smarty_Variable(unserialize(base64_decode($_SESSION['register']['user_auth'])), null, 0);?>
<?php $_smarty_tpl->tpl_vars['postOne'] = new Smarty_Variable($_smarty_tpl->tpl_vars['post']->value[0], null, 0);?>
<!DOCTYPE html>
<html class="no-js" lang="en" id="html-element">
    <head>

        <meta charset="utf-8" />

        <title><?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_seo_title;?>
</title>
        <meta name="author" content="Pogorelov Vlad" />
        <meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_seo_desc;?>
" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta property="fb:app_id" content="id"/>
        <link rel="canonical" href="" />
        <meta property="og:site_name" content="MoonStore"/><meta property="og:title" content="<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_seo_title;?>
"/>
        <meta property="og:type" content="article"/>
        <link href="/web/templates/Site/assets/css/site.css" type="text/css" rel="stylesheet"/>
        <link href="/web/templates/Site/assets/css/article.css" type="text/css" rel="stylesheet"/>  
        <link href="/web/templates/Site/assets/css/home-with-carousel.css" type="text/css" rel="stylesheet" id="style1" />

        <?php if (is_object($_smarty_tpl->tpl_vars['user']->value)) {?>
            <link media="screen" href="/web/templates/Site/leftblock/css/custom.css" type="text/css" rel="stylesheet" />
            <link rel="stylesheet" href="/web/templates/Site/leftblock/css/style.css" media="screen" type="text/css" />
        <?php }?>
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Site/assets/js/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/jquery-ui.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/video.js"><?php echo '</script'; ?>
>

        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Site/gallery/jquery.easing.1.3.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Site/gallery/jquery.mousewheel.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="http://vk.com/js/api/share.js?93" charset="windows-1251"><?php echo '</script'; ?>
>
        <style type="text/css">

            .hovergallery img{
                -webkit-transform:scale(0.8); /*Webkit: уменьшаем размер до 0.8*/
                -moz-transform:scale(0.8); /*Mozilla: масштабирование*/
                -o-transform:scale(0.8); /*Opera: масштабирование*/
                -webkit-transition-duration: 0.5s; /*Webkit: длительность анимации*/
                -moz-transition-duration: 0.5s; /*Mozilla: длительность анимации*/
                -o-transition-duration: 0.5s; /*Opera: длительность анимации*/
                opacity: 0.7; /*Начальная прозрачность изображений*/
                margin: 0 10px 5px 0; /*Интервалы между изображениями*/
            }

            .hovergallery img:hover{
                -webkit-transform:scale(1.1); /*Webkit: увеличиваем размер до 1.2x*/
                -moz-transform:scale(1.1); /*Mozilla: масштабирование*/
                -o-transform:scale(1.1); /*Opera: масштабирование*/
                box-shadow:0px 0px 30px gray; /*CSS3 тени: 30px размытая тень вокруг всего изображения*/
                -webkit-box-shadow:0px 0px 30px gray; /*Webkit: тени*/
                -moz-box-shadow:0px 0px 30px gray; /*Mozilla: тени*/
                opacity: 1;
            }

        </style>
        <?php echo '<script'; ?>
 src="/web/templates/Site/assets/js/jquery-migrate-1.2.1.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript">
            var safari = $.browser.safari;

            $("#player").fitVids();

            // Target your .container, .wrapper, .post, etc.
            $(document).ready(function () {
                $("#search-btn").click(function () {
                    if ($('body').hasClass('search-open')) {
                        $('body').removeClass('search-open');
                    } else {
                        $('body').addClass('search-open');
                    }
                });
                if ($(window).width() < 767) {
                    $(".article-body").css('margin', '0 auto');
                }
                $("body").fadeIn('slow');
                $("a").click(function (event) {
                    if ($(this).attr('class') != 'show-gallery' && !safari && $(this).attr('class') != 'subscribe-btn' && $(this).attr('class') != 'close-gallery') {
                        event.preventDefault();
                        linkLocation = this.href;
                        $("body").fadeOut(1500, redirectPage);
                    }
                });
                $("#menu-btn").click(function () {
                    if ($(".menu-open").length > 0) {
                        $('body').removeClass('menu-open');
                    } else {
                        $("body").addClass('menu-open');
                    }
                });
                function redirectPage() {
                    window.location = linkLocation;
                }
                function changeSubscribe() {
                    $(".subscribe-form").fadeOut(300);
                    $(".success-subscribe").fadeIn(300);
                    var subscribed = localStorage.getItem('subscribe');
                    if (subscribed != '1') {
                        localStorage.setItem('subscribe', '1');
                    }
                }
                $(".hidden-gallery").hide();
            });
        <?php echo '</script'; ?>
>


    </head>
    <body class="document-ready window-load" data-facebooknamespace="moonstore">
        <?php if (is_object($_smarty_tpl->tpl_vars['user']->value)) {?>
            <?php echo $_smarty_tpl->getSubTemplate ('./leftblock/index.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        <?php }?>
        <div class="hidden-gallery">
            <?php ob_start();
echo $_smarty_tpl->tpl_vars['postOne']->value->gallery;
$_tmp1=ob_get_clean();
if (isset($_tmp1)) {?>
                <?php echo $_smarty_tpl->tpl_vars['postOne']->value->gallery;?>

            <?php }?>
        </div>
        <?php echo $_smarty_tpl->getSubTemplate ('./header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        <div id="main" role="main" class="container clearfix">

            <article class="article clearfix" id="main-article" itemscope="itemscope">



                <div class="headings">
                    <h3 class="column-headings">
                        <a class="column-title" href="#" itemprop="articleSection"><?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_category_id;?>
</a> 
                    </h3>
                    <h2 class="title"><?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_name;?>
</h2>
                    <time class="date" datetime="<?php echo $_smarty_tpl->tpl_vars['postOne']->value->date_created;?>
" itemprop="datePublished" style="color: #c9aa90"><?php echo $_smarty_tpl->tpl_vars['postOne']->value->date_created;?>
</time>

                </div>
                <div class="article-hero landscape">
                    <figure class="main-img">
                        <?php ob_start();
echo $_smarty_tpl->tpl_vars['postOne']->value->vimeo;
$_tmp2=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['postOne']->value->display;
$_tmp3=ob_get_clean();
if (!is_null($_tmp2) && $_tmp3 == 1) {?>
                            <div id="player" style="margin-left: 35px">
                                <iframe src="<?php echo $_smarty_tpl->tpl_vars['postOne']->value->vimeo;?>
"
                                        width="980" height="500" frameborder="0" 
                                        webkitallowfullscreen mozallowfullscreen 
                                        allowfullscreen
                                        class="fluid-width-video-wrapper">
                                </iframe> 
                                <p>
                            </div>
                        <?php } else { ?>
                            <img alt="<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_name;?>
" data-aspect-ratio="1.57" data-aspect-ratio-type="landscape" 
                                 data-max-height="1000" data-max-width="1571" 
                                 data-responsive-widths="375,480,1257" 
                                 itemprop="image" src="<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_img;?>
" style="max-width: 100%; width:1257px" />
                        <?php }?>
                    </figure>

                </div>

                <div class="article-container clearfix" itemprop="articleBody">

                    <div class="sidebar-social-wrapper">

                        <aside class="sidebar-social social-btns hidden-phone hidden-tablet">

                            <div class="social-btns">
                                <div class="social-btn">
                                    <?php echo '<script'; ?>
 type="text/javascript">
                                        document.write(VK.Share.button({
                                            url: '<?php echo $_SERVER['HTTP_HOST'];
echo $_SERVER['REQUEST_URI'];?>
',
                                            title: '<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_seo_title;?>
',
                                            description: '<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_seo_desc;?>
',
                                            image: '<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_img;?>
',
                                            noparse: true
                                        }));<?php echo '</script'; ?>
>
                                </div>

                                <div class="social-btn">
                                    <a name="fb_share" type="button_count"
                                       share_url="<?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_id;?>
">Facebook</a> 
                                    <?php echo '<script'; ?>
 src="http://static.ak.fbcdn.net/connect.php/js/FB.Share"
                                            type="text/javascript">
                                    <?php echo '</script'; ?>
>
                                </div><br>
                            </div>

                        </aside>
                    </div>


                    <div class="article-body"<?php ob_start();
echo $_smarty_tpl->tpl_vars['postOne']->value->type_id;
$_tmp4=ob_get_clean();
if ($_tmp4 != 4) {?> style="margin: 0 210px 40px;"<?php }?>>
                        <?php ob_start();
echo $_smarty_tpl->tpl_vars['postOne']->value->type_id;
$_tmp5=ob_get_clean();
if ($_tmp5 == 4) {?>
                            <div class="subscribe-widget" style=" margin-right: -300px;
                                 margin-top: -150px;">
                                <h4 class="title">Подписаться</h4>
                                <h5 class="description">Подпишись на обновления</h5>

                                <form method="POST" class="subscribe-form" action="/subscribe">
                                    <input type="text" name="email" class="email-input" placeholder="you@email.com" required />
                                    <a class="subscribe-btn"><span class="icon icon-angle-right"></span></a>
                                </form>
                                <div class="success-subscribe" style="display: none">
                                    <p>Вы успешно подписались на рассылку новостей.</p>
                                </div>
                                <div class="social-links">
                                    <a href="https://www.facebook.com/moonstore.it/" class="social-link" target="blank">
                                        <span class="icon icon-facebook"></span></a>
                                    <a href="https://twitter.com/moon__store" class="social-link" target="blank">
                                        <span class="icon icon-twitter"></span></a>
                                    <a href="https://www.instagram.com/moonstore.it/" class="social-link" target="blank">
                                        <span class="icon icon-instagram"></span></a>
                                </div>
                            </div>
                        <?php }?>
                        <?php echo $_smarty_tpl->tpl_vars['postOne']->value->post_text;?>


                    </div>


                </div>




                <div class="article-footer">

                    <div class="tags-list">
                        <strong>Теги:</strong> 
                        <?php
$_from = $_smarty_tpl->tpl_vars['tags']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars["tag"] = new Smarty_Variable;
$_smarty_tpl->tpl_vars["tag"]->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars["tag"]->value) {
$_smarty_tpl->tpl_vars["tag"]->_loop = true;
$foreach_tag_Sav = $_smarty_tpl->tpl_vars["tag"];
?>
                            <?php if (!is_null($_smarty_tpl->tpl_vars['tag']->value)) {?>
                                <a href="/posts/find/tag/<?php echo $_smarty_tpl->tpl_vars['tag']->value;?>
" class="tag-btn">#<?php echo $_smarty_tpl->tpl_vars['tag']->value;?>
</a> 
                            <?php }?>
                        <?php
$_smarty_tpl->tpl_vars["tag"] = $foreach_tag_Sav;
}
?>
                    </div>


                    <div class="more-articles-container">

                        <h3 class="cat-title"><a href="/where-on-earth">Последнее в категории</a></h3>

                        <ul class="articles more-articles grid col-5 clearfix">
                            <?php
$_from = $_smarty_tpl->tpl_vars['last']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['news'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['news']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['news']->value) {
$_smarty_tpl->tpl_vars['news']->_loop = true;
$foreach_news_Sav = $_smarty_tpl->tpl_vars['news'];
?>
                                <li class="grid-item article">
                                    <a href="/posts/show/<?php echo $_smarty_tpl->tpl_vars['news']->value->post_id;?>
" data-track="event" data-title="<?php echo $_smarty_tpl->tpl_vars['postOne']->value->category_id;?>
"
                                       data-value="Click">
                                        <img alt="<?php echo $_smarty_tpl->tpl_vars['news']->value->post_name;?>
" class="img"
                                             data-aspect-ratio="1.77" data-aspect-ratio-type="landscape" 
                                             data-max-height="1000" data-max-width="1773" 
                                             src="<?php echo $_smarty_tpl->tpl_vars['news']->value->post_img_th;?>
" />
                                        
                                        <h4 class="title"><?php echo $_smarty_tpl->tpl_vars['news']->value->post_name;?>
</h4>
                                    </a>
                                </li>
                            <?php
$_smarty_tpl->tpl_vars['news'] = $foreach_news_Sav;
}
?>
                        </ul>
                    </div>
                </div>
            </article>
        </div>


        <div class='foot'>
            <?php echo $_smarty_tpl->getSubTemplate ('./footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        </div>
        <?php echo '<script'; ?>
 type="text/javascript">
            $(".show-gallery").click(function () {
                var ids = $(this).data('id');
                var id = $(".hidden-gallery").find('span');
                $.each(id, function (i, span) {
                    if (ids == $(span).data('id')) {
                        var bg = $('.hidden-gallery').find('#bgimg');
                        $(bg).attr('src', $(span).parent('a').attr('href'));
                    }
                });
                $("#main").hide();
                $("#header").hide();
                $(".foot").hide();
                $(".hidden-gallery").show();
            });
            $(".close-gallery").click(function () {
                //$("#outer_container").css('z-index', '-1000');
                $("#main").show();
                $("#header").show();
                $(".foot").show();
                $(".hidden-gallery").hide();
            });

        <?php echo '</script'; ?>
>
        
            <?php echo '<script'; ?>
>
                //set default view mode
                $defaultViewMode = "original"; //full (fullscreen background), fit (fit to window), original (no scale)
                //cache vars
                $bg = $("#bg");
                $bgimg = $("#bg #bgimg");
                $preloader = $("#preloader");
                $outer_container = $("#outer_container");
                $outer_container_a = $("#outer_container a.thumb_link");
                $toolbar = $("#toolbar");
                $nextimage_tip = $("#nextimage_tip");

                $(window).load(function () {
                    $(".hidden-gallery").show();
                    $toolbar.data("imageViewMode", $defaultViewMode); //default view mode
                    ImageViewMode($toolbar.data("imageViewMode"));
                    //cache vars
                    $customScrollBox = $("#customScrollBox");
                    $customScrollBox_container = $("#customScrollBox .container");

                    $customScrollBox.height($customScrollBox_container.height());

                    //resize browser window functions
                    $(window).resize(function () {
                        FullScreenBackground("#bgimg"); //scale bg image
                    });

                    LargeImageLoad($bgimg);
                    $(".hidden-gallery").hide();
                });

                //loading bg image
                $bgimg.load(function () {
                    LargeImageLoad($(this));
                });

                function LargeImageLoad($this) {
                    $preloader.fadeOut("fast"); //hide preloader
                    $this.removeAttr("width").removeAttr("height").css({width: "", height: ""}); //lose all previous dimensions in order to rescale new image data
                    $bg.data("originalImageWidth", $this.width()).data("originalImageHeight", $this.height());
                    if ($bg.data("newTitle")) {
                        $this.attr("title", $bg.data("newTitle")); //set new image title attribute
                    }
                    FullScreenBackground($this); //scale new image
                    $bg.data("nextImage", $($outer_container.data("selectedThumb")).next().attr("href")); //get and store next image
                    if (typeof itemIndex != "undefined") {
                        if (itemIndex == lastItemIndex) { //check if it is the last image
                            $bg.data("lastImageReached", "Y");
                            $bg.data("nextImage", $outer_container_a.first().attr("href")); //get and store next image
                        } else {
                            $bg.data("lastImageReached", "N");
                        }
                    } else {
                        $bg.data("lastImageReached", "N");
                    }
                    $this.fadeIn("slow"); //fadein background image
                    if ($bg.data("nextImage") || $bg.data("lastImageReached") == "Y") { //don't close thumbs pane on 1st load
                        SlidePanels("close"); //close the left pane
                    }
                    NextImageTip();
                }

                //slide in/out left pane
                $outer_container.hover(
                        function () { //mouse over
                            //SlidePanels("open");
                        },
                        function () { //mouse out
                            SlidePanels("close");
                        }
                );

                $("#arrow_indicator").click(
                        function () { //mouse over
                            SlidePanels("open");
                        }
                );

                //Clicking on thumbnail changes the background image
                $outer_container_a.click(function (event) {
                    event.preventDefault();
                    var $this = this;
                    $bgimg.css("display", "none");
                    $preloader.fadeIn("fast"); //show preloader
                    //style clicked thumbnail
                    $outer_container_a.each(function () {
                        $(this).children(".selected").css("display", "none");
                    });
                    $(this).children(".selected").css("display", "block");
                    //get and store next image and selected thumb 
                    $outer_container.data("selectedThumb", $this);
                    $bg.data("nextImage", $(this).next().attr("href"));
                    $bg.data("newTitle", $(this).children("img").attr("title")); //get and store new image title attribute
                    itemIndex = getIndex($this); //get clicked item index
                    lastItemIndex = ($outer_container_a.length) - 1; //get last item index
                    $bgimg.attr("src", "").attr("src", $this); //switch image
                });

                //clicking on large image loads the next one
                $bgimg.click(function (event) {
                    var $this = $(this);
                    if ($bg.data("nextImage")) { //if next image data is stored
                        $this.css("display", "none");
                        $preloader.fadeIn("fast"); //show preloader
                        $($outer_container.data("selectedThumb")).children(".selected").css("display", "none"); //deselect thumb
                        if ($bg.data("lastImageReached") != "Y") {
                            $($outer_container.data("selectedThumb")).next().children(".selected").css("display", "block"); //select new thumb
                        } else {
                            $outer_container_a.first().children(".selected").css("display", "block"); //select new thumb - first
                        }
                        //store new selected thumb
                        var selThumb = $outer_container.data("selectedThumb");
                        if ($bg.data("lastImageReached") != "Y") {
                            $outer_container.data("selectedThumb", $(selThumb).next());
                        } else {
                            $outer_container.data("selectedThumb", $outer_container_a.first());
                        }
                        $bg.data("newTitle", $($outer_container.data("selectedThumb")).children("img").attr("title")); //get and store new image title attribute
                        if ($bg.data("lastImageReached") != "Y") {
                            itemIndex++;
                        } else {
                            itemIndex = 0;
                        }
                        $this.attr("src", "").attr("src", $bg.data("nextImage")); //switch image
                    }
                });

                //function to get element index (fuck you IE!)
                function getIndex(theItem) {
                    for (var i = 0, length = $outer_container_a.length; i < length; i++) {
                        if ($outer_container_a[i] === theItem) {
                            return i;
                        }
                    }
                }

                //toolbar (image view mode button) hover
                $toolbar.hover(
                        function () { //mouse over
                            $(this).stop().fadeTo("fast", 1);
                        },
                        function () { //mouse out
                            $(this).stop().fadeTo("fast", 0.8);
                        }
                );
                $toolbar.stop().fadeTo("fast", 0.8); //set its original state

                //Clicking on toolbar changes the image view mode
                $toolbar.click(function (event) {
                    if ($toolbar.data("imageViewMode") == "full") {
                        ImageViewMode("fit");
                    } else if ($toolbar.data("imageViewMode") == "fit") {
                        ImageViewMode("original");
                    } else if ($toolbar.data("imageViewMode") == "original") {
                        ImageViewMode("full");
                    }
                });

                //next image balloon tip
                function NextImageTip() {
                    if ($bg.data("nextImage")) { //check if this is the first image
                        $nextimage_tip.stop().css("right", 20).fadeIn("fast").fadeOut(2000, "easeInExpo", function () {
                            $nextimage_tip.css("right", $(window).width());
                        });
                    }
                }

                //slide in/out left pane function
                function SlidePanels(action) {
                    var speed = 900;
                    var easing = "easeInOutExpo";
                    if (action == "open") {
                        $("#arrow_indicator").fadeTo("fast", 0);
                        $outer_container.stop().animate({left: 0}, speed, easing);
                        $bg.stop().animate({left: 585}, speed, easing);
                    } else {
                        $outer_container.stop().animate({left: -710}, speed, easing);
                        $bg.stop().animate({left: 0}, speed, easing, function () {
                            $("#arrow_indicator").fadeTo("fast", 1);
                        });
                    }
                }

                //Image scale function
                function FullScreenBackground(theItem) {
                    var winWidth = $(window).width();
                    var winHeight = $(window).height();
                    var imageWidth = $(theItem).width();
                    var imageHeight = $(theItem).height();
                    if ($toolbar.data("imageViewMode") != "original") { //scale
                        $(theItem).removeClass("with_border").removeClass("with_shadow"); //remove extra styles of orininal view mode
                        var picHeight = imageHeight / imageWidth;
                        var picWidth = imageWidth / imageHeight;
                        if ($toolbar.data("imageViewMode") != "fit") { //image view mode: full
                            if ((winHeight / winWidth) < picHeight) {
                                $(theItem).css("width", winWidth).css("height", picHeight * winWidth);
                            } else {
                                $(theItem).css("height", winHeight).css("width", picWidth * winHeight);
                            }
                            ;
                        } else { //image view mode: fit
                            if ((winHeight / winWidth) > picHeight) {
                                $(theItem).css("width", winWidth).css("height", picHeight * winWidth);
                            } else {
                                $(theItem).css("height", winHeight).css("width", picWidth * winHeight);
                            }
                            ;
                        }
                        //center it
                        $(theItem).css("margin-left", ((winWidth - $(theItem).width()) / 2)).css("margin-top", ((winHeight - $(theItem).height()) / 2));
                    } else { //no scale
                        //add extra styles for orininal view mode
                        $(theItem).addClass("with_border").addClass("with_shadow");
                        //set original dimensions
                        $(theItem).css("width", $bg.data("originalImageWidth")).css("height", $bg.data("originalImageHeight"));
                        //center it
                        $(theItem).css("margin-left", ((winWidth - $(theItem).outerWidth()) / 2)).css("margin-top", ((winHeight - $(theItem).outerHeight()) / 2));
                    }
                }

                //image view mode function - full or fit
                function ImageViewMode(theMode) {

                }

                //preload script images
                var images = ["/web/templates/Site/gallery/ajax-loader_dark.gif", "/web/templates/Site/gallery/round_custom_scrollbar_bg_over.png"];
                $.each(images, function (i) {
                    images[i] = new Image();
                    images[i].src = this;
                });
            <?php echo '</script'; ?>
>
        
    </body>
</html>
<?php }
}
?>