<?php /* Smarty version 3.1.27, created on 2016-01-08 21:09:12
         compiled from "/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Admin/menu.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1595244859569025782cd730_84167097%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dcacc86d8945c492ff69ebe43b730721f2886f02' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Admin/menu.tpl',
      1 => 1452217778,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1595244859569025782cd730_84167097',
  'variables' => 
  array (
    'user' => 0,
    'lastpost' => 0,
    'settings' => 0,
    'slider' => 0,
    'point' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_569025782f51a5_81985008',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_569025782f51a5_81985008')) {
function content_569025782f51a5_81985008 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1595244859569025782cd730_84167097';
?>
<link rel="stylesheet" href="/web/templates/Admin/css/demo.css">
<link rel="stylesheet" href="/web/templates/Admin/css/forkit.css">
<link href="/web/templates/Admin/css/style-slider.css" rel="stylesheet" />
<link href="/web/templates/Admin/css/menu/css/jquery-ui.css" rel="stylesheet" />
<link href="/web/templates/Admin/js/menu/js/image-picker/image-picker/image-picker.css" rel="stylesheet" />


<?php echo '<script'; ?>
 src="/web/templates/Admin/js/menu/js/jquery-ui.js "><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/web/templates/Admin/js/menu/js/image-picker/image-picker/image-picker.js"><?php echo '</script'; ?>
>
<div class="navbar-nav navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin"><img src="/web/templates/Admin/img/Moon-Store-transparent-bg.png" width="50px" height="50px" alt="Moonstore"></a>
        </div> 
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Сайт</a></li>                            
                <li><a href="/admin/posts">Статьи</a></li>
                <li><a href="/admin/post/add">Создать статью</a></li>
                    <?php $_smarty_tpl->tpl_vars['user'] = new Smarty_Variable(unserialize(base64_decode($_SESSION['register']['user_auth'])), null, 0);?>
                    <?php if ($_smarty_tpl->tpl_vars['user']->value->getRoleId() == 1) {?>  
                    <li><a href="/admin/profile/usermanagement"> Пользователи</a></li>
                    <?php }?>
                <li><a href="#" id="showMenu">Расширенное меню</a></li>

            </ul>
        </div><!--/.nav-collapse -->
    </div>    
    <!-- The contents (if there's no contents the ribbon acts as a link) -->
    <?php if (isset($_smarty_tpl->tpl_vars['lastpost']->value)) {?>
        <div class="forkit-curtain">
            <div class="close-button"></div>

            
            <div class="col-sm-3 col-lg-3">
                <div class="dash-unit">
                    <dtitle>Настройки сайта </dtitle>
                    <hr>

                    <div class="text">

                        <label class="checkbox inline">
                            Из одной категории
                            <div class="switch">
                                <input type="radio" class="switch-input" name="isLocked" value="true" id="on" checked="" >
                                <label for="on" class="switch-label switch-label-off">On</label>
                                <input type="radio" class="switch-input" name="isLocked" value="false" id="off"  >
                                <label for="off" class="switch-label switch-label-on">Off</label>
                                <span class="switch-selection"></span><br>

                                <div class="count">
                                    <label for="number">Количество</label>
                                    <div class="number">
                                        <input type="number" value="<?php echo $_smarty_tpl->tpl_vars['settings']->value[0]->category_count;?>
" id="number"/>
                                        <br>

                                    </div>
                                </div>
                                <a href="#" class="save">Сохранить</a>

                                
                                    <?php echo '<script'; ?>
 type="text/javascript">
                                        $("input.switch-input").on("click", function () {
                                            if ($("input:checked").val() == 'true') {

                                                $(".count").fadeIn(400);
                                            } else {
                                                $(".count").fadeOut(400);
                                                $("input#number").val('0');
                                            }
                                        });
                                        $(".save").click(function () {
                                            $.ajax({
                                                url: '/admin/pagination/save/',
                                                method: 'POST',
                                                data: {count: $("#number").val()},
                                                success: function () {
                                                    alert('Сохранено');
                                                },
                                                error: function () {
                                                    alert('Не удачно');
                                                }
                                            });

                                        });<?php echo '</script'; ?>
>
                                    
                            </div> 
                        </label>
                    </div>
                    <br>
                    <div class="info-user">

                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-lg-3">
                <div class="dash-unit">
                    <dtitle>Пагинация </dtitle>
                    <hr>
                    <div class="text">

                        <div class="count-pagination">
                            <label for="number">Количество статей на странице</label>
                            <div class="number">
                                <input type="number" value="<?php echo $_smarty_tpl->tpl_vars['settings']->value[0]->pagination;?>
" id="number-pagination"/>
                                <br>
                            </div>
                        </div>
                        <a href="#" class="save-pagination">Сохранить</a>
                        <?php echo '<script'; ?>
 type="text/javascript">
                            
                                $(".save-pagination").click(function () {
                                    $.ajax({
                                        url: '/admin/pagination/save/pagination/',
                                        method: 'POST',
                                        data: {count: $("#number-pagination").val()},
                                        success: function () {
                                            alert('Сохранено');
                                        },
                                        error: function () {
                                            alert('Не удачно');
                                        }
                                    });
                                    //$.post("/admin/pagination/save/pagination/", $("#number-pagination").val());
                                    //                                   alert("Сохранено");
                                });
                            <?php echo '</script'; ?>
>
                        </div>
                        <br>
                    </div>
                </div>
                <div class="col-sm-3 col-lg-3">
                    <div class="dash-unit">
                        <dtitle>Слайдер</dtitle>
                        <hr>
                        <div class="thumbnail">
                            Количество слайдов
                        </div><!-- /thumbnail -->
                        <div class="text">
                            <form id="upload" method="post" action="/admin/slider/save/slider/" enctype="multipart/form-data">
                                <div id="drop">
                                    Перетащи сюда

                                    <a>Выбрать</a>
                                    <input type="file" name="upl" multiple />
                                </div>

                                <ul>
                                    <!-- The file uploads will be shown here -->
                                </ul>

                            </form>

                        </div>
                        <br>

                    </div>
                </div>
                <div class="col-sm-3 col-lg-3">
                    <div class="dash-unit">
                        <dtitle>Настройки сайта </dtitle>
                        <hr>
                        <div class="text" style="overflow: scroll; height:244px;">
                            <form id="slider-settings" method="get" action="#">
                                <select class="image-picker show-html">
                                    <?php
$_from = $_smarty_tpl->tpl_vars['slider']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['point'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['point']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['point']->value) {
$_smarty_tpl->tpl_vars['point']->_loop = true;
$foreach_point_Sav = $_smarty_tpl->tpl_vars['point'];
?>
                                        <option data-img-src="<?php echo $_smarty_tpl->tpl_vars['point']->value->image_path;?>
" value="<?php echo $_smarty_tpl->tpl_vars['point']->value->is_id;?>
"><?php echo $_smarty_tpl->tpl_vars['point']->value->is_id;?>
</option>
                                    <?php
$_smarty_tpl->tpl_vars['point'] = $foreach_point_Sav;
}
?>
                                </select>
                                <br>
                                <input type="text" class="post" name="post" placeholder="Статья">
                            </form>
                            <br>
                            <a class="save-slider">Сохранить настройки</a><br>
                            <a class="delete-slider" style="display: none">Удалить настройки</a>
                        </div>
                    </div>
                    <br>

                </div>
            </div>
            
            
                <?php echo '<script'; ?>
 type="text/javascript">
                    ;
                    $("select").imagepicker();
                    $(".thumbnail").click(function () {
                        var imgsrc = $(this).find('img').attr('src');
                        $.ajax({
                            type: 'get',
                            url: '/admin/find/post/slider/',
                            data: {'imgsrc': imgsrc},
                            success: function (data) {
                                $('.post').val(data);
                            }
                        });
                        $(".delete-slider").show(300);
                    });
                    $(".delete-slider").click(function () {
                        var frm = $('#slider-settings');
                        $.ajax({
                            type: 'get',
                            url: '/admin/keepost/',
                            data: {'image_id': frm.find(".selected").find("img").attr('src'), 'post': frm.find('.post').val(), 'delete': 1},
                            success: function (data) {
                                //alert('ok');
                                frm.find(".selected").hide(700);
                               // frm.find(".selected").remove();
                            }
                        });
                    });
                    $(".save-slider").click(function () {
                        var frm = $('#slider-settings');

                        $.ajax({
                            type: 'get',
                            url: '/admin/keepost/',
                            data: {'image_id': frm.find(".selected").find("img").attr('src'), 'post': frm.find('.post').val()},
                            success: function (data) {
                                alert('ok');
                            }
                        });
                    });


                    //$(function () {
                    $(".post").autocomplete({
                        source: function (request, response) {
                            $.ajax({
                                url: '/admin/getposts/',
                                method: 'GET',
                                data: {'q': request.term},
                                dataType: 'json',
                                success: function (data) {
//                                            console.log(data);
                                    response(data);
                                }
                            });
                        },
                        minLength: 1,
                        open: function () {
                            $(".ui-autocomplete").insertAfter(".post");
                            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                        },
                        close: function () {
                            $(".ui-autocomplete").hide();
                            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                        }
                    });
                    //});
                <?php echo '</script'; ?>
>  
            
        </div>

        <!-- The ribbon -->
        <a class="forkit" data-text="Настройки" data-text-detached="Протяни вниз >" href="#"></a>

        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/forkit.js"><?php echo '</script'; ?>
>
        <?php }?>
        </div>

        <?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/sliderSettings/assets/js/jquery.knob.js"><?php echo '</script'; ?>
>

        <!-- jQuery File Upload Dependencies -->
        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/sliderSettings/assets/js/jquery.ui.widget.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/sliderSettings/assets/js/jquery.iframe-transport.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/sliderSettings/assets/js/jquery.fileupload.js"><?php echo '</script'; ?>
>

        <!-- Our main JS file -->
        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/sliderSettings/assets/js/script.js"><?php echo '</script'; ?>
>

        


<?php }
}
?>