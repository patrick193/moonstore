<?php /* Smarty version 3.1.27, created on 2016-01-10 15:52:40
         compiled from "/var/www/andreevd/data/www/moonstore.it/web/templates/Site/header.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:90596877056927e48606a54_86533936%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e47aef76de9fe74fffbb6ffc11fc1dbd1b3cb0b5' => 
    array (
      0 => '/var/www/andreevd/data/www/moonstore.it/web/templates/Site/header.tpl',
      1 => 1452428725,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '90596877056927e48606a54_86533936',
  'variables' => 
  array (
    'categoties' => 0,
    'category' => 0,
    'category1' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56927e48674c27_11108039',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56927e48674c27_11108039')) {
function content_56927e48674c27_11108039 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '90596877056927e48606a54_86533936';
?>

<header id="header" class="clearfix">

    <div class="container clearfix">

        <div itemscope >
            <a href="/" id="logo" >
                <img class="logo" src="/web/templates/Site/assets/images/logo.png" alt="MoonStore">
            </a>
        </div>

        <nav class="menus">

            <div class="menu">
                <?php
$_from = $_smarty_tpl->tpl_vars['categoties']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['category1'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['category1']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['category1']->value) {
$_smarty_tpl->tpl_vars['category1']->_loop = true;
$foreach_category1_Sav = $_smarty_tpl->tpl_vars['category1'];
?>
                    <?php if (isset($_smarty_tpl->tpl_vars['category']->value[0]) && is_object($_smarty_tpl->tpl_vars['category']->value[0]) && $_smarty_tpl->tpl_vars['category']->value[0]->posts_category_id == 9 && $_smarty_tpl->tpl_vars['category1']->value->posts_category_id == 9) {?>
                        <a href="/posts/category/<?php echo $_smarty_tpl->tpl_vars['category1']->value->posts_category_id;?>
" 
                           class="menu-link category-link transition"
                           style=" border:1px solid #000; background-color: #000; color: #fff"
                           ><?php echo $_smarty_tpl->tpl_vars['category1']->value->category_name;?>
</a>
                    <?php } else { ?>
                        <a href="/posts/category/<?php echo $_smarty_tpl->tpl_vars['category1']->value->posts_category_id;?>
" class="menu-link category-link transition"><?php echo $_smarty_tpl->tpl_vars['category1']->value->category_name;?>
</a>
                    <?php }?>
                <?php
$_smarty_tpl->tpl_vars['category1'] = $foreach_category1_Sav;
}
?>
            </div>

            <div class="menu social-menu hidden-tablet">
                <a href="https://www.facebook.com/moonstore.it/" class="menu-link social-link" target="_blank"><span class="icon icon-facebook"></span></a>
                <a href="https://twitter.com/moon__store" class="menu-link social-link" target="_blank"><span class="icon icon-twitter"></span></a>
                <a href="https://www.instagram.com/moonstore.it/" class="menu-link social-link" target="_blank"><span class="icon icon-instagram"></span></a>
                <button id="search-btn" class="menu-link social-link hidden-phone"><span class="icon icon-search"></span></button>
            </div>

        </nav>

        <button id="menu-btn" class="visible-phone menu-link"><span class="icon icon-menu"></span></button>

    </div>

    <div id="header-search-container">
        <form class="header-search" action="/posts/search/" method="GET">
            <input type="search" id="search-input" name="q" data-search=" " required="" value="" />
            <button id="header-search-btn" type="submit">Поиск</button>
        </form>
    </div>

</header><?php }
}
?>