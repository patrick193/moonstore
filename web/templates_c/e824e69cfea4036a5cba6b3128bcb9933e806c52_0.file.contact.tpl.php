<?php /* Smarty version 3.1.27, created on 2016-01-10 15:53:20
         compiled from "/var/www/andreevd/data/www/moonstore.it/web/templates/Site/contact.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:80785899456927e70127c94_78507597%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e824e69cfea4036a5cba6b3128bcb9933e806c52' => 
    array (
      0 => '/var/www/andreevd/data/www/moonstore.it/web/templates/Site/contact.tpl',
      1 => 1452428729,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '80785899456927e70127c94_78507597',
  'variables' => 
  array (
    'user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56927e70242c05_87037375',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56927e70242c05_87037375')) {
function content_56927e70242c05_87037375 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '80785899456927e70127c94_78507597';
$_smarty_tpl->tpl_vars['user'] = new Smarty_Variable(unserialize(base64_decode($_SESSION['register']['user_auth'])), null, 0);?>
<!DOCTYPE html>
<html class="no-js" lang="en" id="html-element">
    <head>

        <meta charset="utf-8" />

        <title><?php echo $_smarty_tpl->smarty->registered_objects['post'][0]->post_seo_title;?>
</title>
        <meta name="author" content="Pogorelov Vlad" />
        <meta name="description" content="<?php echo $_smarty_tpl->smarty->registered_objects['post'][0]->post_seo_desc;?>
" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta property="fb:app_id" content="id"/>
        <link rel="canonical" href="" />
        <meta property="og:site_name" content="MoonStore"/><meta property="og:title" content="<?php echo $_smarty_tpl->smarty->registered_objects['post'][0]->post_seo_title;?>
"/>
        <meta property="og:type" content="article"/>
        <link href="/web/templates/Site/assets/css/site.css" type="text/css" rel="stylesheet"/>
        <link href="/web/templates/Site/assets/css/article.css" type="text/css" rel="stylesheet"/>  
        <link href="/web/templates/Site/assets/css/home-with-carousel.css" type="text/css" rel="stylesheet" id="style1" />

        <?php if (is_object($_smarty_tpl->tpl_vars['user']->value)) {?>
            <link media="screen" href="/web/templates/Site/leftblock/css/custom.css" type="text/css" rel="stylesheet" />
            <link rel="stylesheet" href="/web/templates/Site/leftblock/css/style.css" media="screen" type="text/css" />
        <?php }?>
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Site/assets/js/jquery.min.js"><?php echo '</script'; ?>
>

        <?php echo '<script'; ?>
 type="text/javascript">
            // Target your .container, .wrapper, .post, etc.
            $(document).ready(function () {
                $("body").fadeIn(1500);
                $("a").click(function (event) {
                    if ($(this).attr('class' != 'show-gallery')) {
                        event.preventDefault();
                        linkLocation = this.href;
                        $("body").fadeOut(1500, redirectPage);
                    }
                });
                $("#menu-btn").click(function () {
                    if ($(".menu-open").length > 0) {
                        $('body').removeClass('menu-open');
                    } else {
                        $("body").addClass('menu-open');
                    }
                });
                function redirectPage() {
                    window.location = linkLocation;
                }
            })
        <?php echo '</script'; ?>
>


    </head>
    <body class="document-ready window-load" data-facebooknamespace="moonstore">
        <?php if (is_object($_smarty_tpl->tpl_vars['user']->value)) {?>
            <?php echo $_smarty_tpl->getSubTemplate ('./leftblock/index.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        <?php }?>
        
        <?php echo $_smarty_tpl->getSubTemplate ('./header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        <div id="main" role="main" class="container clearfix">

            <article class="article clearfix" id="main-article" itemscope="itemscope">



                <div class="headings">
                    <h2 class="title"><?php echo $_smarty_tpl->smarty->registered_objects['post'][0]->post_name;?>
</h2>
                    <time class="date" datetime="<?php echo $_smarty_tpl->smarty->registered_objects['post'][0]->date_created;?>
" itemprop="datePublished" style="color: #c9aa90"><?php echo $_smarty_tpl->smarty->registered_objects['post'][0]->date_created;?>
</time>

                </div>
                <div class="article-container clearfix" itemprop="articleBody">

                    <div class="article-body"<?php ob_start();
echo $_smarty_tpl->smarty->registered_objects['post'][0]->type_id;
$_tmp1=ob_get_clean();
if ($_tmp1 != 4) {?> style="margin: 0 210px 40px;"<?php }?>>
                        <?php echo $_smarty_tpl->smarty->registered_objects['post'][0]->post_text;?>

                    </div>
                </div>
            </article>
        </div>
        <?php echo $_smarty_tpl->getSubTemplate ('./footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>
   
    </body>
</html>
<?php }
}
?>