<?php /* Smarty version 3.1.27, created on 2016-01-08 21:29:48
         compiled from "/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Site/footer.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:44266746256902a4c46dfd3_35377019%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ee68c1633c30e984dbccf12b8109c83b4545aa54' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Site/footer.tpl',
      1 => 1452217779,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '44266746256902a4c46dfd3_35377019',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56902a4c490ac4_15568229',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56902a4c490ac4_15568229')) {
function content_56902a4c490ac4_15568229 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '44266746256902a4c46dfd3_35377019';
?>
<footer id="footer">

            <div class="container">

                <div class="primary-nav">
                    <a href="/contact" class="footer-link">Связаться с нами</a>
                    <a href="/aboutus" class="footer-link">О нас</a>
                </div>

                <div class="social-menu">
                  <a href="https://www.facebook.com/moonstore.it/" class="social-link" target="blank">
                                        <span class="icon icon-facebook"></span></a>
                                    <a href="https://twitter.com/moon__store" class="social-link" target="blank">
                                        <span class="icon icon-twitter"></span></a>
                                    <a href="https://www.instagram.com/moonstore.it/" class="social-link" target="blank">
                                        <span class="icon icon-instagram"></span></a>
                </div>

                <div class="copyright">&copy; 2016 - 2017 Moon Store.</div>

            </div>

        </footer><?php }
}
?>