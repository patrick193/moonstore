<?php /* Smarty version 3.1.27, created on 2016-01-09 13:58:34
         compiled from "/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Site/footer.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:3336611055691120a563932_65273439%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ee68c1633c30e984dbccf12b8109c83b4545aa54' => 
    array (
      0 => '/var/www/andreevd/data/www/dev.moonstore.it/web/templates/Site/footer.tpl',
      1 => 1452347887,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3336611055691120a563932_65273439',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5691120a565f21_73738670',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5691120a565f21_73738670')) {
function content_5691120a565f21_73738670 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '3336611055691120a563932_65273439';
?>
<footer id="footer">

            <div class="container">

                <div class="primary-nav">
                    <a href="/contact" class="footer-link">Связаться с нами</a>
                    <a href="/aboutus" class="footer-link">О нас</a>
                </div>

                <div class="social-menu">
                  <a href="https://www.facebook.com/moonstore.it/" class="social-link" target="blank">
                                        <span class="icon icon-facebook"></span></a>
                                    <a href="https://twitter.com/moon__store" class="social-link" target="blank">
                                        <span class="icon icon-twitter"></span></a>
                                    <a href="https://www.instagram.com/moonstore.it/" class="social-link" target="blank">
                                        <span class="icon icon-instagram"></span></a>
                <div class="copyright"><span class="coop">&copy;</span> 2014 - 2016 Moon Store.</div>
                </div>


            </div>

        </footer><?php }
}
?>