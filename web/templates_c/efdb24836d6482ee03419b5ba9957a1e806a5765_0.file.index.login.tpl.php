<?php /* Smarty version 3.1.27, created on 2016-02-02 20:19:03
         compiled from "/var/www/moonstore/web/templates/Admin/index.login.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:84613955956b10f37172c88_45920085%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'efdb24836d6482ee03419b5ba9957a1e806a5765' => 
    array (
      0 => '/var/www/moonstore/web/templates/Admin/index.login.tpl',
      1 => 1452629071,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '84613955956b10f37172c88_45920085',
  'variables' => 
  array (
    'err' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56b10f371b3f43_18285926',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56b10f371b3f43_18285926')) {
function content_56b10f371b3f43_18285926 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '84613955956b10f37172c88_45920085';
?>

<!doctype html>
<html><head>
        <meta charset="utf-8">
        <title>MoonStore - добро пожаловать</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Pohorielov Vlad">

        <link href="/web/templates/Admin/css/bootstrap.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/bootstrap.min.css" rel="stylesheet">
        <link href="/web/templates/Admin/css/styles.css" rel="stylesheet">
        <link rel="stylesheet" href="/web/templates/Admin/css/register.css">

        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/jquery-1.10.2.min.js"><?php echo '</script'; ?>
>    
        <?php echo '<script'; ?>
 src="/web/templates/Admin/js/bootstrap.js"><?php echo '</script'; ?>
>


        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <?php echo '<script'; ?>
 src="http://html5shim.googlecode.com/svn/trunk/html5.js"><?php echo '</script'; ?>
>
        <![endif]-->

        <!-- Google Fonts call. Font Used Open Sans & Raleway -->
        <link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
        <!-- Le styles -->

        <link href="/web/templates/Admin/css/login.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="/web/templates/Admin/css/bootstrap.min.css" />

        <style type="text/css">
            body {
                padding-top: 30px;
            }

            pbfooter {
                position:relative;
            }
        </style>

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <?php echo '<script'; ?>
 src="http://html5shim.googlecode.com/svn/trunk/html5.js"><?php echo '</script'; ?>
>
        <![endif]-->

        <!-- Google Fonts call. Font Used Open Sans & Raleway -->
        <link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

        <!-- Jquery Validate Script -->
        <?php echo '<script'; ?>
 type="text/javascript" src="/web/templates/Admin/js/jquery.validate.js"><?php echo '</script'; ?>
>

        <!-- Jquery Validate Script - Validation Fields -->
        <?php echo '<script'; ?>
 type="text/javascript">


            $().ready(function () {
                // validate the comment form when it is submitted
                $("#commentForm").validate();

                // validate signup form on keyup and submit
                $("#signupForm").validate({
                    rules: {
                        firstname: "required",
                        lastname: "required",
                        username: {
                            required: true,
                            minlength: 1
                        },
                        password: {
                            required: true,
                            minlength: 1
                        },
                        confirm_password: {
                            required: true,
                            minlength: 2,
                            equalTo: "#password"
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        topic: {
                            required: "#newsletter:checked",
                            minlength: 2
                        },
                        agree: "required"
                    },
                    messages: {
                        firstname: "Please enter your firstname",
                        lastname: "Please enter your lastname",
                        username: {
                            required: "Please enter a username",
                            minlength: "Your username must consist of at least 1 character"
                        },
                        password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 1 character long"
                        },
                        confirm_password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long",
                            equalTo: "Please enter the same password as above"
                        },
                        email: "Please enter a valid email address",
                        agree: "Please accept our policy"
                    }
                });

                // propose username by combining first- and lastname
                $("#username").focus(function () {
                    var firstname = $("#firstname").val();
                    var lastname = $("#lastname").val();
                    if (firstname && lastname && !this.value) {
                        this.value = firstname + "." + lastname;
                    }
                });

                //code to hide topic selection, disable for demo
                var newsletter = $("#newsletter");
                // newsletter topics are optional, hide at first
                var inital = newsletter.is(":checked");
                var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
                var topicInputs = topics.find("input").attr("disabled", !inital);
                // show when newsletter is checked
                newsletter.click(function () {
                    topics[this.checked ? "removeClass" : "addClass"]("gray");
                    topicInputs.attr("disabled", !this.checked);
                });
            });
        <?php echo '</script'; ?>
>

    </head>

    <style>

        .pbfooter {
            position:relative;
        }

    </style>

    <body style="background:url('web/templates/Admin/img/bg.jpg') no-repeat center center; height:100%;">

        <!-- NAVIGATION MENU -->

        <div class="navbar-nav navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div> 

            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-offset-4 col-lg-4" style="margin-top:100px">
                    <div class="block-unit" style="text-align:center; padding:8px 8px 8px 8px;">
                        <img src="web/templates/Admin/img/face80x80.jpg" alt="" class="img-circle">
                        <br>
                        <br>
                        <form class="cmxform" id="signupForm" method="post" action="/moonstoreadmin/login">
                            <fieldset>
                                <p>
                                    <?php if (isset($_smarty_tpl->tpl_vars['err']->value)) {?>
                                    <p><?php ob_start();
echo $_smarty_tpl->tpl_vars['err']->value;
$_tmp1=ob_get_clean();
echo $_tmp1;?>
</p>
                                    <?php }?>
                                    <input id="username" name="username" type="text" placeholder="Username">
                                    <input id="password" name="password" type="password" placeholder="Password">
                                </p>
                                <input class="submit btn-success btn btn-large" type="submit" value="Login">
                            </fieldset>
                        </form>
                    </div>

                </div>


            </div>
        </div>

    </body></html><?php }
}
?>